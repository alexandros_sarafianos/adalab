cmake_minimum_required(VERSION 2.8)
 include(CMakeParseArguments)
 set(CMAKE_CXX_STANDARD_REQUIRED ON)
project(adalab)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/MetabolicModel/)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/script/modules/")

include(${PROJECT_SOURCE_DIR}/CMakeMacro.txt)

set (DATA_DEFINED_HEADER_FILE ${CMAKE_BINARY_DIR}/data/data.hpp)
file (WRITE ${DATA_DEFINED_HEADER_FILE} "#ifndef ADALAB_DATA_HEADER\n#define ADALAB_DATA_HEADER\n")

ENABLE_TESTING()

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BUILD_TYPE})
set (DATA_DEFINED_HEADER_FILE ${CMAKE_BINARY_DIR}/data/data.hpp)
file (WRITE ${DATA_DEFINED_HEADER_FILE} "#ifndef INSPECTOR_DATA_HEADER\n#define INSPECTOR_DATA_HEADER\n")

set(Boost_USE_MULTITHREADED OFF)
find_package(Boost 1.53 COMPONENTS thread serialization  program_options filesystem system chrono unit_test_framework regex REQUIRED)
find_package(GLPK)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -lstdc++ -lm")

include_directories(${Boost_INCLUDE_DIRS}   ${PROJECT_SOURCE_DIR} )
file (APPEND ${DATA_DEFINED_HEADER_FILE} "#endif\n")

#RCPP RELATED STUFF
set (SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/..)

execute_process(COMMAND R RHOME
                OUTPUT_VARIABLE R_HOME)

find_package(MPI REQUIRED)
set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS})
include_directories(${MPI_INCLUDE_PATH})


set(NUM_TRUNC_CHARS 2)

execute_process(COMMAND R CMD config --cppflags
                OUTPUT_VARIABLE RCPPFLAGS)

string(SUBSTRING ${RCPPFLAGS} ${NUM_TRUNC_CHARS} -1 RCPPFLAGS)
include_directories(${RCPPFLAGS})

execute_process(COMMAND R CMD config --ldflags
                OUTPUT_VARIABLE RLDFLAGS)
string(LENGTH ${RLDFLAGS} RLDFLAGS_LEN)

if (${RLDFLAGS} MATCHES "[-][L]([^ ;])+")
    string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RLDFLAGS_L)
    string(STRIP ${RLDFLAGS_L} RLDFLAGS_L )
    link_directories(${RLDFLAGS_L} )
endif()

if (${RLDFLAGS} MATCHES "[-][l]([^;])+")
    string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RLDFLAGS_l)
    string(STRIP ${RLDFLAGS_l} RLDFLAGS_l )
endif()

execute_process(COMMAND Rscript -e "library(Rcpp); Rcpp:::CxxFlags()"
                OUTPUT_VARIABLE RCPPINCL)
string(SUBSTRING ${RCPPINCL} ${NUM_TRUNC_CHARS} -1 RCPPINCL)
include_directories(${RCPPINCL})

execute_process(COMMAND Rscript -e "library(Rcpp); Rcpp:::LdFlags()"
                OUTPUT_VARIABLE RCPPLIBS)
if (${RCPPLIBS} MATCHES "[-][L]([^ ;])+")
    string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RCPPLIBS_L)
    link_directories(${RCPPLIBS_L} )
endif()

if (${RCPPLIBS} MATCHES "[-][l][R]([^;])+")
    string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RCPPLIBS_l)
endif()

execute_process(COMMAND Rscript -e "library(RInside); RInside:::CxxFlags()"
                OUTPUT_VARIABLE RINSIDEINCL)
string(SUBSTRING ${RINSIDEINCL} ${NUM_TRUNC_CHARS} -1 RINSIDEINCL)
include_directories(${RINSIDEINCL})

execute_process(COMMAND Rscript -e "library(RInside);RInside:::LdFlags()"
                OUTPUT_VARIABLE RINSIDELIBS)
if (${RINSIDELIBS} MATCHES "[-][L]([^ ;])+")
    string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RINSIDELIBS_L)
    link_directories(${RINSIDELIBS_L})
endif()

if (${RINSIDELIBS} MATCHES "[-][l][R]([^;])+")
    string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RINSIDELIBS_l)
endif()

execute_process(COMMAND R CMD config CXXFLAGS
                OUTPUT_VARIABLE RCXXFLAGS)

execute_process(COMMAND R CMD config BLAS_LIBS
                OUTPUT_VARIABLE RBLAS)

execute_process(COMMAND R CMD config LAPACK_LIBS
                OUTPUT_VARIABLE RLAPACK)
#
set (SUBDIRECTORIES
    src
)
INCLUDE_ALL_SUBDIRECTORIES_HEADER_SOURCES(SUBDIRECTORIES)

CREATE_LIBRARY(NAME adalab SOURCES ${SOURCES} HEADERS ${HEADERS} LIBRARIES ${Boost_LIBRARIES}  )
add_dependencies(adalab MIPS_RELEASE)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

if (CMAKE_BUILD_TYPE STREQUAL "DEBUG" OR
    CMAKE_BUILD_TYPE STREQUAL "RelWithDebugInfo" )
    add_definitions("-DDEBUG")
elseif ( CMAKE_BUILD_TYPE STREQUAL "RELEASE" )
    add_definitions("-O3")
endif()

   target_link_libraries(adalab ${RCPPLIBS_L})
   message("Library for Rcpp :" ${RCPPLIBS_L})
   message("Library for Rcpp :" ${RCPPLIBS_l})
   target_link_libraries(adalab ${RLDFLAGS_l})
   message("Library for RLD : " ${RLDFLAGS_l})
   target_link_libraries(adalab ${BLAS_LIBS})
   message("Library for BLAS : " ${BLAS_LIBS})
   target_link_libraries(adalab ${LAPACK_LIBS})
   message("Library for LAPACK :" ${LAPACK_LIBS})
   target_link_libraries(adalab ${MPI_LIBRARIES})
   message("Library for MPI : " ${MPI_LIBRARIES})
   #END RCPP RELATED STUFF
