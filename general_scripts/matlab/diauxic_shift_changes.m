
function [matrix, strain_matrix, summary, summary_2] = diauxic_shift_changes(path, p_value, chi_or_fisher)
  if p_value == 0
    p_value= 0.05
  end
  if isempty(path)
    path="/home/alexs/adalab_personal/Data/diauxiTest_iso1.csv"
  end
  col_index = 8
   if chi_or_fisher ==  0
      col_index = 6
   end
  p_value = 0.05
  X= read_mixed_csv (path,',');
  columnheaders =  X(1,:);
  data_num = X(2:size(X,1),[1,2,3,5,6,7,8,9 ]);
  data_text = X(2:size(X,1),[4]);
  matrix = zeros(6,260,8);
  strain_matrix=  cell(1,260);
  for n =1:1:6
    for k = 1: 260
      row_index = (n-1)*260 + k;
      if  str2double(data_num{row_index,1}) == n &&  str2double(data_num{row_index,col_index}) < p_value %Smaller than -> there is no association between the two
        matrix(n,k,:) = str2double( data_num(row_index,:) );
        strain_matrix{n,k} = data_text{row_index};
      end
    end
   end

   size(strain_matrix)
   for k = 1:260
        counter = 0;
        for n = 1:6
              if isempty(strain_matrix{n,k})
                  counter = counter + 1;
               end
        end
        summary(k) = counter;
  end  

  for n=1:6
        counter = 0;
        for  k = 1:260
              if isempty(strain_matrix{n,k})
                  counter = counter + 1;
               end
        end
        summary_2(n) = counter;
  end  
end


