


x = -1:0.01:2;
y = sigmf(x,[20 0.5]);
figure();
plot(x,y)
xlabel('sigmf, P = [1 0]')



x = -50:0.1:50;
y0 = sigmf(x,[1 15]);y0*2-1;
figure();
plot(x,y0)
xlabel('sigmf, P = [1 0]')


x = -10:0.1:10;
y1 = sigmf(x,[1 0.5]);y1*2-1;
figure();
plot(x,y1)
xlabel('sigmf, P = [1 5]')


x = -10:0.1:10;
y1 = sigmf(x,[2 0.5]);y1*2-1;
figure();
plot(x,y1)
xlabel('sigmf, P = [1 5]')

x = -10:0.1:10;
y3 = sigmf(x,[1 -5]);
y3 = y3*2-1;
figure();
plot(x,y3)
xlabel('sigmf, P = [1 -0.5]')
