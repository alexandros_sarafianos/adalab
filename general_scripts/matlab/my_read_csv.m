## Copyright (C) 2015 Alex Sarafianos
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} my_read_csv (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Alex Sarafianos <alexs@tony>
## Created: 2015-06-29

function [data] = my_read_csv (path, delim)
  % Assuming that the dataset is ";"-delimited and each line ends with ";"
  fid = fopen(path);
  tline = fgetl(fid);
  u=sprintf('%c',tline); c=length(u);
  id=findstr(u,delim); n=length(id);
  data=cell(1,n);
  for I=1:n
      if I==1
          data{1,I}=u(1:id(I)-1);
      else
          data{1,I}=u(id(I-1)+1:id(I)-1);
      end
  end
  ct=1;
  while ischar(tline)
      ct=ct+1;
      tline = fgetl(fid);
      u=sprintf('%c',tline);
      id=findstr(u,delim);
      if~isempty(id)
          for I=1:n
              if I==1
                  data{ct,I}=u(1:id(I)-1);
              else
                  data{ct,I}=u(id(I-1)+1:id(I)-1);
              end
          end
      end
  end
  fclose(fid);
endfunction
