
import sys, getopt

def create_sequence_dict(fasta_file):
	fh = open(fasta_file, 'r')
	new_line = 0
	gene_id = ''
	gene_loc = ''
	sequence = ""
	seq_dict = {}
	for line in fh:
		if line[0] == ">":
			if sequence != "":
				seq_dict[gene_id] = { 'sequence' : sequence, 'gene_loc' : gene_loc }  
			sequence = ""
			splitted = line.split(" ")
			gene_id = splitted[0][1:]
			gene_loc = splitted[1].rstrip()
		else:
			sequence += line.rstrip()
	return seq_dict

def create_feature_dict(feat_file):
	fh = open(feat_file)
	feat_dict = {}
	fh.readline()
	for line in fh:
		splitted = line.rstrip().split(',')
		feat_dict[splitted[1].strip("\"")] = {'gene_id' : splitted[0].strip("\""), \
		'feat_type': splitted[2].strip("\""), 'qualifier' : splitted[3].strip("\""), \
		'gene_status' : splitted[4].strip("\""), 'length' :  int(splitted[5].strip("\"\n")) }
	return feat_dict 
'''
b = create_feature_dict("/home/alexs/adalab_personal/Data/Diauxic_shift_adam/result-1.csv")
a= create_sequence_dict("/home/alexs/adalab_personal/Data/Diauxic_shift_adam/results160.fa")
assert a['S000002327']['sequence'] == 'atgtccgccgctactgttggtaaacctattaagtgcattgctgctgttgcgtatgatgcgaagaaaccattaagtgttgaagaaatcacggtagacgccccaaaagcgcacgaagtacgtatcaaaattgaatatactgctgtatgccacactgatgcgtacactttatcaggctctgatccagaaggacttttcccttgcgttctgggccacgaaggagccggtatcgtagaatctgtaggcgatgatgtcataacagttaagcctggtgatcatgttattgctttgtacactgctgagtgtggcaaatgtaagttctgtacttccggtaaaaccaacttatgtggtgctgttagagctactcaagggaaaggtgtaatgcctgatgggaccacaagatttcataatgcgaaaggtgaagatatataccatttcatgggttgctctactttttccgaatatactgtggtggcagatgtctctgtggttgccatcgatccaaaagctcccttggatgctgcctgtttactgggttgtggtgttactactggttttggggcggctcttaagacagctaatgtgcaaaaaggcgataccgttgcagtatttggctgcgggactgtaggactctccgttatccaaggtgcaaagttaaggggcgcttccaagatcattgccattgacattaacaataagaaaaaacaatattgttctcaatttggtgccacggattttgttaatcccaaggaagatttggccaaagatcaaactatcgttgaaaagttaattgaaatgactgatgggggtctggattttacttttgactgtactggtaataccaaaattatgagagatgctttggaagcctgtcataaaggttggggtcaatctattatcattggtgtggctgccgctggtgaagaaatttctacaaggccgttccagctggtcactggtagagtgtggaaaggctctgcttttggtggcatcaaaggtagatctgaaatgggcggtttaattaaagactatcaaaaaggtgccttaaaagtcgaagaatttatcactcacaggagaccattcaaagaaatcaatcaagcctttgaagatttgcataacggtgattgcttaagaaccgtcttgaagtctgatgaaataaaatag'
assert a['S000002327']['gene_loc'] == 'chrIV:159604-160764'

assert b['YAL044C']['gene_id'] == 'S000000042' and  b['YAL044C']['length'] == 513
assert b['YAL044C']['feat_type'] == 'ORF' and b['YAL044C']['qualifier'] == 'Verified' and b['YAL044C']['gene_status'] == 'Active'
'''

def main(argv):
    #read in command line arguments
    genes =''
    fasta=''
    feats=''
    ofile=''

    try:
        opts, args = getopt.getopt(argv,"hg:f:i:o:t:",["genes=","fasta=","feats=",  "ofile=", "type="])
    except getopt.GetoptError:
        print '-g <gene_file> -f <fasta_file> -i <features> -o <outputfile> -t <type>'
        sys.exit(2)
    print opts
    for opt, arg in opts:
        if opt == '-h':
            print 'python make_feature_file.py -g <gene_file> -f <fasta_file> -i <features> -o <outputfile> -t <type>'
            sys.exit()
        elif opt in ("-g", "--genes"):
            genes = arg
	elif opt in ("-f", "--fasta"):
	    fasta = arg
        elif opt in ("-i", "--feats"):
	    feats= arg
	elif opt in ("-o", "--ofile"):
	    ofile = arg
	elif opt in ("-t" , "--type"):
	    d_type = arg
	else:
	    print "Unknown option ", opt
    #Actual start of program
    if not genes or not fasta or not feats or not ofile or not d_type:
        print 'python make_feature_file.py -g <gene_file> -f <fasta_file> -i <features> -o <outputfile> -t <type>'
        sys.exit()
    feat_dict = create_feature_dict(feats)
    fasta_dict = create_sequence_dict(fasta)
    fh_in = open(genes, 'r')
    fh_out = open(ofile, 'w')
    fh_in.readline()
    for line in fh_in:
        line= line.rstrip()
	splitted =  line.split(",")
	if int(splitted[0]) == int(d_type):
		gene_name = splitted[3]
		gene_id = feat_dict.get(gene_name, {}).get('gene_id', None)
		gene_sequence = fasta_dict.get(gene_id, {}).get('sequence', None)
		gene_loc = fasta_dict.get(gene_id, {}).get('gene_loc', None)
		if gene_id != None and gene_sequence !=None and gene_loc != None:
			fh_out.write(d_type+","+gene_id+","+gene_name+","+gene_sequence+","+gene_loc+","+splitted[4]+","\
			+splitted[6]+","+ splitted[7]+","+ splitted[8]+"\n")
    fh_in.close()
    fh_out.close()


if __name__ == "__main__":
   main(sys.argv[1:])

