from __future__ import division
import os
from os.path import expanduser
from network_import_functions import * 
from definitions import *
import sys
from numpy import nan

#Variable declaration
home = expanduser("~")
data_path = home+"/AdaLab/Data/geneExpressionData/all_spell_exprconn_pcl/"
#more at http://downloads.yeastgenome.org/expression/microarray/
#data_set_files = ["DeRisi_1997_PMID_9381177/2010.diauxic.pcl", "Kuhn_2001_PMID_11154278/2010.Kuhn01.flt.knn.avg.pcl", "Miller-Fleming_2014_PMID_24706893/GSE56124.remapped.final.pcl"]
delimiter = "\t"


def convert_value_to_new_range(initial_lower_range, initial_upper_range, value, new_lower_range,new_upper_range):
    if value != None:
        old_range = initial_upper_range - initial_lower_range;
        new_range = (new_upper_range - new_lower_range);
        new_value = (((value - initial_lower_range) * new_range) / old_range) + new_lower_range;
        return new_value;
    else:
        return None

assert(convert_value_to_new_range(-5,5,-5,-1,1) == -1)
assert(convert_value_to_new_range(-5,5,0,-1,1) == 0)
assert(convert_value_to_new_range(-5,5,5,-1,1) == 1)

def normalize_dataset_from_one_file(exp_dict):
    minimum_val = float('inf')
    maximum_val = -1*float('inf')
    for gene_name, gene_data in  exp_dict.iteritems():
        if gene_name != "col_headers":
            for exp_val in gene_data.itervalues():
                if exp_val < minimum_val and exp_val != None:
                    minimum_val = exp_val
                if exp_val > maximum_val and exp_val != None:
                    maximum_val = exp_val
    for gene_name, gene_data in exp_dict.iteritems():
        if gene_name != "col_headers":
            for key_exp, val_exp in gene_data.iteritems():
                exp_dict[gene_name][key_exp] = convert_value_to_new_range(minimum_val, maximum_val, val_exp, -1., 1.)
    return exp_dict


def load_gene_expression_pcl_data(filename,data_dict, delim = delimiter):
    gene_exp_file = open(filename, 'r')
    data_dict[filename] = {}
    temp_data_dict = {}
    #What to do with the first two lines?
    line1 = gene_exp_file.readline()
    splitted_col_heads = line1.rstrip("\n").split(delim)
    temp_data_dict['col_headers'] = splitted_col_heads[3:]
    #assert(line1.split(delim)[0] == "YORF" or line1.split(delim)[0] == "UID")
    line2 = gene_exp_file.readline()
    for expression_line in gene_exp_file:
        splitted = expression_line.rstrip("\n").split(delim)
        orf_name = splitted[0]
        #Check if gene is useful
        name = splitted[1] # other description, ignore
        if not is_gene_in_network(orf_name) and not is_gene_in_network(name):
            continue
        assert(orf_name != "")
        g_weight = int(splitted[2]) #what to do with this value, indication for replication of gene
        if temp_data_dict.get(orf_name) == None:
            temp_data_dict[orf_name] = {}
        for index in range(3,len(splitted)):
            number =  None
            if splitted[index] ==  "null":
                pass #null will be set to NaN
            elif splitted[index]:
                try:
                    number = float(splitted[index])
                except ValueError:
                    print "ValueError: ", splitted[index], "Filename: ", filename, "Index:",index, "orf_name:",orf_name

            temp_data_dict[orf_name][index-3] = number
    temp_data_dict = normalize_dataset_from_one_file(temp_data_dict)
    data_dict[filename] = temp_data_dict

    return data_dict



def get_time_info(file_name):
    if file_name.endswith("2010.Brauer05_batch1.flt.knn.avg.pcl"):
        times = range(1,14) #in hours
        times = [t*60 for t in times]
    elif file_name.endswith("2010.Brauer05_batch2.flt.knn.avg.pcl"):
        times = range(1,8)
        times = [t*60 for t in times]
    elif file_name.endswith("2010.diauxic.pcl"):
        times = [0,9.5,11.5,13.5,15.5,18.5,20.5]
        times = [int(t*60) for t in times]
    elif file_name.endswith("2010.Cho98.filter.flt.knn.avg.div.log.pcl"):
        times = [t/6. for t in range(17)]
    elif file_name.endswith("2010.Spellman98_cdc15.flt.knn.avg.pcl"):
        times = [10, 30, 50, 70,80, 90,100,110 ,120,130,140,150, 160,160,170,180,190,200,210,220,230,240,250,270,290]
    elif file_name.endswith("2010.Spellman98_elutriation.flt.knn.avg.pcl"):
        times = [int(t*30) for t in range(14)]
    elif file_name.endswith("2010.DNAdamage.pcl"):
        times = [5,15,30,45,60,90, 120,5,15,30,45,60,90,120,30,90,120,5,10,20,30,45,60,90,120,5,30,60,90,5,10,20,30,45,60,90,120,5,30,60,20,20,20,-1,-1,-1,-1,-1,-1,-1,-1,-1]
    elif file_name.endswith("2010.Gasch00_adenineStarvation.flt.knn.avg.pcl"):
        times = [0.5,1,2,4,6]
    elif file_name.endswith("2010.Gasch00_diamideTreatment.flt.knn.avg.pcl"):
        times =[5,5,20,30,40,50,60,90]
    elif file_name.endswith("2010.Gasch00_DTT(y13).flt.knn.avg.pcl"):
        times = [5,15,30,45,60,90,120,180]
    elif file_name.endswith("2010.Gasch00_DTT(y14).flt.knn.avg.pcl"):
        times = [0,15,30,60,120,240,480]
    elif file_name.endswith("2010.Gasch00_HOtimeCourse.flt.knn.avg.pcl"):
        times = [10,20,30,50,60,80,100,120,160]
    elif file_name.endswith("2010.Gasch00_HS25-37.flt.knn.avg.pcl"):
        times = [5,10,15,20,20,40,60,80]
    elif file_name.endswith("2010.Gasch00_HS30-37.flt.knn.avg.pcl"):
        times = [0,5,15,30,60]
    elif file_name.endswith("2010.Gasch00_hypo-osmotic.flt.knn.avg.pcl"):
        times = [5,15,30,45,60]
    elif file_name.endswith("2010.Gasch00_menadione.flt.knn.avg.pcl"):
        times = [10,20,30,40,50,80,105,120,160]
    elif file_name.endswith("2010.Gasch00_Ndepletion.flt.knn.avg.pcl"):
        times = [30,60,2*60,4*60,8*60,12*60,24*60,48*60,72*60]
    elif file_name.endswith("2010.Gasch00_stationaryPhase(y12).flt.knn.avg.pcl"):
        times = [2*60,4*60,8*60,12*60,24*60,48*60,72*60,120*60,168*60,13*24*60]
    elif file_name.endswith("2010.Gasch00_stationaryPhase(y14).flt.knn.avg.pcl"):
        times = [4,6,8,10,12,24,48,72,120]
        times = [t*60 for t in times]
    else:
        print "Time info not found for file", file_name
        times = []
    return times

def get_indices(t_diff, times):
    def approx_equal(a, b, tol):
        return abs(a - b) < tol
    valid_indices = []
    for t1 in xrange(len(times)-1):
        for t2 in xrange(1,len(times)):
            if  times[t2] - times[t1] == t_diff:
                valid_indices.append((t1,t2))
    return valid_indices

assert(get_indices(1, [1,2,3]) == [(0,1),(1,2)] )
#assert(get_indices(0.5, [0,1,2]) ==[])
assert(get_indices(2, [1,2,3,4,5,6,7,8,9,10]) == [(0,2), (1,3), (2,4) , (3,5), (4,6), (5,7), (6,8), (7,9)])
#assert(get_indices(0.2, [0,0.2,0.4,0.6]) == [(0,1), (1,2), (2,3) ] )

def get_time_samples(data_set_dict, t_diff, fn):
    if data_set_dict.get('col_headers', 666) == 666:
        del data_set_dict['col_headers']
    times = get_time_info(fn)
    if len(times) == 0: #can't do anything if we don't have proper time information
        return None
    temp_vals = data_set_dict.values()
    if len(temp_vals) == 0:
        return None
    assert( len(temp_vals[0]) == len(times) )
    indices = get_indices(t_diff, times)
    return indices


def check_subsequent_time_points(time_difference, iterable):
    it = iter(iterable)
    prev_val = it.next()
    for current_val in it:
        if current_val - prev_val != time_difference:
            return False
        prev_val = current_val
    return True


def get_all_data(dir = data_path):
    data_dict = {}
    experiment_number = 0
    for subdir, dirs, files in os.walk(dir):
        for f in files:
            f_path = subdir + os.sep + f
            if f.endswith(".pcl"):
                #print "New datafile:  ",f
                data_dict = load_gene_expression_pcl_data(f_path, data_dict)
    return data_dict

def extract_learnable_data(reactants_orf, product_orf, useful_experiments,experiment_dict):
    y_values, x_values =[], []
    for e in useful_experiments:
        data_point = []
        for reactant in reactants_orf:
            data_point.append( experiment_dict[reactant][e] )
#        if abs(experiment_dict[product_orf][e]) > 1:
#            pass
#            #print "Not using this experiment, because |", experiment_dict[product_orf][e],"| > 1 !"
#        else:
#            problem = False
#            for x_val in data_point:
#                if abs(x_val) > 1:
#                   #print "Not using this experiment, because |", x_val,"| > 1 !"
#                   problem = True
#                   break
#            if not problem:
#                y_values.append( experiment_dict[product_orf][e] )
#                x_values.append(data_point)
        y_values.append( experiment_dict[product_orf][e] )
        x_values.append(data_point)
    return [x_values, y_values]
    

#alternative replace e.g. pop2phosphorylated by reactants POP2
def extract_rxn_specific_data(experiment_dict, rxn, version, t_diff):
    (rxn_cmpl, rxn_cond, rxn_drugs, rxn_evid, rxn_genes, rxn_growth_ph, rxn_mets) = rxn.divide_by_type()
    product = rxn.product
    print "rxn-genes", rxn_genes
    print "product", product

    #check validity of reaction

    reactants_orf= set()
    product_orf = ""
    if len(rxn_genes) == 0:
        print "NO genes in reactants, skipping..."
        return ([],[],[],"")
    if rxn.check_product_type() == "gene":
        for reactant in rxn_genes:
            orf_name = convert_gene_name_to_orf(reactant)
            assert(orf_name != None)
            reactants_orf.add(orf_name)
        product_orf = convert_gene_name_to_orf(product)
        assert(product_orf != None)
    else:
        print "Product type", check_product_type(product), " not supported!"
    reactant_list = list(reactants_orf) #to ensure we get values in the same order
    #get the data
    x_values, y_values = [], []

    if version == 1:
        for dataset_name, data in experiment_dict.iteritems():
            entity_iterator = iter( reactants_orf | {product_orf} )
            useful_keys = set(data.get(entity_iterator.next(), {} ).keys())
            for entity in entity_iterator:
                useful_keys &= set(data.get(entity, {}).keys())
                if len(useful_keys) == 0:
                    break
            if len(useful_keys) > 0:
                for t in useful_keys:
                    data_point = []
                    for reactant in reactant_list:
                        data_point.append(data[reactant][t])
                    x_values.append(data_point)
                    y_values.append(data[product_orf][t])

    elif version == 2: #WITHOUT interpolation
        for dataset_name, data in experiment_dict.iteritems():
            entity_iterator = iter( reactants_orf | {product_orf} )
            useful_keys = set(data.get(entity_iterator.next(), {} ).keys())
            for entity in entity_iterator:
                useful_keys &= set(data.get(entity, {}).keys())
                if len(useful_keys) == 0:
                    break
            if len(useful_keys) > 0:
                useful_keys = sorted(list(useful_keys))
                assert(check_subsequent_time_points(1,useful_keys))
                for t in xrange(1,len(useful_keys)):
                    data_point = []
                    for reactant in reactant_list:
                        data_point.append(data[reactant][useful_keys[t-1]])
                    x_values.append(data_point)
                    y_values.append(data[product_orf][useful_keys[t]])
    elif version == 3 or version == 4:#t_diff
        for dataset_name, data in experiment_dict.iteritems():
            indices = get_time_samples( data, t_diff, dataset_name)
            for index in indices: #index = (t1, t2)
                data_point = []
                for r in reactant_list:
                    data_point.append( data.get(r, {}).get(index[0], None ) )
                p = data.get(product_orf,{}).get(index[1])
                if p!=None and not None in data_point:
                    x_values.append(data_point)
                    y_values.append(p)
    elif version == 5:
        for dataset_name, data in experiment_dict.iteritems():
            indices = get_time_samples( data, t_diff, dataset_name)
            times = get_time_info(dataset_name)
            entity_iterator = iter( reactants_orf | {product_orf} )
            useful_keys = set(data.get(entity_iterator.next(), {} ).keys())
            for entity in entity_iterator:
                useful_keys &= set(data.get(entity, {}).keys())
                if len(useful_keys) == 0:
                    break
            if len(useful_keys) > 0:
                useful_keys = sorted(list(useful_keys))
                assert(check_subsequent_time_points(1,useful_keys))
                for t in xrange(1,len(useful_keys)):
                    data_point = []
                    data_t_diff = times[t] - times [t-1]
                    if data_t_diff < t_diff and data_t_diff != 0:
                        current_y = data[product_orf][useful_keys[t]]
                        previous_y  = data[product_orf][useful_keys[t-1]]
                        if current_y != None and previous_y != None:
                            y_diff = (data[product_orf][useful_keys[t]] - data[product_orf][useful_keys[t-1]])
                            for reactant in reactant_list:
                                data_point.append(data[reactant][useful_keys[t-1]])
                            x_values.append(data_point)
                            dy_dt = y_diff/data_t_diff
                            y_values.append(dy_dt)
    else:
        print "version", version, "not yet supported"
        sys.exit()
    #Do the learning
    assert(len(x_values) == len(y_values))
    return (x_values, y_values, reactant_list, product_orf)
    
'''
def extract_rxn_specific_timeseries_data(experiment_dict, reactants, product, experiment_number):
    (rxn_cmpl, rxn_cond, rxn_drugs, rxn_evid, rxn_genes, rxn_growth_ph, rxn_mets) = divide_by_type(set(reactants))
    print "rxn-genes", rxn_genes
    print "product", product
    reactants_orf= set()
    product_orf = ""

    if len(rxn_genes) == 0:
        print "NO genes in reactants, skipping..."
        return None
    if check_product_type(product) == "gene":
        for reactant in rxn_genes:
            orf_name = convert_gene_name_to_orf(reactant)
            assert(orf_name != None)
            reactants_orf.add(orf_name)
        product_orf = convert_gene_name_to_orf(product)
        assert(product_orf != None)
    else:
        print "Product type", check_product_type(product), " not supported!"
    #return intersection of keys!!!!!
    exp_keys = set(range(experiment_number + 1 ) )
    for r in reactants_orf:
        edr = experiment_dict.get(r, {})
        exp_keys &=  set(edr.keys())
    edp = experiment_dict.get(product_orf,{})
    exp_keys &= set(edp.keys())
    if len(exp_keys) == 0:
        print "No sufficient amount of keys"
        return None
    if product_orf in reactants_orf:
        print "Removing ", product_orf, " from reactants."
        reactants_orf.remove(product_orf)
        return None
    reactants_orf = list(reactants_orf) #iteration over set is not guaranteed to be stable
    [x_values, y_values] = extract_learnable_data(reactants_orf, product_orf, exp_keys, experiment_dict)
    return (reactants_orf, product_orf, x_values, y_values)
'''

    
    
