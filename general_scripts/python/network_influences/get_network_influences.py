from os.path import expanduser
from network_import_functions import *
from gene_expression_data import *

from sklearn import linear_model
from sklearn.metrics import explained_variance_score, mean_absolute_error, mean_squared_error, median_absolute_error, r2_score
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVR, SVC
from sklearn.svm import LinearSVR
from sklearn import tree
from file_io import *
from numpy import nan
import random
from sklearn.linear_model import RidgeCV



'''
SCH9phosphorylated is only prodct
RDS2phosphorylated is only product
MIG1phosphorylated is only product
PHO4phosphorylated is only product
SNF1phosphorylated is only product
POP2phosphorylated is only product 
SIP4phosphorylated is only product


OAF1/PIP2inactive both reactant and product

OAF1/PIP2active only reactant

related to DS
CAT8phosphorylated is only product
ADR1phosphorylated is only product
'''
def get_values_from_indices(x_values, y_values, indices):
    assert( isinstance(x_values, list) and isinstance(y_values,list) )
    x_values_sample = []
    y_values_sample = []
    for index in indices:
        x_values_sample.append(x_values[index])
        y_values_sample.append(y_values[index])
    return (x_values_sample, y_values_sample)

def calculate_performance(y_true, y_pred):
    return (explained_variance_score(y_true, y_pred), mean_absolute_error(y_true,y_pred), mean_squared_error(y_true,y_pred),median_absolute_error(y_true,y_pred),r2_score(y_true, y_pred))
    

'''
def remove_invalid_data(x_values, y_values):
    filtered_x_vals, filtered_y_vals = [], []
    for data_point_index in xrange(len(y_values)):
        if not nan in x_values[data_point_index] and not y_values[data_point_index] is nan:
            filtered_x_vals.append(x_values[data_point_index])
            filtered_y_vals.append(y_values[data_point_index])
        else:
            print "Nan found", x_values[data_point_index], y_values[data_point_index]
    return filtered_x_vals, filtered_y_vals
'''

def learn_influence(reactants_orf, product_orf, x_vals, y_vals, regression_report_file, full_rxn,regressor_type, version):
    x_values, y_values = [], []
    for data_point_index in xrange(len(y_vals)):
        if not None in x_vals[data_point_index] and not y_vals[data_point_index] is None:
            x_values.append(x_vals[data_point_index])
            y_values.append(y_vals[data_point_index])
        else:
            print "Nan found", x_vals[data_point_index], y_vals[data_point_index]
    #(x_values, y_values) = remove_invalid_data(x_vals, y_vals)
    #print "X", x_values, "Y", y_values
    assert( isinstance(x_values, list) )
    assert( isinstance(y_values,list) )
    assert( isinstance(reactants_orf,list) )
    assert( product_orf != "" )
    if len(x_values) == 0:
        print "NO values after filtering"
        return None
    if version == 4:
        # remove product_orf from reactants orf, new y_value = y_value - x_value,
        index_of_product_in_reactants = reactants_orf.index(product_orf)
        del reactants_orf[index_of_product_in_reactants]
        for index in range(len(y_values)):
            y_values[index] = y_values[index] - x_values[index][index_of_product_in_reactants]
            del x_values[index][index_of_product_in_reactants]
            if len(x_values[index]) == 0:
                return None
    if version == 5:
        index_of_product_in_reactants = reactants_orf.index(product_orf)
        del reactants_orf[index_of_product_in_reactants]
        for index in range(len(y_values)):
            del x_values[index][index_of_product_in_reactants]
            if len(x_values[index]) == 0:
                return None
    print "Dataset size: " , len(y_values)
    all_indices = range(len(y_values))
    test_size = int( len(y_values) * 0.2 )
    training_size = len(y_values) - test_size
    print "Training set size and test set size:", training_size, test_size
    training_set_indices = random.sample(all_indices, training_size)
    test_set_indices = list( set(all_indices) - set(training_set_indices) )
    
    assert( set(training_set_indices) | set(test_set_indices) == set(all_indices) )
    assert( len(test_set_indices) == test_size)
    assert( len(training_set_indices) == training_size) 
    
    (training_set_x, training_set_y) = get_values_from_indices(x_values, y_values, training_set_indices)
    (test_set_x, test_set_y) = get_values_from_indices(x_values, y_values, test_set_indices)
    clf = ""
    param_grid = {}

    if regressor_type == "LS":
        clf = linear_model.LinearRegression()
        clf.fit( training_set_x, training_set_y )
    elif regressor_type == "Ridge":
        clf = RidgeCV()
        clf.fit(training_set_x, training_set_y)
    else:
        if regressor_type == "DT":
            clf = tree.DecisionTreeRegressor() #LinearSVR()
            param_grid = {"max_depth": [x for x in range(2,50)],
                          "max_features": [1, 3, 10],
                          "min_samples_split": [1, 3, 10],
                          "min_samples_leaf": [1, 3, 10],
                          "bootstrap": [True, False],
                          "criterion": ["gini", "entropy"]}
        elif regressor_type == "SVRLin":
            clf = LinearSVR()
            param_grid = { "C" :[ 2**k for k in range(-10, 15) ] ,
                           "epsilon" : [0,0.01,0.05,0.1,0.15,0.2]} #0,0.2
        elif regressor_type == "SVRRBF":
            clf = SVR()
            param_grid = { "C" :[ 2**k for k in range(-10, 15) ] ,
                           "epsilon" : [0,0.01,0.05,0.1,0.15,0.2],
                           "gamma" : [ 2**k for k in range(-10, 15) ],
                           "kernel" : ['rbf']} #0,0.2
        grid_search = GridSearchCV(clf, param_grid=param_grid, refit=True,cv=10)
        clf.fit(training_set_x, training_set_y)


    pred_test = clf.predict(test_set_x)

    #report in files
#    assert(len(reactants_orf) == len(clf.coef_) )
    if regressor_type in ("LS","SVRLin",  "Ridge"):
        append_to_file(regression_report_file, "<reaction>\n")
        append_to_file(regression_report_file, "<?size:" + str(len(all_indices)) + "?>\n")
        for index in xrange(len(reactants_orf)):
            append_to_file(regression_report_file, "<input symbol=\""+reactants_orf[index]+"\" weight=\""+str(clf.coef_[index])+"\"/>\n")
        full_rxn.print_reaction_unknowns_to_file(regression_report_file)
        if isinstance(clf.intercept_, float):
            append_to_file(regression_report_file, "<constant weight=\""+str(clf.intercept_)+"\"/>\n")
        else:
            append_to_file(regression_report_file, "<constant weight=\""+str(clf.intercept_[0])+"\"/>\n")
        append_to_file(regression_report_file,"<output symbol=\""+product_orf+"\"/>\n")
        append_to_file(regression_report_file, "</reaction>\n")

    #    regression_param_report = ""
    #    reactant_it = iter(reactants_orf)
    #    regression_param_report = reactant_it.next()
    #    for reactant in reactant_it:
    #        regression_param_report += ","+reactant
    #    regression_param_report += "->" + product_orf + ":" + str(clf.coef_) + ":" +str(clf.intercept_)+"\n"

    #    regression_report_file

#        filename_regression_report = "version"+str(version)+"/regression_param_report"
#        if version > 2:
#            filename_regression_report += str(t_diff)
#        filename_regression_report += ".txt"
#        fh = open(filename_regression_report, 'a')
#        fh.write(regression_param_report)
#        fh.close()
    return calculate_performance(test_set_y, pred_test)
