
for version in {5..5}
do
    for regressor in {LS,DT,SVRLin,SVRRBF,Ridge}
    do
        logfile=version$version/log_$regressor.txt
        echo $logfile
        python main.py -v $version -r $regressor > $logfile
    done

done
