import sys, getopt
from definitions import network, genes, reaction, read_in_stringified_dict_from_file
from os.path import expanduser
from network_import_functions import *
from gene_expression_data import get_all_data
from file_io import *


def main(argv):
    version = 3
    force_read_in = False
    regressor_type = "DT"
    try:
        opts, args = getopt.getopt(argv, "hv:r:f:", ["version=", "regressor=", "force_read_in="])
    except getopt.GetoptError:
        print 'PARAMETER ERROR'
        sys.exit(2)
    print opts
    for opt, arg in opts:
        if opt == '-h':
            print 'python main.py -v <version> -r <regressor> -f <force-read-in>'
            sys.exit()
        elif opt in ("-v", "--version"):
            version = int(arg)
        elif opt in ("-r", "--regressor"):
            regressor_type = str(arg)
        elif opt in ("-f", "--colony-size"):
            force_read_in = bool(arg)
    if version <3 or version > 5:
        print " Bad version" , version
        sys.exit()
    if regressor_type not in ("LS","DT","SVRLin", "SVRRBF", "Ridge"):
        print " wrong regressor-type" , regressor_type

    #get nodes influencing a certain gene from file
    home = expanduser("~")
    zimmer_file = home+"/AdaLab/Data/zimmer_boolean_network.txt"
    network_file = open(zimmer_file, 'r')
    my_dict = {}
    n = network()
    for rxn in network_file:
        (ins,out) = parse_line(rxn)
        my_dict[out] = my_dict.get(out, []) + ins
    #Create set of reactions
    #Version 1, for prediction value of gene based on current values,
    #Cannot use itself for it(would give perfect prediction
    #Version 2, predict based on previous values, also fixes problem of
    #some genes not changing

    reactions_v1,  reactions_v2 = set(), set()
    all_products = set()

    for product, reactants in my_dict.iteritems():
        if product in reactants:
            r = set(reactants) - {product}
            reactions_v1.add(reaction(product, r ))
            reactions_v2.add(reaction(product, set(reactants)))
        else:
            reactions_v1.add(reaction(product, set(reactants)))
            reactions_v2.add(reaction(product, set(reactants) | {product} ))
        all_products |= {product}
    unused_genes =  set(genes) - all_products #genes is defined in network_import_functions, is list of all genes present in zimmer
    unused_not_genes = all_products-set(genes)
    print "Unused"
    print unused_genes
    #add gene(t-1) -> gene(t)
    for gene in unused_genes:
        reactions_v2.add(reaction(gene, {gene}) )
    for r in unused_not_genes:
        reactions_v2.add(reaction(r, {r}))
    print network(reactions_v2)
    if version == 1:
        print "STOP, version 1 is useless"
        sys.exit()
        network_v1 = network(reactions_v1)
        #version 1 can use all data
        data_dict_v1 = get_all_data(home+"/AdaLab/Data/geneExpressionData/")
        write_to_file("version1/data_dict", str(data_dict_v1))
        network_v1.learn_influences(data_dict_v1, version)
    elif version == 2:
        #version 2 can only use time series
        network_v2 = network(reactions_v2)
        write_to_file("version2/network", str(network_v2))
        data_dict_v2 = {}
        if force_read_in:
            data_dict_v2 = read_in_stringified_dict_from_file("version2/data_dict")
        else:
            data_dict_v2 = get_all_data(home+"/AdaLab/Data/geneExpressionData/time_series")
            write_to_file("version2/data_dict", str(data_dict_v2))
        network_v2.learn_influences(data_dict_v2, version,regressor_type)
    elif version == 3: #y = y[t]
        network_v3 = network(reactions_v2)
        write_to_file("version3/network", str(network_v3))
        data_dict_v3 = {}
        if force_read_in:
            data_dict_v3 =  read_in_stringified_dict_from_file("version3/data_dict")
        else:
            data_dict_v3 = get_all_data(home+"/AdaLab/Data/geneExpressionData/time_series/hours_in_code")
            write_to_file("version3/data_dict", str(data_dict_v3))
        network_v3.learn_influences(data_dict_v3, version, regressor_type)
    elif version == 4: #y = y[t] - y[t-1]
        network_v4 = network(reactions_v2)
        write_to_file("version4/network", str(network_v4) )
        data_dict_v4 = {}
        if force_read_in:
            data_dict_v4 =read_in_stringified_dict_from_file("version4/data_dict")
        else:
            data_dict_v4 = get_all_data(home+"/AdaLab/Data/geneExpressionData/time_series/hours_in_code")
            write_to_file("version4/data_dict", str(data_dict_v4))
        network_v4.learn_influences(data_dict_v4, version, regressor_type)
    elif version == 5: #version 5, y = dy/dt for dt = [10,..,180]
        network_v5 = network(reactions_v2)
        write_to_file("version5/network", str(network_v5) )
        data_dict_v5 = {}
        if force_read_in:
            data_dict_v5 = read_in_stringified_dict_from_file("version5/data_dict")
        else:
            data_dict_v5 = get_all_data(home+"/AdaLab/Data/geneExpressionData/time_series/hours_in_code")
            write_to_file("version5/data_dict", str(data_dict_v5))
        network_v5.learn_influences(data_dict_v5, version, regressor_type)
    else:
        print "Not sure what to do"


    #To do
    #Time series data sets
    #interpolation : http://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html


    '''
    Possible prediction tasks:
        1) predict current value based on current values of genes
        2) Predict current value based on previous values of genes -> Need to interpolate
        3) Predict difference between current and previous value (divided by time?) based on previous gene values
    '''
if __name__ == "__main__":
   main(sys.argv[1:])

