#!/usr/bin/env python
# -*- coding: utf-8 -*-
from definitions import *

def get_filename(version, t_diff = None, regressor_type = "" ):
    filename_regression_report = "version"+str(version)+"/regression_param_report"
    if version == 3:
        t_diff_str = str(t_diff)+"min"
        filename_regression_report += t_diff_str
    filename_regression_report += regressor_type
    filename_regression_report += ".txt"
    return filename_regression_report


def write_to_file(fn, str):
    fh = open(fn, 'w')
    fh.write(str)
    fh.close()

def append_to_file(fn, str):
    fh = open(fn, 'a')
    fh.write(str)
    fh.close()
