#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Tkinter
import tkMessageBox
from get_network_influences import  learn_influence
from gene_expression_data import extract_rxn_specific_data
from numpy import nan
from file_io import *
from network_import_functions import *

complexes = ["BCY1/cAMP","CCR4/NOT/POP2","GPA2/GDP","GPA2/GTP","GPR1/GPG1/GPB/GPA2/GDP","HAP2/3/4/5","INO2/4","OAF1/PIP2","RAS/GDP","RAS/GTP","REG1/GCL7","RTG1/3","SNF1/4","SNF1/4/GAL83","TORC1","TPK/BCY1"]
conditions = ["Phosphate_depletion","aa_starvation","anaerob","dys_mit","freeze_shock","gluc_depl","heat_shock","high_pH","osmotic_shock","ox_stress","sporulation"]
drugs = ["HU","NEM","PDR","antimycin","fluphenazine","myxothiazol","rapamycin"]
evidences = ["2d-gel","ChIP","ChIP-chip","Expression evidence","High-throughput mRNA expression evidence","MS","RT-PCR","author-statement","binding","co-IP","consensus","emsa","footprinting","kPCR","lacZ","mRNA","microarray","northern_blot","prot","prot-act","proteomics","review","site-mutation","western_blot","yeast-2-hybrid"]
genes = ["ABF1","ACE2","ACH1","ACN9","ACO1","ACO2","ACS1","ACS2","ADH1","ADH2","ADH3","ADH5","ADR1","ADY2","AFT1","AFT2","AGP1","AGP2","ALD3","ALD4","ALD5","ALD6","ANT1","APE2","APE3","ARA1","ARG80","ARG81","ARO80","ARR1","ASH1","ATF1","ATO3","AZF1","BAS1","BCY1","BMH1","BMH2","CAD1","CAR1","CAR2","CAT2","CAT5","CAT8","CBF1","CCR4","CDC19","CDC25","CIN1","CIN5","CIT1","CIT2","CIT3","CNB1","COX6","CRC1","CRZ1","CSRE","CTA1","CTT1","CUP2","CYB2","CYC1","CYR1","CYT1","DAK1","DAL2","DAL3","DAL4","DAL5","DAL7","DAL80","DAL81","DAL82","DCI1","DCS2","DLD1","DLD3","DSE2","DUR1,2","DUR3","DUS3","ECI1","EEB1","ELM1","ENO1","ENO2","ERT1","FAA1","FAA2","FAA4","FBA1","FBP1","FDH1","FHL1","FKH1","FKH2","FOX2","FPS1","FUM1","FZF1","GAL83","GCL7","GCN2","GCN4","GCR1","GCR2","GDH3","GID8","GIS1","GLK1","GLN3","GPB1","GPB2","GPD1","GPG1","GPM1","GPM2","GPR1","GRX3","GSM1","GTS1","GUT1","GUT2","HAL9","HAP1","HAP2","HAP3","HAP4","HAP5","HMS1","HOG1","HOR2","HOT1","HSF1","HSP104","HXK1","HXK2","HXT1","HXT2","HXT3","HXT4","HXT5","HXT6","HXT7","ICL1","ICL2","IDH1","IDH2","IDP1","IDP2","IDP3","IFA38","INO2","INO4","IRA1","IRA2","IXR1","IZH2","JEN1","KGD1","KGD2","KOG1","LEU3","LPD1","LSC1","LSC2","LST8","MAE1","MAL33","MBP1","MCM1","MDH1","MDH2","MDH3","MET32","MIG1","MIG2","MIP6","MKS1","MLS1","MOT2","MOT3","MSN2","MSN4","MSS1","NDI1","NNK1","NOT3","NOT5","NRG1","NRG2","OAF1","OAF3","OPI1","OTU1","PCK1","PDC1","PDC2","PDC5","PDC6","PDE1","PDE2","PDR1","PDR3","PEX1","PEX11","PEX25","PEX5","PEX6","PFK1","PFK2","PFK26","PFK27","PGI1","PGK1","PGM1","PGM2","PHD1","PHO2","PHO4","PHO85","PIP2","PKH1","PKH2","PLB2","POP2","POT1","POX1","PTP1","PXA1","PXA2","PYC1","PYC2","PYK2","QCR8","QDR1","RAP1","RDS1","RDS2","REB1","REG1","REG2","RGM1","RGT1","RIM1","RIM101","RIM15","RLM1","RME1","ROX1","RPM2","RPN4","RSF1","RSF2","RTG1","RTG2","RTG3","SAK1","SAM50","SCH9","SDH1","SDH2","SDH3","SDH4","SER1","SFC1","SFP1","SIP2","SIP4","SKN7","SKO1","SNF1","SNF3","SNF4","SOD2","SOK2","SPC25","SPG1","SPS18","SPS19","SSA3","SSA4","STB5","STE12","STL1","STP1","STP2","SUR1","SWI4","SWI5","SWI6","TCO89","TDH1","TDH2","TDH3","TEC1","TES1","TKL2","TOR1","TOR2","TOS3","TPI1","TPK1","TPK2","TPK3","TPS1","TPS2","TYE7","UGA1","UGA2","UGA3","UGA4","UME6","UPC2","URA3","VID24","XBP1","YAK1","YAP1","YAP5","YAP6","YAP7","YAT1","YAT2","YGR290W","YHR033W","YJL045W","YJL218W","YMR086W","YNL035C","ZAP1","ZWF1"]
growth_phases = ["stationary_phase","diauxic_shift","growth"]

metabolites = ["AMP","ATP","Ammonia","CO2","H2O2","NO","NaCl","O2","Phosphorus source","Proline","acetate","acrolein","allophanate","arsenic","asparagine","cAMP","calcium","choline","ethanal","ethanol","gaba","galactose","glucose","glucose6P","glutamate","glutamine","glycerol","heme","inositol","lactate","nfcs","nitrogen","oleic_acid","phosphate","raffinose","selenite","sorbac","sorbitol","sugar","sulfur","thiamine","glucoseext"]

#dict = {'genename1' : {exp_number1: exp_value1, exp_number2: exp_value2},'genename2' : {exp_number1: exp_value1, exp_number2: exp_value2}}


def read_in_stringified_dict_from_file(fn):
    fh = open(fn, 'r')
    tempstr = ""
    for line in fh:
        tempstr += line
    my_dict = eval(line)
    return my_dict

def message_box(title_text, message_text ):
    window = Tkinter.Tk()
    window.wm_withdraw()
    tkMessageBox.showinfo(title=title_text, message=message_text)


class reaction:
    def __init__(self, rh_product = set(), rh_reactants = set()):
        self.reactants = rh_reactants
        self.product = rh_product

    def __str__(self):
        stringify = str(self.reactants) + "->" + str(self.product) + "\n"
        return stringify

    def valid_reaction(self):
        return self.check_product_type == "gene" and len((self.divide_by_type())[4]) > 0

    def check_product_type(self):
        if self.product in complexes:
            return "gene_complex"
        elif self.product in conditions:
            return "condition"
        elif self.product in drugs:
            return "drug"
        elif self.product in evidences:
            return "evidence"
        elif self.product in genes:
            return "gene"
        elif self.product in growth_phases:
            return "growth_phase"
        elif self.product in metabolites:
            return "metabolite"
        else:
            print "Product ERROR: type of", self.product, "not found!"
            return "unknown"

    def divide_by_type(self):
        rxn_cmpl, rxn_cond, rxn_drugs, rxn_evid, rxn_genes, rxn_growth_ph, rxn_mets = [],[],[],[],[],[],[]
        for reactant in self.reactants:
            if reactant in complexes:
                rxn_cmpl += [reactant]
            elif reactant in conditions:
                rxn_cond += [reactant]
            elif reactant in drugs:
                rxn_drugs += [reactant]
            elif reactant in evidences:
                rxn_evid += [reactant]
            elif reactant in genes:
                rxn_genes += [reactant]
            elif reactant in growth_phases:
                rxn_growth_ph += [reactant]
            elif reactant in metabolites:
                rxn_mets += [reactant]
            else:
                print "Reactant ERROR: type of", reactant, "not found!"
        return (rxn_cmpl, rxn_cond, rxn_drugs, rxn_evid, rxn_genes, rxn_growth_ph, rxn_mets)

    def print_reaction_unknowns_to_file(self, fn):
        (rxn_cmpl, rxn_cond, rxn_drugs, rxn_evid, rxn_genes, rxn_growth_ph, rxn_mets) = self.divide_by_type()
        if len(rxn_cmpl) > 0:
            for n in rxn_cmpl:
                append_to_file(fn,"<input symbol=\""+n+"\" weight=\"NA\" type=\"cmpl\"/>\n")
        if len(rxn_cond) > 0:
            for n in rxn_cond:
                append_to_file(fn,"<input symbol=\""+n+"\" weight=\"NA\" type=\"cond\"/>\n")
        if len(rxn_drugs) > 0:
            for n in rxn_drugs:
                append_to_file(fn, "<input symbol=\""+n+"\" weight=\"NA\" type=\"drug\"/>\n")
        if len(rxn_mets) > 0:
            for n in rxn_mets:
                append_to_file(fn, "<input symbol=\""+n+"\" weight=\"NA\" type=\"met\"/>\n")
    def print_genes_to_file(self, fn):
        (rxn_cmpl, rxn_cond, rxn_drugs, rxn_evid, rxn_genes, rxn_growth_ph, rxn_mets) = self.divide_by_type()
        if len(rxn_genes) > 0:
            for n in rxn_genes:
                orf_gene =convert_gene_name_to_orf(n)
                append_to_file(fn, "<input symbol=\""+orf_gene+"\" weight=\"NA\" type=\"gene\"/>\n")
        append_to_file(fn, "<constant weight=\"NA\"/>\n")
class regression_results:
    def __init__(self):
        self.expl_var = []
        self.maes = []
        self.mses = []
        self.medaes = []
        self.r2s = []
    def add_results(self, explained_var, mae, mse, medae, r2):
        self.expl_var.append(explained_var)
        self.maes.append(mae)
        self.mses.append(mse)
        self.medaes.append(medae)
        self.r2s.append(r2)
    def __str__(self):
        stringify = "Type&Min&Max&Mean&Stdev&Median\\\\\n"
        stringify += "Expl var&"+str(round(min(self.expl_var), 4))+"&"+str(round(max(self.expl_var), 4))+"&"+str(round(mean(self.expl_var), 4))+"&"+str(round(shifted_data_variance(self.expl_var), 4))+"&"+str(round(median(self.expl_var), 4))+"\\\\\n"
        stringify +="mae&"+str(round(min(self.maes), 4))+"&"+str(round(max(self.maes), 4)) +"&"+str(round(mean(self.maes), 4))+"&"+str(round(shifted_data_variance(self.maes), 4)) +"&"+str(round(median(self.maes), 4))+"\\\\\n"
        stringify +="mse&"+str(round(min(self.mses), 4))+"&"+str(round(max(self.mses), 4))+"&"+str(round(mean(self.mses), 4))+"&"+str(round(shifted_data_variance(self.mses), 4))+"&"+str(round(median(self.mses), 4))+"\\\\\n"
        stringify +="medae&"+str(round(min(self.medaes), 4))+"&"+str(round(max(self.medaes), 4))+"&"+str(round(mean(self.medaes), 4))+"&"+str(round(shifted_data_variance(self.medaes), 4))+"&"+str(round(median(self.medaes), 4))+"\\\\\n"
        stringify +="r2&"+str(round(min(self.r2s), 4))+"&"+str(round(max(self.r2s), 4))+"&"+str(round(mean(self.r2s), 4))+"&"+str(round(shifted_data_variance(self.r2s), 4))+"&"+str(round(median(self.r2s), 4))+"\\\\\n"
        return stringify

class network:
      def __init__(self, reactions = None):
          self.rxns = reactions
      def add_reaction(self, reaction):
            self.rxns.add(reaction)
      def __iter__(self):
          return iter(self.rxns)
      def __str__(self):
          stringify = ""
          for rxn in self.rxns:
              stringify += str(rxn)
          return stringify
      def get_reaction_dict(self):
          reaction_dict = {}
          for rxn in self.rxns:
              gn_orf = rxn.product
              if rxn.check_product_type == "gene":
                gn_orf = convert_gene_name_to_orf(rxn.product)
              reaction_dict[gn_orf] = rxn
          return reaction_dict
      def learn_influences(self, data_dict, version, regressor_type):
          #reaction_dict = self.get_reaction_dict()
          time_diffs = [0]
          if version == 3 or version == 4 or version == 5:
              time_diffs = [10,15,20,30,60,120,180]  # equivalent of 10 min; 30min, 1h, 2h, 3h
          for t_diff in time_diffs:
              regression_report_file = get_filename(version, t_diff,regressor_type)
              write_to_file(regression_report_file, "<network>\n")
              regr_performance = regression_results()
              for rxn in self.rxns:
                  (x_values, y_values, reactant_list, product_orf) = extract_rxn_specific_data(data_dict, rxn, version, t_diff)
                  p_t = rxn.check_product_type()

                  if len(x_values) != 0:
                      #full_rxn = reaction_dict[product_orf]
                      out = learn_influence(reactant_list, product_orf, x_values, y_values, regression_report_file, rxn, regressor_type, version)#add t_diff as this function prints to file
                      if out != None:
                          (expl_var, mae,mse,medae,r2) = out
                          regr_performance.add_results(expl_var, mae,mse,medae,r2)
                  else:
                      print "Nothing for reaction", str(rxn)
                      #TODO: add product to reactants if
                      append_to_file(regression_report_file, "<reaction>\n")
                      rxn.print_genes_to_file(regression_report_file)
                      rxn.print_reaction_unknowns_to_file(regression_report_file)
                      append_to_file(regression_report_file,"<output symbol=\""+ convert_gene_name_to_orf(rxn.product) +"\"/>\n")
                      append_to_file(regression_report_file, "</reaction>\n")
              print "Time diff: ", t_diff, "version", version, "regressor", regressor_type
              print str(regr_performance)
              print "-------------------"
              append_to_file(regression_report_file, "</network>\n")

def mean(l):
    return sum(l)/len(l)

def shifted_data_variance(data):
   if len(data) == 0:
      return 0
   K = data[0]
   n = 0
   Sum = 0
   Sum_sqr = 0
   for x in data:
      n = n + 1
      Sum += x - K
      Sum_sqr += (x - K) * (x - K)
   variance = (Sum_sqr - (Sum * Sum)/n)/(n - 1)
   # use n instead of (n-1) if want to compute the exact variance of the given data
   # use (n-1) if data are samples of a larger population
   return variance

def median(lst):
    sortedLst = sorted(lst)
    lstLen = len(lst)
    index = (lstLen - 1) // 2
    if (lstLen % 2):
        return sortedLst[index]
    else:
        return (sortedLst[index] + sortedLst[index + 1])/2.0

#def sigmoid_function



