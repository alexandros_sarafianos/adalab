
from os.path import expanduser
from definitions import *

complexes = set(["BCY1/cAMP","CCR4/NOT/POP2","GPA2/GDP","GPA2/GTP","GPR1/GPG1/GPB/GPA2/GDP","HAP2/3/4/5","INO2/4","OAF1/PIP2","RAS/GDP","RAS/GTP","REG1/GCL7","RTG1/3","SNF1/4","SNF1/4/GAL83","TORC1","TPK/BCY1"])
conditions = set(["Phosphate_depletion","aa_starvation","anaerob","dys_mit","freeze_shock","gluc_depl","heat_shock","high_pH","osmotic_shock","ox_stress","sporulation"])
drugs = set(["HU","NEM","PDR","antimycin","fluphenazine","myxothiazol","rapamycin"])
evidences = set(["2d-gel","ChIP","ChIP-chip","Expression evidence","High-throughput mRNA expression evidence","MS","RT-PCR","author-statement","binding","co-IP","consensus","emsa","footprinting","kPCR","lacZ","mRNA","microarray","northern_blot","prot","prot-act","proteomics","review","site-mutation","western_blot","yeast-2-hybrid"])
genes = set(["ABF1","ACE2","ACH1","ACN9","ACO1","ACO2","ACS1","ACS2","ADH1","ADH2","ADH3","ADH5","ADR1","ADY2","AFT1","AFT2","AGP1","AGP2","ALD3","ALD4","ALD5","ALD6","ANT1","APE2","APE3","ARA1","ARG80","ARG81","ARO80","ARR1","ASH1","ATF1","ATO3","AZF1","BAS1","BCY1","BMH1","BMH2","CAD1","CAR1","CAR2","CAT2","CAT5","CAT8","CBF1","CCR4","CDC19","CDC25","CIN1","CIN5","CIT1","CIT2","CIT3","CNB1","COX6","CRC1","CRZ1","CSRE","CTA1","CTT1","CUP2","CYB2","CYC1","CYR1","CYT1","DAK1","DAL2","DAL3","DAL4","DAL5","DAL7","DAL80","DAL81","DAL82","DCI1","DCS2","DLD1","DLD3","DSE2","DUR1,2","DUR3","DUS3","ECI1","EEB1","ELM1","ENO1","ENO2","ERT1","FAA1","FAA2","FAA4","FBA1","FBP1","FDH1","FHL1","FKH1","FKH2","FOX2","FPS1","FUM1","FZF1","GAL83","GCL7","GCN2","GCN4","GCR1","GCR2","GDH3","GID8","GIS1","GLK1","GLN3","GPB1","GPB2","GPD1","GPG1","GPM1","GPM2","GPR1","GRX3","GSM1","GTS1","GUT1","GUT2","HAL9","HAP1","HAP2","HAP3","HAP4","HAP5","HMS1","HOG1","HOR2","HOT1","HSF1","HSP104","HXK1","HXK2","HXT1","HXT2","HXT3","HXT4","HXT5","HXT6","HXT7","ICL1","ICL2","IDH1","IDH2","IDP1","IDP2","IDP3","IFA38","INO2","INO4","IRA1","IRA2","IXR1","IZH2","JEN1","KGD1","KGD2","KOG1","LEU3","LPD1","LSC1","LSC2","LST8","MAE1","MAL33","MBP1","MCM1","MDH1","MDH2","MDH3","MET32","MIG1","MIG2","MIP6","MKS1","MLS1","MOT2","MOT3","MSN2","MSN4","MSS1","NDI1","NNK1","NOT3","NOT5","NRG1","NRG2","OAF1","OAF3","OPI1","OTU1","PCK1","PDC1","PDC2","PDC5","PDC6","PDE1","PDE2","PDR1","PDR3","PEX1","PEX11","PEX25","PEX5","PEX6","PFK1","PFK2","PFK26","PFK27","PGI1","PGK1","PGM1","PGM2","PHD1","PHO2","PHO4","PHO85","PIP2","PKH1","PKH2","PLB2","POP2","POT1","POX1","PTP1","PXA1","PXA2","PYC1","PYC2","PYK2","QCR8","QDR1","RAP1","RDS1","RDS2","REB1","REG1","REG2","RGM1","RGT1","RIM1","RIM101","RIM15","RLM1","RME1","ROX1","RPM2","RPN4","RSF1","RSF2","RTG1","RTG2","RTG3","SAK1","SAM50","SCH9","SDH1","SDH2","SDH3","SDH4","SER1","SFC1","SFP1","SIP2","SIP4","SKN7","SKO1","SNF1","SNF3","SNF4","SOD2","SOK2","SPC25","SPG1","SPS18","SPS19","SSA3","SSA4","STB5","STE12","STL1","STP1","STP2","SUR1","SWI4","SWI5","SWI6","TCO89","TDH1","TDH2","TDH3","TEC1","TES1","TKL2","TOR1","TOR2","TOS3","TPI1","TPK1","TPK2","TPK3","TPS1","TPS2","TYE7","UGA1","UGA2","UGA3","UGA4","UME6","UPC2","URA3","VID24","XBP1","YAK1","YAP1","YAP5","YAP6","YAP7","YAT1","YAT2","YGR290W","YHR033W","YJL045W","YJL218W","YMR086W","YNL035C","ZAP1","ZWF1"])
growth_phases = set(["stationary_phase","diauxic_shift","growth"])

metabolites = set(["AMP","ATP","Ammonia","CO2","H2O2","NO","NaCl","O2","Phosphorus source","Proline","acetate","acrolein","allophanate","arsenic","asparagine","cAMP","calcium","choline","ethanal","ethanol","gaba","galactose","glucose","glucose6P","glutamate","glutamine","glycerol","heme","inositol","lactate","nfcs","nitrogen","oleic_acid","phosphate","raffinose","selenite","sorbac","sorbitol","sugar","sulfur","thiamine","glucoseext"])


'''
Next two dictionaries obtained (manually) from uniprot with the mapping tool (from gene name to uniprot id for yeast), then selecting only those entries of Saccharomyces Cerevisiae and selecting the orf name as well. Obtained on 14/12/2015
'''
additional_dict_gn_orf = {'HSF1': 'YGL073W', 'PCK1': 'YKR097W', 'TKL2': 'YBR117C', 'UGA1': 'YGR019W', 'TPI1': 'YDR050C', 'HXK1': 'YFR053C', 'FBP1': 'YLR377C', 'TDH2': 'YJR009C', 'HXK2': 'YGL253W', 'POX1': 'YGL205W', 'CIN5': 'YOR028C', 'PGI1': 'YBR196C', 'SOD2': 'YHR008C', 'ALD6': 'YPL061W', 'ALD5': 'YER073W', 'ALD4': 'YOR374W', 'ALD3': 'YMR169C', 'ALD2': 'YMR170C', 'IDP3': 'YNL009W', 'IDP2': 'YLR174W', 'IDP1': 'YDL066W', 'URA3': 'YEL021W', 'VID24': 'YBR105C', 'YAT1': 'YAR035W', 'ICL1': 'YER065C', 'ICL2': 'YPR006C', 'TDH1': 'YJL052W', 'SDL1': 'YIL167W', 'GUT1': 'YHL032C', 'GUT2': 'YIL155C', 'TDH3': 'YGR192C', 'SSA4': 'YER103W', 'TPK1': 'YJL164C', 'TPK3': 'YKL166C', 'TPK2': 'YPL203W', 'CNB1': 'YKL190W', 'FBA1': 'YKL060C', 'CDC19': 'YAL038W', 'IFA38': 'YBR159W', 'PGK1': 'YCR012W', 'SER1': 'YOR184W', 'RIM101': 'YHL027W', 'ZWF1': 'YNL241C', 'ACO2': 'YJL200C', 'EEB1': 'YPL095C', 'GPP2': 'YER062C', 'HSP104': 'YLL026W', 'CTA1': 'YDR256C', 'GDH3': 'YAL062W', 'POT1': 'YIL160C', 'MAE1': 'YKL029C', 'SNF4': 'YGL115W', 'FAA4': 'YMR246W', 'FAA1': 'YOR317W', 'FAA2': 'YER015W', 'SOR1': 'YJR159W', 'CAR2': 'YLR438W', 'CAR1': 'YPL111W', 'GLK1': 'YCL040W', 'SSA3': 'YBL075C', 'SDH2': 'YLL041C', 'LSC2': 'YGR244C', 'LSC1': 'YOR142W', 'CYR1': 'YJL005W', 'KGD2': 'YDR148C', 'KGD1': 'YIL125W', 'PYK2': 'YOR347C', 'LPD1': 'YFL018C', 'GPM2': 'YDL021W', 'GPM1': 'YKL152C', 'CAT5': 'YOR125C', 'PTK1': 'YKL198C', 'SDH1': 'YKL148C', 'CIT1': 'YNR001C', 'CIT2': 'YCR005C', 'CIT3': 'YPR001W', 'SDH4': 'YDR178W', 'SDH7': 'YDR511W', 'GRX3': 'YDR098C', 'CAT2': 'YML042W', 'MDH3': 'YDL078C', 'MDH2': 'YOL126C', 'MDH1': 'YKL085W', 'ARG81': 'YML099C', 'ARG80': 'YMR042W', 'TES1': 'YJR019C', 'PFK26': 'YIL107C', 'PFK27': 'YOL136C', 'DAL7': 'YIR031C', 'MLS1': 'YNL117W', 'BCY1': 'YIL033C', 'PFK2': 'YMR205C', 'PFK1': 'YGR240C', 'ARR1': 'YPR199C', 'CYT1': 'YOR065W', 'CAD1': 'YDR423C', 'MOT2': 'YER068W', 'DAL3': 'YIR032C', 'DAL2': 'YIR029W', 'CTT1': 'YGR088W', 'FUM1': 'YPL262W'}

additional_dict_orf_gn = {'YIL167W': 'SDL1', 'YIL107C': 'PFK26', 'YMR042W': 'ARG80', 'YKL060C': 'FBA1', 'YMR205C': 'PFK2', 'YDR178W': 'SDH4', 'YCR005C': 'CIT2', 'YEL021W': 'URA3', 'YBL075C': 'SSA3', 'YPR006C': 'ICL2', 'YBR196C': 'PGI1', 'YGR240C': 'PFK1', 'YKL190W': 'CNB1', 'YER073W': 'ALD5', 'YKL152C': 'GPM1', 'YER015W': 'FAA2', 'YMR170C': 'ALD2', 'YIL155C': 'GUT2', 'YER062C': 'GPP2', 'YFL018C': 'LPD1', 'YDR098C': 'GRX3', 'YPR001W': 'CIT3', 'YPL262W': 'FUM1', 'YKL029C': 'MAE1', 'YAL038W': 'CDC19', 'YDR511W': 'SDH7', 'YCL040W': 'GLK1', 'YIR031C': 'DAL7', 'YCR012W': 'PGK1', 'YJR009C': 'TDH2', 'YPL111W': 'CAR1', 'YNL241C': 'ZWF1', 'YDL066W': 'IDP1', 'YOR184W': 'SER1', 'YER068W': 'MOT2', 'YBR159W': 'IFA38', 'YBR105C': 'VID24', 'YDR423C': 'CAD1', 'YOR374W': 'ALD4', 'YJL052W': 'TDH1', 'YKL085W': 'MDH1', 'YLL041C': 'SDH2', 'YER103W': 'SSA4', 'YMR169C': 'ALD3', 'YLR377C': 'FBP1', 'YIL033C': 'BCY1', 'YGL205W': 'POX1', 'YJL005W': 'CYR1', 'YML042W': 'CAT2', 'YOR142W': 'LSC1', 'YJL200C': 'ACO2', 'YNL117W': 'MLS1', 'YGR088W': 'CTT1', 'YGL073W': 'HSF1', 'YGR244C': 'LSC2', 'YLL026W': 'HSP104', 'YIL125W': 'KGD1', 'YDL078C': 'MDH3', 'YOL126C': 'MDH2', 'YDR256C': 'CTA1', 'YML099C': 'ARG81', 'YDL021W': 'GPM2', 'YOL136C': 'PFK27', 'YIL160C': 'POT1', 'YGR019W': 'UGA1', 'YDR050C': 'TPI1', 'YKL198C': 'PTK1', 'YHL027W': 'RIM101', 'YIR032C': 'DAL3', 'YPL061W': 'ALD6', 'YLR174W': 'IDP2', 'YGL253W': 'HXK2', 'YLR438W': 'CAR2', 'YAL062W': 'GDH3', 'YHL032C': 'GUT1', 'YKL148C': 'SDH1', 'YAR035W': 'YAT1', 'YPR199C': 'ARR1', 'YDR148C': 'KGD2', 'YPL203W': 'TPK2', 'YIR029W': 'DAL2', 'YNL009W': 'IDP3', 'YER065C': 'ICL1', 'YHR008C': 'SOD2', 'YJR159W': 'SOR1', 'YGL115W': 'SNF4', 'YJL164C': 'TPK1', 'YJR019C': 'TES1', 'YBR117C': 'TKL2', 'YNR001C': 'CIT1', 'YOR125C': 'CAT5', 'YKL166C': 'TPK3', 'YOR347C': 'PYK2', 'YGR192C': 'TDH3', 'YFR053C': 'HXK1', 'YOR065W': 'CYT1', 'YMR246W': 'FAA4', 'YOR028C': 'CIN5', 'YOR317W': 'FAA1', 'YKR097W': 'PCK1', 'YPL095C': 'EEB1'}

'''
Still errors for acn9, csre, dur1,2 , gcl7, hor2. Obtained manually from uniprot on 14/12/2015 (queries:
GENE organism:yeast AND reviewed:yes AND organism:"Saccharomyces cerevisiae (strain ATCC 204508 / S288c) (Baker's yeast) [559292]" 
CSRE -> possibly related to SIP4:     YJL089W (from SGD)
GCL7 -> synonyms Cid1/DIS2: P32598 : YER133W
'''
upd_gn_orf = {'ACN9': 'YDR511W', 'CSRE': 'YJL089W', 
              'DUR1,2': 'YBR208C', 'GCL7': 'YER133W',
              'HOR2' : 'YER062C'}
upd_orf_gn= {'YDR511W': 'ACN9', 'YJL089W': 'CSRE', 
              'YBR208C': 'DUR1,2', 'YER133W': 'GCL7',
              'YER062C' : 'HOR2'}
              
additional_dict_gn_orf.update(upd_gn_orf)
additional_dict_orf_gn.update(upd_orf_gn)

possible_metabolites = ["Phosphorus", "Phosphate"]
possible_complexes = ["OAF1/PIP2inactive", "OAF1/PIP2active", "OAF1/PIP2inactive"]
possible_genes = ["SCH9phosphorylated","RDS2phosphorylated" ]
    
def parse_line(m_line):
	ins = []
	outs = ""
	if len(m_line) > 0 and m_line[0] == "#":
		return []
	else:	
		temp_string = ""
		reaction_product = False
		for ch in m_line:
			if ch ==  '&':
				if reaction_product:
					outs.extend([temp_string])
				else:
					ins.extend([temp_string])	
				invert = False
				temp_string = "";

			elif ch == ' ':
				pass

			elif ch =='-':
				ins.extend([temp_string])
				invert = False
				temp_string = "-"
			elif ch == '>':
				if temp_string == "-":
				    stimulation=True
				temp_string = ""
			elif ch == '|':
				if temp_string == "-":
				    stimulation=False
				temp_string = ""
			elif ch == '^':
				invert = True
			else:
				temp_string += ch
		if temp_string:
			outs = temp_string.rstrip("\n")
	return (ins,outs)

def check_product_type(product):
    if product in complexes:
        return "gene_complex"
    elif product in conditions:
        return "condition"
    elif product in drugs:
        return "drug"
    elif product in evidences:
        return "evidence"
    elif product in genes:
        return "gene"
    elif product in growth_phases:
        return "growth_phase"
    elif product in metabolites:
        return "metabolite"
    else:
        print "Product ERROR: type of", product, "not found!"
        return "unknown"

def divide_by_type(reactants):
    rxn_cmpl, rxn_cond, rxn_drugs, rxn_evid, rxn_genes, rxn_growth_ph, rxn_mets = [],[],[],[],[],[],[]
    for reactant in reactants:
        if reactant in complexes:
            rxn_cmpl += [reactant]
        elif reactant in conditions:
            rxn_cond += [reactant]
        elif reactant in drugs:
            rxn_drugs += [reactant]
        elif reactant in evidences:
            rxn_evid += [reactant]
        elif reactant in genes:
            rxn_genes += [reactant]
        elif reactant in growth_phases:
            rxn_growth_ph += [reactant]
        elif reactant in metabolites:
            rxn_mets += [reactant]
        else:
            print "Reactant ERROR: type of", reactant, "not found!"
    return (rxn_cmpl, rxn_cond, rxn_drugs, rxn_evid, rxn_genes, rxn_growth_ph, rxn_mets)



home = expanduser("~")

def create_upkb_ensembl_dicts():
    mapping_file = home+"/AdaLab/Data/uniprot_data/grep_upkb_ensembl.tsv"
    fh_in = open(mapping_file, 'r')
    first = ""
    first_dict=  {}
    for line in fh_in:
        splitted = line.rstrip("\n").replace("_YEAST", "").split("\t") 
        first_dict[splitted[2]] = splitted[0]
    return first_dict 
     
mapping_dict = create_upkb_ensembl_dicts()


def convert_gene_name_to_orf(gene_name): #works both ways 
    #try 1
    orf_name = additional_dict_gn_orf.get(gene_name)
    if orf_name != None:
        return orf_name
    gn = additional_dict_orf_gn.get(gene_name)
    if gn != None:
        return gn
    #try 2
    uid = mapping_dict.get(gene_name)
    if uid == None:
        print "None error for gene_name", gene_name
        return gene_name
    else:
        for key,val in mapping_dict.iteritems():
            if val == uid and key != gene_name:
                return key
                
    print "Not found for uid", uid
    return gene_name
    
def test_gene_conversions():
    error = 0
    el = []
    for gene in genes:
        if convert_gene_name_to_orf(gene)== None:
            error += 1
            el += [gene]
    for e in el:
        print e
    print len(genes), error
    
test_gene_conversions()   

def analyze_network(netw_dict):
    genes_with_input, genes_with_input_set = 0, set()
    for key, value in netw_dict.iteritems():
        if key in genes:   
            genes_with_input += 1
            genes_with_input_set.add(key)
    return ( set(genes) - genes_with_input_set )
     
    #assert(genes_with_input == len(genes) )

def is_gene_in_network(gene_name):
    if gene_name in genes:
        return True
    else:
        #check if it is an orf name and whether this is in genes
        orf_name = convert_gene_name_to_orf(gene_name)
        if orf_name in genes:
            return True
    #print gene_name, "not in network!"
    return False

assert(is_gene_in_network("ABF1"))
assert(is_gene_in_network("YKL112W"))
assert(not is_gene_in_network("NOGENE"))







