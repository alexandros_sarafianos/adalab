#!/usr/bin/env python
# -*- coding: utf-8 -*-
files = ["/home/alexandros/AdaLab/Data/geneExpressionData/time_series/DeRisi_1997_PMID_9381177/2010.diauxic.pcl" ]
files.append("/home/alexandros/AdaLab/Data/geneExpressionData/time_series/Brauer_2005_PMID_15758028/2010.Brauer05_batch1.flt.knn.avg.pcl")
from gene_expression_data import *
exp_dict, time_series_dict = {}, {}
experiment_number = 0
for f_path in files:
    print "New datafile:  ",f_path
    (experiment_number, exp_dict, time_series_dict) = load_gene_expression_pcl_data(f_path, experiment_number, exp_dict, time_series_dict)
#return exp_dict, experiment_number
print experiment_number
for key, val in exp_dict.iteritems():
    print key, val

print "\n"
for key, val in time_series_dict.iteritems():
    print key,val
