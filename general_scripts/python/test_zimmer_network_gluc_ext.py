import subprocess
import re

#Inputs
workpath= "/home/alexs/AdaLab/"
#workpath = raw_input("Give path to AdaLab executable!\n")
th_1 = "0.1"
th_2 = "0.8"



#derived from input
ifile = workpath+"Data/diauxiTest_iso1-characteristics1-labelled_extended_seq.csv"
experiment_file = workpath+"Data/test_experiment.xml"
executable_file = workpath+"build/src/simulator"
mapping = workpath+"Data/gene_mappings.csv"
executable = workpath+"build/src/simulator -e " + experiment_file + " --save-output 1 --colony-size 100"
my_re =  re.compile(r'Diauxic_shift=(\d\.*\d*)')

print executable
def return_type(th1, th2):
	if th1 == "0.01" and th2 == "0.99":
		return "type1"
	elif th1 == "0.05" and th2 == "0.95":
		return "type2"
	elif th1 == "0.1" and th2 == "0.9":
		return "type3"
	elif th1 == "0.15" and th2 == "0.85":
		return "type4"
	elif th1 == "0.2" and th2 == "0.8":
		return "type5"
	elif th1 == "0.1" and th2 == "0.8":
		return "type6"
	else:
		return "type"+th1+th2

def run_command(command):
    p = subprocess.Popen(command,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT,
                         shell=True )
    return iter(p.stdout.readline, b'')


def create_experiment_file(gene, exp_file):
    fh_exp = open(experiment_file, 'w')
    fh_exp.write('<?xml version="1.0" encoding="UTF-8"?>')
    fh_exp.write('<experiment>')
    fh_exp.write('<genes>')
    if gene != "":
        fh_exp.write('<gene id= "'+gene+'" value = "0" fixed = "1"/>')
    fh_exp.write('</genes>')
    fh_exp.write('<metabolites>')
    fh_exp.write('<metabolite id= "glucoseext" value = "1" fixed = "0"/>')
    fh_exp.write('<metabolite id= "nitrogen" value = "1" fixed = "0"/>')
    fh_exp.write('<metabolite id= "O2" value = "1" fixed = "1"/>')
    fh_exp.write('</metabolites>')
    fh_exp.write('<drug>')
    fh_exp.write('</drug>')
    fh_exp.write('<condition>')
    fh_exp.write('</condition>')
    fh_exp.write('</experiment>')
    fh_exp.close()

def load_gene_mappings(gene_file):
    fh_g = open(gene_file, 'r')
    gene_dict = {}
    for gene_line in fh_g:
        splitted = gene_line.split(",")
        gene_dict[splitted[1]] = splitted[0]
    return gene_dict
# create_experiment_file('MIG1', experiment_file)

fh_in = open(ifile, 'r')
fh_in.readline()

ds_pos = 0
ds_neg = 0

#gene_map_dict = load_gene_mappings(mapping)
experiment_comparison_dict = {}
genes_error = []
for line in fh_in:
    splitted_line = line.split("\t")
    if splitted_line[0] == th_1 and splitted_line[1] == th_2:
        gene_to_test = splitted_line[3]
        if gene_to_test == "":
            print "error for gene ", splitted_line[3]
        create_experiment_file(gene_to_test, experiment_file)
	found = False
        for line_output in run_command( executable ):
            ro = my_re.search(line_output)
            if ro:
                ds = ro.groups()[0]
                experiment_comparison_dict[splitted_line[3]] = {'predicted_ds': ds, 'mutant_growth_rate': splitted_line[15], 'chi_sq'
                : splitted_line[11], 'fischer': splitted_line[12]}
		if float(ds)=='1':
			ds_pos += 1
		else:
			ds_neg += 1
            else:
                print line_output
		genes_error += [gene_to_test]

create_experiment_file("", experiment_file)

for line_output in run_command( executable ):
    ro = my_re.search(line_output)
    if ro:
        ds = ro.groups()[0]
        experiment_comparison_dict['wild'] = {'predicted_ds': ds, 'mutant_growth_rate': splitted_line[15], 'chi_sq'
        : splitted_line[11], 'fischer': splitted_line[12]}
	if ds=='1':
		ds_pos += 1
	else:
		ds_neg += 1

print "ERRORS ", genes_error 
print "POS:" , ds_pos
print"NEG:" , ds_neg
print experiment_comparison_dict
results = experiment_comparison_dict
