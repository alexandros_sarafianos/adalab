
resume(off).
use_packs(0).
sampling_strategy(none).

typed_language(yes).

type(go_molecular_function_enables(id,go)).
type(go_biological_process_involvedin(id,go)).
type(go_cellular_component_partof(id,go)).
type(go_molecular_function_contributesto(id,go)).
type(go_molecular_function_not(id,go)).

predict(experiment(+,-)).
rmode(go_molecular_function_enables(+ID,#)).
rmode(go_biological_process_involvedin(+ID,#)).
rmode(go_cellular_component_partof(+ID,#)).
rmode(go_molecular_function_contributesto(+ID,#)).
rmode(go_molecular_function_not(+ID,#)).

random_validation_set(0.2).
random_test_set(0.15).
pruning(c45).



