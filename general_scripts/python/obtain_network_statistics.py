
import subprocess
import re

import sys, getopt
from network_import_functions import *

genes_in_model = set(["ABF1","ACE2","ACH1","ACN9","ACO1","ACO2","ACS1","ACS2","ADH1","ADH2","ADH3","ADH5","ADR1","ADY2","AFT1","AFT2","AGP1","AGP2","ALD3","ALD4","ALD5","ALD6","ANT1","APE2","APE3","ARA1","ARG80","ARG81","ARO80","ARR1","ASH1","ATF1","ATO3","AZF1","BAS1","BCY1","BMH1","BMH2","CAD1","CAR1","CAR2","CAT2","CAT5","CAT8","CBF1","CCR4","CDC19","CDC25","CIN1","CIN5","CIT1","CIT2","CIT3","CNB1","COX6","CRC1","CRZ1","CSRE","CTA1","CTT1","CUP2","CYB2","CYC1","CYR1","CYT1","DAK1","DAL2","DAL3","DAL4","DAL5","DAL7","DAL80","DAL81","DAL82","DCI1","DCS2","DLD1","DLD3","DSE2","DUR1","2","DUR3","DUS3","ECI1","EEB1","ELM1","ENO1","ENO2","ERT1","FAA1","FAA2","FAA4","FBA1","FBP1","FDH1","FHL1","FKH1","FKH2","FOX2","FPS1","FUM1","FZF1","GAL83","GCL7","GCN2","GCN4","GCR1","GCR2","GDH3","GID8","GIS1","GLK1","GLN3","GPB1","GPB2","GPD1","GPG1","GPM1","GPM2","GPR1","GRX3","GSM1","GTS1","GUT1","GUT2","HAL9","HAP1","HAP2","HAP3","HAP4","HAP5","HMS1","HOG1","HOR2","HOT1","HSF1","HSP104","HXK1","HXK2","HXT1","HXT2","HXT3","HXT4","HXT5","HXT6","HXT7","ICL1","ICL2","IDH1","IDH2","IDP1","IDP2","IDP3","IFA38","INO2","INO4","IRA1","IRA2","IXR1","IZH2","JEN1","KGD1","KGD2","KOG1","LEU3","LPD1","LSC1","LSC2","LST8","MAE1","MAL33","MBP1","MCM1","MDH1","MDH2","MDH3","MET32","MIG1","MIG2","MIP6","MKS1","MLS1","MOT2","MOT3","MSN2","MSN4","MSS1","NDI1","NNK1","NOT3","NOT5","NRG1","NRG2","OAF1","OAF3","OPI1","OTU1","PCK1","PDC1","PDC2","PDC5","PDC6","PDE1","PDE2","PDR1","PDR3","PEX1","PEX11","PEX25","PEX5","PEX6","PFK1","PFK2","PFK26","PFK27","PGI1","PGK1","PGM1","PGM2","PHD1","PHO2","PHO4","PHO85","PIP2","PKH1","PKH2","PLB2","POP2","POT1","POX1","PTP1","PXA1","PXA2","PYC1","PYC2","PYK2","QCR8","QDR1","RAP1","RDS1","RDS2","REB1","REG1","REG2","RGM1","RGT1","RIM1","RIM101","RIM15","RLM1","RME1","ROX1","RPM2","RPN4","RSF1","RSF2","RTG1","RTG2","RTG3","SAK1","SAM50","SCH9","SDH1","SDH2","SDH3","SDH4","SER1","SFC1","SFP1","SIP2","SIP4","SKN7","SKO1","SNF1","SNF3","SNF4","SOD2","SOK2","SPC25","SPG1","SPS18","SPS19","SSA3","SSA4","STB5","STE12","STL1","STP1","STP2","SUR1","SWI4","SWI5","SWI6","TCO89","TDH1","TDH2","TDH3","TEC1","TES1","TKL2","TOR1","TOR2","TOS3","TPI1","TPK1","TPK2","TPK3","TPS1","TPS2","TYE7","UGA1","UGA2","UGA3","UGA4","UME6","UPC2","URA3","VID24","XBP1","YAK1","YAP1","YAP5","YAP6","YAP7","YAT1","YAT2","YGR290W","YHR033W","YJL045W","YJL218W","YMR086W","YNL035C","ZAP1","ZWF1"])

def check_existing_key_and_create(key, dictionary ):
	if dictionary.get(key, {} ) == {}:
		dictionary[key] = {}
	

def update_connectivity_dict( parsed, dictionary ):
	ins = parsed[0] 
	outs = parsed[1]
	for i in ins: 
		check_existing_key_and_create(i, dictionary)
		dictionary[i]['out_edges'] = dictionary[i].get('out_edges', 0) + len(outs)
	for o in outs:
		check_existing_key_and_create(o,dictionary)
		dictionary[o]['in_edges'] = dictionary[o].get('out_edges', 0) + len(ins)

def update_graph_dict(parsed, dictionary):
	ins = parsed[0] 
	outs = parsed[1]
	for i in ins: 
		check_existing_key_and_create(i, dictionary)
		dictionary[i]['out_edges'] = dictionary[i].get('out_edges', []) + outs
	for o in outs:
		check_existing_key_and_create(o,dictionary)
		dictionary[o]['in_edges'] = dictionary[o].get('in_edges', []) + ins

def main(argv):
	#Inputs
        networkfile=''

        try:
		opts, args = getopt.getopt(argv,"hn:",["networkfile="])
        except getopt.GetoptError:
		print 'PARAMETER ERROR'
		sys.exit(2)
	print opts
    	for opt, arg in opts:
		if opt == '-h':
		    print 'python obtain_network_statistics.py -n <networkfile>'
		    sys.exit()
		elif opt in ("-n", "--networkfile"):
		    networkfile = arg
		else:
		    print "Unknown option ", opt
        #Actual start of program
        
	if not networkfile:
		networkfile = "/home/alexs/AdaLab/Data/zimmer_boolean_network.txt"
	print networkfile
	
	fh_in = open(networkfile, 'r')
	connectivity_dict = {}
	graph_dict = {}
	for line in fh_in:
		line = line.rstrip("\n")
		parsed_line = parse_line(line)
		update_connectivity_dict(parsed_line, connectivity_dict)
		update_graph_dict(parsed_line, graph_dict)
	print connectivity_dict
	new_dict_in = {}
	new_dict_out = {}
	for key,val in connectivity_dict.iteritems():
		num_outs = val.get('out_edges', 0)
		num_ins = val.get('in_edges',0)
		new_dict_in[num_ins] = new_dict_in.get(num_ins, 0) + 1
		new_dict_out[num_outs] = new_dict_out.get(num_outs, 0) + 1  
	list1=[]
	list2=[]
	for key in sorted(new_dict_in.keys()):
		list1 += [key]
		list2 += [new_dict_in[key]]
	print "degrees_in =", list1
	print "amounts_in = ",list2 
	print "-----"
	list3=[]
	list4=[]
	for key in sorted(new_dict_out.keys()):
		list3 += [key]
		list4 += [new_dict_out[key]]
	print "degrees_out = ", list3
	print "amounts_out = ", list4 
#	for key,value in graph_dict.iteritems():
#		print key, value

#Octave: generate figures
#degrees_in = [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 15, 16, 17, 18, 19, 23, 27, 28, 31, 36, 37, 42, 45, 72, 95, 235];
#amounts_in = [102, 39, 73, 65, 5, 2, 8, 4, 3, 1, 3, 3, 1, 1, 1, 1, 1, 1, 2, 1, 3, 1, 1, 1, 2, 1, 1, 1]
#degrees_out = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24, 25, 27, 30, 31, 32, 33, 37, 40, 42, 46, 49, 51, 52, 53, 54, 55, 61, 63, 64, 65, 73, 76, 92, 101, 105, 116, 143, 151, 167, 195, 217, 715]
#amounts_out = [160, 17, 9, 19, 16, 12, 6, 4, 5, 4, 7, 5, 6, 2, 2, 2, 3, 1, 1, 3, 1, 1, 1, 2, 4, 2, 1, 1, 1, 2, 3, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
#figure(); scatter(degrees_in, amounts_in);legend("In edges");xlabel("degree");ylabel("amount of nodes")
#figure(); scatter(degrees_out, amounts_out);legend("out edges"); xlabel("degree"); ylabel("amount of nodes"
if __name__ == "__main__":
   main(sys.argv[1:])
