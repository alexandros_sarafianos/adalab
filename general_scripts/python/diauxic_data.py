#!/usr/bin/env python
from __future__ import division

rules=dict()
rule_types=set()
rule_id=0
index = 1
with open('/home/alexs/AdaLab/Data/xml_zimmer/diauxic_grn.xml','Ur') as infile:
    for line in infile:
	print index        
	if '<relation' in line:
            terms=line.strip('<').strip('>\n').split(' ')
            reaction_type=[term.split('=')[1].strip('"') for term in terms if 'type' in term][0]
            inputs=dict()
            outputs=dict()
            expression=dict()
        elif '<input' in line:
            terms=line.strip('<').strip(' />').split(' ')
            input_type=[term.split('=')[1].strip('"') for term in terms if 'type' in term][0]
            symbol=[term.split('=')[1].strip('"') for term in terms if 'symbol' in term][0]
            param=[term.split('=')[1].strip('"') for term in terms if 'param' in term]
            if len(param)>0:
                parameter=param[0]
            else:
                parameter=''
            if reaction_type not in ['activate','inactivate','phosphorylate','dephosphorylate','transport']:
                expression[symbol]=parameter
                inputs[symbol]=''
            else:
                expression[symbol]=''
                inputs[symbol]=parameter
        elif '<output' in line:
            terms=line.strip('<').strip(' />').split(' ')
            output_type=[term.split('=')[1].strip('"') for term in terms if 'type' in term][0]
            symbol=[term.split('=')[1].strip('"') for term in terms if 'symbol' in term][0]
            param=[term.split('=')[1].strip('"') for term in terms if 'param' in term]
            if len(param)>0:
                parameter=param[0]
            else:
                parameter=''
            if reaction_type not in ['activate','inactivate','phosphorylate','dephosphorylate','transport']:
                expression[symbol]=parameter
                outputs[symbol]=''
            else:
                expression[symbol]=''
                outputs[symbol]=parameter
        elif '</relation>' in line:
            for output in outputs:
                if outputs[output] in ['int','active']:
                    outputs[output]=''
                if output+outputs[output] not in rules:
                    rules[output+outputs[output]]=dict()
                if 'positive' not in rules[output+outputs[output]]:
                    rules[output+outputs[output]]['positive']=set()
                if 'negative' not in rules[output+outputs[output]]:
                    rules[output+outputs[output]]['negative']=set()
                if reaction_type=='inhibit':
                    #rule='('+' && '.join(['! '+input+inputs[input] for input in inputs])+')'
                    rule='&'.join([input+inputs[input] for input in inputs])
                    rules[output+outputs[output]]['negative'].add(rule)
                elif reaction_type=='catalyse':
                    #rule='('+' && '.join([input+inputs[input] for input in inputs])+')'
                    rule='&'.join([input+inputs[input] for input in inputs])
                    rules[output+outputs[output]]['positive'].add(rule)
                elif reaction_type=='phosphorylate':
                    #rule='('+' && '.join([input+inputs[input] for input in inputs])+')'
                    if outputs[output]=='':
                        outputs[output]='phosphorylated'
                    rule='&'.join([input+inputs[input] for input in inputs])
                    rules[output+outputs[output]]['positive'].add(rule)
                elif reaction_type=='dephosphorylate':
                    #rule='('+' && '.join([input+inputs[input] for input in inputs])+')'
                    rule='&'.join([input+inputs[input] for input in inputs])
                    rules[output+outputs[output]]['positive'].add(rule)
                elif reaction_type=='transport':
                    #rule='('+' && '.join([input+inputs[input] for input in inputs])+')'
                    rule='&'.join([input+inputs[input] for input in inputs])
                    rules[output+outputs[output]]['positive'].add(rule)
                elif reaction_type=='activate':
                    #rule='('+' && '.join([input+inputs[input] for input in inputs])+')'
                    rule='&'.join([input+inputs[input] for input in inputs])
                    rules[output+outputs[output]]['positive'].add(rule)
                elif reaction_type=='inactivate':
                    #rule='('+' && '.join([input+inputs[input] for input in inputs])+')'
                    rule='&'.join([input+inputs[input] for input in inputs])
                    rules[output+outputs[output]]['positive'].add(rule)
                elif reaction_type=='stimulate':
                    #rule='('+' && '.join([input+inputs[input] for input in inputs])+')'
                    rule='&'.join([input+inputs[input] for input in inputs])
                    rules[output+outputs[output]]['positive'].add(rule)
                elif reaction_type=='expression':
                    if len(set(inputs.values()).intersection(set(['ko',])))==0:
                        if 'down' in expression[output]:
                            #rule='('+' && '.join(['! '+input+inputs[input] if expression[input] not in ['absent','low'] else input+inputs[input]  for input in inputs])+')'
                            rule='&'.join(['^'+input+inputs[input] if expression[input] in ['absent','low'] else input+inputs[input]  for input in inputs])
                            rules[output]['negative'].add(rule)
                        elif 'up' in expression[output]:
                            #rule='('+' && '.join([input+inputs[input] if expression[input] not in ['absent','low'] else '! '+input+inputs[input] for input in inputs])+')'
                            rule='&'.join([input+inputs[input] if expression[input] not in ['absent','low'] else '^'+input+inputs[input] for input in inputs])
                            rules[output]['positive'].add(rule)

            for input in inputs:
                if inputs[input] in ['int','active']:
                    inputs[input]=''
                if input+inputs[input] not in rules:
                    rules[input+inputs[input]]=dict()
                if 'positive' not in rules[input+inputs[input]]:
                    rules[input+inputs[input]]['positive']=set()
                if 'negative' not in rules[input+inputs[input]]:
                    rules[input+inputs[input]]['negative']=set()
                if reaction_type in ['transport','phosphorylate','activate','inactivate']:
                    #rule='('+' && '.join([inp+inputs[inp] if inp in outputs else '! '+inp+inputs[inp] for inp in inputs])+')'
                    if input not in outputs:
                        continue
                    rule='&'.join([inp+inputs[inp] if inp in outputs else inp+inputs[inp] for inp in inputs])
                    rules[input+inputs[input]]['negative'].add(rule)

for entity in rules:
    for rule in rules[entity]['positive']:
        temp=rule+' -> '+entity
        temp.replace('/','_')
        temp.replace(',','_')
        print temp
    for rule in rules[entity]['negative']:
        temp=rule+' -| '+entity
        temp.replace('/','_')
        temp.replace(',','_')
        print temp

