from simulator_experiment_functions import *
import subprocess
import re
import sys, getopt

# List from zimmer model, bottom right corner, cfr martin

genes = "HXT,HXK1,HXK2,SNF1,SAK1,ELM1,TOS3,REG1,GCL7,SNF4,SNF1,GAL83,MIG1,ADR1,CAT8"
genes = genes.split(",")


def main(argv):
    # Inputs
    workpath = ''
    networkfile = ''
    ratio = ""
    type = ""
    colonysize = 10
    result_directory=""
    try:
        opts, args = getopt.getopt(argv, "hw:n:c:t:r:d:", ["workpath=", "networkfile=", "colony-size=", "type=", "ratio=", "directory="])
    except getopt.GetoptError:
        print 'PARAMETER ERROR'
        sys.exit(2)
    print opts
    for opt, arg in opts:
        if opt == '-h':
            print 'python additional_simulator_experiments.py -w <workpath> -n <networkfile> -t <type>'
            sys.exit()
        elif opt in ("-w", "--workpath"):
            workpath = arg
        elif opt in ("-n", "--networkfile"):
            networkfile = arg
        elif opt in ("-c", "--colony-size"):
            colonysize = str(arg)
        elif opt in ("-t", "--type"):
            type = str(arg)
        elif opt in ("-r", "--ratio"):
            ratio = str(arg)
        elif opt in ("-d", "--directory"):
            result_directory = str(arg)
        else:
            print "Unknown option ", opt
        if not networkfile:
            networkfile = workpath + "/AdaLab/Data/zimmer_boolean_network.txt"
        if not result_directory:
            result_directory = workpath+ "/tmp/"
    executable = workpath + "/AdaLab/build/src/simulator"
    experiment_file = workpath + "/AdaLab/Data/additional_experiments.xml"
    command = executable + \
              " -e " + experiment_file + \
              " --colony-size " + str(colonysize) + \
              " --save-output 1 " \
              "--result-directory "+result_directory+ \
              " --network-file " + networkfile + \
              " --initialization-type " + type + \
              " --ratio " + ratio
    print command
    experiment_dict = {}
    for gene in genes:
        create_experiment_file(gene, experiment_file)
        output_from_simulator = check_for_ds_and_ethanol(command)
        assert isinstance(output_from_simulator, tuple)
        experiment_dict[gene] = output_from_simulator
    print experiment_dict


if __name__ == "__main__":
    main(sys.argv[1:])
