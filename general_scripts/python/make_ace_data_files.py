
import sys, getopt, os

type_treshold_map = { (0.01,0.99) : 1,(0.05,0.95):2,(0.1,0.9):3,(0.15,0.85):4,(0.2,0.8):5,(0.1,0.8):6}
def create_dir(dirname):
	if not os.path.exists(dirname):
		os.mkdir(dirname)

def filter_string(string):
  return ''.join([c for c in string if( ord(c) >= 97 and ord(c) <= 122 ) or ord(c) == 95 or ord(c) == 58 or (ord(c) >= 48 and ord(c) <=57) or (ord(c) >=65 and ord(c) <= 90) ])

def create_prolog_line(functor, list_of_elements):
	funct_string = functor
	element_string = str(list_of_elements[0])
	for element in list_of_elements[1:]:
		element_string += ", "+str(element)
	return funct_string+"("+element_string+").\n"

def main(argv):
    #read in command line arguments
    ifile ='' 
    ofile=''
    thresh_type = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:t:p:",["ifile=",  "ofile=", "type=","pred-type="])
    except getopt.GetoptError:
        print 'PARAMETER ERROR'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'python make_ace_data_files.py -i <inputfile> -o <outputfile> -t <type> -p <prediction_type>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            ifile = arg
	elif opt in ("-o", "--ofile"):
	    ofile = arg
	elif opt in ("-t", "--type"):
	    thresh_type = int(arg)
	elif opt in ("-p", "--pred-type"):
	    pred_type = arg
	else:
	    print "Unknown option ", opt
    #Actual start of program
    if not ifile or not ofile:
        print 'PARAMETER ERROR'
        sys.exit(2)
    if pred_type != "regression" and pred_type != "classification":
	print "Unknown option for prediction type:", pred_type
	sys.exit(2)


    f = ''
    if pred_type == "classification":
	f = lambda x: str(bool(x))
    elif pred_type == "regression":
	f = lambda x: str(x)
	
    ##variables related to columns of file
    GO_molecular_function = 8 
    EC_number = 7
    GO_biological_process = 9
    GO_cellular_component = 10
    diauxic_chisq = 11
    diauxic_fischer = 12
    example_name_col = 2
    seqThrld1_col = 0
    seqThrld2_col = 1
    uniprot_id = 4
    description = 5 
    S_ontology_term = 13
    dna_seq = 14
    diauxic_rate_MT = 15
    chromosome = 16
    internal_column_delimiter = ";"
    feature_cols = [GO_molecular_function, GO_biological_process, GO_cellular_component, dna_seq, chromosome]


    fh_in = open(ifile, 'r')
    create_dir(ofile)
    fh_kb_out = open(ofile+'/'+ofile+'.kb', 'w')
    fh_s_out = open(ofile+'/'+ofile+'.s', 'w')
    headers = fh_in.readline()
    headers = headers.split('\t')
    for line in fh_in:
	splitted_line = line.split('\t')
	if type_treshold_map[(float(splitted_line[seqThrld1_col]),float(splitted_line[seqThrld2_col]))] == thresh_type:
		#fh_kb_out.write(create_prolog_line("experiment",[splitted_line[example_name_col], splitted_line[diauxic_chisq]]).lower())
		fh_kb_out.write(create_prolog_line("experiment",[splitted_line[example_name_col], f(float(splitted_line[diauxic_rate_MT]))] ))
		for col in feature_cols:
			if col == dna_seq: #dna_seq
				print "dna_seq", splitted_line[col]
				pass			
			elif col == chromosome:
				print "chrom",splitted_line[col]
				pass
			else:	
				further_split = splitted_line[col].split(internal_column_delimiter)
				for f_sp in further_split:
					try:
						fh_kb_out.write(create_prolog_line(headers[col]+"_"+filter_string(f_sp.split(':')[0] ), [splitted_line[example_name_col], "go"+filter_string(f_sp.split(':')[2])]).lower())
					except IndexError:
						print "soft error"
    #print headers
    fh_kb_out.close()
    fh_s_out.write("predict(experiment(+id,-diauxicshift)).\n")
#    fh_s_out.write("classes([true,false]).\n")

if __name__ == "__main__":
   main(sys.argv[1:])
