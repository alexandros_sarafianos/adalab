
import subprocess
import re

import sys, getopt

my_re =  re.compile(r'Diauxic_shift=(\d\.*\d*)')
my_re2 =  re.compile(r'Ethanol=(\d\.*\d*)')


def create_experiment_file(gene, exp_file):
    fh_exp = open(exp_file, 'w')
    fh_exp.write('<?xml version="1.0" encoding="UTF-8"?>')
    fh_exp.write('<experiment>')
    fh_exp.write('<genes>')
    if gene != "":
        fh_exp.write('<gene id= "'+gene+'" value = "0" fixed = "1"/>')
    fh_exp.write('</genes>')
    fh_exp.write('<metabolites>')
    fh_exp.write('<metabolite id= "glucoseext" value = "1" fixed = "0"/>')
    fh_exp.write('<metabolite id= "nitrogen" value = "1" fixed = "0"/>')
    fh_exp.write('<metabolite id= "O2" value = "1" fixed = "1"/>')
    fh_exp.write('</metabolites>')
    fh_exp.write('<drug>')
    fh_exp.write('</drug>')
    fh_exp.write('<condition>')
    fh_exp.write('</condition>')
    fh_exp.write('</experiment>')
    fh_exp.close()

def run_command(command):
    p = subprocess.Popen(command,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT,
                         shell=True )
    return iter(p.stdout.readline, b'')

def return_type(th1, th2):
	if th1 == "0.01" and th2 == "0.99":
		return 1
	elif th1 == "0.05" and th2 == "0.95":
		return 2
	elif th1 == "0.1" and th2 == "0.9":
		return 3
	elif th1 == "0.15" and th2 == "0.85":
		return 4
	elif th1 == "0.2" and th2 == "0.8":
		return 5
	elif th1 == "0.1" and th2 == "0.8":
		return 6
	else:
		return "type"+th1+th2

def check_for_ds_and_ethanol(command):
	ethanol =""
	ds = ""
	for line_o in run_command(command):
		re_eth = my_re2.search(line_o)
		re_ds = my_re.search(line_o)
		if re_eth:
			ethanol = re_eth.groups()[0] 
		elif re_ds:
			ds = re_ds.groups()[0]
	return (ds,ethanol)
