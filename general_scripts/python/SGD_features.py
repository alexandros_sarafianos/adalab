path = "/home/alexs/adalab_personal/Data/SGD/"
files = ["SGD_features.tab","phenotype_data.tab","interaction_data.tab","protein_properties.tab"]
cols = [3,0,0,0]
import sys, getopt


def create_gene_feature_dict_from_file(ffile, primary_key_column, delim):
	fh = open(ffile, 'r')
	f_dict = {}
	column_headers = fh.readline()
	column_headers = column_headers.rstrip().split(delim)
	for line in fh:
		splitted = line.rstrip().split(delim)
		index = 0
		sub_dict = {}
		if len(splitted) != len(column_headers):
			print "ERROR ("+ffile+"): ",len(splitted),len(column_headers), splitted, "\n" 
			if len(splitted) == 0:
			    continue 
		while index < len(column_headers):
			if index != primary_key_column:
				try:
				   sub_dict[column_headers[index]] = splitted[index]
				except IndexError:
				   print "Indexerror", splitted, index
			index+=1
		try: 
		   f_dict[splitted[primary_key_column]] = sub_dict
		except IndexError:
		  print "Indexerror",splitted,	primary_key_column

	return f_dict
'''
print create_gene_feature_dict_from_file(path+files[1], 0, "	")
print create_gene_feature_dict_from_file(path+files[2], 0, "	")
print create_gene_feature_dict_from_file(path+files[3], 0, "	")
'''
def main(argv):
    #read in command line arguments
    ifile ='' 
    ofile=''

    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=",  "ofile="])
    except getopt.GetoptError:
        print 'PARAMETER ERROR'
        sys.exit(2)
    print opts
    for opt, arg in opts:
        if opt == '-h':
            print 'python SGD_features.py -i <inputfile> -o <outputfile>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            ifile = arg
	elif opt in ("-o", "--ofile"):
	    ofile = arg
	else:
	    print "Unknown option ", opt
    #Actual start of program
    if not ifile or not ofile:
        print 'PARAMETER ERROR'
        sys.exit()

    #fh_in = open(ifile, 'r')
    #fh_out = open(ofile, 'w')
    dict_list = []
    for index in range(0,4):
	dict_list.append(create_gene_feature_dict_from_file(path+files[index],cols[index], "\t"))
    print dict_list
    sys.exit()
    for line in fh_in:
        line= line.rstrip()
	splitted =  line.split(",")
	if int(splitted[0]) == int(d_type):
		gene_name = splitted[3]
		gene_id = feat_dict.get(gene_name, {}).get('gene_id', None)
		gene_sequence = fasta_dict.get(gene_id, {}).get('sequence', None)
		gene_loc = fasta_dict.get(gene_id, {}).get('gene_loc', None)
		if gene_id != None and gene_sequence !=None and gene_loc != None:
			fh_out.write(d_type+","+gene_id+","+gene_name+","+gene_sequence+","+gene_loc+","+splitted[4]+","\
			+splitted[6]+","+ splitted[7]+","+ splitted[8]+"\n")
    fh_in.close()
    fh_out.close()


if __name__ == "__main__":
   main(sys.argv[1:])

