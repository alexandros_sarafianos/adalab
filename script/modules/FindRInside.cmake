# - find RINSIDE

# input: hints for FindRINSIDE
# RINSIDE_INCLUDEDIR - where the RINSIDE include files can be located
# RINSIDE_LIBRARYDIR - where the RINSIDE library files can be located
# RINSIDE_ROOT       - root directory, RINSIDE_ROOT/include is the include directory, RINSIDE_ROOT/lib is the library directory


# output
# RINSIDE_INCLUDE_DIR - Where to find RINSIDE header files (directory)
# RINSIDE_LIBRARY - RINSIDE library
# RINSIDE_FOUND - Set to TRUE if we found everything (library, includes and executable)

IF( RINSIDE_INCLUDE_DIR AND RINSIDE_LIBRARY )
    SET( RINSIDE_FIND_QUIETLY TRUE )
ENDIF( RINSIDE_INCLUDE_DIR AND RINSIDE_LIBRARY )

FIND_PATH( RINSIDE_INCLUDE_DIR
        NAMES			RInside.h
        HINTS	 		${RINSIDE_INCLUDEDIR} ${RINSIDE_ROOT}/include 
	PATHS 			/usr/include /usr/local/include $ENV{HOME}/R/x86_64-pc-linux-gnu-library/3.0/RInside/include/
)

FIND_LIBRARY( RINSIDE_LIBRARY
        NAMES         		libRInside.so libRInside.dylib libRInside.dll
        HINTS         		${RINSIDE_LIBRARYDIR} ${RINSIDE_ROOT}/lib ${PROJECT_SOURCE_DIR}/lib
        PATH			/usr/local/lib /usr/lib $ENV{HOME}/R/x86_64-pc-linux-gnu-library/3.0/RInside/lib/
)

IF(RINSIDE_INCLUDE_DIR AND RINSIDE_LIBRARY)
  	SET(RINSIDE_FOUND TRUE)
	message("RINSIDE found header at ${RINSIDE_INCLUDE_DIR} and library at ${RINSIDE_LIBRARY}")
ELSE()
  	SET(RINSIDE_FOUND False)
	message("RINSIDE not found")
ENDIF(RINSIDE_INCLUDE_DIR AND RINSIDE_LIBRARY)

MARK_AS_ADVANCED( RINSIDE_INCLUDE_DIR RINSIDE_LIBRARY )

