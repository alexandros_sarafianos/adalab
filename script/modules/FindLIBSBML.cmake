# - find LIBSBML

# input: hints for FindLIBSBML
# LIBSBML_INCLUDEDIR - where the LIBSBML include files can be located
# LIBSBML_LIBRARYDIR - where the LIBSBML library files can be located
# LIBSBML_ROOT       - root directory, LIBSBML_ROOT/include is the include directory, LIBSBML_ROOT/lib is the library directory


# output
# LIBSBML_INCLUDE_DIR - Where to find LIBSBML header files (directory)
# LIBSBML_LIBRARY - LIBSBML library
# LIBSBML_FOUND - Set to TRUE if we found everything (library, includes and executable)
# Written by Thomas Fannes, 2012  <thomas.fannes@cs.kuleuven.be >

IF( LIBSBML_INCLUDE_DIR AND LIBSBML_LIBRARY )
    SET( LIBSBML_FIND_QUIETLY TRUE )
ENDIF( LIBSBML_INCLUDE_DIR AND LIBSBML_LIBRARY )

FIND_PATH( LIBSBML_INCLUDE_DIR
        NAMES			sbml
        HINTS	 		${LIBSBML_INCLUDEDIR} ${LIBSBML_ROOT}/include ${PROJECT_SOURCE_DIR}/include
	PATHS 			/usr/include /usr/local/include ${PROJECT_SOURCE_DIR}/include
)

FIND_LIBRARY( LIBSBML_LIBRARY
        NAMES         		sbml
        HINTS         		${LIBSBML_LIBRARYDIR} ${LIBSBML_ROOT}/lib ${PROJECT_SOURCE_DIR}/lib
        PATH			/usr/local/lib /usr/lib ${PROJECT_SOURCE_DIR}/lib
)

IF(LIBSBML_INCLUDE_DIR AND LIBSBML_LIBRARY)
  	SET(LIBSBML_FOUND TRUE)
	message("LIBSBML found header at ${LIBSBML_INCLUDE_DIR} and library at ${LIBSBML_LIBRARY}")
ELSE()
  	SET(LIBSBML_FOUND False)
	message("LIBSBML not found")
ENDIF(LIBSBML_INCLUDE_DIR AND LIBSBML_LIBRARY)

MARK_AS_ADVANCED( LIBSBML_INCLUDE_DIR LIBSBML_LIBRARY )

