# Finds the mips release library
#
# There are three ways how to use the mips_release library:
# 1) mips_release is installed:
#      - MIPS_INSTALLED_ROOT: Specifies the root directory, where ${MIPS_ROOT}/include is the include directory and ${MIPS_ROOT}/lib is the library directory
#      - MIPS_INSTALLED_LIBRARYDIR: The directory where the mips library can be located
#      - MIPS_INSTALLED_INCLUDEDIR: The directory where the mips include files can be located
#      - MIPS_INSTALLED_SKIP: If this flag is set, cmake will not look for the installed library, but rather check for option 2 or 3. Note that, even if none of the MIPS_INSTALL* flags are set,
#        cmake will still look in the default directories (.e.g, /usr/local) for an installed mips library. The skip flag can be used when one wants to explictly skip the automatic detection of an
#        installed mips library.
#
# 2) there is a local clone of the mips_release repository:
#      - MIPS_LOCAL_REPO: The directory where the local repository can be found
#      - MIPS_LOCAL_SKIP: If this flag is set, cmake will not look for a local repository, but rather go for option 3. Note that not setting MIPS_LOCAL_REPO will have the same result has setting 
#        MIPS_LOCAL_SKIP
#
# 3) mips_release should be obtained through a central repository:
#      - MIPS_GIT_REPO: The URL where the git repository can be found, if not set, https://mips-build.cs.kuleuven.be/gerrit/mips is used.
#      - MIPS_GIT_REFSPEC: The ref spec to checkout, by default origin/release is used.
#
# After executing the script, the following targets and variables are set:
#
#  - MIPS_RELEASE_FOUND        - set to TRUE if cmake found everything
#  - MIPS_RELEASE_INCLUDE_DIR  - Where to find the MIPS header files (include directory)
#  - MIPS_RELEASE_LIBRARY      - A direct target to the mips library
#  - MIPS_RELEASE_RELEASE      - A target on which your library should dependent on.

# Written by Thomas Fannes, 2014  <thomas.fannes@cs.kuleuven.be >

macro(APPEND_OPTION LISTVAR NAME)
 if(${NAME})
    set(INNAME "${${NAME}}")
    set(VAR "-D${NAME}=${${NAME}}")
    set(${LISTVAR} ${${LISTVAR}} ${VAR})
 endif()
endmacro()


IF( MIPS_RELEASE_FOUND AND MIPS_RELEASE_INCLUDE_DIR AND MIPS_RELEASE_LIBRARY )
    SET( MIPS_RELEASE_FIND_QUIETLY TRUE )
ENDIF()


# check for an installed mips version
IF (NOT MIPS_INSTALLED_SKIP AND NOT MIPS_RELEASE_FIND_QUIETLY AND NOT MIPS_RELEASE_FOUND)
  FIND_PATH(MIPS_RELEASE_INCLUDE_DIR
                        NAMES			mips/mips.hpp
			HINTS	 		${MIPS_INSTALLED_INCLUDEDIR} ${MIPS_INSTALLED_ROOT}/include
			PATHS 			/usr/include /usr/local/include
			)

    FIND_LIBRARY( MIPS_RELEASE_LIBRARY
			NAMES         		mips
			HINTS         		${MIPS_INSTALLED_LIBRARYDIR} ${MIPS_INSTALLED_ROOT}/lib
			PATH			/usr/local/lib
			)

    IF(MIPS_RELEASE_INCLUDE_DIR AND MIPS_RELEASE_LIBRARY)
        SET(MIPS_RELEASE_FOUND TRUE)
        message("Installed mips found header at ${MIPS_RELEASE_INCLUDE_DIR}")
        mark_as_advanced( MIPS_INCLUDE_DIR MIPS_LIBRARY MIPS_FOUND)
        add_custom_target(MIPS_RELEASE COMMENT "mips_release (INSTALLED_VERSION)")
    ELSE()
        IF(MIPS_INSTALLED_ROOT OR MIPS_INSTALLED_INCLUDEDIR OR MIPS_INSTALLED_LIBRARYDIR)
            message(FATAL_ERROR "Unable to find installed mips library")
        ENDIF()
    ENDIF()
ENDIF()

IF(NOT MIPS_RELEASE_FOUND)
    include(${CMAKE_CURRENT_LIST_DIR}/ExternalProject.cmake)

    # set the right flags for building mips
    set(MIPS_ROOT ${CMAKE_BINARY_DIR}/mips_release/local)
    set(MIPS_RELEASE_CMAKE_ARGS "-DCMAKE_INSTALL_PREFIX:PATH=${MIPS_ROOT}")
    APPEND_OPTION(MIPS_RELEASE_CMAKE_ARGS BOOST_ROOT)
    APPEND_OPTION(MIPS_RELEASE_CMAKE_ARGS BOOST_INCLUDEDIR)
    APPEND_OPTION(MIPS_RELEASE_CMAKE_ARGS BOOST_LIBRARYDIR)
    APPEND_OPTION(MIPS_RELEASE_CMAKE_ARGS CMAKE_BUILD_TYPE)

    # check for a local repository
    IF(NOT MIPS_LOCAL_SKIP AND MIPS_LOCAL_REPO)
	ExternalProject_Add(MIPS_RELEASE
		SOURCE_DIR "${MIPS_LOCAL_REPO}"
		CMAKE_ARGS "${MIPS_RELEASE_CMAKE_ARGS}"
		)
		
        set(MIPS_RELEASE_INCLUDE_DIR ${MIPS_ROOT}/include)
        set(MIPS_RELEASE_LIBRARY ${MIPS_ROOT}/lib/libmips.so)
        SET(MIPS_RELEASE_FOUND TRUE)
	message("local mips repository found at ${MIPS_LOCAL_REPO}")
        mark_as_advanced( MIPS_RELEASE_INCLUDE_DIR MIPS_RELEASE_LIBRARY MIPS_RELEASE_FOUND)

    # download central repository
    ELSE()
        IF(NOT MIPS_GIT_REPO)
            set(MIPS_GIT_REPO "https://mips-build.cs.kuleuven.be/gerrit/mips")
        ENDIF()
	
        IF(NOT MIPS_GIT_REFSPEC)
            set(MIPS_GIT_REFSPEC "origin/release")
        ENDIF()
		
        ExternalProject_Add(MIPS_RELEASE
			GIT_REPOSITORY "${MIPS_GIT_REPO}"
			GIT_TAG "${MIPS_GIT_REFSPEC}"
			CMAKE_ARGS "${MIPS_RELEASE_CMAKE_ARGS}"
			)
	
        set(MIPS_RELEASE_INCLUDE_DIR ${MIPS_ROOT}/include)
        set(MIPS_RELEASE_LIBRARY ${MIPS_ROOT}/lib/libmips.so)
        SET(MIPS_RELEASE_FOUND TRUE)
        message("central mips repository from  ${MIPS_GIT_REPO}")
        mark_as_advanced( MIPS_RELEASE_INCLUDE_DIR MIPS_RELEASE_LIBRARY MIPS_RELEASE_FOUND)
    ENDIF()
ENDIF()

