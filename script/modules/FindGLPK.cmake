# - find GLPK

# input: hints for FindGLPK
# GLPK_INCLUDEDIR - where the GLPK include files can be located
# GLPK_LIBRARYDIR - where the GLPK library files can be located
# GLPK_ROOT       - root directory, GLPK_ROOT/include is the include directory, GLPK_ROOT/lib is the library directory


# output
# GLPK_INCLUDE_DIR - Where to find GLPK header files (directory)
# GLPK_LIBRARY - GLPK library
# GLPK_FOUND - Set to TRUE if we found everything (library, includes and executable)

IF( GLPK_INCLUDE_DIR AND GLPK_LIBRARY )
    SET( GPLK_FIND_QUIETLY TRUE )
ENDIF( GLPK_INCLUDE_DIR AND GLPK_LIBRARY )

FIND_PATH( GLPK_INCLUDE_DIR
        NAMES			glpk.h
        HINTS	 		${GLPK_INCLUDEDIR} ${GLPK_ROOT}/include
	PATHS 			/usr/include /usr/local/include
)

FIND_LIBRARY( GLPK_LIBRARY
        NAMES         		glpk
        HINTS         		${GLPK_LIBRARYDIR} ${GLPK_ROOT}/lib
        PATH			/usr/local/lib /usr/lib
)

IF(GLPK_INCLUDE_DIR AND GLPK_LIBRARY)
  	SET(GLPK_FOUND TRUE)
	message("GLPK found header at ${GLP_INCLUDE_DIR} and library at ${GLPK_LIBRARY}")
ELSE()
  	SET(GLPK_FOUND False)
	message("GLPK not found")
ENDIF(GLPK_INCLUDE_DIR AND GLPK_LIBRARY)

MARK_AS_ADVANCED( GLPK_INCLUDE_DIR GLPK_LIBRARY )

