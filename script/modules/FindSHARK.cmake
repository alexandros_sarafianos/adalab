# - find SHARK

# input: hints for FindSHARK
# SHARK_INCLUDEDIR - where the SHARK include files can be located
# SHARK_LIBRARYDIR - where the SHARK library files can be located
# SHARK_ROOT       - root directory, SHARK_ROOT/include is the include directory, SHARK_ROOT/lib is the library directory


# output
# SHARK_INCLUDE_DIR - Where to find SHARK header files (directory)
# SHARK_LIBRARY - SHARK library
# SHARK_FOUND - Set to TRUE if we found everything (library, includes and executable)

IF( SHARK_INCLUDE_DIR AND SHARK_LIBRARY )
    SET( SHARK_FIND_QUIETLY TRUE )
ENDIF( SHARK_INCLUDE_DIR AND SHARK_LIBRARY )
message("$ENV{HOME}/libraries/include/")
FIND_PATH( SHARK_INCLUDE_DIR
        NAMES			shark	
        HINTS	 		${SHARK_INCLUDEDIR} ${SHARK_ROOT}/include
	PATHS 			/usr/include /usr/local/include $ENV{HOME}/libraries/include/
)

FIND_LIBRARY( SHARK_LIBRARY
        NAMES         		libshark.so
        HINTS         		${SHARK_LIBRARYDIR} ${SHARK_ROOT}/lib
        PATH			/usr/local/lib /usr/lib $ENV{HOME}/libraries/lib
)

message("${SHARK_INCLUDE_DIR}  and ${SHARK_LIBRARY} ")
IF(SHARK_INCLUDE_DIR AND SHARK_LIBRARY)
  	SET(SHARK_FOUND TRUE)
	message("SHARK found header at ${SHARK_INCLUDE_DIR} and library at ${SHARK_LIBRARY}")
ELSE()
  	SET(SHARK_FOUND False)
	message("SHARK not found")
ENDIF(SHARK_INCLUDE_DIR AND SHARK_LIBRARY)

MARK_AS_ADVANCED( SHARK_INCLUDE_DIR SHARK_LIBRARY )

