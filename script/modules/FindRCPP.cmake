# - find RCPP

# input: hints for FindRCPP
# RCPP_INCLUDEDIR - where the RCPP include files can be located
# RCPP_LIBRARYDIR - where the RCPP library files can be located
# RCPP_ROOT       - root directory, RCPP_ROOT/include is the include directory, RCPP_ROOT/lib is the library directory


# output
# RCPP_INCLUDE_DIR - Where to find RCPP header files (directory)
# RCPP_LIBRARY - RCPP library
# RCPP_FOUND - Set to TRUE if we found everything (library, includes and executable)

IF( RCPP_INCLUDE_DIR AND RCPP_LIBRARY )
    SET( RCPP_FIND_QUIETLY TRUE )
ENDIF( RCPP_INCLUDE_DIR AND RCPP_LIBRARY )

FIND_PATH( RCPP_INCLUDE_DIR
        NAMES			Rcpp.h
        HINTS	 		${RCPP_INCLUDEDIR} ${RCPP_ROOT}/include 
	PATHS 			/usr/include /usr/local/include $ENV{HOME}/R/x86_64-pc-linux-gnu-library/3.0/Rcpp/include/
)

FIND_LIBRARY( RCPP_LIBRARY
        NAMES         		Rcpp.so Rcpp.dylib Rcpp.dll
        HINTS         		${RCPP_LIBRARYDIR} ${RCPP_ROOT}/lib ${PROJECT_SOURCE_DIR}/lib
        PATH			/usr/local/lib /usr/lib $ENV{HOME}/R/x86_64-pc-linux-gnu-library/3.0/Rcpp/libs/
)
message("$ENV{HOME}/R/x86_64-pc-linux-gnu-library/3.0/Rcpp/libs/")
message("$ENV{HOME}/R/x86_64-pc-linux-gnu-library/3.0/Rcpp/include/")
message(" ${RCPP_INCLUDE_DIR} and ${RCPP_LIBRARY}" ) 
IF(RCPP_INCLUDE_DIR AND RCPP_LIBRARY)
  	SET(RCPP_FOUND TRUE)
	message("RCPP found header at ${RCPP_INCLUDE_DIR} and library at ${RCPP_LIBRARY}")
ELSE()
  	SET(RCPP_FOUND False)
	message("RCPP not found")
ENDIF(RCPP_INCLUDE_DIR AND RCPP_LIBRARY)

MARK_AS_ADVANCED( RCPP_INCLUDE_DIR RCPP_LIBRARY )

IF(RCPP_INCLUDE_DIR)
  	SET(RCPP_FOUND TRUE)
	message("RCPP found header at ${RCPP_INCLUDE_DIR}")
ELSE()
  	SET(RCPP_FOUND False)
	message("RCPP not found")
ENDIF(RCPP_INCLUDE_DIR)

#MARK_AS_ADVANCED( RCPP_INCLUDE_DIR RCPP_LIBRARY )

