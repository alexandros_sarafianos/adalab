The descriptive statistics for isoenzyme experimental data 

Two spreadsheets were used to report the descriptive statistics for the isoenzyme experiment data: isoDescStats_norm1.xls and isoPgDescStats_norm1.xls for growth in minimal median and the pregrowth in rich media, respectively. 

Data show in the spread sheet have been normalized to the median of wildtype for each plate. That is, all curve parameters have been normalized by subtracting the median parameter of the wildtype in the same plate. 

For each plate: ParameterNormalized = ParameterRaw - medianParameterOfWildTypeOfThePlate 
Negative value in parameters means smaller value compared to the wildtype. 

Four curve parameters have been reported in the table including
linear_slope, global_max_od, miy_lag_time and double_time.

There are in total 191 strains (190 mutants + wildtype). For each mutant, there are 12 to 48 replicates acrossing 1-3 different plates. Wildtype strain have intotal over 1000 replcates as it was used as the control across different plates. 

For each parameter, the descriptive statitics have been listed including:
'median', 'mad', 'mean', 'sd'.

Then four different statistical tests have been performed to check if the difference between the strain with the wild type for that parameter is significant or not. The four p-values resulting from the tests are listed:
1. 'pr_ttest_1sample': 
one sample t-test to check if the normalized parameters are significantly different from zero or not.

2. 'pr_wilcox_1sample': 
one sample wilcoxon rank sum test (non-parametric method) to check if the normalized parameters are significantly different from zero or not.

3. 'pr_ttest_2sample': 
Two sample t-test to check if the normalized parameters are significantly different from zero or not. The contrast sample is the normalized data of wildtype in the same plates. 

4. 'pr_wilcox_2sample':
Two sample wilcoxon rank sum test (non-parametric method) to check if the normalized parameters are significantly different from zero or not. The contrast sample is the normalized data of wildtype in the same plates. 

Note: the p-values have not been adjusted for multiple-comparisons. Thus the siglificant level should be lower than 1-comparison test. E.g. Using the bonferroni correction, significance level of 0.05 should be 0.05/n for multiple comparison, where n is the number of simutaneus tests.
