#!/usr/bin/python
'''

'''

import sys, getopt
 

 
def filter_non_printable(string):
  return ''.join([c for c in string if ord(c) > 31 or ord(c) == 9])
 
def main(argv):
    #read in command line arguments
    inputfilename = ''
    delimiter = "\t"
    outputfilename = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:d:",["ifile=", "ofile=", "delim="])
    except getopt.GetoptError:
        print 'diauxic_shift_data_processor.py -i <inputfile> -o <outputfile> -d <delimiter'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'diauxic_shift_data_processor.py -i <inputfile> -o <outputfile> -d <delimiter'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfilename = arg
        elif opt in ("-o", "--ofile"):
            outputfilename = arg
	elif opt in ("-d", "--delim"):
	    delimiter = arg
    print 'Input file: ', inputfilename, ' Output file: ', outputfilename, ' Delimiter: ', delimiter

    #Actual start of program
    fh_in = open(inputfilename, 'r')
    fh_out = open(outputfilename, 'w')
    for line in fh_in:
	print line
'''
    import re
    for line in fh_in:
      splittedLine = line.split(delimiter)
      seq = ''
      newline = delimiter.join(splittedLine)
      fh_out.write(newline)
    fh_in.close()
    fh_out.close()
'''
 
if __name__ == "__main__":
   main(sys.argv[1:])

