
import csv
fn = "diauxiTest_iso1-characteristics1.csv"


def clean_print_dict( my_dictionary ):
	for key, value in my_dictionary.iteritems():
		if key != "":
			print key, value/6


fh_in = open(fn, 'r')
reader = csv.reader( fh_in )
headers = reader.next()
protein_dict = {}
ec_dict = {}
molec_funct_dict = {}
biological_proc_dict = {}
cell_comp_dict = {}
index = 0

for line in reader:
	index += 1
	protein_dict[line[4]] = protein_dict.get(line[4], 0) + 1 
	ec_dict[line[7]] = ec_dict.get(line[7], 0)+1
	splitted = line[8].split(";")
	for entry in splitted:
		molec_funct_dict[entry] = molec_funct_dict.get(entry, 0) +1	
	splitted2 = line[9].split(";")
	for entry2 in splitted2:
		biological_proc_dict[entry2] = biological_proc_dict.get(entry2,0 ) +1
	splitted3 = line[10].split(";")
	for entry3 in splitted3:
		cell_comp_dict[entry3] = cell_comp_dict.get(entry3,0 ) +1

print "protein_dict", clean_print_dict(protein_dict)
print "size",len(protein_dict)
print "------------------------------------"
print "ec_dict", clean_print_dict(ec_dict)
print "size", len(ec_dict)
print "------------------------------------"
print "molec_funct_dict", clean_print_dict(molec_funct_dict)
print "size", len(molec_funct_dict)
print "------------------------------------"
print "biological_proc_dict", clean_print_dict(biological_proc_dict)
print "size", len(biological_proc_dict)
print "------------------------------------"
print "cell_comp_dict", clean_print_dict(cell_comp_dict)
print "size", len(cell_comp_dict)
print "------------------------------------"
print "SIZE:",index
