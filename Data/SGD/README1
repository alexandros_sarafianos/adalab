Date 30/06/2015
****************
SGD_features.tab
****************
The latest version of the SGD_features.tab file is based on Genome Version R64-2-1.

The SGD_features.tab file is updated weekly (Saturday).

NOTE: On 4 September 2004, the SGD_features.tab file replaced the previously
used chromosomal_feature.tab file.

File contents:

1. Information on current chromosomal features in SGD, including Dubious ORFs. 
Also contains coordinates of intron, exons, and other subfeatures that are located
within a chromosomal feature.

2. The relationship between subfeatures and the feature in which they
are located is identified by the feature name in column #7 (parent
feature). For example, the parent feature of the intron found in
ACT1/YFL039C will be YFL039C. The parent feature of YFL039C is
chromosome 6.

3. The coordinates of all features are in chromosomal coordinates.


Columns within SGD_features.tab:

1.   Primary SGDID (mandatory)
2.   Feature type (mandatory)
3.   Feature qualifier (optional)
4.   Feature name (optional)
5.   Standard gene name (optional)
6.   Alias (optional, multiples separated by |)
7.   Parent feature name (optional)
8.   Secondary SGDID (optional, multiples separated by |)
9.   Chromosome (optional)
10.  Start_coordinate (optional)
11.  Stop_coordinate (optional)
12.  Strand (optional)
13.  Genetic position (optional)
14.  Coordinate version (optional)
15.  Sequence version (optional)
16.  Description (optional)

Note that "chromosome 17" is the mitochondrial chromosome.

The SGD_features.tab file is complemented by GFF3 file saccharomyces_cerevisiae.gff

******************
phenotype_data.tab	This file is TAB delimited and contains curated phenotype data.
******************

1) Feature Name (Mandatory)     		-The feature name of the gene
2) Feature Type (Mandatory)     		-The feature type of the gene	
3) Gene Name (Optional) 			-The standard name of the gene
4) SGDID (Mandatory)    			-The SGDID of the gene
5) Reference (SGD_REF Required, PMID optional)  -PMID: #### SGD_REF: #### (separated by pipe)(one reference per row)
6) Experiment Type (Mandatory)     		-The method used to detect and analyze the phenotype
7) Mutant Type (Mandatory)      		-Description of the impact of the mutation on activity of the gene product
8) Allele (Optional)    			-Allele name and description, if applicable
9) Strain Background (Optional) 		-Genetic background in which the phenotype was analyzed
10) Phenotype (Mandatory)       		-The feature observed and the direction of change relative to wild type
11) Chemical (Optional) 			-Any chemicals relevant to the phenotype
12) Condition (Optional)        		-Condition under which the phenotype was observed
13) Details (Optional)  			-Details about the phenotype
14) Reporter (Optional) 			-The protein(s) or RNA(s) used in an experiment to track a process 

For further details about how phenotype information is recorded, please see:

http://www.yeastgenome.org/help/function-help/phenotypes

This file is updated weekly.

********************
interaction_data.tab
********************

Contains interaction data incorporated into SGD from BioGRID (http://www.thebiogrid.org/).  Tab-separated columns are:

1) Feature Name (Bait) (Required)       	- The feature name of the gene used as the bait
2) Standard Gene Name (Bait) (Optional) 	- The standard gene name of the gene used as the bait
3) Feature Name (Hit) (Required)        	- The feature name of the gene that interacts with the bait
4) Standard Gene Name (Hit) (Optional)  	- The standard gene name of the gene that interacts with the bait
5) Experiment Type (Required)   		- A description of the experimental used to identify the interaction
6) Genetic or Physical Interaction (Required)   - Indicates whether the experimental method is a genetic or physical interaction
7) Source (Required)    			- Lists the database source for the interaction
8) Manually curated or High-throughput (Required)	- Lists whether the interaction was manually curated from a publication or added as part of a high-throughput dataset
9) Notes (Optional)     			- Free text field that contains additional information about the interaction
10) Phenotype (Optional)        		- Contains the phenotype of the interaction
11) Reference (Required)        		- Lists the identifiers for the reference as an SGDID (SGD_REF:) or a PubMed ID (PMID:)
12) Citation (Required) 			- Lists the citation for the reference

***********************
protein_properties.tab:
***********************

Contains basic protein information about each ORF in SGD such as predicted molecular
weight, protein length, codon bias, etc a. This file does not include information on 
deleted or merged ORFs. Note, however, that it includes ORFs of all other classifications 
(Verified, Uncharacterized, and Dubious). This file is updated when there is a new genome release.

Last Modified: 26 Jan 2015 for genome release R64-2-1.

The columns are below; only the first column is mandatory.  The
column designated by an amino acid is the number of that particular
residue in the protein sequence.  For example, if the ALA column is 2,
then the protein contains 2 alanines.


Columns in protein_properties.tab:

ORF (Systematic Name)
Mw (Molecular Weight)
PI (Isoelectric Point)
Protein Length
N_term_seq
C_term_seq
GRAVY Score (Hydropathicity of Protein)
Aromaticity Score (Frequency of aromatic amino acids: Phe, Tyr, Trp)
CAI (Codon Adaptation Index)
Codon Bias
FOP Score (Frequency of Optimal Codons)>
Ala
Cys
Asp
Glu
Phe
Gly
His
Ile
Lys
Leu
Met
Asn
Pro
Gln
Arg
Ser
Thr
Val
Trp
Tyr
CARBON
HYDROGEN
NITROGEN
OXYGEN
SULPHUR
INSTABILITY INDEX (II)
ASSUMING ALL CYS RESIDUES APPEAR AS HALF CYSTINES
ASSUMING NO CYS RESIDUES APPEAR AS HALF CYSTINES
ALIPHATIC INDEX


For information about how protein_info data were generated, see the 
Help page for the Protein Information page at:

http://www.yeastgenome.org/help/protein_page.html

For the Oracle database schema and specifications for SGD, please refer to:

http://www.yeastgenome.org/cgi-bin/viewSchema.pl


