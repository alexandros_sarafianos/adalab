##stan model for bernstein polynomials
data {
  int<lower=0> N; #number of series
  int<lower=0> M; #number of coefficients
  int<lower=0> T; #number of timepoints
  int<lower=1> PL; #number of plates
  #int<lower=1> S;#number of strains
  int<lower=1,upper=PL> pl[N];#plate id
  # int<lower=1,upper=S> s[N];# strain id
  real MAXT;
  matrix<lower=0,upper=1>[M,T] X[PL];
  matrix<lower=0>[M-1,T] XD[PL];
  real timeN[T];
  matrix[N,T] OD[PL]; #optical density data
  vector[3] MU;
  real MIN;
  real MAX;
  real<lower=0> LAMBDA;
  int P;
  int prior[PL,P];
  int L[PL];
  int LAGT[PL];
  real NU;
  #real S;
}
transformed data{
  matrix[M,M] identity; //Identity for scaling matrix

  identity = diag_matrix(rep_vector(1.0,M));
}
parameters {
  matrix<lower=0>[M,PL] A_coef;
  matrix<lower=0>[M,N] N_A_coef;
  matrix<lower=0,upper=1>[2,PL] sigma_o;

  vector<lower=0>[5] lambda;
  real<lower=0,upper=0.01> LAG;
  real<lower=-MU[1],upper=1> VL;
  real<lower=-MU[2],upper=1> VG;
  real<lower=-MU[3],upper=1> VE;
  cov_matrix[M] SIGMA[PL];
  matrix<lower=0,upper=0.2>[M,PL] alpha;
  real<lower=0.3, upper=0.7> C;
  real<lower=0,upper=30> G;
  
}
transformed parameters{
  matrix[P,P] lp[PL];
  matrix[M-1,PL] DA_coef;
  matrix[PL,T] loss;
  matrix[PL,T] curve;
  matrix[PL,T] derivative;
  real MUL;
  real MUG;
  real MUE;
  real MULAG;
  vector[PL] biomass;


  matrix<lower=0>[M,PL] betaO;
   for(i in 1:PL)
   lp[i] = rep_matrix(-log(P),P,P);
     MUL=MU[1]+VL;
        MUG=MU[2]+VG;
        MUE=MU[3]+VE;
        MULAG=0;#+LAG[i];
      

  for(i in 1:PL){
    betaO[1,i]=alpha[1,i];
     for(k in 2:M)
        betaO[k,i]=betaO[k-1,i]+alpha[k,i];
        DA_coef[,i]=(1/MAXT)*(A_coef[2:M,i]-A_coef[1:(M-1),i]);
        curve[i,]=A_coef[,i]'*X[i];
        derivative[i,]=DA_coef[,i]'*XD[i];
        loss[i,] = derivative[i,] ./ curve[i,];
        biomass[i] = C*curve[i,1];
        
        {
                    vector[T+1] lpS_12;
                    vector[T+1] lpS_21;
                    vector[T+1] lpS_32;
                    vector[T+1] lpS_lag;
                    #vector[T+1] lpS_d;
                    real glucose;

                    lpS_12[1]=0;
                    lpS_21[1]=0;
                    lpS_32[1]=0;
                     lpS_lag[1]=0;
                     #lpS_d[1]=0;
                    for (t in 1:T){
                      
                      
                      lpS_lag[t+1]=lpS_lag[t]+normal_lpdf(loss[i,t]|0,lambda[1]);
                      lpS_12[t+1]=lpS_12[t]+normal_lpdf(loss[i,t]|MUL,lambda[2]);
                      lpS_21[t+1]=lpS_21[t]+normal_lpdf(loss[i,t]|MUG,lambda[3]);
                      lpS_32[t+1]=lpS_32[t]+normal_lpdf(loss[i,t]|MUE,lambda[4]);
                      #lpS_d[t+1]=lpS_d[t]+normal_lpdf(glucose|-13.8846,lambda[5]);
                    }

                    for(s1 in 1:P-1)
                      for(s2 in (s1+1):P){
                       
                        lp[i,s1,s2]= lp[i,s1,s2] + lpS_32[T+1] + lpS_lag[LAGT[i]]+ (lpS_12[prior[i,s1]]-lpS_12[LAGT[i]]) +(lpS_21[prior[i,s2]]-lpS_21[prior[i,s1]])  - lpS_32[prior[i,s2]]+normal_lpdf((G/MUL)*C*curve[i,LAGT[i]]*(1-exp(MUL*MAXT*(timeN[prior[i,s1]]-timeN[LAGT[i]])))|-13.8846,lambda[5]);
                      }
            }

    } #end for plate


}

model {

  lambda ~  cauchy(0,LAMBDA);

  sigma_o[1,]~ normal(0,0.001);
  sigma_o[2,]~ normal(0,0.01);
   VL~normal(0,0.3);
    VG~normal(0,0.1);
    VE~normal(0,0.01);
   C~ normal(0.5,0.15);
  for(n in 1:N){
    alpha[1,pl[n]]~normal(0,0.1);
    alpha[2:M,pl[n]]~normal(0,1);
   

   SIGMA[pl[n]] ~inv_wishart(M+2,0.001*identity);
   A_coef[,pl[n]] ~ multi_normal(betaO[,pl[n]],SIGMA[pl[n]]);
   N_A_coef[,n] ~ normal(A_coef[,pl[n]],0.01);
    OD[pl[n],n,1:L[pl[n]]]~normal(N_A_coef[,n]'*X[pl[n],,1:L[pl[n]]],sigma_o[1,pl[n]]);
   OD[pl[n],n,(L[pl[n]]+1):T]~normal(N_A_coef[,n]'*X[pl[n],,(L[pl[n]]+1):T],sigma_o[2,pl[n]]);
   target+=log_sum_exp(to_vector(lp[pl[n]]));
  }




}

generated quantities{
     #int s1;
     #int Is2;
     #int counter;
     #real s2;
     #real tauR;
     #real PR;
     matrix[PL,T] OD_pred;
     matrix[PL,T] DOD_pred;
     matrix[N,T] log_lik;
    # vector[T] piece_pred;

     int<lower=1,upper=P*P> tau;
    simplex[P*P] sp;
    sp = softmax(to_vector(lp[1]));
    tau = categorical_rng(sp);
    #tauR=tau;
    #PR=P;
    # s1=modulus(tau,P);
    # s2=ceil(tauR/PR);
     #for(i in 1:P){
    #   if(i<=(s2+0.1))
    #      Is2=i;
    # }
      for (t in 1:T) {
         for(n in 1:N){
           OD_pred[pl[n],t] = A_coef[,1]'*X[pl[n],,t];
           DOD_pred[pl[n],t] = DA_coef[,1]'*XD[pl[n],,t];
         
            if(t<=L[pl[n]])
              log_lik[n,t]=normal_lpdf(OD[pl[n],n,t]|N_A_coef[,n]'*X[pl[n],,t],sigma_o[1,pl[n]]);
            else
              log_lik[n,t]=normal_lpdf(OD[pl[n],n,t]|N_A_coef[,n]'*X[pl[n],,t],sigma_o[2,pl[n]]);
         }

     #    if(t<LAGT){
      #     piece_pred[t]= OD_pred[1]*exp(MULAG[1]*MAXT*timeN[t]);
       #  }
        #else{
         # if(t<prior[s1]){
         # piece_pred[t]= OD_pred[LAGT]*exp(MUL[1]*MAXT*(timeN[t]-timeN[LAGT]));

        #}
        #else{
         # if(t<prior[Is2]){
          # piece_pred[t]= OD_pred[prior[s1]]*exp(MUG[1]*MAXT*(timeN[t]-timeN[prior[s1]]));

          #}
          #else
           # piece_pred[t]= OD_pred[prior[Is2]]*exp(MUE[1]*MAXT*(timeN[t]-timeN[prior[Is2]]));
        #}
        #}


      }

}

