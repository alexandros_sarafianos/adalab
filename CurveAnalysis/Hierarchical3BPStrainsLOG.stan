##stan model for bernstein polynomials
data {
  int<lower=0> N; #number of series
  int<lower=0> M; #number of coefficients
  int<lower=0> T; #number of timepoints
  int<lower=1> PL; #number of plates
  #int<lower=1> S;#number of strains
  int<lower=1,upper=PL> pl[N];#plate id
  # int<lower=1,upper=S> s[N];# strain id
  real MAXT;
  matrix<lower=0,upper=1>[M,T] X;
  matrix<lower=0>[M-1,T] XD;
  real timeN[T];
  matrix[N,T] OD; #optical density data
  vector[3] MU;
  real MIN;
  real MAX;
  real<lower=0> LAMBDA;
  int P;
  int prior[P];
  int L;
  int LAGT;
  real NU;
  #real S;
}
transformed data{
  matrix[M,M] identity; //Identity for scaling matrix
  matrix[N,T] LOD;
  identity = diag_matrix(rep_vector(1.0,M));
  for(i in 1 :N)
    for(j in 1:T)
       LOD[i,j]=log(OD[i,j])-log(OD[i,1]);
}
parameters {

 real<lower=0,upper=1> sigma_o;
  real<lower=0,upper=0.01> LAG[PL];
  real<lower=-MU[1],upper=1> VL[PL];
  real<lower=-MU[2],upper=1> VG[PL];
  real<lower=-MU[3],upper=1> VE[PL];

}
transformed parameters{
  matrix[P,P] lp[PL];
  real MUL[PL];
  real MUG[PL];
  real MUE[PL];
  real MULAG[PL];


   for(i in 1:PL)
   lp[i] = rep_matrix(-log(P),P,P);


  for(i in 1:PL){

        MUL[i]=MU[1]+VL[i];
        MUG[i]=MU[2]+VG[i];
        MUE[i]=MU[3]+VE[i];
        MULAG[i]=0;#+LAG[i];
        for(j in 1:N)
        {
        {

                    vector[T+1] lpS_12;
                    vector[T+1] lpS_21;
                    vector[T+1] lpS_32;
                    vector[T+1] lpS_lag;

                    lpS_12[1]=0;
                    lpS_21[1]=0;
                    lpS_32[1]=0;
                     lpS_lag[1]=0;
                    for (t in 1:T){
                      lpS_lag[t+1]=lpS_lag[t]+normal_lpdf(LOD[j,t]|MULAG[i]*MAXT*timeN[t],sigma_o);
                      lpS_12[t+1]=lpS_12[t]+normal_lpdf(LOD[j,t]|MUL[i]*MAXT*timeN[t],sigma_o);
                      lpS_21[t+1]=lpS_21[t]+normal_lpdf(LOD[j,t]|MUG[i]*MAXT*timeN[t],sigma_o);
                      lpS_32[t+1]=lpS_32[t]+normal_lpdf(LOD[j,t]|MUE[i]*MAXT*timeN[t],sigma_o);
                    }

                    for(s1 in 1:P-1)
                      for(s2 in (s1+1):P){
                        lp[i,s1,s2]= lp[i,s1,s2] + lpS_32[T+1] + lpS_lag[LAGT]+ (lpS_12[prior[s1]]-lpS_12[LAGT]) +(lpS_21[prior[s2]]-lpS_21[prior[s1]])  - lpS_32[prior[s2]];
                      }
                   }

        }

    } #end for plate


}

model {

  sigma_o~ normal(0,0.01);


  for(n in 1:P){
        VL[n]~normal(0,0.3);
      VG[n]~normal(0,0.1);
        VE[n]~normal(0,0.01);
       target+=log_sum_exp(to_vector(lp[n]));
  }




}

generated quantities{
}

