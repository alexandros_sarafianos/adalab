##stan model for bernstein polynomials
data {
  int<lower=0> N; #number of series
  int<lower=0> M; #number of coefficients
  int<lower=0> T; #number of timepoints
  int<lower=1> PL; #number of plates
  #int<lower=1> S;#number of strains
  int<lower=1,upper=PL> pl[N];#plate id
  # int<lower=1,upper=S> s[N];# strain id
  real MAXT;
  matrix<lower=0,upper=1>[M,T] X;
  matrix<lower=0>[M-1,T] XD;
  real timeN[T];
  matrix[N,T] OD; #optical density data
  vector[3] MU;
  real MIN;
  real MAX;
  real<lower=0> LAMBDA;
  int P;
  int prior[P];
  int L;
  int LAGT;
  real NU;
  #real S;
}
transformed data{
  matrix[M,M] identity; //Identity for scaling matrix

  identity = diag_matrix(rep_vector(1.0,M));
}
parameters {
  matrix<lower=0>[M,PL] A_coef;
  matrix<lower=0>[M,N] N_A_coef;
  matrix<lower=0,upper=1>[2,PL] sigma_o;

  real<lower=0> lambda;
  real<lower=0,upper=1.0> A[PL];
  real<lower=-0.2,upper=1> VG[PL];
  cov_matrix[M] SIGMA[PL];
  matrix<lower=0,upper=0.2>[M,PL] alpha;
}
transformed parameters{
  vector[T] lp[PL];
  matrix[M-1,PL] DA_coef;
  matrix[PL,T] loss;
  matrix[PL,T] curve;
  matrix[PL,T] derivative;
  real MUG[PL];
 

  matrix<lower=0>[M,PL] betaO;
  

  for(i in 1:PL){
    betaO[1,i]=alpha[1,i];
     for(k in 2:M)
        betaO[k,i]=betaO[k-1,i]+alpha[k,i];
        DA_coef[,i]=(1/MAXT)*(A_coef[2:M,i]-A_coef[1:(M-1),i]);
        curve[i,]=A_coef[,i]'*X;
        derivative[i,]=DA_coef[,i]'*XD;
        loss[i,] = derivative[i,] ./ curve[i,];
        MUG[i]=0.2+VG[i];
        
        {
           real ml;
           real acum;
           acum=0;          
                    for (t in 1:T){
                       ml=MUG[i]*curve[i,t]*(1-curve[i,t]/A[i]);
                      lp[i,t]=normal_lpdf(derivative[i,t]| ml,lambda);
                    }
            

            }

    } #end for plate


}

model {

  lambda ~  cauchy(0,0.01);

  sigma_o[1,]~ normal(0,0.001);
  sigma_o[2,]~ normal(0,0.01);


  for(n in 1:N){
    alpha[1,pl[n]]~normal(0,0.1);
    alpha[2:M,pl[n]]~normal(0,1);
    
    VG[pl[n]]~normal(0,0.1);
    
   SIGMA[pl[n]] ~inv_wishart(M+2,0.001*identity);
   A_coef[,pl[n]] ~ multi_normal(betaO[,pl[n]],SIGMA[pl[n]]);
   N_A_coef[,n] ~ normal(A_coef[,pl[n]],0.01);
    OD[n,1:L]~normal(N_A_coef[,n]'*X[,1:L],sigma_o[1,pl[n]]);
   OD[n,(L+1):T]~normal(N_A_coef[,n]'*X[,(L+1):T],sigma_o[2,pl[n]]);
   target+=log_sum_exp(lp[pl[n]]);
  }




}

generated quantities{
     #int s1;
     #int Is2;
     #int counter;
     #real s2;
     #real tauR;
     #real PR;
     vector[T] OD_pred;
     vector[T] DOD_pred;
     matrix[N,T] log_lik;
    # vector[T] piece_pred;

  #   int<lower=1,upper=P*P> tau;
   # simplex[P*P] sp;
  #  sp = softmax(to_vector(lp[1]));
   # tau = categorical_rng(sp);
    #tauR=tau;
    #PR=P;
    # s1=modulus(tau,P);
    # s2=ceil(tauR/PR);
     #for(i in 1:P){
    #   if(i<=(s2+0.1))
    #      Is2=i;
    # }
      for (t in 1:T) {
         OD_pred[t] = A_coef[,1]'*X[,t];
         DOD_pred[t] = DA_coef[,1]'*XD[,t];
         for(n in 1:N){
            if(t<=L)
              log_lik[n,t]=normal_lpdf(OD[n,t]|A_coef[,pl[n]]'*X[,t],sigma_o[1,pl[n]]);
            else
              log_lik[n,t]=normal_lpdf(OD[n,t]|A_coef[,pl[n]]'*X[,t],sigma_o[2,pl[n]]);
         }

     #    if(t<LAGT){
      #     piece_pred[t]= OD_pred[1]*exp(MULAG[1]*MAXT*timeN[t]);
       #  }
        #else{
         # if(t<prior[s1]){
         # piece_pred[t]= OD_pred[LAGT]*exp(MUL[1]*MAXT*(timeN[t]-timeN[LAGT]));

        #}
        #else{
         # if(t<prior[Is2]){
          # piece_pred[t]= OD_pred[prior[s1]]*exp(MUG[1]*MAXT*(timeN[t]-timeN[prior[s1]]));

          #}
          #else
           # piece_pred[t]= OD_pred[prior[Is2]]*exp(MUE[1]*MAXT*(timeN[t]-timeN[prior[Is2]]));
        #}
        #}


      }

}

