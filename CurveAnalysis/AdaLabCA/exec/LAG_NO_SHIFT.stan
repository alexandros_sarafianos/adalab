##stan model for bernstein polynomials
data {
  int<lower=0> N; #number of series
  int<lower=0> M; #number of coefficients
  int<lower=0> T; #number of timepoints
  int<lower=1> PL; #number of plates
  #int<lower=1> S;#number of strains
  int<lower=1,upper=PL> pl[N];#plate id
  real MAXT; #Maximum time
  matrix<lower=0,upper=1>[M,T] X; #Bernstein basis
  matrix<lower=0>[M-1,T] XD; #Derivative of the Bernstein Basis
  real timeN[T]; #normalized time between (0,1)
  matrix[N,T] OD; #optical density data
  vector[3] MU; #Prior values for MU if needed
  real MIN;  # Minimum OD
  real MAX;  #Maximum OD
  int P; #Number of breakpoints for curves in the plate
  int prior[P]; #prior over breakpoints
  int L; #index over the time point at which OD reached 0.4
  int LAGT; #index of the time point considered to be lag phase
  real NU; #Degrees of freedom of the student_t likelihood
}
transformed data{
  matrix[M,M] identity; //Identity for scaling matrix
  identity = diag_matrix(rep_vector(1.0,M));
}
parameters {
  matrix<lower=0>[M,PL] A_coef;   #Bernstein coefficients for plate curve
  matrix<lower=0,upper=1>[2,PL] sigma_o; #Standard deviation of the observations (heteroscedastic noise)
  vector<lower=0>[4] lambda; #standard deviation of the approximation error
  real<lower=0,upper=0.01> LAG[PL]; #LAG rate
  real<lower=-MU[1],upper=1> VL[PL]; #standard deviation of the prior over rates(alternatively prior over the rates if MU=0)
  real<lower=-MU[2],upper=1> VG[PL];#standard deviation of the prior over rates(alternatively prior over the rates if MU=0)
  real<lower=-MU[3],upper=1> VE[PL]; #standard deviation of the prior over rates(alternatively prior over the rates if MU=0)
  cov_matrix[M] SIGMA[PL];
  matrix<lower=0,upper=0.2>[M,PL] alpha; #incremental jumps over the coefficient prior 
}
transformed parameters{
  matrix[P,P] lp[PL]; #log likelihood of the jump points
  matrix[M-1,PL] DA_coef; #coefficients of the derivatives
  matrix[PL,T] loss; # logarithmic derivative of the curve
  matrix[PL,T] curve; #the curve
  matrix[PL,T] derivative; #the derivative of the curve
  real MUL[PL]; #First growth rate (exponential growth)
  real MUG[PL]; #Second growth rate (slowdown before quiescence)
  real MUE[PL]; #Ethanol growth rate (quiescence or second growth phase)
  real MULAG[PL]; #Growth rate for lag phase
  matrix<lower=0>[M,PL] betaO; #prior over the A coefficients

   for(i in 1:PL)
   lp[i] = rep_matrix(-log(P),P,P);


  for(i in 1:PL){
     betaO[1,i]=alpha[1,i]; #b0= b1
     
     for(k in 2:M)
        betaO[k,i]=betaO[k-1,i]+alpha[k,i]; #implies alpha=beta0[k]-beta0[k-1]
        
        DA_coef[,i]=(1/MAXT)*(A_coef[2:M,i]-A_coef[1:(M-1),i]); #DA[k]= DA[k]-DA[k-1]
        
        curve[i,]=A_coef[,i]'*X; #curve= A*X
        derivative[i,]=DA_coef[,i]'*XD; #dcurve/dt=DA*XD
        loss[i,] = derivative[i,] ./ curve[i,]; #(dcurve/dt)/curve 


        MUL[i]=MU[1]+VL[i];
        MUG[i]=MU[2]+VG[i];
        MUE[i]=MU[3];#+VE[i];
        MULAG[i]=0+LAG[i];
        {
                    vector[T+1] lpS_12;
                    vector[T+1] lpS_21;
                    vector[T+1] lpS_32;
                    vector[T+1] lpS_lag;

                    lpS_12[1]=0;
                    lpS_21[1]=0;
                    lpS_32[1]=0;
                     lpS_lag[1]=0;
                    for (t in 1:T){
                      lpS_lag[t+1]=lpS_lag[t]+normal_lpdf(loss[i,t]|MULAG[i],lambda[1]);#(dcurve/dt)/curve at time=t ~ normal(MULAG,lambda) 
                      lpS_12[t+1]=lpS_12[t]+normal_lpdf(loss[i,t]|MUL[i],lambda[2]);#(dcurve/dt)/curve at time=t ~ normal(MUL,lambda) 
                      lpS_21[t+1]=lpS_21[t]+normal_lpdf(loss[i,t]|MUG[i],lambda[3]);#(dcurve/dt)/curve at time=t ~ normal(MUG,lambda) 
                      lpS_32[t+1]=lpS_32[t]+normal_lpdf(loss[i,t]|MUE[i],lambda[4]);#(dcurve/dt)/curve at time=t ~ normal(MUE,lambda) 
                    }

                    for(s1 in 1:P-1)
                      for(s2 in (s1+1):P){
                        #lp= p(loss|MUL)p(loss_i|MUL)p(loss_j|MUG)p(loss_k|MUE) for LAGT<i<s1; s1<j<s2;s2<k<T
                        lp[i,s1,s2]= lp[i,s1,s2] + lpS_32[T+1] + lpS_lag[LAGT]+ (lpS_12[prior[s1]]-lpS_12[LAGT]) +(lpS_21[prior[s2]]-lpS_21[prior[s1]])  - lpS_32[prior[s2]];
                      }
            }

    } #end for plate


}

model {

  lambda ~  cauchy(0,1); # variance of the approximation error of the LTV system

  sigma_o[1,]~ normal(0,0.001); #variance of OD observation error before OD=0.4
  sigma_o[2,]~ normal(0,0.01);  #variance of OD observation error after OD=0.4


  for(n in 1:N){
    alpha[1,pl[n]]~normal(0,0.1); #also implies beta0_1 ~ truncated_normal(0,0.1) , which is the intercept
    alpha[2:M,pl[n]]~normal(0,1); #implies betaO[k]-betaO[K-1] ~ truncated_normal(0,01) 
    VL[pl[n]]~normal(0,0.3);
    VG[pl[n]]~normal(0,0.1);
    VE[pl[n]]~normal(0,0.01);

   SIGMA[pl[n]] ~inv_wishart(M+2,0.01*identity); #prior over coefficients covariance matrix
   A_coef[,pl[n]] ~ multi_normal(betaO[,pl[n]],SIGMA[pl[n]]); 
    #OD[n,1:L]~student_t(NU,A_coef[,pl[n]]'*X[,1:L],sigma_o[1,pl[n]]); #student_t likelihood with NU degrees of freedom, with variance 1
   #OD[n,(L+1):T]~student_t(NU,A_coef[,pl[n]]'*X[,(L+1):T],sigma_o[2,pl[n]]);#student_t likelihood with NU degrees of freedom, with variance 1
   OD[n,1:L]~normal(A_coef[,pl[n]]'*X[,1:L],sigma_o[1,pl[n]]);
   OD[n,(L+1):T]~normal(A_coef[,pl[n]]'*X[,(L+1):T],sigma_o[2,pl[n]]);#student_t likelihood with NU degrees of freedom, with variance 1
   target+=log_sum_exp(to_vector(lp[pl[n]])); # add break point term
  }




}

generated quantities{
     #int s1;
     #int Is2;
     #int counter;
     #real s2;
     #real tauR;
     #real PR;
     vector[T] OD_pred;
     vector[T] DOD_pred;
     matrix[N,T] log_lik;
    # vector[T] piece_pred;

     int<lower=1,upper=P*P> tau;
    simplex[P*P] sp;
    sp = softmax(to_vector(lp[1]));
    tau = categorical_rng(sp);
    #tauR=tau;
    #PR=P;
    # s1=modulus(tau,P);
    # s2=ceil(tauR/PR);
     #for(i in 1:P){
    #   if(i<=(s2+0.1))
    #      Is2=i;
    # }
      for (t in 1:T) {
         OD_pred[t] = A_coef[,1]'*X[,t];
         DOD_pred[t] = DA_coef[,1]'*XD[,t];
         for(n in 1:N){
            if(t<=L)
              log_lik[n,t]=normal_lpdf(OD[n,t]|A_coef[,pl[n]]'*X[,t],sigma_o[1,pl[n]]);
            else
              log_lik[n,t]=normal_lpdf(OD[n,t]|A_coef[,pl[n]]'*X[,t],sigma_o[2,pl[n]]);
         }

     #    if(t<LAGT){
      #     piece_pred[t]= OD_pred[1]*exp(MULAG[1]*MAXT*timeN[t]);
       #  }
        #else{
         # if(t<prior[s1]){
         # piece_pred[t]= OD_pred[LAGT]*exp(MUL[1]*MAXT*(timeN[t]-timeN[LAGT]));

        #}
        #else{
         # if(t<prior[Is2]){
          # piece_pred[t]= OD_pred[prior[s1]]*exp(MUG[1]*MAXT*(timeN[t]-timeN[prior[s1]]));

          #}
          #else
           # piece_pred[t]= OD_pred[prior[Is2]]*exp(MUE[1]*MAXT*(timeN[t]-timeN[prior[Is2]]));
        #}
        #}


      }

}

