CostasModel2010- metabolic model used for price lab in PROM and GEMINI, it is a
refinement of IMM904 and other metabolic models
I used cobra to extract all the elemets of the model. also the sbml file (xml
extension) is present

IMMM804 and IMM904- metabolic models used by price lab (xml extensions for the
sbml files)

experiment.xml- data format used by the boolean network simulator

nodetypes.xml- complete definition of the data types in the boolean network
simulator

ZimmerMetabolites.csv- metabolite names extracted from the nodetypes.xml
