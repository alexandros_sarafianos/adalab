# These utilities started as a partial copy of the inspector file script/macro.txt.

get_filename_component(TF_CURRENT_DIR_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)

if(DEFINED TF_CURRENT_DIR)
 set(TF_CURRENT_DIR ${TF_CURRENT_DIR}_${TF_CURRENT_DIR_NAME})
else(DEFINED TF_CURRENT_DIR)
 set(TF_CURRENT_DIR ${TF_CURRENT_DIR_NAME})
endif(DEFINED TF_CURRENT_DIR)

set (SOURCES )
set (HEADERS )
set (SUBDIRECTORIES )

macro(INSTALL_HEADERS_WITH_DIRECTORY LOCATION HEADER_LIST)
	foreach(HEADER ${${HEADER_LIST}})
		string(REGEX MATCH "(.*)[/\\]" DIR ${HEADER})
		install(FILES ${HEADER} DESTINATION include/${LOCATION}/${DIR})
	endforeach(HEADER)
endmacro(INSTALL_HEADERS_WITH_DIRECTORY)


macro(INCLUDE_SUBDIRECTORY_HEADER_SOURCES DIR)
	add_subdirectory(${DIR})
	set(cur_cpp )
	set(cur_hpp )

	get_directory_property(cur_cpp DIRECTORY ${DIR} DEFINITION SOURCES)
	get_directory_property(cur_hpp DIRECTORY ${DIR} DEFINITION HEADERS)

	foreach(item ${cur_cpp})
		list(APPEND SOURCES ${DIR}/${item})
	endforeach(item)

	foreach(item ${cur_hpp})
		list(APPEND HEADERS ${DIR}/${item})
	endforeach(item)
	
endmacro(INCLUDE_SUBDIRECTORY_HEADER_SOURCES)


macro(INCLUDE_ALL_SUBDIRECTORIES_HEADER_SOURCES DIRS)
	foreach(dir ${${DIRS}})
		INCLUDE_SUBDIRECTORY_HEADER_SOURCES(${dir})
	endforeach(dir)
endmacro(INCLUDE_ALL_SUBDIRECTORIES_HEADER_SOURCES)


macro(CREATE_EXECUTABLE NAME SOURCES HEADERS)
        set(CURRENT_EXECUTABLE_NAME ${NAME})
        set(INCLUDEDIR ${INCLUDEDIR} ${PROJECT_SOURCE_DIR}/inspector ${CMAKE_BINARY_DIR}/data)
        set(LIBTARGET ${LIBTARGET} ${MIPS_RELEASE_LIBRARIES} ${OPENMS_LIBRARIES} ${Boost_LIBRARIES})

        include_directories(${INCLUDEDIR})
        add_executable(${CURRENT_EXECUTABLE_NAME} ${${SOURCES}} ${${HEADERS}})
        target_link_libraries(${CURRENT_EXECUTABLE_NAME} ${PROJECT_NAME} ${LIBTARGET})
endmacro(CREATE_EXECUTABLE)


macro(CREATE_TEST TESTNAME FILES)
        set(CURRENT_TEST_NAME ${TF_CURRENT_DIR}_${TESTNAME})
        set(INCLUDEDIR ${INCLUDEDIR} ${PROJECT_SOURCE_DIR}/inspector ${CMAKE_BINARY_DIR}/data)
        set(LIBTARGET ${LIBTARGET} ${MIPS_RELEASE_LIBRARIES} ${OPENMS_LIBRARIES} ${Boost_LIBRARIES})

        include_directories(${INCLUDEDIR})
        add_executable(${TESTNAME} ${FILES})
        target_link_libraries(${TESTNAME} ${PROJECT_NAME} ${LIBTARGET})

        add_test(${CURRENT_TEST_NAME} ${TESTNAME})
endmacro(CREATE_TEST)


function(CREATE_LIBRARY)
    set(options STATIC SHARED)
    set(oneValueArgs NAME)
    set(multiValueArgs SOURCES HEADERS LIBRARIES INCLUDEDIRS)

    cmake_parse_arguments(CREATE_LIBRARY "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    add_library(${CREATE_LIBRARY_NAME} SHARED ${CREATE_LIBRARY_SOURCES} ${CREATE_LIBRARY_HEADERS} )
    target_link_libraries( ${CREATE_LIBRARY_NAME} ${CREATE_LIBRARY_LIBRARIES} )
    install (TARGETS ${CREATE_LIBRARY_NAME} DESTINATION lib )
    INSTALL_HEADERS_WITH_DIRECTORY(${CREATE_LIBRARY_NAME} CREATE_LIBRARY_HEADERS )
endfunction(CREATE_LIBRARY)


macro(ADD_DATA_FILE UID FILENAME)
    set(DATA_TARGET export-${UID})
    set(DATA_DST_DIR ${CMAKE_BINARY_DIR}/data/${UID})
    set(DATA_DST_FILE ${DATA_DST_DIR}/${FILENAME})

    file(APPEND ${DATA_DEFINED_HEADER_FILE} "#define DATA_FILE_${UID} \"${DATA_DST_FILE}\" \n")

    if(NOT TARGET ${DATA_TARGET})
        add_custom_target(${DATA_TARGET} ALL COMMENT "Exporting files into build tree")
    endif(NOT TARGET ${DATA_TARGET})

    get_filename_component(FILENAME "${SRC_FILE}" NAME)
    add_custom_command(TARGET ${DATA_TARGET} COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_SOURCE_DIR}/${FILENAME} ${DATA_DST_FILE})
endmacro(ADD_DATA_FILE)


macro(ADD_DATA_DIR UID DIRNAME)
    set(DATA_TARGET export-${UID})
    set(DATA_DST_DIR ${CMAKE_BINARY_DIR}/data/${UID})

    file(APPEND ${DATA_DEFINED_HEADER_FILE} "#define DATA_DIR_${UID} \"${DATA_DST_DIR}/\" \n")

    if(NOT TARGET ${DATA_TARGET})
        add_custom_target(${DATA_TARGET} ALL COMMENT "Exporting files into build tree")
    endif(NOT TARGET ${DATA_TARGET})

    add_custom_command(TARGET ${DATA_TARGET} COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_CURRENT_SOURCE_DIR}/${DIRNAME}" "${DATA_DST_DIR}")
endmacro(ADD_DATA_DIR)
