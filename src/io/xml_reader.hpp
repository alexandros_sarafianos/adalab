#ifndef XML_READER_HPP
#define XML_READER_HPP
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/unordered_set.hpp>
#include "boost/unordered_map.hpp"

#include <string>
#include <map>
#include <set>
#include <iostream>
#include <tuple>

#include "src/io/container_check.hpp"
#include "src/experiment/experimentalinfo.hpp"
#include "src/general/compile_configuration.hpp"

 using boost::property_tree::ptree;
struct XML_reader
{
public:
    XML_reader(){};
    XML_reader(const std::string m_xml_file);
    std::string get_xml_element_content(const std::string c)const;
    void get_xml_parsed_element_content(const std::string s, std::vector<std::string> & c, const std::string delimiter)const;
    void get_node_info(std::map<std::string, std::set<std::string> > & type_symbol_map) const;
    template<class T, class V>
    void write_map_to_xml_file(const std::map<T,V> &  map, const std::string o_file);
    void get_allowed_nodes(boost::unordered_set<std::string> & node_set)const ;
    void get_experiment_info(std::unordered_map<std::string,ExperimentalInfo > & experimental_nodes)const;
    void read_continuous_network_file( std::vector< std::tuple< std::vector<std::tuple<std::string, std::string, std::string> >, std::string, std::string > >& rxns )const;
    void parse_reaction_from_cont_network_file(const boost::property_tree::ptree::value_type & v,
                                                           std::vector<std::tuple<std::string, std::string,std::string> > &  inputs,
                                                           std::string & constant,
                                                           std::string & output
                                                           )const;
    void test();
private:
    std::string xml_file;
    ptree pt;
};



template< class T, class V>
void XML_reader::write_map_to_xml_file(const std::map<T,V> &  map, const std::string o_file)
{
    std::ofstream ofs(o_file.c_str());
    if( !ofs )
    {
        std::stringstream os; os << "Error : cannot write to " << o_file << "!";LoggerSingleton::Instance()->info_print(os);
        return;
    }
    ofs << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    ofs << "\n<node_info>";
    for(typename std::map<T,V>::const_iterator it = map.begin(); it != map.end(); ++it)
    {
        ofs << "\n<" << it->first << ">\n";
        int counter = 1;
        for(typename V::const_iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
        {
            if( counter == it->second.size())
                ofs << *it2;
            else
                ofs << *it2 << ",";
            ++counter;
        }
        ofs << "\n</" << it->first << ">";
    }
    ofs << "\n</node_info>";
    ofs.close();
}



#endif
