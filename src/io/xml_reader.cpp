#include "src/io/xml_reader.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"

#include <utility>
#include <map>
#include <string>

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>

#include <boost/foreach.hpp>
#include <boost/property_tree/exceptions.hpp>


std::string XML_reader::get_xml_element_content(const std::string c) const
{
    std::string content= pt.get(c, "ERROR");
    if( content == "ERROR" )
    {
        std::stringstream os; os << "Error while reading XML file " << xml_file << " accessing " << c ;LoggerSingleton::Instance()->info_print(os);
    }

    //xml can contain "\n"s, filter this out
    int first= 0;
    if(content.front() == '\n' )
        first = 1;
    if( content.back() == '\n' )
        content.pop_back();

    content = content.substr(first);
    return content;
}

XML_reader::XML_reader(const std::string m_xml_file)
{
    xml_file = m_xml_file;
    using boost::property_tree::ptree;
    read_xml(xml_file.c_str(), pt);
}

void XML_reader::get_xml_parsed_element_content(const std::string s, std::vector<std::string> & c, const std::string delimiter) const
{
    std::string content = get_xml_element_content(s);
    boost::split(c, content, boost::is_any_of(delimiter));

}
const boost::property_tree::ptree& empty_ptree()
{
    static boost::property_tree::ptree t;
    return t;
}

void XML_reader::get_node_info(std::map<std::string, std::set<std::string> > & type_symbol_map) const
{

    BOOST_FOREACH(const boost::property_tree::ptree::value_type & v, pt.get_child("relations"))
    { // iterate over immediate children of the root
        std::vector<std::string> types, symbols,params;
        bool found = false;

        BOOST_FOREACH(const boost::property_tree::ptree::value_type & v2, v.second)
        { // iterate over immediate children of the root
                 std::string nodeName = v2.first;
                 if(nodeName == "<xmlattr>") continue;
                 const boost::property_tree::ptree& attributes =
                     v2.second.get_child("<xmlattr>", empty_ptree());

                 if(!attributes.empty())
                 {
                     std::string type = attributes.get_child("type").data();
                     std::string symbol = attributes.get_child("symbol").data();
                     if(string_contains_substring(symbol, "diaux"))
                         std::cout << "found it " << NEW_LINE;
                     if(string_contains_substring(symbol,"ADR1"))
                         found = true;
                     std::string param;
                     try
                     {
                        param = attributes.get_child("param").data();
                        types.push_back(type);symbols.push_back(symbol);
                        if(!param.empty())
                            params.push_back(param);
                     }
                     catch(boost::property_tree::ptree_bad_path& e )
                     {

                     }
                     std::set<std::string> temp  =  type_symbol_map[type];
                     temp.insert(symbol);
                     type_symbol_map[type] = temp;
                 }
        }
    }

}

void XML_reader::get_allowed_nodes(boost::unordered_set<std::string> & node_set)const
{

    std::string alterable_node_types[] = {"complex", "drug", "condition", "gene", "metabolite"};
    for( int cntr = 0; cntr <= 4; ++cntr)
    {
        std::vector<std::string> c;
        get_xml_parsed_element_content("node_info."+alterable_node_types[cntr], c, ",");
        std::stringstream os; os << c ;LoggerSingleton::Instance()->info_print(os);

        node_set.insert(c.begin(),c.end()) ;// = boost::unordered_set<std::string> (c.begin(),c.end());
    }
}

void XML_reader::get_experiment_info(std::unordered_map<std::string,ExperimentalInfo > & experimental_nodes)const
{
    BOOST_FOREACH(const boost::property_tree::ptree::value_type & v, pt.get_child("experiment"))
    {   // iterate over immediate children of the root
        BOOST_FOREACH(const boost::property_tree::ptree::value_type & v2, v.second)
        { // iterate over immediate children of the root
            std::stringstream os; os << "first " << v2.first ;LoggerSingleton::Instance()->info_print(os);
//          if(nodeName == "<xmlattr>") continue;
            const boost::property_tree::ptree& attributes =
                v2.second.get_child("<xmlattr>", empty_ptree());
                if(!attributes.empty())
                {
                    std::string id = attributes.get_child("id").data();
                    double value = convertStringToType<double>(attributes.get_child("value").data());
                    bool fixed = convertStringToType<bool>(attributes.get_child("fixed").data());
                    experimental_nodes.insert(std::pair<std::string, ExperimentalInfo>
                                              (id, ExperimentalInfo(value, fixed)) );
                    INFO_LOW( ExperimentalInfo(value, fixed) ) ;
                }
        }
    }
}

void XML_reader::read_continuous_network_file( std::vector< std::tuple< std::vector<std::tuple<std::string, std::string, std::string > >, std::string, std::string > >& rxns )const

{
    BOOST_FOREACH(const boost::property_tree::ptree::value_type & v, pt.get_child("network"))
    {   // iterate over immediate children of the root
//        std::cout << v.first << NEW_LINE;
        std::vector<std::tuple<std::string, std::string,std::string> >  inputs;
        std::string constant;
        std::string output;
        BOOST_FOREACH(const boost::property_tree::ptree::value_type & v2, v.second)
        { // iterate over immediate children of the root

            parse_reaction_from_cont_network_file(v2, inputs, constant, output);

        }
        std::tuple< std::vector<std::tuple<std::string, std::string, std::string> >, std::string, std::string > t(inputs, constant, output);
        rxns.push_back(t);
    }
}


void XML_reader::parse_reaction_from_cont_network_file(const boost::property_tree::ptree::value_type & v,
                                                       std::vector<std::tuple<std::string, std::string, std::string> > &  inputs,
                                                       std::string & constant,
                                                       std::string & output
                                                       )const
{
    const boost::property_tree::ptree& attributes =
    v.second.get_child("<xmlattr>", empty_ptree());
    if(!attributes.empty())
    {
        if(v.first == "input")
        {
            std::string tp;
            try{tp = attributes.get_child("type").data();}
            catch(boost::exception_detail::clone_impl<boost::exception_detail::error_info_injector<boost::property_tree::ptree_bad_path> >)
            {tp = "gene";}
            std::tuple<std::string, std::string, std::string> new_tuple = std::make_tuple(attributes.get_child("symbol").data(),
                                                attributes.get_child("weight").data(), tp );
            try
            {
                std::string abc = attributes.get_child("prob").data();
                std::string cde = attributes.get_child("prob_param").data();
//                INFO_LOW( "Continuous network version 2");
            }
            catch(boost::exception_detail::clone_impl<boost::exception_detail::error_info_injector<boost::property_tree::ptree_bad_path> >)
            {
//                INFO_LOW( "Continuous network version 1");
            }
            inputs.push_back(new_tuple);
        }
        else if (v.first =="constant")
            constant = attributes.get_child("weight").data();//convertStringToType<double>(attributes.get_child("weight").data());
        else
        {
            if(v.first == "output")
            {
                output = attributes.get_child("symbol").data();
            }
            else
                std::cout << "Strange type found: '" << v.first << "'!" <<NEW_LINE;
        }
    }
}
