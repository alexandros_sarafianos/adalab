#ifndef Hypergraph_HPP
#define Hypergraph_HPP

#include <map>
#include <functional>
#include <memory>
#include <algorithm>
#include <cassert>
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"

#include <unordered_set>
template<class V, class E, class PV, class PE, class A>
// V is data type of vertex
// E is identifier of Edge
// PV is node sorting predicate
// PE is edge sorting predicate
// A is allocator
class HyperDistrIterator;


template<class V, class E=int, class PV = std::less<V>, class PE=std::less<E>, class A=std::allocator<V> >
class Hypergraph {

#if _MSC_VER <= 1600
    typedef A sub_allocator;
#else
    typedef std::scoped_allocator_adaptor<A> sub_allocator;
#endif

public:
    class vertex;
    class edge;
    typedef std::map<V, vertex, PV, sub_allocator> vertexset;
    typedef std::map<E, edge, PE, sub_allocator> edgeset;
    typedef typename vertexset::iterator vertexiter;
    typedef typename edgeset::iterator edgeiter;
    typedef typename vertexset::const_iterator cvertexiter;
    typedef typename edgeset::const_iterator cedgeiter;

    typedef std::reference_wrapper<const V> rwv;
    typedef std::reference_wrapper<const E> rwe;
    typedef std::reference_wrapper<vertex> rwvertex;
    typedef std::reference_wrapper<edge> rwedge;
    typedef std::map<rwv, rwvertex, PV, sub_allocator> ivertexset;
    typedef std::map<rwe, rwedge, PE, sub_allocator> iedgeset;
    typedef typename ivertexset::iterator ivertexiter;
    typedef typename iedgeset::iterator iedgeiter;
    typedef typename ivertexset::const_iterator civertexiter;
    typedef typename iedgeset::const_iterator ciedgeiter;
//    typedef HyperDistrIterator<V,E,PV,PE,A> iterator;
    class vertex {
        friend class Hypergraph<V,E,PV,PE,A>;
        iedgeset edges_;
//        vertex(const PE&, const sub_allocator&);    /* so users can'V make their own vertices*/
    public:
        vertex(vertex&&);
        vertex& operator=(vertex&&);
        iedgeset& edges();
        vertex(const PE&, const sub_allocator&);    /* so users can'V make their own vertices*/
//        vertex operator=(vertex&);
//        ~vertex(){}
//        vertex(){}
        vertex(const vertex&){}
        const iedgeset& edges() const;
        friend std::ostream& operator<<(std::ostream& out, const vertex){return out;}
    };
    class edge {
        friend class Hypergraph<V,E,PV,PE,A>;
        ivertexset vertices_;
        ivertexiter head_;
//        edge(const PV&, const sub_allocator&); /* so users can'V make their own edges*/
    public:
        edge(edge&&);
        edge& operator=(edge&&);
        void set_head(const V& v);
        const V* get_head() const;
        ivertexset& vertices();
        const ivertexset& vertices() const;
        edge(const PV&, const sub_allocator&);
        edge(const edge&){}
        edge(edge&){}
        friend std::ostream& operator<<(std::ostream& out, const edge){return out;}
    };

    Hypergraph(const PV& vertexpred=PV(), const PE& edgepred=PE(), const A& alloc=A());

    std::pair<vertexiter,bool> add_vertex(V v=V());
    std::pair<edgeiter,bool> add_edge(E e=E());
    vertexiter erase_vertex(const vertexiter& iter);
    vertexiter erase_vertex(const V& rhs);
    edgeiter erase_edge(const edgeiter& iter);
    edgeiter erase_edge(const E& rhs);

    void connect(const E& e, const V& v);
    void connect(const edgeiter& ei, const vertexiter& vi);
    void disconnect(const E& e, const V& v);
    void disconnect(const edgeiter& ei, const vertexiter& vi);
    vertexset& vertices();
    const vertexset& vertices() const;
    edgeset& edges();
    const edgeset& edges() const;

    A get_allocator() const;


    void print_hyperedges()const;
    Hypergraph(const std::string & hypergraph_file);
    void remove_edges();
//  void Hypergraph();
    Hypergraph * my_clone()const
    {
        Hypergraph * h  = new Hypergraph(*this);
        return h;
    }
    HyperDistrIterator<V,E,PV,PE,A> begin()
    {
        HyperDistrIterator<V,E,PV,PE,A> h_g_it(this,true);
        return h_g_it;
    }
    HyperDistrIterator<V,E,PV,PE,A>  end() const
    {
        std::cout << "END" <<NEW_LINE;
//        return nullptr;
        return HyperDistrIterator<V,E,PV,PE,A>(this, false);
    }



//    bool operator==(Hypergraph & h )
//    {
//        return (const_cast<Hypergraph>(*this) == const_cast<Hypergraph>(h) );
//    }
    bool operator==(const Hypergraph & h) const
    {
        vertexset vertices_hgraph1 = this->vertices();
        vertexset vertices_hgraph2 = h.vertices();
        edgeset edges_hypergraph1 = edges();
        edgeset edges_hypergraph2 = h.edges();
        if( ( edges_hypergraph1.size() != edges_hypergraph2.size() ) || (vertices_hgraph1.size() != vertices_hgraph2.size()) )
        {
            std::cout << "Sizes don't match " << edges_hypergraph1.size() <<" =?= "
                         << edges_hypergraph2.size() << " OR "
                            << vertices_hgraph1.size() << "=?=" << vertices_hgraph2.size() << NEW_LINE;
            return false;
        }
//        std::cout << "Sizes match apparantly" << edges_hypergraph1.size() <<" =?= "
//                     << edges_hypergraph2.size() << " OR "
//                        << vertices_hgraph1.size() << "=?=" << vertices_hgraph2.size() << NEW_LINE;
        //vertices should be the same
        for(auto v1_it = vertices_hgraph1.begin(); v1_it != vertices_hgraph1.end(); ++v1_it)
        {
             if( vertices_hgraph2.find(v1_it->first) == vertices_hgraph2.end())
            {
                 std::cout << "Vertex " <<v1_it->first << " not found in second hypergraph! Printing vertices..." << NEW_LINE;
                 for(auto v2_it = vertices_hgraph2.begin(); v2_it != vertices_hgraph2.end(); ++v2_it)
                 {std::cout << v2_it->first << NEW_LINE;}
                 return false;
            }
        }

        //edges should be the same
        for(auto e1_it = edges_hypergraph1.begin(); e1_it != edges_hypergraph1.end(); ++e1_it)
        {
            if( edges_hypergraph2.find(e1_it->first) == edges_hypergraph2.end())
            {
                 std::cout << "Edge " <<e1_it->first << " not found in second hypergraph" << NEW_LINE;
                 for(auto e2_it = edges_hypergraph2.begin(); e2_it != edges_hypergraph2.end(); ++e2_it)
                 {std::cout << e2_it->first << NEW_LINE;}
                 return false;
            }
        }
        return true;
        //vertex equal, edges equal
    }

    bool operator!=(const Hypergraph & h )const{ return !(*this == h); }
    bool operator!=(Hypergraph & h ) {return !(*this==h);}

    //template<class V, class E=int, class PV = std::less<V>, class PE=std::less<E>, class A=std::allocator<V> >


    Hypergraph(const Hypergraph& rhs){vertices_ = rhs.vertices_;edges_ = rhs.edges_;}
    Hypergraph& operator=(const Hypergraph& rhs){Hypergraph h(rhs);return h;}

protected:

    PV pv_;
    PE pe_;
    A a_;
    vertexset vertices_;
    edgeset edges_;
private:
//     std::shared_ptr<Hypergraph> m_Hypergraph;
};


template<class V, class E=int, class PV = std::less<V>, class PE=std::less<E>, class A=std::allocator<V> >
class HyperDistrIterator
{
public:


    HyperDistrIterator(){}

    void next()
    {
        edge_iterator++;
    }

    bool operator==(const HyperDistrIterator<V,E,PV,PE,A> & rhs) const
    {
        std::cout << "Comparing " << NEW_LINE;
        bool result = false;
        if( h_graph && rhs.h_graph )
        {
            result =  *h_graph == *(rhs.h_graph) ;
            std::cout << "graph contents equal " << result <<NEW_LINE;
        }
        else if( !h_graph && !rhs.h_graph )
        {
            result = true;
            std::cout<<" graphs are both nullpointers" << NEW_LINE;
        }
        else
        {   std::cout << "ONe of graphs is nullpointer " <<NEW_LINE;
            return false;
        }

        if ( ( edge_iterator != h_graph->edges().end() ) && (rhs.edge_iterator != rhs.h_graph->edges().end() ))
        {
            std::cout << edge_iterator->first << "=?=" << rhs.edge_iterator->first << NEW_LINE;
            result = result && (edge_iterator->first == rhs.edge_iterator->first );
            std::cout <<"COntent of edge iterator equal"<< result <<NEW_LINE;
        }
        else
        {
            if ( (edge_iterator == h_graph->edges().end()) && (rhs.edge_iterator == rhs.h_graph->edges().end() ))
            {
                result = result;
                std::cout << "both are pointing to end" << NEW_LINE;
            }
            else
            {
                std::cout <<"one of both is end" <<NEW_LINE;
                return false;
            }
        }
    return result;
    }
    bool operator!=(const HyperDistrIterator<V,E,PV,PE,A> & rhs) const
    {
        std::cout <<"!="<<NEW_LINE;
        return !(*this == rhs);
    }

    HyperDistrIterator operator++()
    {
        edge_iterator++;
        return *this;
    }
    void print()const
    {
        std::cout <<"Hallo " << NEW_LINE;
    }
    Hypergraph<V,E,PV,PE,A> * operator()()
    {
        return h_graph;
    }
    HyperDistrIterator(const Hypergraph<V,E,PV,PE,A> * h, bool begin)
    {
        h_graph = h;
        if( begin )
        {
              std::cout << "Initialized with begin " << NEW_LINE;
              edge_iterator = h_graph->edges().begin();
        }

        else
        {
              std::cout << "Initialized with end " << NEW_LINE;
              edge_iterator = h_graph->edges().end();
        }
        bool a = h_graph->edges().begin() == h_graph->edges().end();
        std::cout << "Inside edge_iterator comparison " << a << NEW_LINE;
    }
//    std::shared_ptr< Hypergraph<V,E,PV,PE,A> > operator->()
//    {
//        Hypergraph<V,E,PV,PE,A> hg_tmp = *h_graph;
//        std::cout << "Step 1 " << NEW_LINE;
//        std::shared_ptr< Hypergraph<V,E,PV,PE,A> > hg_last = std::make_shared<Hypergraph<V,E,PV,PE,A> > (hg_tmp);
//        std::cout << "Step 2 " << NEW_LINE;
//        return hg_last;
//    }
    void my_print() const
    {
        std::cout << "here "<< NEW_LINE;
        h_graph->print_hyperedges();
    }
private:

    const Hypergraph<V,E,PV,PE,A> * h_graph;
    typename Hypergraph<V,E,PV,PE,A>::cedgeiter edge_iterator;
    typename Hypergraph<V,E,PV,PE,A>::edgeset edge_set;

};
// -----------------------------------END OF CLASS DEFINITIONS------------------------------------------
namespace std {
    template<class E, class T, class R>
    std::basic_ostream<E,T>& operator<<(std::basic_ostream<E,T>& s, const std::reference_wrapper<R>& r);

    template<class E, class T, class R>
    std::basic_istream<E,T>& operator>>(std::basic_istream<E,T>& s, std::reference_wrapper<R>& r);
}

template<class V, class E, class PV, class PE, class A>
inline Hypergraph<V,E,PV,PE,A>::vertex::vertex(const PE& pred, const typename Hypergraph<V,E,PV,PE,A>::sub_allocator& alloc)
    : edges_(pred, alloc)
{}

template<class V, class E, class PV, class PE, class A>
inline Hypergraph<V,E,PV,PE,A>::vertex::vertex(typename Hypergraph<V,E,PV,PE,A>::vertex&& rhs)
    :  edges_(std::move(rhs.edges_))
{}

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::vertex& Hypergraph<V,E,PV,PE,A>::vertex::operator=(typename Hypergraph<V,E,PV,PE,A>::vertex&& rhs)
{
    edges_ = std::move(rhs);
    return *this;
}

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::iedgeset& Hypergraph<V,E,PV,PE,A>::vertex::edges()
{return edges_;}

template<class V, class E, class PV, class PE, class A>
inline const typename Hypergraph<V,E,PV,PE,A>::iedgeset& Hypergraph<V,E,PV,PE,A>::vertex::edges() const
{return edges_;}

template<class V, class E, class PV, class PE, class A>
inline Hypergraph<V,E,PV,PE,A>::edge::edge(const PV& pred, const typename Hypergraph<V,E,PV,PE,A>::sub_allocator& alloc)
    : vertices_(pred, alloc)
    , head_(vertices_.end())
{}

template<class V, class E, class PV, class PE, class A>
inline Hypergraph<V,E,PV,PE,A>::edge::edge(edge&& rhs)
    : vertices_(rhs.vertices_)
    , head_(rhs.head_!=rhs.vertices_.end() ? vertices_.find(rhs.head_->first) : vertices_.end())
{}

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::edge& Hypergraph<V,E,PV,PE,A>::edge::operator=(typename Hypergraph<V,E,PV,PE,A>::edge&& rhs)
{
    vertices_ = std::move(rhs);
    if (rhs.head_ != rhs.vertices_.end())
        head_ = vertices_.find(rhs.head_->first);
    else
        head_ = vertices_.end();
    return *this;
}
template<class V, class E, class PV, class PE, class A>
inline void Hypergraph<V,E,PV,PE,A>::edge::set_head(const V& v)
{
    ivertexiter iter = vertices_.find(std::ref(v));
    assert(iter != vertices_.end());
    head_ = iter;
}

template<class V, class E, class PV, class PE, class A>
inline const V* Hypergraph<V,E,PV,PE,A>::edge::get_head() const
{return (head_ != vertices_.end() ? &head_->first.get() : NULL);}

template<class V, class E, class PV, class PE, class A>
inline const typename Hypergraph<V,E,PV,PE,A>::ivertexset& Hypergraph<V,E,PV,PE,A>::edge::vertices() const
{ return vertices_; }

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::ivertexset& Hypergraph<V,E,PV,PE,A>::edge::vertices()
{ return vertices_; }

template<class V, class E, class PV, class PE, class A>
inline Hypergraph<V,E,PV,PE,A>::Hypergraph(const PV& vertexpred, const PE& edgepred, const A& alloc)
    :pv_(vertexpred)
    ,pe_(edgepred)
    ,a_(alloc)
    ,vertices_(vertexpred, a_)
    ,edges_(edgepred, a_)
{}

template<class V, class E, class PV, class PE, class A>
inline std::pair<typename Hypergraph<V,E,PV,PE,A>::vertexiter, bool> Hypergraph<V,E,PV,PE,A>::add_vertex(V v)
{ return vertices_.insert(std::pair<V, vertex>(std::move(v),vertex(pe_, a_))); }

template<class V, class E, class PV, class PE, class A>
inline std::pair<typename Hypergraph<V,E,PV,PE,A>::edgeiter, bool> Hypergraph<V,E,PV,PE,A>::add_edge(E e)
{ return edges_.insert(std::pair<E,edge>(std::move(e), edge(pv_, a_))); }

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::vertexiter Hypergraph<V,E,PV,PE,A>::erase_vertex(const typename Hypergraph<V,E,PV,PE,A>::vertexiter& iter)
{
    for(auto i = iter->edges().begin(); i != iter->edges().end(); ++i)
        i->erase(*iter);
    return vertices_.erase(iter);
}

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::vertexiter Hypergraph<V,E,PV,PE,A>::erase_vertex(const V& rhs)
{
    vertexiter vi = vertices_.find(rhs);
    assert(vi != vertices_.end());
    vertex& v = vi->second;
    for(auto i = v.edges().begin(); i != v.edges().end(); ++i)
        i->second.get().vertices_.erase(std::ref(vi->first));
    return vertices_.erase(vi);
}

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::edgeiter Hypergraph<V,E,PV,PE,A>::erase_edge(const typename Hypergraph<V,E,PV,PE,A>::edgeiter& iter)
{
    for(auto i = iter->vertices().begin(); i != iter->vertices().end(); ++i)
        i->edges_.erase(*iter);
    return edges_.erase(iter);
}

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::edgeiter Hypergraph<V,E,PV,PE,A>::erase_edge(const E& rhs)
{
    edgeiter ei = edges_.find(rhs);
    assert(ei != edges_.end());
    edge& e = ei->second;
    for(auto i = e.vertices().begin(); i != e.vertices().end(); ++i)
        i->second.get().edges_.erase(std::ref(ei->first));
    return edges_.erase(ei);
}

template<class V, class E, class PV, class PE, class A>
inline void Hypergraph<V,E,PV,PE,A>::connect(const E& e, const V& v)
{
    vertexiter vi = vertices_.find(v);
    edgeiter ei = edges_.find(e);
    assert(vi != vertices_.end());
    assert(ei != edges_.end());
    vi->second.edges_.insert(typename iedgeset::value_type(std::ref(ei->first), std::ref(ei->second)));
    auto n = ei->second.vertices_.insert(typename ivertexset::value_type(std::ref(vi->first), std::ref(vi->second)));
    if (ei->second.vertices_.size()==1)
        ei->second.head_ = n.first;
}

template<class V, class E, class PV, class PE, class A>
inline void Hypergraph<V,E,PV,PE,A>::connect(const typename Hypergraph<V,E,PV,PE,A>::edgeiter& ei, const typename Hypergraph<V,E,PV,PE,A>::vertexiter& vi)
{
    assert(std::distance(vertices_.begin(), vi)>=0); //actually asserts that the iterator belongs to this container
    assert(std::distance(edges_.begin(), ei)>=0); //actually asserts that the iterator belongs to this container
    vi->edges_.insert(typename iedgeset::value_type(std::ref(ei->first), std::ref(ei->second)));
    auto n = ei->vertices_.insert(typename ivertexset::value_type(std::ref(vi->first), std::ref(vi->second)));
    if (ei->second.vertices_.size()==1)
        ei->second.head_ = n.first;
}

template<class V, class E, class PV, class PE, class A>
inline void Hypergraph<V,E,PV,PE,A>::disconnect(const E& e, const V& v)
{
    edgeiter ei = edges_.find(e);
    vertexiter vi = vertices_.find(v);
    assert(ei != edges.end());
    assert(vi != vertices_.end());
    if (ei->head_.first == v) {
        if (ei->head_ != ei->vertices.begin())
            ei->head = ei->vertices.begin();
        else
            ei->head = ei->vertices.end();
    }
    ei->vertices_.erase(std::ref(vi->first));
    vi->edges_.erase(std::ref(ei->first));
}

template<class V, class E, class PV, class PE, class A>
inline void Hypergraph<V,E,PV,PE,A>::disconnect(const typename Hypergraph<V,E,PV,PE,A>::edgeiter& ei, const typename Hypergraph<V,E,PV,PE,A>::vertexiter& vi)
{
    assert(std::distance(edges_.begin(), ei)>=0); //actually asserts that the iterator belongs to this container
    assert(std::distance(vertices_.begin(), vi)>=0); //actually asserts that the iterator belongs to this container
    if (ei->head_.first == vi->first) {
        if (ei->head_ != ei->vertices.begin())
            ei->head = ei->vertices.begin();
        else
            ei->head = ei->vertices.end();
    }
    ei->vertices_.erase(std::ref(vi->first));
    vi->edges_.erase(std::ref(ei->first));
}


template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::vertexset& Hypergraph<V,E,PV,PE,A>::vertices()
{ return vertices_;}

template<class V, class E, class PV, class PE, class A>
inline const typename Hypergraph<V,E,PV,PE,A>::vertexset& Hypergraph<V,E,PV,PE,A>::vertices() const
{ return vertices_;}

template<class V, class E, class PV, class PE, class A>
inline typename Hypergraph<V,E,PV,PE,A>::edgeset& Hypergraph<V,E,PV,PE,A>::edges()
{ return edges_;}

template<class V, class E, class PV, class PE, class A>
inline const typename Hypergraph<V,E,PV,PE,A>::edgeset& Hypergraph<V,E,PV,PE,A>::edges() const
{ return edges_;}

template<class V, class E, class PV, class PE, class A>
inline A Hypergraph<V,E,PV,PE,A>::get_allocator() const
{ return a_;}

template<class V, class E, class PV, class PE, class A>
Hypergraph<V,E,PV,PE,A>::Hypergraph(const std::string & hypergraph_file)
{
    std::ifstream ifs;
    std::cout << hypergraph_file << NEW_LINE;
    open_file_safely(hypergraph_file, ifs);
    std::string line;
    int linenumber = 1;
    std::unordered_set<Vertex> node_set;
    boost::unordered_set<std::string> unused;
    while(getline(ifs,line))
    {
        std::vector<std::pair<std::string,bool> > causes;
        bool stimulation;
        std::string effect;

        parse_network_line(line, causes, stimulation, effect, unused );
        //add reaction hyperedge
        std::map<Vertex, Influence> influence_map;
        for( auto it= causes.begin(); it != causes.end(); ++it)
        {
            V v(it->first);
            Influence inf;
            if( it->second )
                inf = Inhibition;
            else
                inf = Activation;
            influence_map[v] = inf;
        }



        Edge e(!stimulation, influence_map); //inhibiton = !stimulation
        std::pair<edgeiter, bool> edge_added = add_edge(e);
        if( edge_added.second == false)
        {
            std::cout << "Error adding edge on line " << linenumber << NEW_LINE;
            exit(1);
        }
        for( auto it= causes.begin(); it != causes.end(); ++it)
        {
            //add all entities before the arrow as vertices
            V v(it->first);
            //node not in node_set, add to hypergraph
            std::pair<vertexiter, bool> vertex_added = add_vertex(v);
            if( vertex_added.second == false)
            {
                std::cout << "Error adding vertex " << it->first << ", was it already present?!" <<NEW_LINE;
            }
            //Connect vertices with edges
            connect(e, v);
        }
        //add head (directed hypergraph)
        V v_out(effect);
        std::pair<vertexiter, bool> out_vertex_added = add_vertex(v_out);
        if( out_vertex_added.second == false)
        {
            std::cout << "Error adding vertex " << effect << ", was it already present?!" <<NEW_LINE;

        }
        connect(e, v_out);
        edge_added.first->second.set_head(v_out);
        linenumber++;

    }
}
//template< class V, class E, class PV, class PE, class A>
//void Hypergraph<V,E,PV,PE,A>::foo()
//{
//    // Adapt existing edge: change direction, remove, change meaning
//    // Add new edge between nodes ( Given two unconnected nodes $X,Y$, the possibilities are $X \multimap Y$, $Y \multimap X$, $X \rightarrow Y$, $Y \rightarrow X$, inhibition and activation in same directions can be regarded in a similar way )
//    // (Add a hidden variable between two nodes.)
//    // Adapt existing hyperedge: change direction, change type(inhibition/stimulation), add or remove reactant
//    // Add new hyperedge between vertices: high amount of combinations possible $\dbinom{n}{k}$, with $n$ the number of possible vertices, and $k$ the size of the subset of vertices used in the hyperedge.
//    remove_edges();
//}
template< class V, class E, class PV, class PE, class A>
void Hypergraph<V,E,PV,PE,A>::remove_edges()
{
    edgeset & e_set = edges();

    for(auto eit = e_set.begin(); eit != e_set.end(); ++eit )
    {
        std::cout << "------------------------" << NEW_LINE;
        E e = eit->first;
        erase_edge(eit->first);
//        print_hyperedges();

    }
}

/* For testing purposes */
template<class V, class E, class PV, class PE, class A>
void Hypergraph<V,E,PV,PE,A>::print_hyperedges()const
{
    for( auto edge_iterator = edges().begin(); edge_iterator != edges().end(); ++edge_iterator)
    {
       //first -> Edge, second edge
        V head = *(edge_iterator->second.get_head());
        for(auto v_it = edge_iterator->second.vertices().begin(); v_it != edge_iterator->second.vertices().end();++v_it )
            if( v_it->first.get() != head)
                std::cout << v_it->first << "&";
        std::cout << "-> ";
        std::cout << head << NEW_LINE;
    }
//   std::cout << "-----------part 2 ----------- " << NEW_LINE;
//   for( auto edge_iterator = edges().begin(); edge_iterator != edges().end(); ++edge_iterator)
//   {
//      //first -> Edge, second edge
//       V head = *(edge_iterator->second.get_head());
//       for(auto v_it = edge_iterator->second.vertices().begin(); v_it != edge_iterator->second.vertices().end();++v_it )
//           std::cout<<edge_iterator->first.get_id()<< " : " << v_it->first.get() << " Inf : " << edge_iterator->first.get_influence_of_vertex_in_hyperedge(v_it->first) << NEW_LINE;
//   }
}


namespace std {

    template<class E, class T, class R>
    std::basic_ostream<E,T>& operator<<(std::basic_ostream<E,T>& s, const std::reference_wrapper<R>& r)
    {return s << r.get();}

    template<class E, class T, class R>
    std::basic_istream<E,T>& operator>>(std::basic_istream<E,T>& s, std::reference_wrapper<R>& r)
    {return s >> r.get();}

}

#endif
