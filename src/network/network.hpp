#ifndef HEADER_NETWORK_HPP
#define HEADER_NETWORK_HPP

#include "src/network/edge.hpp"
#include "src/network/vertex.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include "src/general/maths/sigmoid.hpp"
#include "src/general/maths/RandomWrapper.hpp"
#include "src/experiment/experiment.hpp"
#include "src/experiment/experimentaloutput.hpp"
#include "src/general/compile_configuration.hpp"

#include <iostream>                  // for std::stringstream os; os
#include <utility>                   // for std::pair
#include <algorithm>                 // for std::for_each
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <random>

#include <boost/config.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/adjacency_iterator.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/adjacency_iterator.hpp>
#include <boost/graph/bron_kerbosch_all_cliques.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/numeric/ublas/matrix.hpp>


using namespace boost::numeric::ublas;


using namespace boost;

template<class _VertexOutputType>
class Network
{
public:
    typedef Vertex<_VertexOutputType> _Vertex;
    typedef Vertex<bool> _VertexBool;
    typedef Vertex<double> _VertexCont;
    typedef typename boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, _Vertex , Edge > Graph; //,Country : bundle graph properties
//    typedef typename boost::adjacency_matrix<boost::directedS, _Vertex , Edge > Graph; //,Country : bundle graph properties
    typedef typename boost::adjacency_matrix< boost::directedS, _Vertex, Edge > MatrixGraph;
//    typedef typename boost::adjacency_matrix<boost::bidirectionalS, Vertex , Edge > Graph; //,Country : bundle graph properties

//    typedef typename boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, Vertex , Edge > UndirectedGraph; //,Country : bundle graph properties

    typedef typename std::pair<_Vertex  ,_Vertex  > vert_p;
    typedef typename boost::graph_traits<Graph>::vertex_descriptor vertex_t;
    //typedef typename std::pair<boost::graph_traits<Graph>::edge_descriptor, bool> edge_t;
    typedef typename boost::graph_traits<Graph>::in_edge_iterator in_edge_it;
    typedef typename boost::graph_traits<Graph>::out_edge_iterator out_edge_it;

    typedef typename boost::graph_traits<Graph>::vertex_iterator vertex_iter;
    typedef typename boost::graph_traits<Graph>::edge_iterator edge_iter;
    typedef typename boost::property_map<Graph, boost::vertex_index_t>::type IndexMap;
    typedef typename std::pair<vertex_iter, vertex_iter> vertex_pair_iterators;
    typedef typename std::pair<in_edge_it, in_edge_it> edge_pair_iterators;
    typedef typename std::pair<out_edge_it, out_edge_it> edge_pair_iterators_out;
    typedef typename boost::graph_traits<Graph>::adjacency_iterator adjacency_it;
//    typedef typename boost::graph_traits<UndirectedGraph>::adjacency_iterator undirected_adjacency_it;
//    typedef typename Graph::inv_adjacency_iterator inv_adjacency_iterator;

/*Constructors*/
    Network(){}
    Network(const std::string toy_network_file_name, RandomWrapper & m_r/*, ThresholdFunction * t_func*/,const std::string & xml_nodes);
    Network(const matrix<double> & adjacency_matrix, const std::vector<std::string> & node_names, RandomWrapper & m_r );
//    Network( std::vector<Vertex > vertices_set, bool full_connections );
//    Network( std::vector<Vertex > vertices_set );//full connections


/*Getters*/
    Graph getNetworkObject() const { return network;}
    void get_adjacency_matrix(matrix<double> &adjacency_matrix, std::vector<std::string> & node_names) const;
    inline int get_maximum_clique_number()const {return bron_kerbosch_clique_number(network);}
    void get_filtered_graph();
    vertex_t get_vertex_t_from_vertex(const std::string & v_id)const;
    ExperimentalOutput extract_time_series(bool reset_state_maps);

    bool get_vertex_by_id(const std::string name, _Vertex  & v)const;

    void reset_state_map();
    void print_network(std::ostream & os)const;
    void test_print() const
    {
        std::cout << "Vertices" << NEW_LINE;
        for(std::pair<vertex_iter, vertex_iter> vp = vertices(network); vp.first!=vp.second;++vp.first)
        {
            network[*vp.first].test_print();
        }
        std::cout << "Edges " << NEW_LINE;
        for(auto ep = edges(network); ep.first!= ep.second;++ep.first)
        {
            network[*ep.first].test_print();
        }
    }


    //friend std::ostream& operator<<(std::ostream& out, const Network& n);

    std::vector<std::string> DF_search(const std::string& source_node_id, const std::string & target_node_id )const;
    std::vector<std::vector<std::string> > DF_search_all_results(const std::string source_node_id, const std::string target_node_id )const;
    std::vector<std::string> DF_search(const vertex_t& v_t_source, const vertex_t & v_t_target)const;
    void DF_search_all_results(const vertex_t v_t_source, const vertex_t v_t_target, std::vector<std::vector<std::string> > & result,
                               std::vector<std::string> partial = std::vector<std::string>() )const;
    void foo();
    void determine_node_value(const vertex_t & v , int t, const Experiment & e,const std::unordered_map<std::string,double> & met_map );
    void perform_experiment(const Experiment & e, const int final_time = 0, const float ratio = 0.,
                            const int initialization_type = 0, const std::unordered_map<std::string,double> & initial_values = std::unordered_map<std::string,double>(),
                            const int iteration=0, const std::unordered_map<std::string,double> & met_map = std::unordered_map<std::string, double>(),
                            const bool allow_initialization=true, const bool check_for_no_more_changes = true);
    void initialize_node_values(const int initialization_type, const std::unordered_map<std::string,double> & initial_values, const float ratio);

    void foobar()const;
    bool no_more_changes()const;
    void random_initialization_node_values(const float ratio= 0.5, const bool only_genes = true);
    void initialization_node_values_with_map( const std::unordered_map<std::string, double> & initial_values);
    void print_adjacency_matrix()const;

    template<class _Visitor>
    void visit_edges(_Visitor v)
    {
        for(std::pair<edge_iter,edge_iter> edge_iterator_pair = edges(network); edge_iterator_pair.first!=edge_iterator_pair.second; ++edge_iterator_pair.first)
            v(network[*edge_iterator_pair.first]);
    }
    template< typename _Visitor>
    void visit_vertices(_Visitor v)
    {
        for(std::pair<vertex_iter, vertex_iter> vp = vertices(network); vp.first!=vp.second;++vp.first)
        {
            v(network[*vp.first]);
        }
    }
    int greedy_colouring()const;

    void set_initial_conditions(const Experiment & e);
    int add_vertex_if_necessary(std::unordered_map<_Vertex, vertex_t, MyVertexHash<_VertexOutputType> > & v_map
                                                            , _Vertex v, vertex_t & vt);
    void print_network ( std::ostream& os );
    template<typename ParameterContainer>
    void update_parameters_of_network(const ParameterContainer & params);
    void set_metabolite_levels(const typename std::unordered_map<std::string, double> & met_map, const int iteration, const bool update_t = false);
private:
    struct VertexUpdater
    {
        VertexUpdater(){}
        double operator()(const vertex_t & v,std::pair<in_edge_it,in_edge_it> ep, Graph rh_network, int t)
        {
            double g_i = 0.;

            for (ep;ep.first!=ep.second;++ep.first)
            {
                vertex_t source_v = source(*ep.first, rh_network);
                double g_j = rh_network[source_v].get_value_at_time_t(t-1);
                double w_ij = rh_network[*ep.first].get_weight();
                g_i += w_ij*g_j;
            }
            g_i += rh_network[v].get_constant_term();
            return g_i;
        }
    };
    Graph network;
    RandomWrapper random_number_generator;
    int number_of_vertices = 0 ;
    int number_of_edges = 0 ;
    std::uniform_real_distribution<double> uni_d;
    std::default_random_engine generator;
//    ThresholdFunction * thr_func;
    VertexUpdater vertex_updater;

};


/*Implementation*/
/************************************get_adjacency_matrix********************************/
template<class VertexOutputType>
void Network<VertexOutputType>::get_adjacency_matrix(matrix<double> & adjacency_matrix, std::vector<std::string> & node_names ) const
{
    assert(node_names.size()==0);
    assert(adjacency_matrix.size1()==0);
    assert(adjacency_matrix.size2()==0);
    std::cout << "Adjacency matrix" << NEW_LINE;
//    MatrixGraph mg(num_vertices(network));
    adjacency_matrix = zero_matrix<double>(num_vertices(network), num_vertices(network)+1) ;

    size_t amount_of_parameters = 0;
    for(vertex_pair_iterators vp = vertices(network); vp.first!=vp.second; ++vp.first )
    {
        node_names.push_back(network[*vp.first].get_name());

    }

    for(vertex_pair_iterators vp = vertices(network); vp.first!=vp.second; ++vp.first )
    {
        size_t row_index = get_index(node_names, network[*vp.first].get_name());
        for(edge_pair_iterators_out ei_out = out_edges(*vp.first, network); ei_out.first != ei_out.second;++ei_out.first )
        {
            vertex_t edge_target = target(*ei_out.first, network);
            size_t column_index = get_index(node_names, network[edge_target].get_name() );
            double weight = network[*ei_out.first].get_weight();
//            std::cout << "Weight "<< weight <<NEW_LINE;
            adjacency_matrix(column_index, row_index) = weight;
            amount_of_parameters++;
        }
        adjacency_matrix(row_index, node_names.size()) = network[*vp.first].get_constant_term();
    }

    amount_of_parameters += num_vertices(network);
    // TODO:
    // Weights of edges
    // add column with constant terms
    /* e.g. for TEST_NETWORK_CONT, we get following adjacency_matrix:
     *      A       B       C       D       ct
     * A    0.05    0       0       0       -0.004      //meaning A(t+1) = 0.05 A(t) + 0*B(t) + 0*C(t) + 0 * D(t) -0.004
     * B    -0.05   0.05    0.51    0       -0.0003
     * C    0.23    0.66    0.01    0       -0.012
     * D    0       -0.21   0       0.01    -0.003
     *
     * */
    //print_matrix_with_names(std::cout, adjacency_matrix, node_names);
    /*rows are outgoing edges, columns are incoming edges
     * e.g. B->C,
     *      C->C
     *      C->A
     *      A   B   C
     * A    0   0   0   //No outgoing edges from A to other node
     * B    0   0   1   //B->C
     * C    1   0   1   //C->A, C->C
    */
    /* Equivalent Adjacency list -> outgoing edges can be obtained in O(1)
     * A:
     * B: C
     * C: A, C
     *
     *
     *
     * */
}
/************************************print_adjacency_matrix********************************/
template<class VertexOutputType>
void Network<VertexOutputType>::print_adjacency_matrix() const
{
    std::cout << "Adjacency matrix" << NEW_LINE;
    matrix<double> adjacency_matrix = zero_matrix<double>(num_vertices(network), num_vertices(network)+1) ;
    size_t amount_of_parameters = 0;
    std::vector<std::string> node_names;
    for(vertex_pair_iterators vp = vertices(network); vp.first!=vp.second; ++vp.first )
    {
        node_names.push_back(network[*vp.first].get_name());

    }


    for(vertex_pair_iterators vp = vertices(network); vp.first!=vp.second; ++vp.first )
    {
        size_t row_index = get_index(node_names, network[*vp.first].get_name());
        for(edge_pair_iterators_out ei_out = out_edges(*vp.first, network); ei_out.first != ei_out.second;++ei_out.first )
        {
            vertex_t edge_target = target(*ei_out.first, network);
            size_t column_index = get_index(node_names, network[edge_target].get_name() );
            double weight = network[*ei_out.first].get_weight();
            adjacency_matrix(column_index, row_index) = weight;
            amount_of_parameters++;
        }
        adjacency_matrix(row_index, node_names.size()) = network[*vp.first].get_constant_term();
    }

    std::cout << adjacency_matrix << NEW_LINE;
}

/************************************foobar *********************************************/
template<class VertexOutputType>
void Network<VertexOutputType>::foobar()const
{
    for(std::pair<vertex_iter, vertex_iter> vp = vertices(network); vp.first!=vp.second;++vp.first)
    {
        network[*vp.first].foo();
    }
}
/***********************************perform_experiment*************************************/
template<class VertexOutputType>
void Network<VertexOutputType>::perform_experiment(const Experiment & e,const int final_time, const float ratio,
                                 const int initialization_type, const std::unordered_map<std::string,double> & initial_values,const int iteration
                               , const std::unordered_map<std::string,double> & met_map, const bool allow_initialization, const bool check_for_no_more_changes )
{

    if(iteration == 0 )
    {

        if (allow_initialization)
        {
            std::cout << "Initializing nodes" << NEW_LINE;
            std::cout << initial_values << NEW_LINE;
            initialize_node_values(initialization_type, initial_values, ratio); //How to initialize non-experimental factors
        }
        set_initial_conditions(e);//initial_conditions = experiment

    }

    for(int time = iteration; time < iteration+final_time; ++time)
    {
        std::cout << "Setting Metabolite levels " << NEW_LINE;
        set_metabolite_levels(met_map, time, false);//TODO: make sure that assumption that metabolites are stable during gene regulation can hold.
        std::cout << "Metabolite levels set " << NEW_LINE;
        std::cout << "Time " << time << NEW_LINE;
        for(std::pair<vertex_iter, vertex_iter> vp = vertices(network); vp.first!=vp.second;++vp.first)
        {
            //std::cout <<"gene" << network[*vp.first].get_name() << NEW_LINE;
            determine_node_value(*vp.first, time, e, met_map);
        }
        if(check_for_no_more_changes && no_more_changes() )
        {
            INFO_WARNING("BREAKING!!!");
            break;
        }
        INFO("DEBUG:: " << time << " <?  "<<iteration+final_time );
    }

    return;
}

/******************************************* initialize_node_values ******************************************/
template< class VertexOutputType >
void Network<VertexOutputType>::initialize_node_values(const int initialization_type, const std::unordered_map<std::string,double> & initial_values, const float ratio)
{
    //0 or unspecified -> all 0, 1->random, controlled by parameter ratio, 2-> dictionary file
    switch(initialization_type)
    {

    case 0: //by default is already initialized to 0, initialize all to 0
        break;
    case 1: // random initialization based on ratio
        random_initialization_node_values(ratio);
        break;
    case 2: //initialize to value in map, if not specified -> 1
        initialization_node_values_with_map(initial_values);
        break;
    default:
        std::cout << "Default " << NEW_LINE;
        break;
    }
}

/********************************************no_more_changes*********************************************/
template<class VertexOutputType >
bool Network<VertexOutputType>::no_more_changes()const
{

    std::pair<vertex_iter,vertex_iter> vp ;
    for(vp = vertices(network); vp.first!= vp.second;++vp.first)
    {
         //only initialize genes
        if( network[*vp.first].check_still_changes_occurring() )
            return false;

    }
    return true;
}



/****************************************Clique_printer***********************************************/

template <typename OutputStream>
struct Clique_printer
{
    Clique_printer(OutputStream& stream)
        : os(stream)
    { }

    template <typename Clique, typename Graph>
    void clique(const Clique& c, const Graph& g)
    {
        // Iterate over the clique and print each vertex within it.
        typename Clique::const_iterator i, end = c.end();
        os << "New clique " ;LoggerSingleton::Instance()->info_print(os);
        for(i = c.begin(); i != end; ++i) {
            os << g[*i].name << " ";
        }
        os ;LoggerSingleton::Instance()->info_print(os);
    }
    OutputStream& os;
};




/*******************************print_network****************************************/
template<class VertexOutputType>
void Network<VertexOutputType>::print_network(std::ostream & os)const
{
//    boost::write_graphviz(os, network,
//    boost::make_label_writer(boost::get(&Vertex ::name, network)),
//   // boost::make_label_writer(boost::get(&Edge::get_name, network))
//    boost::make_label_writer(boost::get(&Edge::name, network))
//                          );

}
/******************************************************DF_search***********************/
template< class VertexOutputType>
std::vector<std::string> Network<VertexOutputType>::DF_search(const std::string& source_node_id, const std::string & target_node_id )const
{
    vertex_t v_t_source = get_vertex_t_from_vertex(source_node_id);
    vertex_t v_t_target = get_vertex_t_from_vertex(target_node_id);
    std::vector<std::string> o = DF_search(v_t_source, v_t_target);
    std::reverse(o.begin(),o.end());
    std::stringstream os; os <<"DFS (" << source_node_id <<","<<target_node_id<<"): " << o ;LoggerSingleton::Instance()->info_print(os);
    return o;
}
/*****************************************DF_search_all_results********************************/
template< class VertexOutputType>
std::vector<std::vector<std::string> > Network<VertexOutputType>::DF_search_all_results(const std::string source_node_id, const std::string target_node_id )const
{
    vertex_t v_t_source = get_vertex_t_from_vertex(source_node_id);
    vertex_t v_t_target = get_vertex_t_from_vertex(target_node_id);
    std::vector<std::vector<std::string> > result;
    DF_search_all_results(v_t_source, v_t_target,result);
    //std::reverse(o.begin(),o.end());
    if( !result.empty() && v_t_source != v_t_target )
    {
        std::stringstream os; os <<"DFS (" << source_node_id <<","<<target_node_id<<"): " << result ;LoggerSingleton::Instance()->info_print(os);
    }
    return result;
}

/****************************************DF_search*****************************************/
template< class VertexOutputType>
std::vector<std::string> Network<VertexOutputType>::DF_search(const vertex_t& v_t_source, const vertex_t & v_t_target)const
 {
     //get vertex_t

    std::vector<std::string> v;

     if( v_t_source == v_t_target)
     {
         std::string s = network[v_t_source].get_name();
         v.push_back(s);
         return v;

     }
     else
     {
         int i = 0;
         for( edge_pair_iterators_out epio = out_edges(v_t_source, network); epio.first!=epio.second;++epio.first)
         { ++i;
             std::vector<std::string> temp_v = DF_search(target(*epio.first, network), v_t_target);
             if( !temp_v.empty() )
             {
                 temp_v.push_back(network[source(*epio.first, network)].get_name());
                 return temp_v;
             }
         }
         return v;
     }
 }


/*******************************************************DF_search_all_results*********************************************/

template<class VertexOutputType>
void Network<VertexOutputType>::DF_search_all_results(const vertex_t v_t_source, const vertex_t v_t_target, std::vector<std::vector<std::string> > & result,
                            std::vector<std::string> partial )const
 {
     //get vertex_t

    std::vector<std::string> v;
    partial.push_back(network[v_t_source].get_name());

     if( v_t_source == v_t_target)
     {
         result.push_back(partial);
     }
     else
     {
         int i = 0;
         for( edge_pair_iterators_out epio = out_edges(v_t_source, network); epio.first!=epio.second;++epio.first)
         {
             if( !contains_duplicate_elements(partial))
             {
                 DF_search_all_results(target(*epio.first, network), v_t_target, result, partial);
             }
         }
     }
 }

/*****************************************************foo****************************************************/
template<class VertexOutputType>
void Network<VertexOutputType>::foo()
{   std::vector<std::string> targets; targets.push_back("gluc_depl");
    targets.push_back("stationary_phase");targets.push_back("aa_starvation");
    targets.push_back("nfcs");targets.push_back("dys_mit");
    targets.push_back("CSRE");targets.push_back("glucoseext");

    for(std::pair<vertex_iter, vertex_iter> vp = vertices(network); vp.first!=vp.second;++vp.first)
    {
        for( std::vector<std::string>::const_iterator it = targets.begin(); it!= targets.end(); ++it)
        {
            DF_search_all_results(network[*vp.first].get_name(), *it );
        }
    }
}
/***************************************************get_vertex_t_from_vertex************************************/
template<class VertexOutputType>
typename Network<VertexOutputType>::vertex_t Network<VertexOutputType>::get_vertex_t_from_vertex(const std::string & v_id)const
 {
     std::pair<vertex_iter, vertex_iter> vp;
     for (vp = vertices(network); vp.first != vp.second; ++vp.first)
     {
         if ( network[*vp.first] == v_id) //test
         {
             return *vp.first;
         }
     }
     std::stringstream os; os << v_id << " not found in graph!" ;LoggerSingleton::Instance()->info_print(os);
     return *vp.second;
 }

/*********************************************set_initial_conditions*********************************************/

template< class VertexOutputType>
void Network<VertexOutputType>::set_initial_conditions(const Experiment & e)
{
    std::pair<vertex_iter,vertex_iter> vp ;
    std::map<std::string,double> experimental_nodes_ids = e.get_initial_conditions();
    for(vp = vertices(network); vp.first!= vp.second;++vp.first) //iterate over vertices in network
    {
        //iterate over experimental nodes
        for(std::map<std::string,double> ::iterator id_iterator = experimental_nodes_ids.begin(); id_iterator!=experimental_nodes_ids.end();++id_iterator )
        {
            if(network[*vp.first] == id_iterator->first) //if experimental node == node in network ->
            {
                network[*vp.first].set_initial_condition(id_iterator->second); //adapt initial value
                experimental_nodes_ids.erase(id_iterator); //erase experimental condition
            }
        }
    }
}


/**************************************************initialization_node_values_with_map******************************/

template<class VertexOutputType>
void Network<VertexOutputType>::initialization_node_values_with_map( const std::unordered_map<std::string, double> & initial_values)
{
    std::pair<vertex_iter, vertex_iter> vp;
    for( vp = vertices(network); vp.first != vp.second; ++vp.first)
    {
        std::unordered_map<std::string, double>::const_iterator value_it = initial_values.find(network[*vp.first].get_name());
        if(  value_it != initial_values.end() )
        {
            network[*vp.first].initialize_state_map(my_conversion<VertexOutputType, double>(value_it->second));
            std::cout << "initializing " << network[*vp.first].get_name() << " to " << my_conversion<VertexOutputType, double>(value_it->second) << NEW_LINE;
        }
    }
}
/**************************************************add_vertex_if_necessary********************************************/
template<class VertexOutputType>
int Network<VertexOutputType>::add_vertex_if_necessary(std::unordered_map<_Vertex, vertex_t, MyVertexHash<VertexOutputType> > & v_map
                                                        , _Vertex v, vertex_t & vt)
{
    if( v_map.find(v) == v_map.end() )
    {
        vt = boost::add_vertex(v, network);
        v_map[v] = vt;
        return 0;
    }
    else
    {
        vt = v_map.find(v)->second;
        return 1;
    }
}
/*********************************************update_parameters_of_network******************************************/
template< typename VertexOutputType>
template< typename ParameterContainer >
void Network<VertexOutputType>::update_parameters_of_network(const ParameterContainer & params)
{
    std::cout << "Fill in update_parameters_of_network in network.hpp" << NEW_LINE;
}

/*********************************************    void reset_state_map();*******************************************/
template<class VertexOutputType>
void Network<VertexOutputType>::reset_state_map()
{
    std::pair<vertex_iter, vertex_iter> vp;
    for( vp = vertices(network); vp.first != vp.second; ++vp.first)
    {
        network[*vp.first].initialize_state_map(0);
    }
}



#endif
