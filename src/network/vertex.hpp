#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <map>

#include <boost/functional/hash.hpp>
#include <iostream>
#include "src/general/maths/sigmoid.hpp"
#include "src/general/maths/RandomWrapper.hpp"

#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include "src/general/compile_configuration.hpp"
#include "src/io/xml_reader.hpp"

#define LOWER_RANGE_LOGIC 0.0
#define UPPER_RANGE_LOGIC 1.0
enum VertexType
{
    complex,
    condition,
    drug,
    gene,
    growthphase,
    metabolite,
    reaction,
    unknown
};

enum Version
{
    logical_or,
    sumV
};

std::string get_string_for_vertex_type(const VertexType v );

//TODO: link with nodetypes, not all of them need noise, for instance does not make sense for e.g. condition
std::unordered_map<std::string, VertexType> initialize_vertex_type_map(const std::string & vertex_type_file);
template <class _OutputType>
struct Vertex
{
public:
//    Vertex(std::string m_name);
//    Vertex(std::string m_name, bool m_is_reaction );
//    Vertex(std::string m_name, bool m_is_reaction, ThresholdFunction * m_t_func  );
    Vertex(std::string m_name, bool m_is_reaction=false/*, ThresholdFunction *m_t_func*/,const double rh_constant_term = 0. );
    Vertex();

//    void update_value(const double m_value, RandomWrapper & noise_generator, const int t , const bool is_fixed_node);
//    void update_value(const std::vector<Vertex > & parents, const std::vector<bool> & inhibitions
//                      , RandomWrapper & noise_generator, const int t);
   // void foo()const;
    std::string get_name() const {return name;}
    bool get_fixed()const {return fixed;}
    //void intervention( bool state ){ state_map[0] = state; fixed = true;}
    //void reset() {fixed = false;}
    //inline bool operator==(const Vertex & rhs){return get_id() == rhs.get_id();}
    inline bool operator==(const Vertex & a)const {/*std::cout << "comparison :" << get_name() << a.get_name() << NEW_LINE;*/
                                                                return get_name() == a.get_name(); }
    inline bool operator!=( const Vertex & rhs ){return !(*this == rhs);}

    inline bool operator==(Vertex & a)const {return get_name() == a.get_name(); }
    inline bool operator!=( Vertex & rhs )const{return !(*this == rhs);}



    inline bool operator==(const std::string & vertex_id)const {return get_name() == vertex_id; }

    inline bool operator!=(const std::string & vertex_id)const { return !(*this==vertex_id); }

    inline bool operator() (const Vertex & v) const {return ( name == v.get_name() );}


    friend std::ostream& operator<<(std::ostream& os,const Vertex& v){
        os <<  v.get_name() <<"(" << v.get_id() << ")"; return os;}
    bool is_checked_at_time_step_t(int t)const{ return discrete_t == t;}
    inline bool is_reaction_vertex()const { return is_reaction_node; }

    static void set_vertextype_map(const std::string & vertex_type_file);
    int update_maps(const std::string &);
    VertexType get_vertex_type()const;
    VertexType get_vertex_type_by_name(const std::string & m_name)const;

    inline bool operator<(const Vertex v)const {return name < v.get_name();}
    inline int get_id()const{return id;}



    static void set_version(Version rh_version){Vertex::version = rh_version;}
    static Version get_version(){return version;}
    static void add_to_vertextype_map(const std::string name ,const std::string type);

    static std::unordered_map<std::string,VertexType> get_vertex_map(){return vertextype_map;}
    static void set_limit_range(const bool rh_limit_range){Vertex::limit_range = rh_limit_range;}
    /*Templated functions*/

    std::pair<std::string, std::map<int,_OutputType>> extract_time_series(bool reset);
    void initialize_state_map();
    void initialize_state_map(const _OutputType m_value);
    void set_initial_condition(const _OutputType m_value);
    bool check_still_changes_occurring()const;
    void update_value(_OutputType m_value, RandomWrapper & noise_generator, const int t, const bool is_fixed_node, const bool update_t = true );
    void update_value(const std::vector<Vertex> & parents, const std::vector<bool> & inhibitions,
                                                   RandomWrapper & noise_generator, const int t);
    _OutputType previous_value()const;
    double get_constant_term()const{return constant_term;}
    inline _OutputType get_value_at_time_t(const int t )const;
    inline void set_constant_term(const double rh_constant_term){ constant_term = rh_constant_term;}
    void test_print()const{std::cout << name << ",c-t : " << constant_term; }

private:
    VertexType vertex_type;
    bool fixed;
    bool is_reaction_node;
//    ThresholdFunction * t_func;
    std::string name;
    int discrete_t;
    static std::unordered_map<std::string,VertexType> vertextype_map;
    static std::unordered_map<std::string, int> map_name_id;
    static std::unordered_map<int, std::string> map_id_name;
    int id;
    static int next_id;
    /*templated parameters*/
    double constant_term = 0.;
    std::map<int, _OutputType> state_map;
    static Version version;
    static bool limit_range;
};

template<class OutputType>
int Vertex<OutputType>::next_id = 0;
template<class OutputType>
std::unordered_map<std::string, int> Vertex<OutputType>::map_name_id;
template<class OutputType>
std::unordered_map<int, std::string> Vertex<OutputType>::map_id_name;
template<class OutputType>
Version Vertex<OutputType>::version = logical_or;
template<class OutputType>
std::unordered_map<std::string,VertexType> Vertex<OutputType>::vertextype_map;
template<class OutputType>
bool Vertex<OutputType>::limit_range = false;

template<class OutputType>
struct MyVertexHash {
  std::size_t operator()(const Vertex<OutputType>& x) const { return boost::hash<std::string>()(x.get_name()); }
};

namespace std
{
template<class OutputType>
struct hash<Vertex<OutputType> >
{
   typedef Vertex<OutputType> argument_type;
   typedef size_t result_type;
    result_type operator () (const argument_type& x) const
    {
       return boost::hash<int>()(x.get_id());
    }
};

}

/************************************
 * **********************************
 * **********************************
 * Implementation templated functions
 */

/*****************************extract time_series*********************************/
template<class OutputType>
std::pair<std::string, std::map<int,OutputType>> Vertex<OutputType>::extract_time_series(bool reset)
{
    std::pair<std::string, std::map<int,OutputType>> temp;
    if( ! is_reaction_vertex() )
        temp =std::pair<std::string, std::map<int,OutputType> > (name, state_map);
    if( reset )
        initialize_state_map();
    return temp;
}

/**************************initialize_state_map with param**************************/

template<class OutputType>
void Vertex<OutputType>::initialize_state_map(const OutputType m_value)
{
    state_map.clear();
    state_map.insert(std::pair<int,OutputType>(-1, m_value ) );//TODO: add noise? Start at default values defined by Beta?
    discrete_t = -1;
}


/**********************set_initial_condition********************/
template<class OutputType>
void Vertex<OutputType>::set_initial_condition(const OutputType m_value)
{
    INFO_VALUE_UPDATE("Initial condition set: " << get_name() << " to " << m_value << " !");
    state_map[-1] = m_value;
}

/**********************check_still_changes_occurring***********/
template<class OutputType>
bool Vertex<OutputType>::check_still_changes_occurring()const
{
    bool still_changing = true;
    if(state_map.size() > 2)
    {
        OutputType last_value = get_value_at_time_t(discrete_t);
        OutputType previous_value = get_value_at_time_t(discrete_t-1);

        if( last_value == previous_value)
            still_changing = false;
    }
    return still_changing;
}


/****************************get_value_at_time_t*********************************/
template<class OutputType>
inline OutputType Vertex<OutputType>::get_value_at_time_t(const int t )const
{
    if(state_map.find(t) != state_map.end() )
    {
//        return convert_value_to_new_range(t_func->lower_y_range(),t_func->upper_y_range(),state_map.find(t)->second,LOWER_RANGE_LOGIC,UPPER_RANGE_LOGIC);
          return state_map.find(t)->second;
    }
    else
    {
        INFO_ERROR("ERROR "   << state_map << " time " << t);
        std::cout << "Error " << state_map << NEW_LINE;
        exit(1);
    }
}
/**************************previous_value *****************************************/
template< class OutputType>
OutputType Vertex<OutputType>::previous_value()const
{
    unsigned int temp = discrete_t+2;
    assert(temp==state_map.size());
    return state_map.find(discrete_t)->second;
}

//---//



/*****************************get_vertex_type***********************************/

template<class OutputType>
VertexType Vertex<OutputType>::get_vertex_type()const
{
    std::unordered_map<std::string, VertexType>::const_iterator it = vertextype_map.find(name);
    if( it == vertextype_map.end() )
    {
        INFO_WARNING("No vertextype found for " << name << "!");
        std::cout << "MAP"<< NEW_LINE <<vertextype_map << NEW_LINE;
        return unknown;
    }
    return it->second;
}

/*****************************get_vertex_type_by_name***********************************/

template<class OutputType>
VertexType Vertex<OutputType>::get_vertex_type_by_name(const std::string & m_name)const
{
    std::unordered_map<std::string, VertexType>::const_iterator it = vertextype_map.find(m_name);
    if( it == vertextype_map.end() )
    {
        INFO_WARNING("No vertextype found for " << m_name << "!");
        return unknown;
    }
    return it->second;
}



/************************update_maps***************************************************/

template<class OutputType>
int Vertex<OutputType>::update_maps(const std::string & name)
{
    std::unordered_map<std::string, int>::const_iterator id_iterator = Vertex<OutputType>::map_name_id.find(name);
    int m_id;
    if( id_iterator == Vertex<OutputType>::map_name_id.end() )
    {
        //not present yet
        m_id = Vertex<OutputType>::next_id++;
        Vertex<OutputType>::map_name_id[name] = m_id;
        Vertex<OutputType>::map_id_name[id] = name;
    }
    else
    {
        //it is present, use id from map
        m_id = id_iterator->second;
    }
    return m_id;
}

/********************************************Constructors***************************/


template<class OutputType>
Vertex<OutputType>::Vertex()
{
//    id = Vertex::next_id++;
//    std::cout<< "Unknown vertex created" << NEW_LINE;
}
//template<class OutputType>
//Vertex<OutputType>::Vertex(std::string m_name)
//{
//    name= m_name;
////    id = Vertex::next_id++;
//    id = Vertex<OutputType>::update_maps(name);
////    Vertex::update_maps(name, id);
//}

//template<class OutputType>
//Vertex<OutputType>::Vertex(std::string m_name, bool m_is_reaction )
//{
//    name = m_name;
//    is_reaction_node = m_is_reaction;
//    //t_func = __ThresholdFunction(1.0,0.0);
//    discrete_t = -1;
//    initialize_state_map();
//    id = Vertex<OutputType>::update_maps(name);

////    id = Vertex::next_id++;
////    Vertex::update_maps(name, id);
//}

//template<class OutputType>
//Vertex<OutputType>::Vertex(std::string m_name, bool m_is_reaction, ThresholdFunction * m_t_func  )
//{
//    name = m_name;
//    is_reaction_node = m_is_reaction;
//    t_func = m_t_func;
//    discrete_t = -1;
//    initialize_state_map();
//    id = Vertex<OutputType>::update_maps(name);
//    constant_term = 0.;
////    id = Vertex::next_id++;
////    Vertex::update_maps(name, id);
//}
template<class OutputType>
Vertex<OutputType>::Vertex(std::string m_name, bool m_is_reaction/*, ThresholdFunction *m_t_func*/,const double rh_constant_term )
{
    name = m_name;
    is_reaction_node = m_is_reaction;
//    t_func = m_t_func;
    discrete_t = -1;
    initialize_state_map();
    id = Vertex<OutputType>::update_maps(name);
    constant_term = rh_constant_term;

}

/***************************set_vertextype_map***************************************/
template<typename OutputType>
void Vertex<OutputType>::set_vertextype_map(const std::string &vertex_type_file)
{
    vertextype_map = initialize_vertex_type_map(vertex_type_file);
}
/*************************add_to_vertextype_map*************************************/
template<typename OutputType>
void Vertex<OutputType>::add_to_vertextype_map(const std::string name ,const std::string str_type)
{
    VertexType type;
    if(str_type == "gene")
        type = gene;
    else if(str_type == "cmpl")
        type = complex;
    else if (str_type == "cond")
        type = condition;
    else if (str_type == "met" )
        type = metabolite;
    else if (str_type == "drug" )
        type = drug;
    else
        type = unknown;
    if(Vertex::vertextype_map.find(name) == Vertex::vertextype_map.end())
        Vertex::vertextype_map[name]=  type;

}

#endif
