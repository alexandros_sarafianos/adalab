#ifndef VISITORS_HXX
#define VISITORS_HXX
#include <iostream>
#include "src/network/edge.hpp"
#include "src/general/compile_configuration.hpp"
#include "src/network/vertex.hpp"


template< class Type>
struct Printer
{
    Printer(){}
    void operator()(const Type & example ){std::cout << example << NEW_LINE;}
};

//struct Time_Series_Extractor
//{
//    Time_Series_Extractor(){}
//    std::map<int,double> operator()(const Vertex & v){v.extract_time_series(true);}//bool reset = true
//};
#endif
