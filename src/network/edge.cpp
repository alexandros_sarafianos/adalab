#include <src/network/edge.hpp>

Edge::Edge(const std::string & v1,const std::string & v2, const bool m_inhib )
{
    name = v1+ "_" + v2;
    if( m_inhib)
        inf = Inhibition;
    else
        inf = Activation;
    id = Edge::next_id++;
    assert(stimulates()==!inhibits());
}
Edge::Edge( const std::string & rh_weight )
{
    id = Edge::next_id++;
    name = std::to_string(id);
    if( rh_weight == "NA")
    {
        //TO dO std::cout << "DO something, random initialization? Found value: "<< weight << NEW_LINE;
        weight = 0;
        assert(weight==0);
    }
    else
    {
        weight = convertStringToType<double>(rh_weight);
    }
}
Edge::Edge(const double rh_weight)
{
    weight = rh_weight;
}

std::ostream& operator<<(std::ostream& os, const Edge & e)
{

    os << "Edge '" << e.name <<"' relation " << e.stimulates();
//    os << "Edge: influence("<< e.inf <<
//          "), influences(" <<e.influences <<")";
    return os;
}

Edge::Edge(bool m_inhib)
{
    if( m_inhib)
        inf = Inhibition;
    else
        inf = Activation;
    id = Edge::next_id++;
}
//Edge::Edge(bool m_inhib, const std::map<Vertex, Influence> & m_influences)
//{
//    if( m_inhib)
//        inf = Inhibition;
//    else
//        inf = Activation;
//    id = Edge::next_id++;
//    influences = m_influences;
//}
//Influence Edge::get_influence_of_vertex_in_hyperedge(const Vertex & v)const
//{
//    std::map<Vertex, Influence>::const_iterator it = influences.find(v);
//    if(it == influences.end())
//        return Unknown;
//    else
//        return it->second;
//}
//bool Edge::operator==(const Edge & e ) const
//{
//    std::cout << "Comparing edges " << NEW_LINE;
//    bool ok1 = inf == e.inf;
//    if( !ok1 )
//        std::cout << "INF not equal " << inf << "-" <<e.inf << NEW_LINE;
//    bool ok2 = equal_maps<Vertex,Influence>(influences, e.influences);
//    if( !ok2 )
//        std::cout << "influences not equal " << influences << NEW_LINE << e.influences << NEW_LINE;
//    return ok1 && ok2 ;
//    return ok1;
//}
//bool Edge::operator<(const Edge & e) const
//{
//    if( inf < e.inf )
//    {
//        return true;
//    }
//    else
//    {
//        if(inf > e.inf )
//        {
//            return false;
//        }
//        else
//        {
//            return influences < e.influences;
//        }
//    }
//}
int Edge::next_id = 0;


