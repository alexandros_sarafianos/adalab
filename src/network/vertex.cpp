
#include "src/network/vertex.hpp"
/*********************************initialize_vertex_type_map**************************/
std::unordered_map<std::string, VertexType> initialize_vertex_type_map(const std::string & vertex_type_file)
{
    std::string alterable_node_types[] = {"complex", "condition", "drug","gene", "growthphase" ,"metabolite"};
    VertexType vertex_types[] = {complex, condition, drug, gene, growthphase , metabolite};
    XML_reader xml_reader(vertex_type_file);
    std::unordered_map<std::string,VertexType> type_map;
    for( int cntr = 0; cntr <= 5; ++cntr)
    {
        std::vector<std::string> elements;
        xml_reader.get_xml_parsed_element_content("node_info."+alterable_node_types[cntr], elements, ",");
        for(std::vector<std::string>::const_iterator it = elements.begin();it!=elements.end();it++)
            type_map.insert(std::pair<std::string,VertexType>(*it, vertex_types[cntr]));

    }
    INFO_DEBUG("Type map; " << type_map );
    return type_map;
}


/*****************************initialize_state_map no param**********************/
template<>
void Vertex<double>::initialize_state_map()
{
   state_map.clear();
   state_map.insert(std::pair<int,double>(-1, 0. ) );//TODO: add noise? Start at default values defined by Beta?
   discrete_t = -1;
}
template<>
void Vertex<bool>::initialize_state_map()
{
    state_map.clear();
    state_map.insert(std::pair<int,bool>(-1, false ) );//TODO: add noise? Start at default values defined by Beta?
    discrete_t = -1;
}


/**********************update_value*****************************/
// double
template<>
void Vertex<double>::update_value(double m_value, RandomWrapper & noise_generator, const int t, const bool is_fixed_node, const bool update_t )
{
    double noise = noise_generator();
    double new_value= m_value;
    if( !is_fixed_node )
    {
        new_value+=noise;
    }

    if( limit_range )
    {
        if (new_value > 1 )
        {
            INFO_VALUE_UPDATE( "Limiting new_value " << new_value <<" to range [-1,1] ");
            new_value = 1.;

        }
        else
        {
            if(new_value < -1)
            {
                INFO_VALUE_UPDATE( "Limiting new_value " << new_value <<" to range [-1,1] ");
                new_value = -1.;
            }
        }
    }
    INFO_VALUE_UPDATE(name << ": Value " << m_value << " Noise " << noise << " became " << new_value << " at time " << t<< " !");
    std::cout << "Debug  " << update_t << std::endl;
    if( update_t )
    {
        state_map.insert(std::pair<int,double>( t,  new_value ));
        assert(state_map[t] == new_value);
    }
    else
        state_map[t] = new_value;
    INFO( "Testing : " << state_map[t] << "=?=" << new_value);
    if(update_t)
        ++discrete_t;
}
//bool
template<>
void Vertex<bool>::update_value(bool m_value, RandomWrapper & noise_generator, const int t, const bool is_fixed_node, const bool update_t )
{
    double noise = noise_generator();
    bool new_value= m_value;
    if(false)
        new_value = !new_value;
    INFO_VALUE_UPDATE(name << ": Value " << m_value << " Noise " << noise << " became " << new_value << " at time " << t<< " !");
    if( update_t )
    {
        state_map.insert(std::pair<int,bool>( t,  new_value )); //insert does not update if key already exists
        assert(state_map[t] == new_value);
    }
    else
    {
        state_map[t] = new_value;
    }
    INFO( "Testing : " << state_map[t] << "=?=" << new_value);
    if( update_t )
    {
        ++discrete_t;
        INFO("Discrete_t updated to " << discrete_t << " for " << get_name() );
    }
}

/*****************************update_value********************************/
//double
//template<>
//void Vertex<double>::update_value(const std::vector<Vertex<double> > & parents, const std::vector<bool> & inhibitions,
//                                               RandomWrapper & noise_generator, const int t)
//{
//    assert(t-1==discrete_t);
//    double val = 0.0;
//    if( is_reaction_node )
//    {
//        val = t_func->upper_y_range();
//        std::vector<bool>::const_iterator inhib_it = inhibitions.begin();
//        for(typename std::vector<Vertex>::const_iterator it = parents.begin(); it !=parents.end(); ++it)
//        {
//            double temp_val2 = it->get_value_at_time_t(t-1);
//            if(*inhib_it)
//                temp_val2 = t_func->negation(temp_val2);
//            val = t_func->conjunction(val, temp_val2);
//            inhib_it++;
//        }
//    }
//    else
//    {
//        assert(parents.size()==1);
//        val = parents[0].get_value_at_time_t(t-1);
//    }
//    update_value(val, noise_generator, t, false);
//    return;
//}
//bool
template<>
void Vertex<bool>::update_value(const std::vector<Vertex> & parents, const std::vector<bool> & inhibitions,
                                               RandomWrapper & noise_generator, const int t)
{
    INFO("T" << t-1 <<  " discrete time " << discrete_t);
    assert(t-1==discrete_t);
    bool val = false;

    if( is_reaction_node )
    {
        val = true;
        std::vector<bool>::const_iterator inhib_it = inhibitions.begin();
        for(typename std::vector<Vertex>::const_iterator it = parents.begin(); it !=parents.end(); ++it)
        {
            bool temp_val2 = it->get_value_at_time_t(t-1);
            if(*inhib_it)
                temp_val2 = !temp_val2;
            val =val && temp_val2;
            inhib_it++;
        }
    }
    else
    {
        if(version == logical_or)
        {
            //version 0
            std::vector<bool>::const_iterator inhib_it = inhibitions.begin();
            for(typename std::vector<Vertex>::const_iterator it = parents.begin(); it != parents.end();++it)
            {
                bool temp_val2 = it->get_value_at_time_t(t-1);
                if(*inhib_it)
                    temp_val2 = !temp_val2;
                val = val || temp_val2;
                ++inhib_it;
            }
        }
        else if(version == (Version) sumV)
        {
            std::vector<int> weights = ones<std::vector<int> >(inhibitions.size());
            std::vector<bool>::const_iterator inhib_it = inhibitions.begin();
            std::vector<int>::const_iterator weight_it = weights.begin();
            int sum = 0; //change to double if weights are floats
            std::cout << "DEBUG:: new=" << NEW_LINE;
            for(typename std::vector<Vertex>::const_iterator it = parents.begin(); it != parents.end();++it)
            {
                bool boolean_value =it->get_value_at_time_t(t-1);
                int temp_val2 = boolean_value? 1 : 0 ;
                int sign = +1;
                if(*inhib_it)
                    sign = -1;
                std::cout << sign << "*" << temp_val2 <<"+";
                sum += sign * *weight_it *  temp_val2;
                ++weight_it;
                ++inhib_it;

            }
            if( sum > 0 )
                val = true;
            else if(sum < 0)
                val = false;
            else
                val = previous_value();
            std::cout<<"="<< val << NEW_LINE;
        }
    }
    update_value(val, noise_generator, t, false);
    return;
}
/**********************check_still_changes_occurring***********/
//Double
template<>
bool Vertex<double>::check_still_changes_occurring()const
{
    bool still_changing = true;
    if(state_map.size() > 2)
    {
        double last_value = get_value_at_time_t(discrete_t);
        double previous_value = get_value_at_time_t(discrete_t-1);

        if( approximately_equal<double>(last_value,previous_value))
            still_changing = false;
    }
    return still_changing;
}





/*

void Vertex::update_value(const std::vector<Vertex> & parents, const std::vector<bool> & inhibitions,
                                               RandomWrapper & noise_generator, const int t)
{
    assert(t-1==discrete_t);
    double val = t_func->lower_y_range();

    if( is_reaction_node )
    {
        val = t_func->upper_y_range();
        std::vector<bool>::const_iterator inhib_it = inhibitions.begin();
        for(typename std::vector<Vertex>::const_iterator it = parents.begin(); it !=parents.end(); ++it)
        {
            double temp_val2 = it->get_value_at_time_t(t-1);
            if(*inhib_it)
                temp_val2 = t_func->negation(temp_val2);
            val = t_func->conjunction(val, temp_val2);
            inhib_it++;
        }
    }
    else
    {
        if(version == logical_or)
        {
            //version 0
            std::vector<bool>::const_iterator inhib_it = inhibitions.begin();
            for(typename std::vector<Vertex>::const_iterator it = parents.begin(); it != parents.end();++it)
            {
                double temp_val2 = it->get_value_at_time_t(t-1);
                if(*inhib_it)
                    temp_val2 = t_func->negation(temp_val2);
                val = t_func->disjunction(val, temp_val2);
                ++inhib_it;
            }
        }
        else if(version == sumV)
        //version1 cfr. paper "The yeast cell-cycle  is robustly designed"
        {
            std::vector<int> weights = ones<std::vector<int> >(inhibitions.size());
            std::vector<bool>::const_iterator inhib_it = inhibitions.begin();
            std::vector<int>::const_iterator weight_it = weights.begin();
            double sum = 0;
            std::cout << "DEBUG:: new=" << NEW_LINE;
            for(typename std::vector<Vertex>::const_iterator it = parents.begin(); it != parents.end();++it)
            {
                double temp_val2 = it->get_value_at_time_t(t-1);
                int sign = +1;
                if(*inhib_it)
                    sign = -1;
                std::cout << sign << "*" << temp_val2 <<"+";
                sum += sign * *weight_it *  temp_val2;
                ++weight_it;
                ++inhib_it;

            }
            if( sum > 0 )
                val = 1.;
            else if(sum < 0)
                val = 0.;
            else
                val = previous_value();
            std::cout<<"="<< val << NEW_LINE;

        }
    }
    update_value(val, noise_generator, t, false);
    return;
}
*/


/*
void Vertex::foo() const
{
    if( !is_reaction_vertex())
    {
        std::map<int, double>::const_iterator it = state_map.begin();
        std::stringstream os1; os1 << name<< ":"  << it->second ;
        ++it;
        for(it; it!= state_map.end(); ++it  )
            os1; os1 << "," << (double) it->second;
        LoggerSingleton::Instance()->info_print(os1);
    }
}*/

//void Vertex::initialize_state_map()
//{
//    state_map.clear();
//    state_map.insert(std::pair<int,double>(-1,t_func->lower_y_range() ) );//TODO: add noise? Start at default values defined by Beta?
//    discrete_t = -1;
//}



//void Vertex::initialize_state_map(const float value)
//{
//    state_map.clear();
//    state_map.insert(std::pair<int,double>(-1,value ) );//TODO: add noise? Start at default values defined by Beta?
//    discrete_t = -1;
//}


//std::pair<std::string, std::map<int,double>> Vertex::extract_time_series(bool reset)
//{
//    std::pair<std::string, std::map<int,double>> temp;
//    if( ! is_reaction_vertex() )
//        temp =std::pair<std::string, std::map<int,double> > (name, state_map);
//    if( reset )
//        initialize_state_map();
//    return temp;
//}

//inline double Vertex::get_value_at_time_t(const int t )const
//{
//    if(state_map.find(t) != state_map.end() )
//    {
////        return convert_value_to_new_range(t_func->lower_y_range(),t_func->upper_y_range(),state_map.find(t)->second,LOWER_RANGE_LOGIC,UPPER_RANGE_LOGIC);
//          return  state_map.find(t)->second;
//    }
//    else
//    {
//        INFO_ERROR("ERROR " << state_map << " time " << t);
//        exit(1);
//    }
//}


//void Vertex::set_initial_condition(const double m_value)
//{

//    state_map[-1] = m_value; //m_value is already transformed from range [0,1] to correct range [-1,1] in experimentalinfo
//}


//bool Vertex::check_still_changes_occurring()const
//{
//    bool still_changing = true;
//    if(state_map.size() > 2)
//    {
//        double last_value = get_value_at_time_t(discrete_t);
//        double previous_value = get_value_at_time_t(discrete_t-1);

//        if( last_value == previous_value)
//            still_changing = false;
//    }
//    return still_changing;
//}
/*
 *Implementation of overloaded operators./ convert_value_to_new_range<double>(-1.,1.,it->get_value_at_time_t(t-1),0.,1.));
 *
 * */
//template
//void Vertex::operator()(const Vertex & v)const
//{

//}

//double Vertex::operator()()const
//{
//    double tt = t_func(values.back());
////    if( is_reaction_vertex() )
////        tt = t_func(values.back()-1);
////    else
////        tt = t_func(values.back());
//    return (double) tt;
//}

//double Vertex::operator()(const int t ) const
//{
//    double tt = t_func(values.back());
////    if( is_reaction_vertex() )
////        tt = t_func(values.back()-1);
////    else
////        tt = t_func(values.back());
//    return (double) tt;
//}


//double Vertex::operator+(const Vertex & v)const
//{
//     return (double) this->operator()() + v();
//}

//void Vertex::operator+=(const Vertex & v)
//{
//    val += v();
//}

//double Vertex::operator*(const Vertex & v)const
//{
//    return (double) this->operator()() * v();
//}

//void Vertex::operator*=(const Vertex & v )
//{get_value_at_time_t
//    val*=v();
//}

std::string get_string_for_vertex_type(const VertexType v )

{
    std::string s;
    switch(v)
    {
    case complex:
        s = "complex";
        break;
    case condition:
        s = "condition";
        break;
    case drug:
        s = "drug";
        break;
    case gene:
        s = "gene";
        break;
    case growthphase:
        s = "growthphase";
        break;
    case metabolite:
        s = "metabolite";
        break;
    case reaction:
        s = "reaction";
        break;
    default:
        s = "unknown";

   }
    return s;
}

