#include <iostream>
#include "src/network/vertex.hpp"
#include <vector>
#include "src/general/maths/unit_step.hpp"
#define STDOUT


bool test_static_variable()
{
    Vertex<bool>::set_version( sumV );
    Vertex<bool> v;
    bool ok = Vertex<bool>::get_version() == sumV;
    if(!ok )
        std::cout << "Expected " << sumV << ", but found " << Vertex<bool>::get_version() << "!\n";
    Vertex<bool>::set_version(logical_or);
    if( ! (Vertex<bool>::get_version()==logical_or ) )
    {
        std::cout <<" Expected " << logical_or << ", but found " << Vertex<bool>::get_version() << "!\n";
        ok = false;
    }
    return ok;
}
bool test_limit_range()
{
    Vertex<double>::set_limit_range(true);
    Vertex<double> f;
    RandomWrapper noise_generator;
//<<<<<<< HEAD
////    ThresholdFunction* thresh;
//    f.update_value( -2., noise_generator, 0, false);
//    bool ok1 =  approximately_equal(f.get_value_at_time_t(0), -1.);
//    if( !ok1 )
//        std::cout << "Expected -1., but found " <<  f.get_value_at_time_t(0) << NEW_LINE;
//    f.update_value( 3., noise_generator, 1, false);
//    bool ok2 = approximately_equal(f.get_value_at_time_t(1), 1.);
//    if( !ok2 )
//        std::cout << "Expected 1., but found " <<  f.get_value_at_time_t(1) << NEW_LINE;
//    f.update_value(0.5, noise_generator, 2, false);
//    bool ok3 = approximately_equal(f.get_value_at_time_t(2), 0.5);
//    if( !ok3 )
//        std::cout << "Expected .5, but found " <<  f.get_value_at_time_t(2) << NEW_LINE;

//    if ( !(ok1 && ok2 && ok3) )
//        std::cout << " NOK : test_limit_range" <<NEW_LINE;
//=======
    ThresholdFunction* thresh;
    f.update_value( -2., noise_generator, 0, false);
    bool ok1 =  approximately_equal(f.get_value_at_time_t(0), -1.);
    if( !ok1 )
        std::cout << "Expected -1., but found " <<  f.get_value_at_time_t(0) << std::endl;
    f.update_value( 3., noise_generator, 1, false);
    bool ok2 = approximately_equal(f.get_value_at_time_t(1), 1.);
    if( !ok2 )
        std::cout << "Expected 1., but found " <<  f.get_value_at_time_t(1) << std::endl;
    f.update_value(0.5, noise_generator, 2, false);
    bool ok3 = approximately_equal(f.get_value_at_time_t(2), 0.5);
    if( !ok3 )
        std::cout << "Expected .5, but found " <<  f.get_value_at_time_t(2) << std::endl;
//>>>>>>> remotes/origin/master
    return ok1 && ok2 && ok3;
}
bool test_update_unit_v0_rxn()
{
    //Thresholdfuntion unit step, version 0,update reaction node
    Vertex<bool>::set_version(logical_or);
    std::cout << "------test_update_unit_v0_rxn()------" << NEW_LINE;
    RandomWrapper noise_generator;
//    ThresholdFunction* thresh;
//    Unit_step unit(0.,1.,0.5);
//    thresh = &unit;
    Vertex<bool> A("A", false/*, thresh*/ );
    A.set_initial_condition(true);
    Vertex<bool> B("B", false/*, thresh*/ );
    B.set_initial_condition(false);
    std::vector<Vertex<bool>> parents = {A,B};
    Vertex<bool> C("C", true/*, thresh*/ );
    C.set_initial_condition(false);
    std::vector<bool> inhibitions = {true, false};
    C.update_value(parents, inhibitions, noise_generator, 0);
    bool ok=true;
    if(C.previous_value() != false)
    {
        std::cout << "1:Expected value " << false << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }


    A=Vertex<bool>("A", false/*, thresh*/ );
    A.set_initial_condition(true);
    B=Vertex<bool>("B", false/*, thresh*/ );
    B.set_initial_condition(false);
    parents = {A,B};
    C=Vertex<bool>("C", true/*, thresh*/ );
    C.set_initial_condition(false);
    inhibitions = {false, false};
    C.update_value(parents, inhibitions, noise_generator, 0);

    if(C.previous_value() != false )
    {
        std::cout << "2:Expected value " << false << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }


    A=Vertex<bool>("A", false/*, thresh*/ );
    A.set_initial_condition(true);
    B=Vertex<bool>("B", false/*, thresh*/ );
    B.set_initial_condition(true);
    parents = {A,B};
    C=Vertex<bool>("C", true/*, thresh*/ );
    C.set_initial_condition(false);
    inhibitions = {false, false};
    C.update_value(parents, inhibitions, noise_generator, 0);

    if(C.previous_value() != true)
    {
        std::cout << "3:Expected value " << true << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }

    if ( !ok )
        std::cout << " NOK : test_update_unit_v0_rxn" <<NEW_LINE;
    return ok;
}

bool test_update_unit_v0_notrxn()
{
    //Thresholdfuntion unit step, version 0,  not reaction node -> or relation
    std::cout << "------test_update_unit_v0_notrxn()------" << NEW_LINE;
    Vertex<bool>::set_version(logical_or);
    RandomWrapper noise_generator;
//    ThresholdFunction* thresh;
//    Unit_step unit(0.,1.,0.5);
//    thresh = &unit;
    Vertex<bool> A("A", true/*, thresh*/ );
    A.set_initial_condition(true);
    Vertex<bool> B("B", true/*, thresh*/ );
    B.set_initial_condition(false);
    std::vector<Vertex<bool>> parents = {A,B};
    Vertex<bool> C("C", false/*, thresh*/ );
    C.set_initial_condition(false);
    std::vector<bool> inhibitions = {true, false};
    C.update_value(parents, inhibitions, noise_generator, 0);
    bool ok=true;
    if(C.previous_value() != false)
    {
        std::cout << "1:Expected value " << false << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }


    A=Vertex<bool>("A", true/*, thresh*/ );
    A.set_initial_condition(true);
    B=Vertex<bool>("B", true/*, thresh*/ );
    B.set_initial_condition(false);
    parents = {A,B};
    C=Vertex<bool>("C", false/*, thresh*/ );
    C.set_initial_condition(false);
    inhibitions = {false, false};
    C.update_value(parents, inhibitions, noise_generator, 0);

    if(C.previous_value() != true)
    {
        std::cout << "2:Expected value " << false << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }

    A=Vertex<bool>("A", true/*, thresh*/ );
    A.set_initial_condition(false);
    B=Vertex<bool>("B", true/*, thresh*/ );
    B.set_initial_condition(false);
    parents = {A,B};
    C=Vertex<bool>("C", false/*, thresh*/ );
    C.set_initial_condition(false);
    inhibitions = {true, true};
    C.update_value(parents, inhibitions, noise_generator, 0);

    if(C.previous_value() != true)
    {
        std::cout << "3:Expected value " << true << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }


    if ( !ok )
        std::cout << " NOK : test_update_unit_v0_notrxn" <<NEW_LINE;
    return ok;
}
bool test_update_unit_v1_notrxn()
{
    //Thresholdfuntion unit step, version 0,update reaction node
    std::cout << "------test_update_unit_v1_notrxn()------" << NEW_LINE;
    RandomWrapper noise_generator;
    Vertex<bool>::set_version(sumV);
//    ThresholdFunction* thresh;
//    Unit_step unit(0.,1.,0.5);
//    thresh = &unit;
    Vertex<bool> A("A", true/*, thresh*/ );
    A.set_initial_condition(true);
    Vertex<bool> B("B", true/*, thresh*/ );
    B.set_initial_condition(false);
    std::vector<Vertex<bool>> parents = {A,B};
    Vertex<bool> C("C", false/*, thresh*/ );
    C.set_initial_condition(false);
    std::vector<bool> inhibitions = {false, false};
    C.update_value(parents, inhibitions, noise_generator, 0);
    bool ok=true;
    if(C.previous_value() != true )
    {
        std::cout << "1:Expected value " << true << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }

    A=Vertex<bool>("A", true/*, thresh*/ );
    A.set_initial_condition(false);
    B=Vertex<bool>("B", true/*, thresh*/ );
    B.set_initial_condition(false);
    parents = {A,B};
    C=Vertex<bool>("C", false/*, thresh*/ );
    C.set_initial_condition(true);
    inhibitions = {true, false};
    C.update_value(parents, inhibitions, noise_generator, 0);

    if(C.previous_value() != true )
    {
        std::cout << "2:Expected value " << true << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }


    A=Vertex<bool>("A", true/*, thresh*/ );
    A.set_initial_condition(true);
    B=Vertex<bool>("B", true/*, thresh*/ );
    B.set_initial_condition(false);
    parents = {A,B};
    C=Vertex<bool>("C", false/*, thresh*/ );
    C.set_initial_condition(false);
    inhibitions = {true, true};
    C.update_value(parents, inhibitions, noise_generator, 0);

    if(C.previous_value() != false)
    {
        std::cout << "3:Expected value " << false << ", but found " << C.previous_value() << "!\n";
        ok = false;
    }

    if ( !ok )
        std::cout << " NOK : test_update_unit_v1_notrxn" <<NEW_LINE;
    return ok;

}
int main(int argc, char * argv[])
{

    bool value = true;
    if(!test_static_variable())
        value = false;
    if (!test_update_unit_v0_rxn())
        value = false;
    if( !test_update_unit_v0_notrxn())
        value = false;
    if( !test_update_unit_v1_notrxn())
        value = false;
    if( ! test_update_unit_v1_notrxn() )
        value = false;
    if( ! test_limit_range())
        value = false;

    if(!value)
    {
       std::cout << "testVertex<bool> failed " << NEW_LINE;
       return  -1;
    }

    return 0;
}
