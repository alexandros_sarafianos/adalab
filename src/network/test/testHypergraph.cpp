#include <iostream>
#include "data.hpp"
#include "src/network/network.hpp"
#include "src/experiment/experiment.hpp"
#include "src/experiment/experimentaloutput.hpp"
#include "src/network/hypergraph.hpp"
#include "src/network/vertex.hpp"
#include "src/network/edge.hpp"
#include <vector>
#define STDOUT

bool check_oks(std::vector<bool> & oks)
{
    bool ok = true;
    int index  = 1;
    for(std::vector<bool>::const_iterator ok_it = oks.begin(); ok_it!=oks.end(); ++ok_it )
    {
        if( !(*ok_it) )
            std::cout << index<<"st/nd/rd/th 'ok'test failed! " << NEW_LINE;
        ok = ok && *ok_it;
        index++;
    }
    return ok;
}
bool check_noks(std::vector<bool> & noks)
{
    bool ok = false;
    int index  = 1;
    for(std::vector<bool>::const_iterator nok_it = noks.begin(); nok_it!=noks.end(); ++nok_it )
    {
        if( (*nok_it) )
            std::cout << index<<"st/nd/rd/th 'nok'test failed! " << NEW_LINE;
        ok = ok || *nok_it;
        index++;
    }
    return !ok;
}
bool test_equality_operators()
{
    Hypergraph<Vertex, Edge> h (DATA_FILE_TEST_HG_NETWORK);
    std::cout << " h " <<NEW_LINE;
    h.print_hyperedges();
    Hypergraph<Vertex, Edge> h_dupl (DATA_FILE_TEST_HG_NETWORK);
    std::cout << " h_dupl " <<NEW_LINE;
    h_dupl.print_hyperedges();
    Hypergraph<Vertex, Edge> h_diff (DATA_FILE_TEST_HG_NETWORK2);
    std::cout << " h_diff " <<NEW_LINE;
    h_diff.print_hyperedges();
    Hypergraph<Vertex, Edge> h_diff2 (DATA_FILE_TEST_HG_NETWORK3);

    std::cout << " h_diff2 " <<NEW_LINE;
    h_diff2.print_hyperedges();
    std::vector<bool> oks,noks;

    std::cout << "h == h_dupl " << NEW_LINE;
    oks.push_back(h == h_dupl);
    std::cout << "h == h " << NEW_LINE;
    oks.push_back(h == h );
    std::cout << "h != h_diff " << NEW_LINE;
    oks.push_back(h != h_diff );
    std::cout << "!(h != h_dupl) " << NEW_LINE;
    oks.push_back(!(h != h_dupl) );
    std::cout << "h != h_diff2" << NEW_LINE;
    oks.push_back( h != h_diff2 );
    std::cout << " h " <<NEW_LINE;
    h.print_hyperedges();
    std::cout << " h_diff2 " <<NEW_LINE;
    h_diff2.print_hyperedges();
    std::cout << "NOK h != h" << NEW_LINE;
    noks.push_back(h != h);
    std::cout << "NOK h != h_dupl" << NEW_LINE;
    noks.push_back(h!= h_dupl);
    std::cout << "NOK h == h_diff2" << NEW_LINE;
    noks.push_back(h == h_diff);
    std::cout << "NOK h == h_diff2" << NEW_LINE;
    noks.push_back(h == h_diff2 );
    bool ok1= check_oks(oks);
    bool ok2 = check_noks(noks);
    bool ok = ok1 && ok2; //to avoid lazy evaluation
    if ( !ok )
        std::cout << "Error with test equality operators " << NEW_LINE;
    std::cout << noks << NEW_LINE;
    std::cout << oks << NEW_LINE;
    return ok;

}

bool testIterator()
{
    Hypergraph<Vertex, Edge> h (DATA_FILE_TEST_HG_NETWORK);
    int index = 1;
    bool result = true;
    HyperDistrIterator<Vertex, Edge>  h_it = h.begin();
    for( h_it; h_it != h.end() ; ++h_it)
    {
        std::cout << index << NEW_LINE;
        index++;
        if( index == 10)
            result = false;
    }
    if( ! result )
        std::cout << "Iterator test failed " << NEW_LINE;
   return result;
}

bool test_my_clone()
{
    Hypergraph<Vertex, Edge> h (DATA_FILE_TEST_HG_NETWORK);
    Hypergraph<Vertex, Edge> * h_cloned = h.my_clone();

    bool check = h == *h_cloned;
    if( ! check )
    {
        std::cout << "Cloning failed, printing networks ... " << NEW_LINE;
        h.print_hyperedges();
        std::cout << "and clone ..."<< NEW_LINE;
        h_cloned->print_hyperedges();
     }
    h.erase_edge(h.edges().begin()->first);
    bool check2 = h!=*h_cloned;
    if (!check2)
        std::cout<< "Graphs were still the same; erasure probably also affected clone" << NEW_LINE;
    return check&& check2;
}

int main(int argc, char * argv[])
{

    bool value = test_equality_operators();
    value = testIterator() && value;
    if(!value)
    {
       std::cout << "testHypergraph failed " << NEW_LINE;
       return  -1;
    }

    return 0;
}
