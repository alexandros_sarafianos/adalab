#include <iostream>

#include "src/network/network.hpp"
#include "src/general/maths/unit_step.hpp"
#include "src/experiment/experimentaloutput.hpp"
#include "src/general/storage_adaptors.hpp"
#include <vector>

#include "data.hpp"

bool comparison(const std::map<int,double> & values, const std::vector<double> & precalculated_values)
{
    std::vector<double>::const_iterator precalc_iterator = precalculated_values.begin();
    int index = 0;
    for(auto map_it = values.begin(); map_it != values.end(); ++map_it)
    {
        std::cout << map_it->second << " : " << *precalc_iterator << NEW_LINE;
        if( !approximately_equal<double>(map_it->second, *precalc_iterator) )
        {
            std::cout <<" Error at index " << index << "! Printing map and vector..." << NEW_LINE;
            return false;
        }
        precalc_iterator++;
        index++;
    }
    return true;
}
//template < class _Type>
//bool approximately_equal(_Type a, _Type b)
bool test_continuous_propagation()
{
    std::vector<double> A = { 0,-0.0004,-0.00042,-0.000421,-0.00042105,-0.0004210525,-0.0004210526,-0.0004210526,-0.0004210526,-0.0004210526,-0.0004210526,-0.0004210526};
    std::vector<double> B = { 0,-0.0003,-0.006415,-0.00692885,-0.0090172385,-0.0093153621,-0.0100351627,-0.0101785503,-0.0104290786,-0.0104923028,-0.0105802988,-0.0106068283};
    std::vector<double> C = {0,-0.012,-0.01241,-0.0164546,-0.016834417,-0.0182165631,-0.0184271467,-0.0189043209,-0.0190037285,-0.0191700712,-0.0192134627,-0.019271974};
    std::vector<double> D = {0,-0.003,-0.002967,-0.00168252,-0.0015617667,-0.0011219976,-0.0010549939,-0.0009031658,-0.0008715361,-0.0008186089,-0.0008048025,-0.0007861853};
    //ThresholdFunction* thresh;
//    Unit_step unit = Unit_step(0., 1.0, 0.5);
//    thresh = &unit;
    RandomWrapper m_r ("uniform", 0.0);
    Network<double> network(DATA_FILE_TEST_CONT_NETWORK, m_r/*, thresh*/,"");
    Experiment e;
    network.perform_experiment(e,4,0.0 ,0, std::unordered_map<std::string,double>( )   );
    ExperimentalOutput time_series = network.extract_time_series(true);
    bool val_A = comparison(time_series.get_values_of_node<double>("A"), A);
    if( !val_A )
        std::cout << "Error with node A. Map: " << time_series.get_values_of_node<double>("A") << "Vector " << A << NEW_LINE;
    bool val_B = comparison(time_series.get_values_of_node<double>("B"), B);
    if( !val_B )
        std::cout << "Error with node B. Map: " << time_series.get_values_of_node<double>("B") << "Vector " << B << NEW_LINE;
    bool val_C = comparison(time_series.get_values_of_node<double>("C"), C);
    if( !val_C )
        std::cout << "Error with node C. Map: " << time_series.get_values_of_node<double>("C") << "Vector " << C << NEW_LINE;
    bool val_D = comparison(time_series.get_values_of_node<double>("D"), D);
    if( !val_D )
        std::cout << "Error with node D. Map: " << time_series.get_values_of_node<double>("D") << "Vector " << D << NEW_LINE;
    return val_A && val_B && val_C && val_D;
}
bool test_network_creation()
{
    std::vector<double> A = { 0,-0.0004,-0.00042,-0.000421,-0.00042105,-0.0004210525,-0.0004210526,-0.0004210526,-0.0004210526,-0.0004210526,-0.0004210526,-0.0004210526};
    std::vector<double> B = { 0,-0.0003,-0.006415,-0.00692885,-0.0090172385,-0.0093153621,-0.0100351627,-0.0101785503,-0.0104290786,-0.0104923028,-0.0105802988,-0.0106068283};
    std::vector<double> C = {0,-0.012,-0.01241,-0.0164546,-0.016834417,-0.0182165631,-0.0184271467,-0.0189043209,-0.0190037285,-0.0191700712,-0.0192134627,-0.019271974};
    std::vector<double> D = {0,-0.0003,-0.00024,0.00104475,0.001165506,0.0016052751,0.0016722788,0.0018241069,0.0018557366,0.0019086639,0.0019224702,0.0019410875};
    //ThresholdFunction* thresh;
//    Unit_step unit = Unit_step(0., 1.0, 0.5);
//    thresh = &unit;
    RandomWrapper m_r ("uniform", 0.0);
    Network<double> network(DATA_FILE_TEST_CONT_NETWORK, m_r/*, thresh*/,"");

    return true;
}

bool test_network_constructor_matrix()
{
    std::cout << "<<<<<<<<<<<<<<<<<<<<<<<test-network-constructor-matrix>>>>>>>>>>>>>>>>>>>>>>>>>>>" << NEW_LINE;
    //ThresholdFunction* thresh;
//    Unit_step unit = Unit_step(0., 1.0, 0.5);
//    thresh = &unit;
    RandomWrapper m_r ("uniform", 0.0);
    Network<double> network(DATA_FILE_TEST_CONT_NETWORK, m_r, /*thresh,*/"");
    network.print_adjacency_matrix();
    double new_matrix[4][5] = {
        0.05,0,0,0,-0.0004,
        -0.05,0.05,0.51,0,-0.0003,
        0.23,0.66,0.01,0,-0.012,
        0,-0.21,0,0.01,-0.003

    };
    matrix<double> true_matrix(4,5);
    true_matrix    = make_matrix_from_pointer(new_matrix);
    matrix<double> found_matrix;
    std::vector<std::string> node_names;
    network.get_adjacency_matrix(found_matrix, node_names);
    std::cout << "test1"<<NEW_LINE;
    bool ok1 = equal_matrices<double>(true_matrix, found_matrix);
    if( !ok1 )
        std::cout << "1: matrices didn't match, expected " << true_matrix << " , but found " << found_matrix << NEW_LINE;

    double new_matrix_2[4][5] = {
        0.05,0,0,0.9,-0.0004,
        -0.05,0.05,0.51,0,-0.0003,
        0.23,0.66,0.01,0,-0.012,
        0,-0.21,0,0.01,-0.003

    };


   matrix<double> true_matrix_2(4,5) ;
   true_matrix_2 = make_matrix_from_pointer(new_matrix_2);
    std::cout << "test2"<<NEW_LINE;
    bool ok2 = !equal_matrices<double>(true_matrix_2, found_matrix);
    if( !ok2 )
        std::cout << "2: matrices shouldn't match, they were " << true_matrix_2 << " , and found " << found_matrix << NEW_LINE;

    network = Network<double>(true_matrix_2, node_names, m_r);
    matrix<double> found_matrix_2;
    std::vector<std::string> node_names_2;
    network.get_adjacency_matrix(found_matrix_2, node_names_2);
    std::cout << "test3"<<NEW_LINE;
    bool ok3 = equal_matrices<double>(true_matrix_2, found_matrix_2);
    if( !ok3 )
        std::cout << "3: matrices didn't match, expected " << true_matrix_2 << " , but found " << found_matrix_2 << NEW_LINE;
    std::cout << "test4"<<NEW_LINE;
    bool ok4 = !equal_matrices<double>(true_matrix, found_matrix_2);
    if( !ok4 )
        std::cout << "4: matrices shouldn't match, expected " << true_matrix << " , but found " << found_matrix_2 << NEW_LINE;
//    void Network<VertexOutputType>::get_adjacency_matrix(matrix<double> & adjacency_matrix, std::vector<std::string> & node_names ) const

    return ok1 && ok2 && ok3 && ok4;
}
int main(int argc, char * argv[])
{

    bool value = test_continuous_propagation();
    bool val2 = test_network_creation();
    bool val3 = test_network_constructor_matrix();

    if(!value || !val2 || !val3)
    {
       std::cout << "testNetwork failed " << NEW_LINE;
       return  -1;
    }

    return 0;
}
