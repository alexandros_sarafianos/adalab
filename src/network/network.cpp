#include <src/network/network.hpp>

/*************************************************random_initialization_node_values******************************/
//bool
template<>
void Network<bool>::random_initialization_node_values(const float ratio, const bool only_genes)
{
    std::pair<vertex_iter,vertex_iter> vp ;
    for(vp = vertices(network); vp.first!= vp.second;++vp.first)
    {
         //only initialize genes
         if( !only_genes || (only_genes && network[*vp.first].get_vertex_type() == gene) )
         {
             bool set_value;
             double value = uni_d(generator);

             if( value >= ratio )
                 set_value = true; //problem here
             else
                 set_value = false; //problem here
             //std::cout <<" RANDOM: " <<value << " and " << set_value << NEW_LINE;
             network[*vp.first].initialize_state_map(set_value);
         }
    }
}
//double
template<>
void Network<double>::random_initialization_node_values(const float ratio, const bool only_genes)
{
    std::pair<vertex_iter,vertex_iter> vp ;
    std::cout << "Random Init " << ratio << NEW_LINE;
    for(vp = vertices(network); vp.first!= vp.second;++vp.first)
    {
         //only initialize genes
        std::cout << network[*vp.first].get_name() << " type; " << network[*vp.first].get_vertex_type() << NEW_LINE;
         if( !only_genes || (only_genes && network[*vp.first].get_vertex_type() == gene) )
         {
             double set_value;
             double value = uni_d(generator); //TO DO: IMplementation should change here, value should be drawn from [-1,1] iso forcing to 1 or -1

             if( value >= ratio )
                 set_value = 1.; //problem here
             else
                 set_value = -1.; //problem here
             std::cout <<" RANDOM: " <<value << " and " << set_value << NEW_LINE;
             network[*vp.first].initialize_state_map(set_value);
         }
    }
}




/************************************Constructor from file ********************************************/

//bool
template<>
Network<bool>::Network(const std::string toy_network_file_name, RandomWrapper & m_r/*, ThresholdFunction * t_func*/,const std::string & xml_nodes)
{
    /*Future additional implementation*/
    // Also need to see how to add functionality of gene/edges into this file -> xml like stuff/ via relational database
    // 1: <name=g2, function=x, type=gene> -> <name=g1, function=b, type=metabolite> and <name=g2, function=c, type=gene>
    // and so on
    /*Current implementation*/
    // Only specify ids, look up info based on id
    // 1: G1 -> G2,G3
//    Vertex v;v.set_t_func(t_func);
//    network = Graph(411);
    uni_d = std::uniform_real_distribution<double>(BOOL_MIN_EXP_VAL,BOOL_MAX_EXP_VAL);
    if( !xml_nodes.empty())
    {
        _VertexBool::set_vertextype_map(xml_nodes);
    }
    std::stringstream os; os << "Creating network from file " << toy_network_file_name << " ... " ;LoggerSingleton::Instance()->info_print(os);
    std::string line;
    std::ifstream inf;
    open_file_safely(toy_network_file_name, inf);
    const char * SEP = "\t";
    int id = 1;
    number_of_vertices=0;
    number_of_edges=0;
    boost::unordered_set<std::string> node_set;

    std::vector<std::vector<std::pair<std::string,bool> > > all_causes;
    std::vector<bool> all_stimulations;
    std::vector<std::string> all_effects;

    while(getline(inf,line))
    {
        std::vector<std::pair<std::string,bool> > causes;
        bool stimulation;
        std::string effect;
        parse_network_line(line, causes, stimulation, effect, node_set);
        all_causes.push_back(causes);
        all_stimulations.push_back(stimulation);
        all_effects.push_back(effect);
    }
    /*create vertices and edges from reactions
    bipartite reaction graph!
    */
    std::vector<bool>::const_iterator all_stimulations_it = all_stimulations.begin();
    std::vector<std::string>::const_iterator all_effects_it = all_effects.begin();
    std::unordered_map<_VertexBool , vertex_t, MyVertexHash<bool> > vertex_t_map;
    unsigned reaction_nr = 1;
    for(std::vector<std::vector<std::pair<std::string,bool> > >::const_iterator it = all_causes.begin(); it != all_causes.end(); ++it )
    {
        std::string reaction_id = to_string(reaction_nr);
        /* e.g. A + B -> C*/
        // Add nodes A and B
        std::vector<std::pair<_VertexBool ,bool > > causal_vertices;
        std::vector<vertex_t> boost_vertex_container;
        _VertexBool  temp_v;
        for(std::vector<std::pair<std::string,bool> >::const_iterator it_internal = it->begin() ; it_internal != it->end(); ++it_internal)
        {
            /* check if vertex is already present */

            boost::unordered_set<std::string>::iterator el_find_it = node_set.find(it_internal->first) ;
            temp_v = _VertexBool (it_internal->first, false/*,t_func*/);
            causal_vertices.push_back(std::pair<_VertexBool , bool>(temp_v, it_internal->second));

            if(el_find_it != node_set.end() )
            {
                vertex_t v = boost::add_vertex(temp_v, network); ++number_of_vertices;
                vertex_t_map.insert ( std::pair<_VertexBool ,vertex_t>(temp_v, v) );
                boost_vertex_container.push_back(v);
                node_set.erase(el_find_it);
            }
            reaction_id += "_";
            reaction_id += it_internal->first;
        }
        //add C
        boost::unordered_set<std::string>::iterator effect_it = node_set.find( *all_effects_it );
        _VertexBool  effect_vertex(*all_effects_it,false/*,t_func*/);
        vertex_t boost_effect_vertex;

        if(  effect_it != node_set.end() )
        {
            boost_effect_vertex = boost::add_vertex( effect_vertex , network); ++number_of_vertices;
            vertex_t_map.insert ( std::pair<_VertexBool ,vertex_t>( effect_vertex , boost_effect_vertex) );
            node_set.erase(effect_it);
        }
        //add interaction node AB
        _VertexBool  reaction_vertex(reaction_id, true/*, t_func*/);
//        Vertex  reaction_vertex =Vertex (reaction_id, true, t_func);

        vertex_t boost_reaction_vertex = boost::add_vertex(reaction_vertex,network); ++number_of_vertices;
        //Create edges
        //A -> AB and B -> AB
        typename std::vector<vertex_t>::iterator boost_vertex_container_iterator = boost_vertex_container.begin();
        for(typename std::vector<std::pair<_VertexBool ,bool> >::const_iterator vv_it = causal_vertices.begin(); vv_it != causal_vertices.end(); ++vv_it)
        {
            Edge e(vv_it->first.get_name(), reaction_vertex.get_name(), vv_it->second);
            if( boost_vertex_container_iterator != boost_vertex_container.end())
            {
                if( vertex_t_map[vv_it->first] != *boost_vertex_container_iterator )
                {
                    boost::add_edge(vertex_t_map[vv_it->first], boost_reaction_vertex, e, network); ++number_of_edges;
                }
                else
                {
                    boost::add_edge(*boost_vertex_container_iterator, boost_reaction_vertex, e, network);++number_of_edges;
                }
                ++boost_vertex_container_iterator;
            }
            else
            {
                boost::add_edge(vertex_t_map[vv_it->first], boost_reaction_vertex, e, network);++number_of_edges;
            }
        }
        //AB->C
        Edge e1(reaction_vertex.get_name(), effect_vertex.get_name(), !(*all_stimulations_it) ); //function expects inhibition and inhibition = !stimulation
        if( vertex_t_map[effect_vertex] != boost_effect_vertex)
            boost_effect_vertex = vertex_t_map[effect_vertex];
        boost::add_edge(boost_reaction_vertex, boost_effect_vertex, e1, network );++number_of_edges;
        ++all_stimulations_it;
        ++all_effects_it;
        ++reaction_nr;
    }
//    thr_func = t_func;
    random_number_generator = m_r;
    std::cout << "Succesfully created network specified in " << toy_network_file_name << " number of vertices :'"
                               << number_of_vertices << "', number of edges: '" << number_of_edges << "'!" << NEW_LINE;
    if( number_of_vertices == 0 && number_of_edges == 0)
    {
        std::cout << "No vertices or edges found in " << toy_network_file_name << NEW_LINE;
        exit(0);
    }
    //this->print_network(std::stringstream os; os);
}


//Double
template<>
Network<double>::Network(const std::string toy_network_file_name, RandomWrapper & m_r /*, ThresholdFunction * t_func*/,const std::string & xml_nodes)
{
//    network = Graph(411);
    uni_d = std::uniform_real_distribution<double>(BOOL_MIN_EXP_VAL,BOOL_MAX_EXP_VAL);
    if( !xml_nodes.empty())
    {
        _VertexBool::set_vertextype_map(xml_nodes);
    }
    XML_reader network_reader(toy_network_file_name);
    std::vector< std::tuple< std::vector<std::tuple<std::string, std::string,std::string> >, std::string, std::string > > rxns;
    network_reader.read_continuous_network_file( rxns );
    std::unordered_map<_VertexCont , vertex_t, MyVertexHash<double> > vertex_t_map;
//    add_vertex_type(v_map, const std::string &  n, const VertexType & t);
    unsigned number_of_vertices = 0;
    unsigned number_of_edges = 0;
    for( auto it = rxns.begin(); it!= rxns.end(); ++it)
    {
        std::vector<std::tuple<std::string, std::string, std::string> > inputs = std::get<0>(*it);
        std::string constant_term_str = std::get<1>(*it);
        double constant_term = 0.; //TODO: Random init here when NA?
        if(constant_term_str != "NA")
            constant_term = convertStringToType<double>(constant_term_str);
        std::string output = std::get<2>(*it);
        /* e.g. A + B -> C*/
        //add C
        _VertexCont out_vert (output, false/*, t_func*/, constant_term);
        vertex_t v_out;
        int code = add_vertex_if_necessary(vertex_t_map, out_vert, v_out);
        if(code == 0 )
        {
            number_of_vertices++;
        }
        else
        {
            network[v_out].set_constant_term(constant_term);
        }
        //Add A and B
        for( auto reactant_it = inputs.begin(); reactant_it != inputs.end(); reactant_it++)
        {
            // Add node
            _VertexCont vert(std::get<0>(*reactant_it), false/*, t_func */); //std::string m_name, bool m_is_reaction, ThresholdFunction *m_t_func, double rh_constant_term
            vertex_t v;
            int code = add_vertex_if_necessary(vertex_t_map, vert, v);
            if(code ==0)
            {
                number_of_vertices++;
            }
            // Add edge
            Edge e(std::get<1>(*reactant_it));
            boost::add_edge(v, v_out, e, network);
            ++number_of_edges;
            _VertexCont::add_to_vertextype_map(std::get<0>(*reactant_it) ,std::get<2>(*reactant_it));
        }
    }
//    thr_func = t_func;
    random_number_generator = m_r;
    std::cout << "Succesfully created network specified in " << toy_network_file_name << " number of vertices :'"
                               << number_of_vertices << "', number of edges: '" << number_of_edges << "'!" <<NEW_LINE;
    if( number_of_vertices == 0 && number_of_edges == 0)
    {
        std::cout << "No vertices or edges found in " << toy_network_file_name << NEW_LINE;
        exit(0);
    }

}

/************************************Constructor from matrix ********************************************/
template<>
Network<double>::Network(const matrix<double> & adjacency_matrix, const std::vector<std::string> & node_names, RandomWrapper & m_r )
{
    random_number_generator = m_r;
    std::unordered_map<_VertexCont , vertex_t, MyVertexHash<double> > vertex_t_map;
    assert(adjacency_matrix.size1()+1 == adjacency_matrix.size2());
    int i = 0;
    //Add all vertices
    for( auto node_name_it = node_names.begin(); node_name_it != node_names.end(); ++node_name_it )
    {
        double constant_term = adjacency_matrix(i, adjacency_matrix.size2()-1);
        _VertexCont v(*node_name_it,false/*, thr_func*/, constant_term);
        vertex_t v_t;
        int code = add_vertex_if_necessary(vertex_t_map, v, v_t);
        ++i;

    }
    //for every row
    for( int row_counter = 0; row_counter < adjacency_matrix.size1(); ++row_counter )
    {
        _VertexCont v_in(node_names[row_counter]);
        vertex_t v_in_t = vertex_t_map[v_in];
        //for every column, except the last one
        for( int col_counter =  0; col_counter < adjacency_matrix.size2()-1; ++col_counter)
        {
            if(!approximately_equal<double>(adjacency_matrix(row_counter, col_counter),0))
            {
                _VertexCont v_out(node_names[col_counter]);
                vertex_t v_out_t = vertex_t_map[v_out];
//                std::cout << "Non-zero value at location ("<< row_counter <<","<<col_counter<<") " <<adjacency_matrix(row_counter, col_counter) << NEW_LINE;
                Edge e(adjacency_matrix(row_counter, col_counter));
                boost::add_edge(v_out_t, v_in_t, e, network);
            }
        }
    }
}


/**********************************extract_time_series****************************/
//bool
template <>
ExperimentalOutput Network<bool>::extract_time_series(bool reset_state_maps)
{
    bool diauxic_shift = false;
    std::map<std::string, std::map<int,bool>> big_gene_level_map;
    for(std::pair<vertex_iter, vertex_iter> vp = vertices(network); vp.first!=vp.second;++vp.first)
    {
        std::pair<std::string, std::map<int,bool> > temp = network[*vp.first].extract_time_series(reset_state_maps);
        big_gene_level_map.insert(temp);
        if(network[*vp.first].get_name() == "diauxic_shift")
        {
            for(std::map<int,bool>::const_iterator it = temp.second.begin(); it != temp.second.end(); it++)
            {
                if( it->second )
                {
                    diauxic_shift = true;
                    break;
                }
            }
        }
    }
    return ExperimentalOutput(big_gene_level_map, diauxic_shift);
}
//double
template <>
ExperimentalOutput Network<double>::extract_time_series(bool reset_state_maps)
{
    bool diauxic_shift = false;
    std::map<std::string, std::map<int,double>> big_gene_level_map;
    for(std::pair<vertex_iter, vertex_iter> vp = vertices(network); vp.first!=vp.second;++vp.first)
    {
        std::pair<std::string, std::map<int,double> > temp = network[*vp.first].extract_time_series(reset_state_maps);
        big_gene_level_map.insert(temp);
        if(network[*vp.first].get_name() == "diauxic_shift")
        {
            for(std::map<int,double>::const_iterator it = temp.second.begin(); it != temp.second.end(); it++)
            {
                if( it->second > 0. )
                {
                    diauxic_shift = true;
                    break;
                }
            }
        }
    }
    return ExperimentalOutput(big_gene_level_map, diauxic_shift);
}


/********************************************determine_node_value*****************************************/
//Bool
template<>
void Network<bool>::determine_node_value(const vertex_t & v , int t, const Experiment & e, const std::unordered_map<std::string,double> & met_map)//,const std::unordered_map<std::string,double> & met_map)
{

    if( network[v].is_checked_at_time_step_t(t))
    {
        INFO_WARNING( network[v] << " already checked at time step " << t << "!");
        return;
    }
//    if ( met_map.find(network[v].get_name()) != met_map.end() )
//    {
//    }
    else
    {
        ExperimentalInfo info;
        if(e.return_experimental_condition( network[v].get_name(), info) && (info.is_factor_fixed_throughout_experiment() || t == 0 )) // Node is related to experimental condition
        {
            INFO_HIGH("Node " << network[v].get_name() << " is fixed " <<  info.is_factor_fixed_throughout_experiment() << " with value " << info.initial_condition_value<bool>());
            network[v].update_value(info.initial_condition_value<bool>(), random_number_generator, t, true);//true = fixed_node
            return;
        }
        else
        {
            std::pair<in_edge_it, in_edge_it> ep;
            std::vector<_VertexBool > parent_vertices;
            std::vector<bool> inhibitions; //for edges, inhibition_or_activation
            for (ep = in_edges(v, network);ep.first!=ep.second;++ep.first)
            {
                vertex_t source_v = source(*ep.first, network);
                inhibitions.push_back(network[*ep.first].inhibits());
                parent_vertices.push_back(network[source_v]);
            }
            network[v].update_value(parent_vertices,inhibitions, random_number_generator,t);//update value based on parent vertices
            return;
        }
    }
}
//Double
template<>
void Network<double>::determine_node_value(const vertex_t & v , int t, const Experiment & e, const std::unordered_map<std::string,double> & met_map)//, const std::unordered_map<std::string,double> & met_map)
{
    if( network[v].is_checked_at_time_step_t(t) )
        return;
//    if ( met_map.find(network[v].get_name()) != met_map.end() )
//    {
//    }
    else
    {
        ExperimentalInfo info;
        if(e.return_experimental_condition( network[v].get_name(), info) && (info.is_factor_fixed_throughout_experiment() || t == 0 )) // Node is related to experimental condition
        {
            INFO_HIGH("Node " << network[v].get_name() << " is fixed " <<  info.is_factor_fixed_throughout_experiment()
                      << " with value " << info.initial_condition_value<double>());
            network[v].update_value(info.initial_condition_value<double>(), random_number_generator, t, true);//true = fixed_node
            return;
        }
        else
        {
            double g_i = vertex_updater(v, in_edges(v,network), network, t);
            network[v].update_value(g_i , random_number_generator,t, false);//update value based on parent vertices
            return;
        }
    }
}
/********************************************print_network*****************************************/
template<>
void Network<double>::print_network ( std::ostream& os )
{
    os << "Printing network " << NEW_LINE;
    for(std::pair<vertex_iter, vertex_iter> vp = vertices(network); vp.first!= vp.second;++vp.first)
    {
        os << network[*vp.first].get_name()<< " :- " ;
        for (std::pair<in_edge_it, in_edge_it> ep = in_edges(*vp.first, network);ep.first!=ep.second;++ep.first)
        {
            os << network[*ep.first].get_weight() << "*" << network[source(*ep.first, network)].get_name() <<" + ";
        }
        os << network[*vp.first].get_constant_term() <<NEW_LINE;
    }
    os << "------------ " << NEW_LINE;
}
template<>
void Network<bool>::print_network ( std::ostream& os )
{
//    for(vp = vertices(rh_network); vp.first!= vp.second;++vp.first)
//    {
//        os << rh_network[*vp.first].get_name()<< " :- " ;
//        for (ep = in_edges(v, rh_network);ep.first!=ep.second;++ep.first)
//        {
//            os << rh_network[*ep.first].get_weight() << rh_network[source(*ep.first, rh_network)].get_name();
//        }
//        os << NEW_LINE;
//    }
}
/****************************************************void set_metabolite_levels(const std::unordered_map<std::string, double> & met_map)***/
template<>
void Network<double>::set_metabolite_levels(const typename std::unordered_map<std::string, double> & met_map, const int iteration, const bool update_t)
{
    /**Not very efficient, replace this by keeping unordered_map in memory
     * that maps Vertex to vertex_t -> ct time lookup!
     * */
    std::pair<vertex_iter, vertex_iter> vp;
    for(vp = vertices(network);vp.first != vp.second; ++vp.first)
    {
        auto it = met_map.find(network[*vp.first].get_name());
        if(  it != met_map.end() )
        {
            //Metabolite inside the map, update!
            network[*vp.first].update_value(it->second,random_number_generator, iteration-1, false, update_t); //TODO: Metabolites are allowed noise
        }
    }
}
template<>
void Network<bool>::set_metabolite_levels(const typename std::unordered_map<std::string, double> & met_map, const int iteration, const bool update_t)
{
    /**Not very efficient, replace this by keeping unordered_map in memory
     * that maps Vertex to vertex_t -> ct time lookup!
     * */
    std::pair<vertex_iter, vertex_iter> vp;
    for(vp = vertices(network);vp.first != vp.second; ++vp.first)
    {
         auto it = met_map.find(network[*vp.first].get_name());
        if(  it != met_map.end() )
        {
            //Metabolite inside the map, update!
            bool value = false;
            if( it->second > 0.5)
                value = true;
            network[*vp.first].update_value(value,random_number_generator, iteration-1, false, update_t); //TODO: Metabolites are allowed noise
        }
    }
}
