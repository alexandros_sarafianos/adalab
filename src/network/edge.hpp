#ifndef EDGE_HPP
#define EDGE_HPP
#include <utility>
#include "src/network/vertex.hpp"
#include <string>
#include <iostream>
#include <assert.h>
#include "src/general/compile_configuration.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
enum Influence
{
    Activation,
    Inhibition,
    Unknown
};

struct Edge
{
public:
    Edge(){ id = Edge::next_id++;} //Default, necessary for boost!
    Edge(bool m_inhib);
    //Edge(bool m_inhib, const std::map<Vertex, Influence> & m_influences);
    Edge( const std::string & v1, const std::string & v2, const bool m_inhib );
    Edge( const std::string & weight );
    Edge( const double rh_weight);
    inline std::string get_name()const{return name;}
    std::string name;
    inline bool inhibits()const {return inf == Inhibition;}
    inline bool stimulates()const {return inf == Activation;}
    friend std::ostream& operator<<(std::ostream& os, const Edge & e);
    int get_id()const {return id;}
    double get_weight()const {return weight;}
    //void set_influences(const std::map<Vertex, Influence> & m_influences){influences = m_influences;}
    //Influence get_influence_of_vertex_in_hyperedge(const Vertex & v )const ;
    //void print_influence_map( ){std::cout << "INFLUENCE_MAP :" << influences << NEW_LINE;}
//    bool operator==(const Edge &) const;
//    bool operator!=(const Edge & e) const{return !(*this==e);}
//    bool operator<(const Edge & e) const;
    void test_print()const{ std::cout << id << ", weight - " << weight << NEW_LINE;}
private:
    Influence inf;
    int id;
    static int next_id;
    double weight = 1 ;
    //std::map<Vertex, Influence> influences;
};

//namespace std
//{
//template<>
//struct hash<Edge>
//{
//   typedef Vertex argument_type;
//   typedef size_t result_type;
//    result_type operator () (const argument_type& x) const
//    {
//       return boost::hash<int>()(x.get_id());
//    }
//};

#endif
