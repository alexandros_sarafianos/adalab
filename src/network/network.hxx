#ifndef NETWORK_HXX
#define NETWORK_HXX

#include "src/network/network.hpp"
#include <vector>
#include "src/network/vertex.hpp"
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <iostream>
#include <boost/graph/graph_utility.hpp>
/*
//Constructs network with vertices in vertices_set with all possible connection
extern template class Network<Sigmoid>;

//template<class __ThresholdFunction>
//Network<__ThresholdFunction>::Network( std::vector<Vertex> vertices_set )
//{
//    std::vector<vertex_t> boost_vertex_container;
//    //Add all vertices to the network
//    for( std::vector<Vertex>::iterator it = vertices_set.begin(); it!= vertices_set.end(); ++it)
//    {
//        vertex_t v= boost::add_vertex(*it,network);
//        boost_vertex_container.push_back(v);
//    }
//    for( std::vector<vertex_t>::iterator it = boost_vertex_container.begin(); it != boost_vertex_container.end();++it)
//    {
//        for( std::vector<vertex_t>::iterator it2 = it+1; it!= boost_vertex_container.end(); ++it)
//        {
//            Edge e;
//            boost::add_edge(*it,*it2,e,network);
//        }
//    }

//}
template<class __ThresholdFunction>
std::ostream& operator<<(std::ostream& out, const Network<__ThresholdFunction>& n)
{
    n.print_network(out);
    return out;
}


//template<class __ThresholdFunction>
//Network<__ThresholdFunction>::Network( std::vector<Vertex> vertices_set, bool full_connections )
//{
//    if( full_connections)
//    {
//        std::vector<vertex_t> boost_vertex_container;
//        //Add all vertices to the network
//        for( std::vector<Vertex>::iterator it = vertices_set.begin(); it!= vertices_set.end(); ++it)
//        {
//            vertex_t v= boost::add_vertex(*it,network);
//            boost_vertex_container.push_back(v);
//        }
//        std::vector<Vertex>::iterator v_it = vertices_set.begin();
//        for( std::vector<vertex_t>::iterator it = boost_vertex_container.begin(); it != boost_vertex_container.end();++it)
//        {
//            std::vector<Vertex>::iterator v_it2 = vertices_set.begin();
//            for( std::vector<vertex_t>::iterator it2 = boost_vertex_container.begin(); it2!= boost_vertex_container.end(); ++it2)
//            {
//                if( it != it2 )
//                {
//                    Edge e(*v_it,*v_it2);
////                  Edge e;
//                    boost::add_edge(*it,*it2,e,network);
//                }
//                v_it2++;
//            }
//            v_it++;
//        }
//    }
//    else
//    {
//        std::vector<vertex_t> boost_vertex_container;
//        //Add all vertices to the network
//        for( std::vector<Vertex>::iterator it = vertices_set.begin(); it!= vertices_set.end(); ++it)
//        {
//            vertex_t v= boost::add_vertex(*it,network);
//            boost_vertex_container.push_back(v);
//        }
//        std::vector<Vertex>::iterator v_it = vertices_set.begin();
//        for( std::vector<vertex_t>::iterator it = boost_vertex_container.begin(); it!= boost_vertex_container.end()-1; ++it)
//        {

//            Edge e(*v_it,*(v_it+1));
////                  Edge e;
//            boost::add_edge(*it,*(it+1),e,network);
//            v_it++;
//        }
//    }
//}


template<class __ThresholdFunction>
bool Network<__ThresholdFunction>::reset_intervention(const std::string name)
{
    std::pair<vertex_iter, vertex_iter> vp;
    for (vp = vertices(network); vp.first != vp.second; ++vp.first)
    {
        if ( network[*vp.first].get_name() == name)
        {
            network[*vp.first].reset();
            return true;
        }
    }
    return false;
}
template<class __ThresholdFunction>
bool Network<__ThresholdFunction>::intervention(const std::string name)
{
    std::pair<vertex_iter, vertex_iter> vp;
    for (vp = vertices(network); vp.first != vp.second; ++vp.first)
    {
        if ( network[*vp.first].get_name() == name)
        {
            network[*vp.first].intervention(false);
            return true;
        }
    }
    return false;
}
template<class __ThresholdFunction>
int Network<__ThresholdFunction>::greedy_colouring()const
{

    // use bdg

    // create an undirected graph with the vertices and edges of the first one
    typedef boost::property_map<Graph, boost::vertex_index_t>::type IndexMap;
    IndexMap index = boost::get(boost::vertex_index, network);
    Graph g;
    //UndirectedGraph g;
    copy_graph(network, g);

     const int vertices_amount = num_vertices(g);
     // Assign the first color to first vertex
     std::map<std::string, int> vertex_colouring;
     vertex_pair_iterators vp = vertices(g);
     vertex_colouring[g[*vp.first].name] = 0;
        ++vp.first; //start from second vertex
        for( ; vp.first!=vp.second; ++vp.first)
            vertex_colouring[g[*vp.first].name] = -1;
        // A temporary array to store the available colors. True
        // value of available[cr] would mean that the color cr is
        // assigned to one of its adjacent vertices
       // bool available[vertices_amount];
        boost::dynamic_bitset<> available(vertices_amount);
        for (int cr = 0; cr < vertices_amount; cr++)
            available[cr] = false;

        // Assign colors to remaining V-1 vertices
        vp = vertices(g); //reset to beginning
        ++vp.first; //start from second vertex
        for (; vp.first!=vp.second; ++vp.first)
        {

           for (std::pair<adjacency_it, adjacency_it> neighbours = boost::adjacent_vertices(*vp.first,g); neighbours.first != neighbours.second; ++neighbours.first)
           {
               if (vertex_colouring[g[*neighbours.first].name] != -1)
               {
                   available[vertex_colouring[g[*neighbours.first].name]] = true;
               }
               std::cout << "NEIGHBOUR " << g[*neighbours.first].name << " of " << g[*vp.first].name << " has colour " <<
               vertex_colouring[g[*neighbours.first].name] << NEW_LINE;
           }
           for (std::pair<inv_adjacency_iterator, inv_adjacency_iterator> neighbours = boost::inv_adjacent_vertices(*vp.first,g); neighbours.first != neighbours.second; ++neighbours.first)
           {
               if (vertex_colouring[g[*neighbours.first].name] != -1)
               {
                   available[vertex_colouring[g[*neighbours.first].name]] = true;
               }
               std::cout << "NEIGHBOUR " << g[*neighbours.first].name << " of " << g[*vp.first].name << " has colour " <<
               vertex_colouring[g[*neighbours.first].name] << NEW_LINE;
           }
           // Find the first available color
            int cr;
            for (cr = 0; cr < vertices_amount; cr++)
                if (available[cr] == false)
                    break;

            vertex_colouring[g[*vp.first].name] = cr; // Assign the found color
            for (std::pair<adjacency_it, adjacency_it> neighbours = boost::adjacent_vertices(*vp.first,g); neighbours.first != neighbours.second; ++neighbours.first)
                if (vertex_colouring[g[*neighbours.first].name] != -1)
                    available[vertex_colouring[g[*neighbours.first].name]] = false;
            for (std::pair<inv_adjacency_iterator, inv_adjacency_iterator> neighbours = boost::inv_adjacent_vertices(*vp.first,g); neighbours.first != neighbours.second; ++neighbours.first)
                if (vertex_colouring[g[*neighbours.first].name] != -1)
                    available[vertex_colouring[g[*neighbours.first].name]] = false;
        }

        // print the result
        int colour_number = 0;
        for (std::map<std::string, int>::iterator it = vertex_colouring.begin(); it != vertex_colouring.end(); ++it)
        {
            std::cout << "Vertex " << it->first << " --->  Color " << it->second << NEW_LINE;
            if(it->second > colour_number)
                colour_number = it->second;
        }
        return colour_number+1;


}
template<class __ThresholdFunction>
bool Network<__ThresholdFunction>::get_vertex_by_id(const std::string name, Vertex & v)const
{
    std::pair<vertex_iter, vertex_iter> vp;
    for (vp = vertices(network); vp.first != vp.second; ++vp.first)
    {
        if ( network[*vp.first].get_name() == name)
        {
            v = network[*vp.first];
            return true;
        }
    }
    return false;
}
//template<class __ThresholdFunction>
//void Network<__ThresholdFunction>::print_network(std::ostream & os)const;




*/
#endif // NETWORK_HXX
