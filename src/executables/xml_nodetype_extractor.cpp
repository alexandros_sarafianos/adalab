
#include <boost/program_options.hpp>
#include <string>
#include "src/io/xml_reader.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include "src/general/compile_configuration.hpp"

using namespace boost::program_options;

  int main(int argc, char * argv[])
  {
      std::vector<std::string> i_file;
      std::string o_file;
      options_description desc("Allowed options");
      desc.add_options()
              ("help,h", "print help message")
              ("input_file,i", value<std::vector<std::string> >(&i_file),"Input file")
              ("output_file,o", value<std::string>(&o_file), "Output file")
            ;

      variables_map vm;
      store(parse_command_line(argc, argv, desc), vm);
      if(!vm.count("input_file"))
      {
        std::string path ="/home/alexs/AdaLab/Data/xml_zimmer/";

        //networks
        i_file.push_back(path+"diauxic_grn_1.xml");
        i_file.push_back(path+"diauxic_grn_2.xml");
        i_file.push_back(path+"diauxic_grn_3.xml");
        i_file.push_back(path+"diauxic_grn_4.xml");
        i_file.push_back(path+"diauxic_grn_5.xml");
        i_file.push_back(path+"diauxic_grn_6.xml");
        i_file.push_back(path+"diauxic_grn_7.xml");
        i_file.push_back(path+"diauxic_grn_8.xml");

        //pathways
        i_file.push_back(path+"diauxic_shift.xml");
        i_file.push_back(path+"ethanol_metabolism.xml");
        i_file.push_back(path+"fatty_acid_metabolism.xml");

        i_file.push_back(path+"gluconeogenesis.xml");

        i_file.push_back(path+"glycerol_metabolism.xml");

        i_file.push_back(path+"glyoxylate_cycle.xml");
        i_file.push_back(path+"lactate_metabolism.xml");
        i_file.push_back(path+"TCA_cycle.xml");
      }
      std::map<std::string, std::set<std::string> > type_symbol_map;
      if(!vm.count("output_file"))
          o_file = "node_list";
      for(std::vector<std::string>::const_iterator it = i_file.begin(); it != i_file.end(); ++it)
      {
          XML_reader xml_reader(*it);
          xml_reader.get_node_info(type_symbol_map);
      }
      for(std::map<std::string, std::set<std::string> >::const_iterator it = type_symbol_map.begin(); it != type_symbol_map.end(); ++it)
      {
          std::ofstream os;
          open_output_file_safely("/home/alexs/AdaLab/"+o_file, os);
          if( it->first != "evidence" && it->first != "growthphase")
          {
          os << "#" << it->first << NEW_LINE;
          for( std::set<std::string>::const_iterator set_it = it->second.begin(); set_it != it->second.end(); ++set_it)
              os << *set_it << NEW_LINE;
          }
      }


//      XML_reader writer;
//      writer.write_map_to_xml_file(type_symbol_map, o_file);

      //open_file_safely(const std::string filename, std::ifstream &opened_file );
//      std::string p = "/home/alexs/AdaLab/Data/zimmer_cont_network.xml";
//      XML_reader network_reader(p);
////      std::vector<std::pair< std::string, std::string> > inputs;
////      double constant;
////      std::string output;
////      network_reader.read_continuous_network_file(inputs, constant, output );
////      network_reader.test();
//      std::vector< std::tuple< std::vector<std::pair<std::string, std::string> >, double, std::string > > rxns;
//      network_reader.read_continuous_network_file( rxns );
//      for( auto it = rxns.begin(); it!= rxns.end(); ++it)
//      {
//          std::cout << "\nNew tuple ";
//          std::cout << std::get<0>(*it);
//          std::cout << std::get<1>(*it);
//          std::cout << std::get<2>(*it);
//      }
      return 0;
  }
