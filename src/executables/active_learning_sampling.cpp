#include <fstream>
#include <iostream>
#include <istream>
#include <ostream>

#include <iostream>
//#include "src/optimization/parameters.hpp"

#include <shark/Data/Csv.h>
#include <shark/Algorithms/GradientDescent/CG.h>
#include <shark/ObjectiveFunctions/ErrorFunction.h>
#include <shark/Models/Kernels/GaussianRbfKernel.h>
#include <shark/Models/Kernels/KernelExpansion.h>
#include <shark/Algorithms/Trainers/CSvmTrainer.h>


//struct MyRegressionFunction
//{
//    MyRegressionFunction(){}
//    long double operator()(const Parameters & p)
//    {
//        return 0.;
//    }
//};
//struct MyClassificationFunction
//{
//    MyClassificationFunction(){}

//};



#include <iostream>


using namespace shark;

int main()
{
////        // Load data, use 70% for training and 30% for testing.
////        // The path is hard coded; make sure to invoke the executable
////        // from a place where the data file can be found. It is located
////        // under [shark]/examples/Supervised/data.
//        shark::ClassificationDataset traindata, testdata;
//        shark::importCSV(traindata, "/home/alexs/test/Shark/examples/Supervised/data/quickstartData.csv", shark::LAST_COLUMN, ' ');
//        testdata = shark::splitAtElement(traindata, 70 * traindata.numberOfElements() / 100);

////        // TODO: define a model and a trainer
//    double gamma = 1.0;         // kernel bandwidth parameter
//           double C = 10.0;            // regularization parameter
//           GaussianRbfKernel<RealVector> kernel(gamma);
//           KernelClassifier<RealVector> model(&kernel);
//           RegularizationNetworkTrainer<RealVector> trainer(
//                           &kernel,
//                           C,
//                           true); /* true: train model with offset */



//        trainer.train(model, traindata);

//        Data<unsigned int> prediction = model(testdata.inputs());
////        Data<RealVector> prediction = model(testdata.inputs());
//        ZeroOneLoss<unsigned int> loss;
////        ZeroOneLoss<unsigned int, RealVector> loss;
//        double error_rate = loss(testdata.labels(), prediction);

//        std::cout << "model: " << model.name() << std::endl
//                << "trainer: " << trainer.name() << std::endl
//                << "test error rate: " << error_rate << std::endl;
}

//int main(int argc, char * argv[])
//{
////    // The svm functions use column vectors to contain a lot of the data on which they
////    // operate. So the first thing we do here is declare a convenient typedef.

////    // This typedef declares a matrix with 2 rows and 1 column.  It will be the object that
////    // contains each of our 2 dimensional samples.   (Note that if you wanted more than 2
////    // features in this vector you can simply change the 2 to something else.  Or if you
////    // don't know how many features you want until runtime then you can put a 0 here and
////    // use the matrix.set_size() member function)
////    typedef dlib::matrix<double, 2, 1> sample_type;

////    // This is a typedef for the type of kernel we are going to use in this example.  In
////    // this case I have selected the radial basis kernel that can operate on our 2D
////    // sample_type objects
////    typedef radial_basis_kernel<sample_type> kernel_type;


////    // Now we make objects to contain our samples and their respective labels.
////    std::vector<sample_type> samples;
////    std::vector<double> labels;

////    // Now let's put some data into our samples and labels objects.  We do this by looping
////    // over a bunch of points and labeling them according to their distance from the
////    // origin.
////    for (int r = -20; r <= 20; ++r)
////    {
////        for (int c = -20; c <= 20; ++c)
////        {
////            sample_type samp;
////            samp(0) = r;
////            samp(1) = c;
////            samples.push_back(samp);

////            // if this point is less than 10 from the origin
////            if (sqrt((double)r*r + c*c) <= 10)
////                labels.push_back(+1);
////            else
////                labels.push_back(-1);

////        }
////    }


////    // Here we normalize all the samples by subtracting their mean and dividing by their
////    // standard deviation.  This is generally a good idea since it often heads off
////    // numerical stability problems and also prevents one large feature from smothering
////    // others.  Doing this doesn't matter much in this example so I'm just doing this here
////    // so you can see an easy way to accomplish this with the library.
////    vector_normalizer<sample_type> normalizer;
////    // let the normalizer learn the mean and standard deviation of the samples
////    normalizer.train(samples);
////    // now normalize each sample
////    for (unsigned long i = 0; i < samples.size(); ++i)
////        samples[i] = normalizer(samples[i]);


////    // Now that we have some data we want to train on it.  However, there are two
////    // parameters to the training.  These are the nu and gamma parameters.  Our choice for
////    // these parameters will influence how good the resulting decision function is.  To
////    // test how good a particular choice of these parameters is we can use the
////    // cross_validate_trainer() function to perform n-fold cross validation on our training
////    // data.  However, there is a problem with the way we have sampled our distribution
////    // above.  The problem is that there is a definite ordering to the samples.  That is,
////    // the first half of the samples look like they are from a different distribution than
////    // the second half.  This would screw up the cross validation process but we can fix it
////    // by randomizing the order of the samples with the following function call.
////    randomize_samples(samples, labels);


////    // The nu parameter has a maximum value that is dependent on the ratio of the +1 to -1
////    // labels in the training data.  This function finds that value.
////    const double max_nu = maximum_nu(labels);

////    // here we make an instance of the svm_nu_trainer object that uses our kernel type.
////    svm_nu_trainer<kernel_type> trainer;

////    // Now we loop over some different nu and gamma values to see how good they are.  Note
////    // that this is a very simple way to try out a few possible parameter choices.  You
////    // should look at the model_selection_ex.cpp program for examples of more sophisticated
////    // strategies for determining good parameter choices.
////    cout << "doing cross validation" << endl;
////    for (double gamma = 0.00001; gamma <= 1; gamma *= 5)
////    {
////        for (double nu = 0.00001; nu < max_nu; nu *= 5)
////        {
////            // tell the trainer the parameters we want to use
////            trainer.set_kernel(kernel_type(gamma));
////            trainer.set_nu(nu);

////            cout << "gamma: " << gamma << "    nu: " << nu;
////            // Print out the cross validation accuracy for 3-fold cross validation using
////            // the current gamma and nu.  cross_validate_trainer() returns a row vector.
////            // The first element of the vector is the fraction of +1 training examples
////            // correctly classified and the second number is the fraction of -1 training
////            // examples correctly classified.
////            cout << "     cross validation accuracy: " << cross_validate_trainer(trainer, samples, labels, 3);
////        }
////    }


////    // From looking at the output of the above loop it turns out that a good value for nu
////    // and gamma for this problem is 0.15625 for both.  So that is what we will use.

////    // Now we train on the full set of data and obtain the resulting decision function.  We
////    // use the value of 0.15625 for nu and gamma.  The decision function will return values
////    // >= 0 for samples it predicts are in the +1 class and numbers < 0 for samples it
////    // predicts to be in the -1 class.
////    trainer.set_kernel(kernel_type(0.15625));
////    trainer.set_nu(0.15625);
////    typedef decision_function<kernel_type> dec_funct_type;
////    typedef normalized_function<dec_funct_type> funct_type;

////    // Here we are making an instance of the normalized_function object.  This object
////    // provides a convenient way to store the vector normalization information along with
////    // the decision function we are going to learn.
////    funct_type learned_function;
////    learned_function.normalizer = normalizer;  // save normalization information
////    learned_function.function = trainer.train(samples, labels); // perform the actual SVM training and save the results

////    // print out the number of support vectors in the resulting decision function
////    cout << "\nnumber of support vectors in our learned_function is "
////         << learned_function.function.basis_vectors.size() << endl;

////    // Now let's try this decision_function on some samples we haven't seen before.
////    sample_type sample;

////    sample(0) = 3.123;
////    sample(1) = 2;
////    cout << "This is a +1 class example, the classifier output is " << learned_function(sample) << endl;

////    sample(0) = 3.123;
////    sample(1) = 9.3545;
////    cout << "This is a +1 class example, the classifier output is " << learned_function(sample) << endl;

////    sample(0) = 13.123;
////    sample(1) = 9.3545;
////    cout << "This is a -1 class example, the classifier output is " << learned_function(sample) << endl;

////    sample(0) = 13.123;
////    sample(1) = 0;
////    cout << "This is a -1 class example, the classifier output is " << learned_function(sample) << endl;


////    // We can also train a decision function that reports a well conditioned probability
////    // instead of just a number > 0 for the +1 class and < 0 for the -1 class.  An example
////    // of doing that follows:
////    typedef probabilistic_decision_function<kernel_type> probabilistic_funct_type;
////    typedef normalized_function<probabilistic_funct_type> pfunct_type;

////    pfunct_type learned_pfunct;
////    learned_pfunct.normalizer = normalizer;
////    learned_pfunct.function = train_probabilistic_decision_function(trainer, samples, labels, 3);
////    // Now we have a function that returns the probability that a given sample is of the +1 class.

////    // print out the number of support vectors in the resulting decision function.
////    // (it should be the same as in the one above)
////    cout << "\nnumber of support vectors in our learned_pfunct is "
////         << learned_pfunct.function.decision_funct.basis_vectors.size() << endl;

////    sample(0) = 3.123;
////    sample(1) = 2;
////    cout << "This +1 class example should have high probability.  Its probability is: "
////         << learned_pfunct(sample) << endl;

////    sample(0) = 3.123;
////    sample(1) = 9.3545;
////    cout << "This +1 class example should have high probability.  Its probability is: "
////         << learned_pfunct(sample) << endl;

////    sample(0) = 13.123;
////    sample(1) = 9.3545;
////    cout << "This -1 class example should have low probability.  Its probability is: "
////         << learned_pfunct(sample) << endl;

////    sample(0) = 13.123;
////    sample(1) = 0;
////    cout << "This -1 class example should have low probability.  Its probability is: "
////         << learned_pfunct(sample) << endl;

//}
