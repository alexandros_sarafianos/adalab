#include "src/Eve/evesimulator.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include <boost/filesystem.hpp>

int main(int argc, char * argv[])
{
    std::string path = get_home_directory()+"/test/";
    std::string noise_distribution = "uniform";
    double noise_param = 0.0;
    RandomWrapper random_number_generator (noise_distribution, noise_param);
//    ThresholdFunction* thresh;

    EveSimulator<bool> eve_bool(path+"Data/zimmer_boolean_network.txt", random_number_generator/*, thresh*/, path+"Data/nodetypes.xml");

    EveSimulator<double> eve_cont(path+"Data/regression_param_report30minSVRLin.txt", random_number_generator,/* thresh,*/ "");
    eve_cont.create_settings_file("/home/alexs/test/settings_v2.txt","/home/alexs/test//exp_settings_v2.pl" );

    return 0;
}
