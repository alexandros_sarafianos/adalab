
#include "src/Eve/evesimulator.hpp"

#include "src/io/xml_reader.hpp"

#include <boost/graph/bipartite.hpp>
#include <boost/program_options.hpp>

#include <vector>
#include <random>

#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include "src/general/maths/sigmoid.hpp"
#include "src/general/maths/unit_step.hpp"
#include "src/general/maths/RandomWrapper.hpp"
#include "src/general/compile_configuration.hpp"
#include "src/general/singleton.hxx"
#include "src/general/logger.hpp"
#include <boost/filesystem.hpp>
#include "src/io/xml_reader.hpp"
#include "src/experiment/experimentalspace.hpp"

#include "src/network/vertex.hpp"
//#include <RInside.h>                    // for the embedded R via RInside

using namespace boost::program_options;



int main(int argc, char * argv[])
{
    std::string network_file,xml_nodes, experiment_file, noise_distribution, path ,config_file, result_directory;
    int thresh_func,colony_size, duration;
    std::string initial_values;
    std::vector<double> threshold_func_params;
    bool save_output, silent,booleanVersion;
    double noise_param;
    bool write_boolean_output = false;
    float ratio = 0.5;
    int initialization_type = 0;
    int update_rule;
    bool limit_range = false;
    options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "print help message")
        ("network-file,n",value<std::string>(&network_file),"The network file")
        ("descriptor-nodes,d",value<std::string>(&xml_nodes), "File with nodes contained in network file")
        ("experiment-file,e", value<std::string>(&experiment_file), "File with experiment to perform!")
        ("projectdirectory,p", value<std::string>(&path), "path to adalab project directory")
        ("time-steps,t", value<int>(&duration), "How many times values have to be propagated through network")
        ("ratio", value<float>(&ratio), "(DEVELOPMENT) For random initialization of nodes, approximate value for ratio of nodes allowed at 1, default = 0.5")
        ("save-output,o", value<bool>(&save_output),"Do you want to save the output")
        ("silent,s", value<bool>(&silent), "No logging")
        ("config-file,c", value<std::string>(&config_file), "Path to config file")
        ("output-type", value<bool>(&booleanVersion), "True -> output is boolean, false -> output is continuous ")
        ("result-directory,r", value<std::string>(&result_directory), "(DEVELOPMENT) Directory where result files should be saved")
        ("initialization-type", value<int>(&initialization_type), "0 or unspecified -> all 0, 1->random, controlled by parameter ratio, 2-> dictionary file")
        ("initial-values,i", value<std::string>(&initial_values), "(DEVELOPMENT) File with initial values")
        ("threshold-func", value<int>(&thresh_func), "(DEVELOPMENT) Threshold function, only supports sigmoid(=0) and unit_step(=1) for now")
        ("threshold-func-param", value<std::vector<double> >(&threshold_func_params), "(DEVELOPMENT) Parameters for threshold function, dependent on choice![add more details]")
        ("colony-size", value<int>(&colony_size), "(DEVELOPMENT) Run experiment c times (1 run = one single cell, simulates colony), only makes sense when noise is being generated ")
        ("noise-distribution", value<std::string>(&noise_distribution), "(DEVELOPMENT) Noise distribution, uniform or normal")
        ("noise-param", value<double>(&noise_param), "(DEVELOPMENT) Width of uniform distribution, only used if uniform distribution(noise) is selected- Variance of normal distribution, only used if normal distribution(noise) is selected!!")
        ("update-rule", value<int>(&update_rule), "(DEVELOPMENT) Update rule for reactions with same head. 0-> logical or, 1-> sum-based approach cfr. the yeast cycle is robustly designed")
        ("limit-range", value<bool>(&limit_range), "Cut off gene expression level when it exceeds out of the interval [-1,1], only useful for continuous case. Default = false")
            ;
    variables_map vm;

    store(parse_command_line(argc, argv, desc), vm);
    if( vm.count("config-file") )
    {
        std::ifstream ifs(config_file);
        store(parse_config_file(ifs, desc), vm);
    }
    notify(vm);
    for(int index = 0; index < argc ; ++index)
        std::cout << argv[index] << " ";
    std::cout << NEW_LINE;

    if( vm.count("help"))
    {
        std::cout << desc << NEW_LINE;
        return 0;
    }
    if( !vm.count("save-output"))
        save_output=false;
    if( !vm.count("projectdirectory"))
        path = get_home_directory()+"/AdaLab/";
    if( !vm.count("network-file"))
    {
        if( booleanVersion )
            network_file = path+"Data/zimmer_boolean_network.txt";
        else
            network_file = path+"Data/regression_param_report30minSVRLin.txt";
    }
    if ( !vm.count("descriptor-nodes"))
    {
        if( booleanVersion )
            xml_nodes =path+"Data/nodetypes.xml";
        else
            xml_nodes = "";
    }
    // janr : I think the above if should initialize xml_nodes
    // xml_nodes =path+"Data/nodetypes.xml";
    if( !vm.count("experiment-file"))
        experiment_file = path+"/Data/experiment.xml";
    if( !vm.count("threshold-func"))
        thresh_func = 1;
    if( !vm.count("result-directory"))
    {
        std::string date_time = currentDateTime();
        std::replace( date_time.begin(), date_time.end(), ':', '-');
        result_directory =path+".simulator_experiments/experiment"+date_time;
    }
    if( !vm.count("threshold-func-param"))
    {
        if(thresh_func==0)
        {
            threshold_func_params.push_back(50);
            threshold_func_params.push_back(0.5);
        }
        else if( thresh_func == 1 )
        {
            threshold_func_params.push_back(0.);
            threshold_func_params.push_back(1.);
            threshold_func_params.push_back(0.5);
        }

    }
    if( noise_distribution != "uniform" && noise_distribution != "normal")
    {
        if( !vm.count("noise-distribution"))
        {
            noise_distribution = "uniform";
        }
    }
    if ( !vm.count("noise-param"))
        noise_param = 0.0;
    if(!vm.count("colony-size"))
        colony_size = 1;
    if(!vm.count("time-steps"))
        duration = 100;
    if( !vm.count("silent" ))
        silent = true;
    if(!vm.count("ratio"))
        ratio = 0.5;
    if( thresh_func == 0) //sigmoid
    {
//        sigm = Sigmoid(threshold_func_params[0], threshold_func_params[1]);
//        thresh = &sigm;
    }
    else if( thresh_func == 1 ) //unit step
    {
//        unit = Unit_step(threshold_func_params[0], threshold_func_params[1], threshold_func_params[2]);
//        thresh = &unit;
    }
    if( !vm.count("initial-values"))
        initial_values = "/home/alexs/Desktop/initial_values";

    if( !vm.count("update-rule"))
        update_rule = 0;
    if( !vm.count("limit-range"))
        limit_range = false;
    switch(update_rule)
    {
    case 0: Vertex<bool>::set_version(logical_or);
            Vertex<double>::set_version(logical_or);
            break;
    case 1: Vertex<bool>::set_version(sumV);
            Vertex<double>::set_version(sumV);
            break;
    default: std::cout << "error with version " << NEW_LINE;
    }
//    std::cout << "VErsion " << Vertex::get_version() << NEW_LINE;
    LoggerSingleton::Instance()->set_silent(silent);
    if(boost::filesystem::create_directories(result_directory) && (!silent || save_output) )
    {
            std::cerr << "Directory Created:  "<< result_directory << NEW_LINE;
    }
    if(!silent)
        LoggerSingleton::Instance()->open_log_file(result_directory, std::string(argv[0]));

    RandomWrapper random_number_generator (noise_distribution, noise_param);
    std::vector<ExperimentalOutput> output_vector;
    Experiment e;
    if( booleanVersion )
    {
        std::cout << "Boolean" << NEW_LINE;
        e = Experiment(experiment_file, xml_nodes);
        EveSimulator<bool> t(network_file, random_number_generator/*, thresh*/, xml_nodes);
        //initialize_vertex_type_map(xml_nodes);
        /*Output*/
        output_vector = t.run_experiment_for_colony(e, duration, colony_size, ratio, initialization_type, read_initial_values(initial_values) );
    }
    else if(!booleanVersion)
    {
        std::cout << "Continuous" << NEW_LINE;
        Vertex<double>::set_limit_range(limit_range);
        e = Experiment(experiment_file, ""); //
        EveSimulator<double> t(network_file, random_number_generator/*, thresh*/, "");

//        if (!silent)
//                t.print_underlying_network(std::cout);
        output_vector = t.run_experiment_for_colony(e,duration, colony_size, ratio, initialization_type, read_initial_values(initial_values));
    }


   if(save_output)
   {
       write_parameters(result_directory+"/parameter_info.txt",network_file,xml_nodes, noise_distribution, thresh_func,colony_size,duration,threshold_func_params[0],threshold_func_params[1]
        ,noise_param);
       e.write_experiment_info(result_directory+"/experiment_info.txt");
       #pragma omp parallel for
       for(unsigned int run_number=0; run_number<output_vector.size();++run_number)
       {
            output_vector[run_number].write_to_file(result_directory+"/experiment_output_run"+std::to_string(run_number)+".csv",write_boolean_output);
       }
   }
   return 0;
}
