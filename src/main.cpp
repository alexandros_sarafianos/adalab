//Standard includes
#include <set>
#include <iostream>

//Boost includes
#include <boost/program_options.hpp>

//AdaLab includes
#include "src/network/network.hpp"
#include "src/experiment/experiment.hpp"
#include "src/experiment/experimentaloutput.hpp"
#include "src/network/hypergraph.hpp"
#include "src/network/vertex.hpp"
#include "src/network/edge.hpp"
//#include "src/network/hyperdistriterator.hpp"


using namespace boost::program_options;


/* stub*/
int i = 0;
bool check_end_conditions()
{
    i++;
    if( i == 10)
        return true;
    return false;
}
std::set<Network> sample_networks()
{
    return std::set<Network>();
}

std::set<Experiment> determine_experiments()
{
    return std::set<Experiment>();
}
std::set<ExperimentalOutput> execute_experiments(std::set<Experiment> experiments)
{
    return std::set<ExperimentalOutput>();
}
/* end of stub*/

int main (int argc, char* argv[])
{
    std::string config_file;
    options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "print help message")
        ("config-file,c", value<std::string>(&config_file), "Path to config file")
        ;
    variables_map vm;

    store(parse_command_line(argc, argv, desc), vm);
    if( vm.count("config-file") )
    {
        std::ifstream ifs(config_file);
        store(parse_config_file(ifs, desc), vm);
    }
    notify(vm);
    if( vm.count("help"))
    {
        std::cout << desc << NEW_LINE;
        return 0;
    }

    bool finished = false;
    std::set<Network> networks;
    std::set<Experiment> experiments;
    std::set<ExperimentalOutput> outputs;
    Hypergraph<Vertex, Edge> h ("/home/alexs/Desktop/test.network");
    Hypergraph<Vertex, Edge> h2 ("/home/alexs/Desktop/test.network");
    Hypergraph<Vertex, Edge> * h_ptr = new Hypergraph<Vertex,Edge>("/home/alexs/Desktop/test.network");

    h == h2;
    int index = 1;
    HyperDistrIterator<Vertex, Edge> h_it   = h.begin();
    HyperDistrIterator<Vertex, Edge> h_end = h.end();
    bool a = h_it == h_end;
    std::cout << "Comp1 " << a << NEW_LINE;
    bool b = h.begin() == h.end();
    std::cout << "Comp2 " << b << NEW_LINE;

    for( h_it; h_it != h_end ; ++h_it)
    {
        std::cout << index << NEW_LINE;
        h_it.my_print();
        index++;
        if( index == 10)
            break;
    }
    return 0;

    while( !finished )
    {

        networks = sample_networks();
        experiments = determine_experiments();
        outputs = execute_experiments(experiments);

        finished = check_end_conditions();
    }
//    Hypergraph<Vertex,Edge> * h2 = h.my_clone();
//    std::cout<<"----h---" <<NEW_LINE;
//    h.print_hyperedges();

//    std::cout<<"----h2---" <<NEW_LINE;
//    h2->print_hyperedges();
//    h.remove_edges();
//    std::cout<<"----h---" <<NEW_LINE;
//    h.print_hyperedges();
//    std::cout<<"----h2---" <<NEW_LINE;
//    h2->print_hyperedges();
    //    HyperDistrIterator<Vertex,Edge> h_it(h_ptr);
//    Hypergraph<Vertex, Edge> h2( h );
//    std::cout<<"----h2---" <<NEW_LINE;
//    h2.print_hyperedges();

//    h = Hypergraph<Vertex,Edge>();
//    std::cout<<"----h---" <<NEW_LINE;
//    h.print_hyperedges();
//    std::cout<<"----h2---" <<NEW_LINE;
//    h2.print_hyperedges();
    return 0;
}
