#include "src/statistics/smeasure.hpp"
#include <iostream>

#include "src/general/compile_configuration.hpp"

int main(int argc, char * argv[])
{
  mclInitializeApplication(NULL, 0);
  std::stringstream os; os<<"OK_1"<<NEW_LINE;
  libsMeasureInitialize();
  std::stringstream os; os<<"OK_2"<<NEW_LINE;
  mwArray mapFile(argv[1]);
  std::stringstream os; os<<"OK_3"<<NEW_LINE;
  mwArray threshold(atoi(argv[2]));
  std::stringstream os; os<<"OK_4"<<NEW_LINE;

   if(argc!=3)
  {
      std::stringstream os; os << "usage: "<<argv[0]<<" <mapping file> <threshold>" ;LoggerSingleton::Instance()->info_print(os);
      return 0;
  }

  //read the mapping File
 mwArray map;
  mwArray nMap;
  readLargeMap(2,map,nMap,mapFile,threshold);

  //convert the mappings into the overlap hypergraph
  mwArray oh;
  mwArray tMap2oh;
  map2oh(2,oh,tMap2oh,map);

  //compute the support
  mwArray s;
  mwArray info;
  mwArray tSup;
  sMeaSeDuMi(3,s,info,tSup,oh);

  libsMeasureTerminate();
  mclTerminateApplication();

  std::stringstream os; os << "s = " << s.Get(1,1) << "." ;LoggerSingleton::Instance()->info_print(os);
  std::stringstream os; os << "It takes " << tSup.Get(1,1) << " secs." ;LoggerSingleton::Instance()->info_print(os);
  return 0;
}
