

//#include "patternMining/frequency/subgraphExactFrequency.hpp"
//#include "core/graph/graphAdjList.hpp"
//#include "core/graph/compactadjacencylist.hpp"
//#include "core/utils.hpp"
//#include "patternMining/graph/levelResult.hpp"

#include "src/statistics/smeasure.hpp"
#include <glpk.h>

#include <cstdlib>
#include <fstream>
#include <set>
#include <iostream>
#include  <assert.h>
/*
void s_support(const SupportMeasureEmbeddingContainer& embeddings, unsigned maxEmbd, bool& computable, double& support) {

  static bool warned = false;

  if(!warned) {
    std::cerr << "Warning: no matlab present, all patterns will be frequent!\n";
    warned = true;
  }

  computable = true;
  support = 1.0e20;
}
*/

void s_support(const SupportMeasureEmbeddingContainer& embeddings, unsigned maxEmbd, bool& computable, double& support) {
  //Debug switched on
  const unsigned debug = 1;
  if(embeddings.size() == 0) {
    support = 0;
    computable = true;
    return;
  }
  else if(embeddings.size() > maxEmbd)
    computable = false;
  else
    computable = true;
  glp_prob *lp = glp_create_prob();
  std::set<int> vertices;
  int max_v = 0;
  for(unsigned i=0;i<embeddings.size();i++)
    for(unsigned j=0;j<embeddings[i].size();j++) {
      vertices.insert(embeddings[i][j]);
      if(max_v < embeddings[i][j])
    max_v = embeddings[i][j];
    }
  std::vector<int> vertex_to_row(max_v+1);
  unsigned varidx = 0;
  if(debug)
    std::stringstream os; os << "max_v = "<<max_v;LoggerSingleton::Instance()->info_print(os);
  for(std::set<int>::iterator i = vertices.begin(); i!=vertices.end(); i++) {
    if(debug>2)
      std::stringstream os; os << " *i=" << *i << " <- " <<varidx ;LoggerSingleton::Instance()->info_print(os);
    vertex_to_row[*i] = varidx++;
  }
  if(debug)
    std::stringstream os; os << "#constraints = " << vertices.size() ;LoggerSingleton::Instance()->info_print(os);
  int rows = vertices.size();
  assert(rows == varidx);
  int cols = embeddings.size(); // # variables
  if(debug)
    std::stringstream os; os << "#vars = " << cols ;LoggerSingleton::Instance()->info_print(os);
  unsigned ar_size = embeddings.size() * embeddings[0].size();
  int* ia = new int[ar_size+1];
  int* ja = new int[ar_size+1];
  double *ar = new double[ar_size+1];
  unsigned r=1;
  for(unsigned i=0;i<embeddings.size();i++)
    for(unsigned j=0;j<embeddings[i].size();j++) {
      ia[r] = vertex_to_row[embeddings[i][j]]+1; // row index
      ja[r] = i+1; // column index, variable index
      ar[r] = 1.0; // coefficient
      r++;
    }
  assert(r==ar_size+1);
  glp_add_rows(lp,rows);
  glp_add_cols(lp,cols);
  for(int i=0;i<rows;i++)
    glp_set_row_bnds(lp, i+1, GLP_UP, 0.0, 1.0);
  for(int i=0;i<cols;i++)
    glp_set_col_bnds(lp, i+1, GLP_DB, 0.0, 1.0);
  for(int i=0;i<cols;i++)
    glp_set_obj_coef(lp, i+1, 1.0);
  glp_load_matrix(lp,ar_size,ia,ja,ar);
  glp_set_obj_dir(lp, GLP_MAX);
  int result;
  bool interior_point = true;
  //if(embeddings[0].size() == 2)
  interior_point = false;
  if(embeddings.size() < 200000)
    interior_point = true;
  if(interior_point) {
    glp_iptcp param;
    glp_init_iptcp(&param);
    //param.msg_lev = GLP_MSG_OFF;
    //param.ord_alg = GLP_ORD_AMD;
    result = glp_interior(lp, &param);
    support = glp_ipt_obj_val(lp);
  } else {
    glp_smcp param;
    glp_init_smcp(&param);
    //param.msg_lev = GLP_MSG_OFF;
    param.tm_lim = 30000;
    result = glp_simplex(lp,&param);
    support = glp_get_obj_val(lp);
  }
  if(debug)
    std::stringstream os; os << "return value = " << result ;LoggerSingleton::Instance()->info_print(os);
  glp_delete_prob(lp);

  delete[] ia;
  delete[] ja;
  delete[] ar;
  support += 1.0e-4; // to compensate minor approximation errors
}
