/*******************************************************************
* Copyright (c) 2010-2013 MiGraNT - DTAI.cs.kuleuven.be
* License details in the root directory of this distribution in
* the LICENSE file
********************************************************************/ 



#ifndef _PATMINE_SUPPORT_MEASURE_HPP
#define _PATMINE_SUPPORT_MEASURE_HPP

#include <vector>
#include "src/general/compile_configuration.hpp"

typedef std::vector<unsigned> SupportMeasureEmbedding;
typedef std::vector<SupportMeasureEmbedding> SupportMeasureEmbeddingContainer;

void s_support(const SupportMeasureEmbeddingContainer& embeddings, unsigned maxEmbd, bool& computable, double& support);

#endif // _PATMINE_SUPPORT_MEASURE_HPP

