
#include "src/experiment/experiment.hpp"
#include "src/io/xml_reader.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include <boost/foreach.hpp>
#include "src/general/logger.hpp"
#include "src/experiment/experiment.hpp"

/*Constructors*/

Experiment::Experiment(const std::string knockout_gene,
           const std::vector< std::vector< std::string> > & metabolites_over_time,
           const std::vector<std::vector< double> > &metabolite_ccs_over_time,
           const std::vector<std::string> &grey_genes,
           const std::vector<double> & grey_gene_vals,
           const std::vector<double> &temp_over_time,
           const std::vector<double> &co2_over_time)
{
    std::unordered_map<std::string,ExperimentalInfo> experiments;//    ExperimentalInfo(double m_value,bool m_fixed);

    experiments[knockout_gene] = ExperimentalInfo(0., true);
    assert(metabolites_over_time[0].size() == metabolite_ccs_over_time[0].size());
    assert(metabolites_over_time.size() == metabolite_ccs_over_time.size());
    assert(metabolite_ccs_over_time.size() == temp_over_time.size());
    assert(temp_over_time.size() == co2_over_time.size());
    assert(co2_over_time.size() > 0);
    auto metab_cc_iterator = metabolite_ccs_over_time[0].begin();
    for( auto metab_name_iterator = metabolites_over_time[0].begin(); metab_name_iterator != metabolites_over_time[0].end(); ++metab_name_iterator)
    {
        experiments[*metab_name_iterator] = ExperimentalInfo(*metab_cc_iterator, false);
        ++metab_cc_iterator;
    }
    experiments["CO2"] = ExperimentalInfo(co2_over_time[0], false);
    init_conds = InitialConditions(experiments);
    experiment_update_scheme = ExperimentUpdate(metabolites_over_time, metabolite_ccs_over_time, temp_over_time, co2_over_time);
}


Experiment::Experiment( const std::string experiment_file, const std::string xml_nodes)
{
    XML_reader xml_reader_exp(experiment_file);
    boost::unordered_set<std::string> node_set;
    std::unordered_map<std::string, ExperimentalInfo> experimental_nodes;
    if( !xml_nodes.empty())
    {
        XML_reader xml_reader_nodes(xml_nodes);
        //complex, condition, drug, gene, metabolite
        xml_reader_nodes.get_allowed_nodes(node_set);
    }
    xml_reader_exp.get_experiment_info(experimental_nodes);
    init_conds = InitialConditions(experimental_nodes);
    experiment_update_scheme = ExperimentUpdate();//Empty

    //adjust_experimentalinfo_value_ranges(value_range_min,value_range_max);
    assert(check_validity());
    INFO_LOW("===== Experiment ====="<< experimental_nodes << "===== End experiment =====");
}
Experiment::Experiment(std::unordered_map<std::string,ExperimentalInfo> & experiments)
{
    init_conds = InitialConditions(experiments);
    experiment_update_scheme = ExperimentUpdate();//Empty
    //experimental_nodes = experiments;
    //adjust_experimentalinfo_value_ranges(value_range_min,value_range_max);
    assert(check_validity());
    INFO_LOW("===== Experiment ====="<< init_conds.get_exp_nodes() << "===== End experiment =====");
}

/*Friendly functions*/
std::ostream& operator<<(std::ostream& out, const Experiment & classObj)
{
    out << "Experiment: " << classObj.get_exp_nodes();
    return out;
}


/*Other member functions*/
/*--------Functions wrt initialization*/

void Experiment::adjust_experimentalinfo_value_ranges(double value_range_min, double value_range_max)
{
    std::unordered_map<std::string, ExperimentalInfo> experimental_nodes = init_conds.get_exp_nodes();
    for(std::unordered_map<std::string,ExperimentalInfo >::iterator it =  experimental_nodes.begin(); it!=experimental_nodes.end();++it)
        it->second.adjust_value_with_value_ranges(value_range_min, value_range_max);
#ifndef NDEBUG
    for(std::unordered_map<std::string,ExperimentalInfo >::iterator it =  experimental_nodes.begin(); it!=experimental_nodes.end();++it)
        assert(in_range(value_range_min, value_range_max, it->second.initial_condition_value<double>()));
#endif
}
bool Experiment::check_validity(const boost::unordered_set<std::string> & node_set)const
{
    //TO DO: Check whether experiment is valid, only one knockout gene, fixed parameter(unsure), existing nodes, ..
    //check for allowed nodes
    std::unordered_map<std::string, ExperimentalInfo> experimental_nodes = init_conds.get_exp_nodes();
    bool result = true;
    if( node_set.size() != 0)
    {
        for(std::unordered_map<std::string,ExperimentalInfo>::const_iterator it = experimental_nodes.begin();it != experimental_nodes.end(); ++it)
        {
            if( node_set.find(it->first) == node_set.end()) // not found
            {
                result = false;
                INFO_ERROR("Experimental node " << it->first << " not found in " << node_set << "!" );
                break;
            }
        }
    }
    else
    {
        INFO_WARNING("Node_set was empty!");
    }
    return result;
}
//TODO: additional tests: check experiment fixed and metabolite change (e.g. B = 0, fixed, see correct operation when metabolite changes, [corner case as metabolite will never be fixed])
//TODO: some continuous tests

bool Experiment::check_validity()
{
    //TO DO: Check whether experiment is valid, only one knockout gene, fixed parameter(unsure), existing nodes, ..
    return true;
}

/*--------Real functionality*/
bool Experiment::return_experimental_condition(const std::string node_id, ExperimentalInfo & info)const
{
    std::unordered_map<std::string, ExperimentalInfo> experimental_nodes = init_conds.get_exp_nodes();
    std::unordered_map<std::string,ExperimentalInfo>::const_iterator it = experimental_nodes.find(node_id);
    if( it == experimental_nodes.end())
        return false;
    info = it->second;
    return true;
}

std::map<std::string,double> Experiment::get_initial_conditions()const
{
    std::map<std::string,double> experimental_nodes_ids;
    std::unordered_map<std::string, ExperimentalInfo> experimental_nodes = init_conds.get_exp_nodes();
    for(std::unordered_map<std::string,ExperimentalInfo >::const_iterator it = experimental_nodes.begin(); it!= experimental_nodes.end(); ++it)
        experimental_nodes_ids.insert(std::pair<std::string, double>(it->first,it->second.initial_condition_value<double>() ));
    return experimental_nodes_ids;
}

void Experiment::write_experiment_info(const std::string & file) const
{
  std::unordered_map<std::string, ExperimentalInfo> experimental_nodes = init_conds.get_exp_nodes();
  std::ofstream os;
  open_output_file_safely(file, os);
  os << experimental_nodes;
  os.close();
}

/*RCPP**/

//RCPP_MODULE(experiment)
//{
//    Rcpp::class_<Experiment>("Experiment")
//    .constructor<std::string,double, double,std::string>()
//            ;
//}
