#include "src/experiment/experimentalinfo.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include "src/network/vertex.hpp"

ExperimentalInfo::ExperimentalInfo(double m_value,bool m_fixed)
{
//    if(!in_range(LOWER_RANGE_LOGIC,UPPER_RANGE_LOGIC,m_value))
//    {
//        INFO_ERROR("m_value should be in range [0,1], 0 meaning inactive, 1 meaning active. Found value of " << m_value << " instead!");
//        assert(in_range(LOWER_RANGE_LOGIC,UPPER_RANGE_LOGIC,m_value));
//    }
    value = m_value;
    fixed = m_fixed;
}
void ExperimentalInfo::adjust_value_with_value_ranges(double value_range_min, double value_range_max)
{
    value = convert_value_to_new_range(LOWER_RANGE_LOGIC, UPPER_RANGE_LOGIC,
                                       value, value_range_min, value_range_max);//Transform to correct range [-1,1]
}

template<>
double ExperimentalInfo::initial_condition_value()const
{
    return value;
}
template<>
bool ExperimentalInfo::initial_condition_value()const
{
    if(value > 0.5)
        return true;
    else
        return false;
}
