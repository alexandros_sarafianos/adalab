#ifndef EXPERIMENTALOUTPUT_HPP
#define EXPERIMENTALOUTPUT_HPP

#include <boost/array.hpp>
#include "src/general/compile_configuration.hpp"

#include <map>
#include <vector>
#include <string>
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"

class ExperimentalOutput
{
public:
    //empty initializer
    ExperimentalOutput();
    // Simple initializer, don't use time series data
    //double version
    ExperimentalOutput( std::map<std::string, std::map<int,double> > & big_gene_level_map, const bool m_diauxic_shift){gene_map = big_gene_level_map; diauxic_shift = m_diauxic_shift;}
    //bool version
    ExperimentalOutput( std::map<std::string, std::map<int,bool> > & big_gene_level_map, const bool m_diauxic_shift);

    friend std::ostream& operator<<(std::ostream& out, const ExperimentalOutput& exp_output);
    template<class OutputType>
    typename std::map<int, OutputType> get_values_of_node(const std::string & node_id ) const;
    template < class OutputType>
    OutputType get_final_value_of_node( const std::string & node_id) const;
//    std::map<int, bool> get_values_of_node(const std::string & node_id ) const ;
    void write_to_file(const std::string & output_file ,const bool write_boolean_output)const;
    bool check_for_diauxic_shift(double threshold_val) const;
    bool get_diauxic_value()const {return diauxic_shift;}
    bool node_upregulated_once(const std::string &) const;
private:
    std::map<std::string, std::map<int,double>> gene_map;
    bool diauxic_shift;
};

#endif // EXPERIMENTALOUTPUT_HPP
