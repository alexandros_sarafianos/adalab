//#include "src/experiment/experimentgenerator.hpp"
#include <iostream>
#include "src/general/compile_configuration.hpp"
#include "data.hpp"
#include "src/Eve/evesimulator.hpp"
#include <chrono>

bool test_experiment_sampler()
{
    std::cout << "TEST_EXPERIMENT_SAMPLER " << NEW_LINE;
    bool ok = true;
    std::ofstream os;
//    Sigmoid sigmoid(15,0.5);
//    ThresholdFunction * thresh;
//    thresh = &sigmoid;
    RandomWrapper random_number_generator ("uniform", 0.1);
    EveSimulator<double> eve(DATA_FILE_SAMPLING_NETWORK, random_number_generator/*, thresh*/);
    ExperimentalSpace<> exp_f((Exp_Type) continuous,os);
    std::cout << "----------gene---------" << NEW_LINE;
    std::unordered_map<std::string, int> histogram_genes;
    for( int i = 0; i < 1000; ++i)
    {
        std::string g = exp_f.sample_gene();
        std::cout << g << NEW_LINE;
    }
    std::cout << "----------met---------" << NEW_LINE;
    std::unordered_map<std::string, int> histogram_met;
    for( int i = 0; i < 1000; ++i)
    {
        std::string m = exp_f.sample_metabolite();
        std::cout << m << NEW_LINE;
    }

//    std::unordered_map<std::string, int> histogram_met;
    std::cout << "----------temp---------" << NEW_LINE;
    for( int i = 0; i < 1000; ++i)
    {
        double t = exp_f.sample_temperature();
        std::cout << t << NEW_LINE;
    }
    std::cout << "----------co2---------" << NEW_LINE;
    for( int i = 0; i < 1000; ++i)
    {
        double c = exp_f.sample_co2();
        std::cout << c << NEW_LINE;
    }
    size_t number = 10;
    for( int n = 0; n < number ; ++n)
    {
        exp_f.sample_experiment();
    }
    std::cout << "END OF TEST_EXPERIMENT_SAMPLER " << NEW_LINE;
    return ok;
}

bool test_differences_when_subsequent_sampling()
{
    std::cout << "test_differences_when_subsequent_sampling " << NEW_LINE;
    std::ofstream os;
//    Sigmoid sigmoid(15,0.5);
//    ThresholdFunction * thresh;
//    thresh = &sigmoid;
    RandomWrapper random_number_generator ("uniform", 0.1);
    EveSimulator<double> eve(DATA_FILE_SAMPLING_NETWORK, random_number_generator/*, thresh*/);
    ExperimentalSpace<> exp_f((Exp_Type) continuous,os);
    std::vector<std::string> genes = exp_f.sample_n_genes_without_replacement(5);


    bool check1 = all_different(genes);
    if( !check1)
        std::cout << "Should be all different, but found " << genes << NEW_LINE;
    bool check2 = genes.size() == 5;
    if(!check2 )
        std::cout << "Expected size 5, but found " << genes.size() << NEW_LINE;
    std::vector<std::string> genes2 = exp_f.sample_n_genes_without_replacement(5);
    bool check3 = !equal_containers(genes, genes2);
    if(!check3)
        std::cout << "Two vectors should be different with very high probability, but they were found equal: " << genes << genes2 << NEW_LINE;
    std::cout << "end of test_differences_when_subsequent_sampling " << NEW_LINE;

    return check1 && check2 && check3;

}

void print_dimensionality()
{
     std::ofstream os;
//     Sigmoid sigmoid(15,0.5);
//     ThresholdFunction * thresh;
//     thresh = &sigmoid;
     RandomWrapper random_number_generator ("uniform", 0.1);
     EveSimulator<double> eve(DATA_FILE_SAMPLING_NETWORK, random_number_generator/*, thresh*/);
     ExperimentalSpace<> exp_f((Exp_Type) continuous,os);
       std::cout << "<<<<<<<dimensionality>>>>>>>" <<std::endl;
     exp_f.print_dimensionality();
}

bool test_sampling_iterator()
{
    std::ofstream os;
//    Sigmoid sigmoid(15,0.5);
//    ThresholdFunction * thresh;
//    thresh = &sigmoid;
//    RandomWrapper random_number_generator ("uniform", 0.1);
//    EveSimulator<double> eve(DATA_FILE_SAMPLING_NETWORK, random_number_generator/*, thresh*/);
//    ExperimentalSpace<> exp_f((Exp_Type) continuous,os);
//    ExperimentSampler exp_sampler;
//    auto start = std::chrono::system_clock::now();
//    exp_sampler.obtain_samples(exp_f, 1000000);
//    auto end = std::chrono::system_clock::now();
//    auto elapsed_init = std::chrono::duration_cast<std::chrono::milliseconds>(end - start );
//    std::cout << "elapsed: " << elapsed_init.count() << " ms! " << std::endl;
//    for( auto it = exp_sampler.begin(); it != exp_sampler.end(); ++it)
//    {
//        std::cout << *it << std::endl;
//    }
    return true;
}

int main(int, char*[])
{
   bool value = test_experiment_sampler();
   bool value2 = test_differences_when_subsequent_sampling();
   bool value3 = test_sampling_iterator();
   print_dimensionality();
   if( !value  || !value2)
   {
           std::cout << "test experiment sampler failed " << NEW_LINE;
           return 1;
   }
   return 0;
}
