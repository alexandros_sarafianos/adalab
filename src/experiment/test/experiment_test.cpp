//#include "src/experiment/experimentgenerator.hpp"
#include <iostream>
#include "src/general/compile_configuration.hpp"

int test_experiment_generator()
{
    return 0;
}

int main(int, char*[])
{
    int value = test_experiment_generator();
    if(value == 0)
    {
        std::stringstream os; os << "Test experiment_generator succeeded!";LoggerSingleton::Instance()->info_print(os);
    }
    else
    {
        std::stringstream os; os << "Test experiment_generator failed!" ;LoggerSingleton::Instance()->info_print(os);
    }
    return 0;
}
