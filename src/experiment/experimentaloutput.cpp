#include "experimentaloutput.hpp"

ExperimentalOutput::ExperimentalOutput()
{
}
ExperimentalOutput::ExperimentalOutput( std::map<std::string, std::map<int,bool> > & big_gene_level_map, const bool m_diauxic_shift)
{
//    gene_map = big_gene_level_map;
    diauxic_shift = m_diauxic_shift;
    std::map<std::string, std::map<int,double> > outer_map;
    for(std::map< std::string, std::map<int,bool> >::iterator it = big_gene_level_map.begin(); it != big_gene_level_map.end(); ++it)
    {
        //initialize new map
        std::map<int,double> inner_map;
        for(std::map<int,bool>::iterator inner_it = it->second.begin(); inner_it != it->second.end(); ++inner_it)
        {
            double value = inner_it->second? 1.:0.;
            inner_map[inner_it->first] = value;
        }
        outer_map[it->first] = inner_map;
    }
    gene_map = outer_map;
}

std::ostream& operator<<(std::ostream& out, const ExperimentalOutput& exp_output)
{
    out << "Printing gene map... " << NEW_LINE;
    for(std::map<std::string, std::map<int,double> >::const_iterator it = exp_output.gene_map.begin(); it != exp_output.gene_map.end(); ++it)
    {
        out << it->first << ";";
        for(std::map<int,double>::const_iterator it2= it->second.begin(); it2 != it->second.end(); ++it2 )
            out << it2->second << ";";
        out << NEW_LINE;
    }
    return out;
}


//get_values_of_node
template<>
std::map<int, double> ExperimentalOutput::get_values_of_node(const std::string & node_id )const
{
    std::map<std::string, std::map<int,double>>::const_iterator it;
    it = gene_map.find(node_id);
    if(it == gene_map.end())
    {
        INFO_ERROR("Value " << node_id << " not found!");
        return std::map<int, double>() ;
    }
    return it->second;
}
template<>
std::map<int, bool> ExperimentalOutput::get_values_of_node(const std::string & node_id ) const
{
    std::map<std::string, std::map<int,double>>::const_iterator it;
    it = gene_map.find(node_id);
    if(it == gene_map.end())
    {
        INFO_ERROR("Value " << node_id << " not found!");
        return std::map<int, bool>() ;
    }
    std::map<int, bool> boolean_copy;
    for(std::map<int,double>::const_iterator copy_it = it->second.begin(); copy_it != it->second.end(); ++copy_it)
    {
        boolean_copy[ copy_it->first ] = copy_it->second > 0.5? true: false;
    }
    return boolean_copy;
}
//typename std::map<int, OutputType> get_final_value_of_node( const std::string & node_id) const;
template<>
double ExperimentalOutput::get_final_value_of_node( const std::string & node_id) const
{
    std::map<std::string, std::map<int,double>>::const_iterator it;
    it = gene_map.find(node_id);
    if(it == gene_map.end())
    {
        INFO_ERROR("Value " << node_id << " not found!");
        return 0.;
    }

    double val = 0.;
    bool adjusted = false;
    for( auto internal_it = it->second.begin(); internal_it != it->second.end(); ++internal_it)
    {
        val = internal_it->second;
        adjusted = true;
    }
    if (adjusted)
            std::cout << "Adjusted " << NEW_LINE;
    return val;
}

template<>
bool ExperimentalOutput::get_final_value_of_node( const std::string & node_id) const
{
    std::map<std::string, std::map<int,double>>::const_iterator it;
    it = gene_map.find(node_id);
    if(it == gene_map.end())
    {
        INFO_ERROR("Value " << node_id << " not found!");
        return false ;
    }
    std::map<int, bool> boolean_copy;
    for(std::map<int,double>::const_iterator copy_it = it->second.begin(); copy_it != it->second.end(); ++copy_it)
    {
        boolean_copy[ copy_it->first ] = copy_it->second > 0.5? true: false;
    }
    bool val =  false;
    for ( auto internal_it =  boolean_copy.begin(); internal_it != boolean_copy.end() ; ++internal_it)
    {
        val = internal_it->second;

    }

    return val;
}

void ExperimentalOutput::write_to_file(const std::string & output_file,const bool write_boolean_output )const
{

  std::ofstream os;
  open_output_file_safely(output_file, os);
  std::vector<std::string> keys; keys.reserve(gene_map.size());
  for ( auto keyValue : gene_map){ keys.push_back(keyValue.first); }
  os << keys << NEW_LINE;
  os << keys << NEW_LINE;
  for(std::map<std::string, std::map<int,double> >::const_iterator it = gene_map.begin(); it != gene_map.end(); ++it)
  {
      if(!it->first.empty() && !(it->first[0] >= 48 && it->first[0] <=57) ) //filter out reaction vertices, TODO cleaner solution
      {
          os << it->first << ";";
          for(std::map<int,double>::const_iterator it2= it->second.begin(); it2 != it->second.end(); ++it2 )
          {
              if( write_boolean_output)
              {
                 bool value = (it2->second > 0.5);
                 os << value << ";";
              }
              else
              {
                  os << it2->second << ";";
              }
          }
          os << NEW_LINE;
      }
  }
  os << "#EOF#" << NEW_LINE;
  os.close();
}
bool ExperimentalOutput::check_for_diauxic_shift(double threshold_val) const
{
    std::map<int,double> diauxic_shift_node_vals = get_values_of_node<double>("diauxic_shift");
    bool i_diauxic_shift=false;
    for(std::map<int,double>::const_iterator it = diauxic_shift_node_vals.begin(); it!=diauxic_shift_node_vals.end(); ++it)
    {
        if( it->second >= threshold_val )
        {
            i_diauxic_shift = true;
            break;
        }
    }
    std::cout<< "Diauxic_shift="<<i_diauxic_shift<<NEW_LINE;
    return i_diauxic_shift;
}
bool ExperimentalOutput::node_upregulated_once(const std::string & node) const
{
    bool upregulated = false;
    std::map<int, double> vals = get_values_of_node<double>(node);
    for(std::map<int,double>::const_iterator it = vals.begin(); it!=vals.end(); ++it)
    {
        if( it->second >= 0.5 )
        {
            upregulated = true;
            break;
        }
    }
    return upregulated;
}
