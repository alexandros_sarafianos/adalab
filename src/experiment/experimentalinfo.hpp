#ifndef EXPERIMENTALINFO_HPP
#define EXPERIMENTALINFO_HPP
#include <iostream>
#include <fstream>
#include "src/general/compile_configuration.hpp"

struct ExperimentalInfo
{
public:
    ExperimentalInfo(){}

    ExperimentalInfo(double m_value,bool m_fixed);
    bool is_factor_fixed_throughout_experiment()const{ return fixed;}
    template<class OutputType>
    OutputType initial_condition_value()const;
    friend std::ostream& operator<<(std::ostream& out, const ExperimentalInfo e)
    {
        out << "Value = " << e.value << " Fixed = " << e.fixed;
        return out;
    }
    void adjust_value_with_value_ranges(double value_range_min, double value_range_max);
private:
    double value;
    bool fixed;
};


#endif // EXPERIMENTALINFO_HPP
