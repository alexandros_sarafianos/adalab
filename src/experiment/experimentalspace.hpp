#ifndef ExperimentalSpace_HPP
#define ExperimentalSpace_HPP

#include "src/network/vertex.hpp"

#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include <random>
#include "src/experiment/experiment.hpp"
#include <algorithm>    // std::find
#include <chrono>
#include <boost/algorithm/string.hpp>
#include <string>
#include <cctype>


class ExperimentSampler
{
private:
    typedef std::vector<Experiment> ExperimentList;
    ExperimentList experiments;
public:
    typedef ExperimentList::iterator iterator;
    typedef ExperimentList::const_iterator const_iterator;
    inline iterator begin(){return experiments.begin();}
    inline iterator end(){return experiments.end();}
    ExperimentSampler(){}
    template<class _ExperimentalSpace_>
    inline std::vector<Experiment> operator()(_ExperimentalSpace_ & exp_space, const size_t sample_size)const
    {
        return exp_space.sample_n_experiments(sample_size);
    }
    template< class _ExperimentalSpace_ >
    inline Experiment operator()(_ExperimentalSpace_ & exp_space)
    {
        return exp_space.sample_experiment();
    }
    template< class _ExperimentalSpace_ >
    inline void obtain_samples(_ExperimentalSpace_ & exp_space, const size_t sample_size)
    {
         experiments = exp_space.sample_n_experiments(sample_size);
    }
};

enum Exp_Type
{
    boolean,
    continuous
};

const double CONT_MIN = -1;
const double CONT_MAX = 1 ;
const double BOOL_MIN = 0;
const double BOOL_MAX = 1;
const double TEMP_MIN = 0.0;
const double TEMP_MAX = 42.0;
const double CARB_MIN = 0.0;
const double CARB_MAX = 1.;
const double GREY_MIN = 0.;
const double GREY_MAX = 1.0;
const double MET_MIN = 0.;
const double MET_MAX = 1.;
template<class _GeneDistribution = std::uniform_int_distribution<int>,
         class _MetaboliteDistribution = std::uniform_int_distribution<int>,
         class _TemperatureDistribution =std::uniform_real_distribution<double>,
         class _CarbonDioxideDistribution = std::uniform_real_distribution<double>,
         class _GreyValueDistribution = std::uniform_real_distribution<double>,
         class _MetaboliteValueDistribution = std::uniform_real_distribution<double>,
         class _RandomEngine = std::default_random_engine>
class ExperimentalSpace
{
public:
    ExperimentalSpace(){} //Empty for template initialization
    ExperimentalSpace(const Exp_Type &t, std::ofstream & os = std::ofstream() );
    void test_print();
    std::string sample_gene();
    std::vector<std::string> sample_n_genes_without_replacement(const size_t n);
    std::vector<std::string> sample_n_metabolites_without_replacement(const size_t n );
    std::string sample_metabolite();
    inline double sample_temperature(){return t_distr(generator);}
    double sample_co2(){return co2_distr(generator);}
    std::vector<Experiment> sample_n_experiments(const size_t n = 1000,
                                                 const size_t amount_of_grey_genes = 2,
                                                 const size_t amount_of_metabolites = 5,
                                                 const size_t time_steps = 80);
    double sample_grey_value(){return grey_value_distr(generator);}
    double sample_metabolite_value(){return metab_val_distr(generator);}

    Experiment sample_experiment(const size_t amount_of_grey_genes = 2,
                                 const size_t amount_of_metabolites = 5,
                                 const size_t time_steps = 80, bool silent = true);
    template<typename SampleType>
    typename std::vector<SampleType> sample_function_n_times_with_replacement(const size_t n, SampleType (ExperimentalSpace::*sampler)(void));
    void print_dimensionality(std::ostream & os = std::cout);

private:
    int amount_of_genes=0;
    int amount_of_metabolites=0;
    _GeneDistribution gene_distr;
    _MetaboliteDistribution metab_distr;
    _TemperatureDistribution t_distr;
    _CarbonDioxideDistribution co2_distr;
    _GreyValueDistribution grey_value_distr;
    _MetaboliteValueDistribution metab_val_distr;

    _RandomEngine generator;
    Exp_Type tt;


    std::set<VertexType> supported_types;
    std::map<VertexType, std::vector<std::string> > node_overview;
    std::map<VertexType, std::pair<double, double> > min_max_per_type;
//    std::map<VertexType, ParameterType> setting_type_per_type;
    std::map<VertexType, double> discretization_per_type;
    void initialize_supported_types();
    void initialize_min_max_per_type();
    void initialize_discretization_per_type();
    void initialize_sample_space(const _GeneDistribution & g = std::uniform_int_distribution<int>(0,1),
                                 const _MetaboliteDistribution & m = std::uniform_int_distribution<int>(0,1) ,
                                 const _TemperatureDistribution & t = std::uniform_real_distribution<double>(TEMP_MIN, TEMP_MAX),
                                 const _CarbonDioxideDistribution & c = std::uniform_real_distribution<double>(CARB_MIN,CARB_MAX),
                                 const _GreyValueDistribution & gv = std::uniform_real_distribution<double>(GREY_MIN, GREY_MAX),
                                 const _MetaboliteValueDistribution & met_val = std::uniform_real_distribution<double>(MET_MIN, MET_MAX),
                                 const _RandomEngine & r = std::default_random_engine() );


};
template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
void ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::initialize_sample_space(const GeneDistribution & g,
                                                                                                                                                            const MetaboliteDistribution & m,
                                                                                                                                                            const TemperatureDistribution & t,
                                                                                                                                                            const CarbonDioxideDistribution & c,
                                                                                                                                                            const GreyValueDistribution & gv,
                                                                                                                                                            const MetaboliteValueDistribution & met_val,
                                                                                                                                                            const RandomEngine & r)
{
    bool randomization = true;
    if( randomization )
    {
        std::random_device rd;
        auto seed_number = rd();
        std::cout << "SEED NUMBER " << seed_number << "used !!!" << NEW_LINE;
        generator = RandomEngine(seed_number);
    }
    else
    {
        generator = r;
    }
    gene_distr = std::uniform_int_distribution<int>(0,amount_of_genes-1);
    metab_distr = std::uniform_int_distribution<int>(0,amount_of_metabolites-1);
    t_distr = t;
    co2_distr = c;
    grey_value_distr = gv;
    metab_val_distr = met_val;

}

template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::ExperimentalSpace(const Exp_Type &t, std::ofstream &os)
{
    std::unordered_map<std::string,VertexType> vertex_type_map;
    std::string exampleExpSpec;
    tt = t;
    if( tt == continuous)
    {
        vertex_type_map = Vertex<double>::get_vertex_map(  );
        exampleExpSpec = "exampleExpSpec0";
    }
    else
    {
        exampleExpSpec = "exampleExpSpec1";
        vertex_type_map = Vertex<bool>::get_vertex_map();
    }
    for( auto pair_name_type_it = vertex_type_map.begin(); pair_name_type_it != vertex_type_map.end(); ++pair_name_type_it)
    {
        node_overview[pair_name_type_it->second].push_back(pair_name_type_it->first);
    }
    amount_of_genes = ((node_overview.find((VertexType) gene))->second).size();
    assert(amount_of_genes > 0);
    amount_of_metabolites = ((node_overview.find((VertexType) metabolite))->second).size();
    assert(amount_of_metabolites > 0);

    os << "experiment_specification("<<exampleExpSpec <<")."<<std::endl;
    initialize_supported_types();
    initialize_min_max_per_type();
    initialize_discretization_per_type();
    initialize_sample_space();
    for( auto it = node_overview.begin(); it != node_overview.end(); ++it )
    {
        if( supported_types.find(it->first) != supported_types.end())
        {
            os <<  get_string_for_vertex_type(it->first) << "("<<exampleExpSpec <<"," << it->second << ")." << NEW_LINE;
        }
    }


    auto disc_it = discretization_per_type.begin();
    for( auto it = min_max_per_type.begin(); it != min_max_per_type.end(); ++it )
    {
        if( supported_types.find(it->first) != supported_types.end())
        {

            using namespace std;
            double min = it->second.first;
            double max = it->second.second;
            std::string type = get_string_for_vertex_type(it->first);
            std::string type_upper = type;
            type_upper[0] = toupper(type_upper[0]);
            if( type != "gene" )
                {
                std::string min_max_string = "real(" + static_cast<ostringstream*>( &(ostringstream() << min) )->str()  +"," + static_cast<ostringstream*>( &(ostringstream() << max) )->str();
                assert(disc_it->first == it->first);
                if( approximately_equal(disc_it->second,0.) )
                {
                    min_max_string +=  ")";
                }
                else
                {
                    min_max_string += ","  + static_cast<ostringstream*>( &(ostringstream() << disc_it->second) )->str()  + ")";
                }

                os <<  "envStateSpec("<< exampleExpSpec <<"," << type <<"("<< type_upper <<")," << min_max_string << "):-" << NEW_LINE;
                os << type << "(" <<exampleExpSpec <<", " << type_upper << "List)," << NEW_LINE;
                os << "member(" <<  type_upper << ", " << type_upper << "List)." << NEW_LINE;
            }
            else
            {
                os << "strainSpec(" << exampleExpSpec << ",knockOut(Gene), bool):-" << NEW_LINE;
                os << type << "(" <<exampleExpSpec <<", " << type_upper << "List)," << NEW_LINE;
                os << "member(" <<  type_upper << ", " << type_upper << "List)." << NEW_LINE;
                os << "strainSpec(" << exampleExpSpec << ",greyScale(Gene), real(0,1,Resolution)):-" << NEW_LINE;
                os << type << "(" <<exampleExpSpec <<", " << type_upper << "List)," << NEW_LINE;
                os << "member(" <<  type_upper << ", " << type_upper << "List)," << NEW_LINE;
                os << "Resolution = 0.1." << NEW_LINE;
            }
        }
        ++disc_it;
    }
//    for( auto it = discretization_per_type.begin(); it != discretization_per_type.end(); ++it )
//    {
//        if( supported_types.find(it->first) != supported_types.end())
//        {

//            os <<  "discretization("<<exampleExpSpec <<"," << get_string_for_vertex_type(it->first) <<","<< it->second << ")." << NEW_LINE;
//        }
//    }


}
template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
void ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::print_dimensionality(std::ostream & os)
{
    os << "Amount of genes : " << ((node_overview.find((VertexType) gene))->second).size() << std::endl; //
    os << "Amount of metabolites: " << ((node_overview.find((VertexType) metabolite))->second).size() << std::endl;
    os<< "Amount of complexes: " << ((node_overview.find((VertexType) complex))->second).size() << std::endl;
    os<< "Amount of conditions: " << ((node_overview.find((VertexType) condition))->second).size() << std::endl;
    os<< "Amount of drugs: " << ((node_overview.find((VertexType) drug))->second).size() << std::endl;
    os<< "Amount of unknowns: " << ((node_overview.find((VertexType) unknown))->second).size() << std::endl;

}
template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
void ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::initialize_supported_types()
{
    supported_types = {complex, condition, drug, gene, metabolite};
}

template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
void ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::test_print()
{
    std::cout << "Supported_types" << supported_types << NEW_LINE;
    std::cout << "***********************************" << NEW_LINE;
    std::cout << "Node_overview" << node_overview << NEW_LINE;
    std::cout << "***********************************" << NEW_LINE;
    std::cout << "Min_max_per_type "<< min_max_per_type << NEW_LINE;
    std::cout << "***********************************" << NEW_LINE;
    std::cout << "Discretization per type "<< discretization_per_type << NEW_LINE;

}


template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
void ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::initialize_min_max_per_type()
{
    if( tt == continuous)
    {
        std::vector<VertexType> types_same_range = {complex, drug, metabolite, gene, condition}; //gene?
        std::pair<double, double> same_range_pair(CONT_MIN, CONT_MAX);
        for( auto same_range_iterator = types_same_range.begin(); same_range_iterator != types_same_range.end(); ++same_range_iterator)
        {
            min_max_per_type[*same_range_iterator] = same_range_pair;
        }
    }
    else
    {
        std::vector<VertexType> types_same_range = {complex, drug, metabolite, gene, condition}; //gene?
        std::pair<double, double> same_range_pair(BOOL_MIN, BOOL_MAX);
        for( auto same_range_iterator = types_same_range.begin(); same_range_iterator != types_same_range.end(); ++same_range_iterator)
        {
            min_max_per_type[*same_range_iterator] = same_range_pair;
        }

    }
}

//template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class RandomEngine>
//void ExperimentalSpace<bool, GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution, RandomEngine>::initialize_min_max_per_type()
//{
//    std::vector<VertexType> types_same_range = {complex, drug, metabolite, gene, condition}; //gene?
//    std::pair<double, double> same_range_pair(BOOL_MIN, BOOL_MAX);
//    for( auto same_range_iterator = types_same_range.begin(); same_range_iterator != types_same_range.end(); ++same_range_iterator)
//    {
//        min_max_per_type[*same_range_iterator] = same_range_pair;
//    }
//}
template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
void ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::initialize_discretization_per_type()
{
    if( tt == continuous)
    {
        for( auto supported_type_it = supported_types.begin(); supported_type_it != supported_types.end(); ++supported_type_it)
        {
            discretization_per_type[*supported_type_it] = 0;
        }
    }
    else
    {
        for( auto supported_type_it = supported_types.begin(); supported_type_it != supported_types.end(); ++supported_type_it)
        {
            discretization_per_type[*supported_type_it] = 1;
        }
    }
}

//template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class RandomEngine>
//void ExperimentalSpace<bool, GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution, RandomEngine>::initialize_discretization_per_type()
//{
//    for( auto supported_type_it = supported_types.begin(); supported_type_it != supported_types.end(); ++supported_type_it)
//    {
//        discretization_per_type[*supported_type_it] = 1;
//    }
//}

template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
std::string ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::sample_gene()
{
    int id = gene_distr(generator);
    return ((node_overview.find((VertexType) gene))->second)[id];
}
template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
std::vector<std::string> ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::sample_n_genes_without_replacement(const size_t n)
{

    int min_val = gene_distr.min();
    int max_val = gene_distr.max();
    assert(n >0 &&  n < (max_val+1 - min_val));
    std::vector<int> vals = python_range(min_val, max_val+1);
    std::random_shuffle(vals.begin(), vals.end());
    vals.resize(n);
    assert(vals.size() == n);
//    int id = gene_distr(generator);
    return get(((node_overview.find((VertexType) gene))->second), vals);
}
template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
std::vector<std::string> ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution,GreyValueDistribution,MetaboliteValueDistribution, RandomEngine>::sample_n_metabolites_without_replacement(const size_t n)
{

    int min_val = metab_distr.min();
    int max_val = metab_distr.max();
    assert(n >0 &&  n < (max_val+1 - min_val));
    std::vector<int> vals = python_range(min_val, max_val+1);
    std::random_shuffle(vals.begin(), vals.end());
    vals.resize(n);
    assert(vals.size() == n);
//    int id = gene_distr(generator);
    return get(((node_overview.find((VertexType) metabolite))->second), vals);
}
template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
std::string ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution, GreyValueDistribution, MetaboliteValueDistribution, RandomEngine>::sample_metabolite()
{
   int id = metab_distr(generator);
   return ((node_overview.find((VertexType) metabolite))->second)[id];
}



template<class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
Experiment ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution, GreyValueDistribution, MetaboliteValueDistribution, RandomEngine>::sample_experiment(const size_t amount_of_grey_genes,
                                                                                                                                                                                                                const size_t amount_of_metabolites,
                                                                                                                                                                                                                const size_t time_steps,
                                                                                                                                                                                                                bool silent)

{


    //Parameters
//    size_t amount_of_grey_genes = 1; //TODO: this will probably need to be a probability distribution as well
//    size_t amount_of_metabolites = 1;
//    size_t time_steps = 1;

    //Function pointers
    std::string (ExperimentalSpace<>::*grey)(void) = &ExperimentalSpace<>::sample_gene;
    double (ExperimentalSpace<>::*grey_val)(void) = &ExperimentalSpace<>::sample_grey_value;
    double (ExperimentalSpace<>::*met_val)(void) = &ExperimentalSpace<>::sample_metabolite_value;
    std::string (ExperimentalSpace<>::*met)(void) = &ExperimentalSpace<>::sample_metabolite;

    //Variables
    std::vector< std::vector<std::string> > metabolites_over_time;
    std::vector< std::vector<double> > metabolite_ccs_over_time;

    std::vector<double> temp_over_time;
    std::vector<double> co2_over_time;
//    TODO: growthmedium?

    //Start all sampling
    std::vector<std::string> all_genes = sample_n_genes_without_replacement(amount_of_grey_genes + 1);
    std::string knockout_gene = *all_genes.begin();
    std::vector<std::string> grey_genes( all_genes.begin() + 1 , all_genes.end());
    assert(grey_genes.size() == amount_of_grey_genes);
    std::vector<double > grey_gene_vals = sample_function_n_times_with_replacement<double>(amount_of_grey_genes, grey_val);


    for( int t = 0; t < time_steps ; ++t)
    {
        metabolites_over_time.push_back(sample_n_metabolites_without_replacement(amount_of_metabolites));
        metabolite_ccs_over_time.push_back(sample_function_n_times_with_replacement<double>(amount_of_metabolites, met_val));
        temp_over_time.push_back(sample_temperature());
        co2_over_time.push_back(sample_co2());
    }
    if (!silent)
    {
//    Experiment(std::unordered_map<std::string,ExperimentalInfo>() );
        std::cout << "---------Experiment-------" << NEW_LINE;
        std::cout << "ko : " << knockout_gene << NEW_LINE;
        std::cout << "metabolites_over_time : " << metabolites_over_time << NEW_LINE;
        std::cout << "metabolite_ccs_over_time : " << metabolite_ccs_over_time << NEW_LINE;
        std::cout << "grey_genes : " << grey_genes << NEW_LINE;
        std::cout << "grey_gene_vals : " << grey_gene_vals << NEW_LINE;
        std::cout << "temp_over_time : " << temp_over_time << NEW_LINE;
        std::cout << "co2_over_time : " << co2_over_time << NEW_LINE;
    }
    return Experiment(knockout_gene, metabolites_over_time, metabolite_ccs_over_time, grey_genes, grey_gene_vals, temp_over_time, co2_over_time);
}

template< class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
template< class SampleType >
typename std::vector<SampleType> ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution, GreyValueDistribution, MetaboliteValueDistribution, RandomEngine>::sample_function_n_times_with_replacement(
        const size_t n, SampleType (ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution, GreyValueDistribution, MetaboliteValueDistribution, RandomEngine>::*sampler)(void))
{

    std::vector<SampleType> samples;
    while( samples.size() < n )
    {
        SampleType sample = (this->*sampler)();
        if( std::find(samples.begin(), samples.end(), sample) == samples.end() )
            samples.push_back(sample);
    }
    return samples;
}
template< class GeneDistribution, class MetaboliteDistribution, class TemperatureDistribution, class CarbonDioxideDistribution, class GreyValueDistribution, class MetaboliteValueDistribution, class RandomEngine>
std::vector<Experiment> ExperimentalSpace<GeneDistribution, MetaboliteDistribution, TemperatureDistribution, CarbonDioxideDistribution, GreyValueDistribution, MetaboliteValueDistribution, RandomEngine>::sample_n_experiments(const size_t n,
                                                                                                                                                                                                                                const size_t amount_of_grey_genes,
                                                                                                                                                                                                                                const size_t amount_of_metabolites,
                                                                                                                                                                                                                                const size_t time_steps)
{
    auto start_init = std::chrono::system_clock::now();
    std::vector<Experiment> experiments;

    while( experiments.size() < n)
    {
        Experiment e = sample_experiment(amount_of_grey_genes, amount_of_metabolites, time_steps);
        experiments.push_back(e);
    }
    auto end_init = std::chrono::system_clock::now();
    auto elapsed_init = std::chrono::duration_cast<std::chrono::milliseconds>(end_init - start_init);
    std::cout << "Sampling "<< n <<" experiments took " << elapsed_init.count() << "ms \n";
    return experiments;
}


#endif // ExperimentalSpace_HPP
