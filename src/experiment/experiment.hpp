
#ifndef EXPERIMENT_HPP
#define EXPERIMENT_HPP


#include <vector>
#include <utility>
#include <string>
#include "experimentalinfo.hpp"
#include "boost/unordered_map.hpp"
#include "src/general/compile_configuration.hpp"
//#include "src/R_interface.hxx"

#define BOOL_MIN_EXP_VAL 0.0
#define BOOL_MAX_EXP_VAL 1.0
#define CONT_MIN_EXP_VAL -1.0
#define CONT_MAX_EXP_VAL +1.0

#include <RcppCommon.h>

//template <typename _CostMeasure >
struct Experiment
{
public:
    /*Class experiment: Create an experiment. An experiment consists of Strain(Gene), Metabolite, GrowthMedium,
     * Temperature, Carbon dioxide, Sugar, {Ethanol, amino acids, NH4}
    **/
    Experiment(){}
    Experiment(std::unordered_map<std::string,ExperimentalInfo> & experiments);
    Experiment( const std::string experiment_file, const std::string xml_nodes = std::string("") );
    Experiment(const std::string knockout_gene,
               const std::vector< std::vector< std::string> > & metabolites_over_time,
               const std::vector<std::vector< double> > &metabolite_ccs_over_time,
               const std::vector<std::string> &grey_genes,
               const std::vector<double> & grey_gene_vals,
               const std::vector<double> &temp_over_time,
               const std::vector<double> &co2_over_time);
    unsigned determine_cost(const Experiment & previous_experiment) const;
    friend std::ostream& operator<<(std::ostream& out, const Experiment & classObj);
    bool return_experimental_condition(const std::string node_id, ExperimentalInfo & info)const;
    bool check_validity(const boost::unordered_set<std::string> & node_set) const;
    bool check_validity();
    std::map<std::string,double> get_initial_conditions()const;
    void write_experiment_info(const std::string & file) const;
    operator SEXP();
    inline std::unordered_map<std::string, ExperimentalInfo> get_exp_nodes()const{return init_conds.get_exp_nodes();}
private:
    struct InitialConditions
    {
    public:
        InitialConditions( const std::unordered_map<std::string, ExperimentalInfo> & initial){experimental_nodes = initial;}
        InitialConditions(){ experimental_nodes = std::unordered_map<std::string, ExperimentalInfo>();}
        inline std::unordered_map<std::string, ExperimentalInfo> get_exp_nodes()const{return experimental_nodes;}
    private:
        std::unordered_map<std::string, ExperimentalInfo> experimental_nodes;
    };
    struct ExperimentUpdate
    {
    public:
        ExperimentUpdate(  const std::vector< std::vector< std::string> > & rh_metabolites_over_time,
                           const std::vector<std::vector< double> > & rh_metabolite_ccs_over_time,
                           const std::vector<double> & rh_temp_over_time,
                           const std::vector<double> & rh_co2_over_time)
        {
            metabolites_over_time = rh_metabolites_over_time;
            metabolite_ccs_over_time = rh_metabolite_ccs_over_time;
            temp_over_time = rh_temp_over_time;
            co2_over_time = rh_co2_over_time;
        }
        ExperimentUpdate(){} //Empty
    private:
        std::vector<std::vector< std::string>  > metabolites_over_time;
        std::vector<std::vector< double> > metabolite_ccs_over_time;
        std::vector<double> temp_over_time;
        std::vector<double> co2_over_time;
    };
    InitialConditions init_conds;
    ExperimentUpdate experiment_update_scheme;

    //std::unordered_map<std::string,ExperimentalInfo > experimental_nodes;
    void adjust_experimentalinfo_value_ranges(double value_range_min, double value_range_max);
};

#include <Rcpp.h>

#endif
