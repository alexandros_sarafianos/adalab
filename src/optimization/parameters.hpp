#ifndef PARAMETERS_HPP
#define PARAMETERS_HPP

#include <cmath>
#include <random>
#include <utility>
#include <algorithm>

#include <chrono>
#include <cstdlib>
#include <iostream>

#include <unordered_map>
#include <memory>
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include <algorithm>    // std::max

// To find a status with lower energy according to the given condition
//template<typename status, typename count, typename energy_function
//       , typename temperature_function, typename next_function, typename generator>
//status simulated_annealing(const status&& i_old, count&& c, energy_function&& ef
//                         , temperature_function&& tf,  next_function&& nf, generator&& g)
//{

struct Parameters
{
    Parameters()
    {
        boolean_variables = std::shared_ptr<std::unordered_map<std::string, bool> >(new std::unordered_map<std::string, bool>);
        fp_variables = std::shared_ptr<std::unordered_map<std::string, long double> >(new std::unordered_map<std::string, long double>);
        boolean_boundaries = std::shared_ptr<std::unordered_map<std::string, std::pair<bool, bool >> >(new std::unordered_map<std::string, std::pair<bool, bool> >);
        fp_boundaries = std::shared_ptr<std::unordered_map<std::string, std::pair<long double, long double >> >(new std::unordered_map<std::string, std::pair<long double, long double> >);
    }
    template<class ParType>
    void add_parameter( const std::string key, ParType p );
    template< class ParType>
    void add_boundaries( const std::string key, const std::pair<ParType, ParType> & boundaries = std::pair<ParType, ParType> () );
    template< typename Value, template <typename, typename,typename,typename> class Container>
    void add_parameters(const  Container<std::string, Value, std::less<std::string>, std::allocator< std::pair<const std::string, Value> > > & parameter_container,
                        const  Container<std::string, std::pair<Value,Value> , std::less<std::string>, std::allocator< std::pair<const std::string, std::pair<Value, Value> > > > & boundary_container
                        = std::map<std::string, std::pair<Value, Value> >() );
    friend std::ostream& operator<<(std::ostream& out, const Parameters params);
    ~Parameters(){boolean_variables.reset(); fp_variables.reset(); boolean_boundaries.reset(); fp_boundaries.reset();}
    template<class ParType>
    ParType get_parameter_value(const std::string key) const;
//    template<class ParType>
//    void add_random_parameter(const std::string key);
    void randomize_parameters();

    Parameters(const Parameters& p)
    {
        boolean_variables = std::shared_ptr<std::unordered_map<std::string, bool> >(new std::unordered_map<std::string, bool>);
        fp_variables = std::shared_ptr<std::unordered_map<std::string, long double> >(new std::unordered_map<std::string, long double>);
        boolean_boundaries = std::shared_ptr<std::unordered_map<std::string, std::pair<bool, bool >> >(new std::unordered_map<std::string, std::pair<bool, bool> >);
        fp_boundaries = std::shared_ptr<std::unordered_map<std::string, std::pair<long double, long double >> >(new std::unordered_map<std::string, std::pair<long double, long double> >);
        *boolean_variables = *p.boolean_variables;
        *boolean_boundaries = *p.boolean_boundaries;
        *fp_variables = *p.fp_variables;
        *fp_boundaries = *p.fp_boundaries;
    }
    Parameters& operator=( const Parameters &p )
    {
        boolean_variables = std::shared_ptr<std::unordered_map<std::string, bool> >(new std::unordered_map<std::string, bool>);
        fp_variables = std::shared_ptr<std::unordered_map<std::string, long double> >(new std::unordered_map<std::string, long double>);
        boolean_boundaries = std::shared_ptr<std::unordered_map<std::string, std::pair<bool, bool >> >(new std::unordered_map<std::string, std::pair<bool, bool> >);
        fp_boundaries = std::shared_ptr<std::unordered_map<std::string, std::pair<long double, long double >> >(new std::unordered_map<std::string, std::pair<long double, long double> >);
        *boolean_variables = *p.boolean_variables;
        *boolean_boundaries = *p.boolean_boundaries;
        *fp_variables = *p.fp_variables;
        *fp_boundaries = *p.fp_boundaries;
        return *this;
    }
    bool operator==( const Parameters& rhs)const/*{ return equal_maps(*boolean_variables, *rhs.boolean_variables)
                                                        && equal_maps(*fp_variables, *rhs.fp_variables)
                                                        && equal_maps(*boolean_boundaries, *rhs.boolean_boundaries)
                                                        && equal_maps(*fp_boundaries, *rhs.fp_boundaries)*/; /*}*/
    inline bool operator!=( const Parameters& rhs)const{ return !(*this == rhs); }

    template< class F >
    Parameters generate_neighbour(const F & neighbour_generator, const long double temperature ) const;
    std::pair<long double, long double> get_boundary(const std::string & key)const{return (*fp_boundaries)[key];}

    typedef std::unordered_map<std::string, bool>::iterator iterator1;
    typedef std::unordered_map<std::string, bool>::const_iterator const_iterator1;
    typedef std::unordered_map<std::string, long double>::iterator iterator2;
    typedef std::unordered_map<std::string, long double>::const_iterator const_iterator2;


    inline iterator1 begin1(){return boolean_variables->begin();}
    inline iterator1 end1(){return boolean_variables->end();}
    inline iterator2 begin2(){return fp_variables->begin();}
    inline iterator2 end2() {return fp_variables->end();}

private:

    template<typename T >
    std::shared_ptr<std::unordered_map<std::string, T, std::hash<std::string>, std::equal_to<std::string>, std::allocator<std::pair<std::string const, T> > > > get_map() const;
    template < class T >
    std::shared_ptr< std::unordered_map<std::string, std::pair< T, T > > > get_boundary_map() const ;
    enum MapTypes
    {
        Bool,
        Double,
        Int
    };
      std::shared_ptr<std::unordered_map<std::string, bool> > boolean_variables;//
      std::shared_ptr<std::unordered_map<std::string, long double> > fp_variables;
      std::shared_ptr<std::unordered_map<std::string, std::pair<bool, bool>  > > boolean_boundaries;
      std::shared_ptr<std::unordered_map<std::string, std::pair<long double, long double> > > fp_boundaries;
};





template<typename Value, template <typename, typename,typename,typename> class Container>
void Parameters::add_parameters(const Container<std::string, Value, std::less<std::string>, std::allocator<std::pair<const std::string, Value> > > & parameter_container,
                                const  Container<std::string, std::pair<Value,Value> , std::less<std::string>, std::allocator< std::pair<const std::string, std::pair<Value,Value> > > > & boundary_container
                                )
{
    typename std::shared_ptr<std::unordered_map<std::string, Value, std::hash<std::string>, std::equal_to<std::string>, std::allocator<std::pair<std::string const, Value> > > > var_map = get_map<Value>();
    size_t size_before = var_map->size();
    size_t size_parameter_container = parameter_container.size();
    var_map->insert(parameter_container.begin(), parameter_container.end());
    size_t size_after = var_map->size();

//    typename std::shared_ptr<std::unordered_map<std::string, std::pair<Value, Value >, std::hash<std::string>, std::equal_to<std::string>, std::allocator<std::pair<std::string const, std::pair<Value, Value> > > > > bound_map = get_boundary_map<Value>();
    typename std::shared_ptr<std::unordered_map<std::string, std::pair<Value, Value > > > bound_map = get_boundary_map<Value>();
    bound_map->insert(boundary_container.begin(), boundary_container.end());
    assert( size_after-size_before == size_parameter_container );
}


template<class ParType>
void Parameters::add_parameter( const std::string key, ParType p )
{
    typename std::shared_ptr<std::unordered_map<std::string, ParType> > var_map = get_map<ParType>();
//    size_t size_before = var_map->size();
    typename std::shared_ptr<std::unordered_map<std::string, std::pair<ParType, ParType > > > bound_map = get_boundary_map<ParType>();
    (*var_map)[key] = p;
    (*bound_map)[key] = std::make_pair<ParType,ParType>(-1.,1.);
//    var_map->insert(typename std::pair<std::string, ParType>(key, p));
//    assert( var_map->size() == size_before +1 );
}

//template<class ParType>
//void Parameters::add_random_parameter(const std::string key)
//{

//    typename std::shared_ptr<std::unordered_map<std::string, ParType> > var_map = get_map<ParType>();
////    size_t size_before = var_map->size();
//    typename std::shared_ptr<std::unordered_map<std::string, std::pair<ParType, ParType > > > bound_map = get_boundary_map<ParType>();
//    (*var_map)[key] = p;
//    (*bound_map)[key] = std::make_pair<ParType,ParType>(-1.,1.);
//}
template<class Value>
Value Parameters::get_parameter_value(const std::string key) const
{
    typename std::shared_ptr<std::unordered_map<std::string, Value, std::hash<std::string>, std::equal_to<std::string>, std::allocator<std::pair<std::string const, Value> > > > var_map = get_map<Value>();
    auto it = var_map->find(key);
    if( it != var_map->end() )
        return it->second;
    else
        exit(1);
}



template< class ParType>
void Parameters::add_boundaries( const std::string key, const std::pair<ParType, ParType> & boundaries )
{
//    typename std::shared_ptr<std::unordered_map<std::string, std::pair<ParType, ParType >, std::hash<std::string>, std::equal_to<std::string>, std::allocator<std::pair<std::string const, std::pair<ParType, ParType> > > > > bound_map = get_boundary_map<ParType>();
    typename std::shared_ptr<std::unordered_map<std::string, std::pair<ParType, ParType > > > bound_map = get_boundary_map<ParType>();
    if ( boundaries == std::pair<ParType, ParType> () )
        (*bound_map)[key] = std::make_pair<ParType, ParType>(-1.,1);
    else
        (*bound_map)[key] = boundaries;

}
template< class F >
Parameters Parameters::generate_neighbour(const F & neighbour_generator, const long double temperature ) const
{
    Parameters neighbour = *this;
    for( auto param_iterator1 = neighbour.begin1(); param_iterator1 != neighbour.end1(); ++param_iterator1 )
    {
        auto change = neighbour_generator.change(param_iterator1->second, temperature );
//        auto new_val = change;
        neighbour.add_parameter(param_iterator1->first, change);
    }
    for ( auto param_iterator2 = neighbour.begin2(); param_iterator2 != neighbour.end2(); ++param_iterator2 )
    {
        auto change = neighbour_generator.change(param_iterator2->second, temperature, neighbour.get_boundary(param_iterator2->first));
        auto new_val = param_iterator2->second + change;
        neighbour.add_parameter(param_iterator2->first, new_val);
    }
    return neighbour;
}



struct Neighbour
{
    Neighbour(int version = 1)
    {
        v = version;
    }
    bool change(const bool val, const long double temperature)const
    {
        long double change;
        change = 0.5 * temperature; //change keeps getting smaller -> limit+inf = 0
        if ((rand()/RAND_MAX) < change)
            return !val;
        else
            return val; // When cooling down this reduces the chance of a bit flip

    }
    long double change(const long double val, const long double temperature, const std::pair<long double, long double > boundary)const
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<long double> rf(0, 1);
        std::uniform_int_distribution<int> r_sign(0, 1);
        long double change, maxChange;
        assert(approximately_equal( abs(boundary.first), abs(boundary.second)));//If boundary is not symmetric, this might need to be changed, too tired to think about it
        long double boundary_range = std::max(boundary.first, boundary.second) - std::min(boundary.first, boundary.second);
        switch(v)
        {
        case 0:  maxChange = (long double) 0.5 * boundary_range; //temperature used to determine probability of acceptance
                 break;

        case 1: maxChange = (long double) 0.5 * boundary_range * temperature;
                    break;

        default:
                std::cout <<" Found undefined version " << v << std::endl;
                exit(1);
        }

        auto r = rf(gen);
        change = r * maxChange;
//        std::cout << " change " << change << std::endl;
        if(r_sign(gen) % 2)
        {
            change *= -1.0;
        }
        return change;
    }
private:
    int v;
};
#endif // PARAMETERS_HPP
