#include "src/optimization/derivative.hpp"
#include <cmath>
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"


struct Obj_func
{
    Obj_func(){}
    double operator()( const double est, const double real)
    {
        return pow(est-real, 2);
    }
};

struct Est
{
    Est(){}
    double operator()(const std::vector<double>& params, const std::vector<double> & X)
    {
        double result = 0;
        auto x_it = X.begin();
        for( auto it = params.begin(); it != params.end(); ++it )
        {
            result += *it * *x_it;
        }
        return result;
    }
};
int main()
{
    typedef matrix<double> mat;

    Obj_func o;
    Est e;
    Derivative<Obj_func, Est> d(e, o, 1.);
    std::vector< std::pair<std::vector<double>, double > > data;
    std::pair<std::vector<double>,double> d1( {1.,1.,1.}, 0.5 );
    std::pair<std::vector<double>,double> d2( {-0.5,0.9,0.8}, 0.29 );
    std::pair<std::vector<double>,double> d3( {0., 5., -4.}, 0.199999999996 );
    std::pair<std::vector<double>,double> d4( {3., -10., -1.}, -1.9 );
    data.push_back(d1);data.push_back(d2);data.push_back(d3);data.push_back(d4);

    std::vector<double> params = {0.5,0.5,0.5};

    std::cout << d(data, params) << std::endl;

    return 0;
}
