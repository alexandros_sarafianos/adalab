#ifndef DERIVATIVE_HPP
#define DERIVATIVE_HPP
#include <boost/numeric/ublas/matrix.hpp>

template<class ObjFunc, class Estimator>
struct Derivative
{
public:
    typedef boost::numeric::ublas::matrix<double> matrix;

    Derivative(const Estimator& rh_e, const ObjFunc & rh_f,const double rh_eps = 1e-7)
    {
        obj_f = rh_f;
        eps = rh_eps;
        est = rh_e;
    }

    template<class Input, class Result, class Parameters>
    boost::numeric::ublas::matrix<double> operator()(const typename std::vector< std::pair<Input, Result> >& data,
               Parameters & params
               );
private:
    template<class Input, class Result, class Parameters>
    double residual(const typename std::pair< Input, Result>& data,
               Parameters & params
               );
    ObjFunc obj_f;
    double eps;
    Estimator est;
};

template<class _ObjFunc, class _Estimator>
template<class Input, class Result, class Parameters>
double Derivative<_ObjFunc,_Estimator>::residual(const typename std::pair< Input, Result>& data,
           Parameters & params)
{
    Result estimated_result = est(params, data.first);
    return obj_f(estimated_result, data.second);
}
template<class _ObjFunc, class _Estimator>
template<class Input, class Result, class Parameters>
boost::numeric::ublas::matrix<double> Derivative<_ObjFunc,_Estimator>::operator()(const typename std::vector<std::pair< Input, Result> >& data,
           Parameters & params)
{
    matrix der(data.size(), params.size());
    int data_point = 0;
    for( auto example_iterator = data.begin(); example_iterator != data.end(); ++example_iterator)
    {
        for( int i = 0; i < params.size(); ++i)
        {
            auto old_param_value = params[i];
            params[i] = old_param_value -eps;
            auto val_minus = residual(*example_iterator, params);
            params[i] = old_param_value + eps;
            auto val_plus = residual(*example_iterator, params);
            der(data_point, i) = (val_plus - val_minus)/(2*eps);
        }
        ++data_point;
    }
    return der;
}

#endif // DERIVATIVE_HPP
