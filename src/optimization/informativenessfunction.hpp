#ifndef INFORMATIVENESSFUNCTION_HPP
#define INFORMATIVENESSFUNCTION_HPP

class InformativenessFunction
{
public:
    InformativenessFunction();

    template<class Experiment, class ExperimentDatabase, class ExperimentalBatch>
    operator()(const Experiment&, const ExperimentDatabase&, const ExperimentalBatch&   ){return 0.0;}
};

#endif // INFORMATIVENESSFUNCTION_HPP
