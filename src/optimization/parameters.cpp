#include "parameters.hpp"

template<>
std::shared_ptr<std::unordered_map<std::string, bool> > Parameters::get_map()const
{
    return boolean_variables;

}

template<>
std::shared_ptr<std::unordered_map<std::string, long double> > Parameters::get_map()const
{
    return fp_variables;
}


template<>
std::shared_ptr<std::unordered_map<std::string, std::pair<bool, bool> > > Parameters::get_boundary_map()const
{
    return boolean_boundaries;

}

template<>
std::shared_ptr<std::unordered_map<std::string, std::pair<long double, long double> > > Parameters::get_boundary_map()const
{
    return fp_boundaries;
}

std::ostream& operator<<(std::ostream& out, const Parameters params)
{
    if( (params.boolean_variables)->size() > 0)
        out << *params.boolean_variables << std::endl;
    if( (params.fp_variables)->size() > 0 )
        out << *params.fp_variables << std::endl;

    return out;
}
void Parameters::randomize_parameters()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> rf(0, 1);
    for( auto bool_iterator = boolean_variables->begin(); bool_iterator != boolean_variables->end(); ++bool_iterator)
    {
        int r = rf(gen);
        add_parameter(bool_iterator->first, r==1 );
    }
    for( auto fp_iterator = fp_variables->begin(); fp_iterator != fp_variables->end(); ++fp_iterator)
    {
        auto boundaries = get_boundary(fp_iterator->first);
        assert(!approximately_equal( boundaries.first - boundaries.second, (long double) 0.));
        std::uniform_real_distribution<long double> rf_real(std::min(boundaries.first ,boundaries.second), std::max(boundaries.first,boundaries.second));
        add_parameter(fp_iterator->first, rf_real(gen));
    }
}

bool Parameters::operator==( const Parameters& rhs)const
{
    //TODO: might need to add checks for boundaries if necessary at certain time
    if( boolean_variables->size() != rhs.boolean_variables->size() ||
            fp_variables->size() != rhs.fp_variables->size() /*||
            boolean_boundaries->size () != rhs.boolean_boundaries->size() ||
            fp_boundaries->size() != rhs.fp_boundaries->size()*/ )
    {
        INFO_DEBUG("Sizes didn't match "<< boolean_variables->size() <<","<< rhs.boolean_variables->size() <<","<<fp_variables->size() <<","<< rhs.fp_variables->size()
                     <<","<<boolean_boundaries->size () <<","<< rhs.boolean_boundaries->size() <<","<<  fp_boundaries->size() <<","<< rhs.fp_boundaries->size());
        return false;
    }
    for(auto bool_var_it = boolean_variables->begin(); bool_var_it != boolean_variables->end(); ++bool_var_it)
    {
            auto it_bool_var2 = rhs.boolean_variables->find(bool_var_it->first);
            if( it_bool_var2 == rhs.boolean_variables->end() )
            {
                std::cout << " Key not found " << bool_var_it->first << std::endl;
                return false;
            }
            else if( bool_var_it->second != it_bool_var2->second )
            {
                 INFO_DEBUG("Values not equal " << bool_var_it->second << "," << it_bool_var2->second);
                 return false;
            }

    }
    for(auto fp_var_it = fp_variables->begin(); fp_var_it != fp_variables->end(); ++fp_var_it)
    {
            auto it_fp_var2 = rhs.fp_variables->find(fp_var_it->first);
            if( it_fp_var2 == rhs.fp_variables->end() )
            {
                std::cout << " Key not found " << fp_var_it->first << std::endl;
                return false;
            }
            else if( !approximately_equal(fp_var_it->second, it_fp_var2->second) )
            {
                 INFO_DEBUG("Values not equal " << fp_var_it->second << "," << it_fp_var2->second);
                 return false;
            }

    }



    return true;

}
