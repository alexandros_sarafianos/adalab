#ifndef SIMULATED_ANNEALING_HPP
#define SIMULATED_ANNEALING_HPP
#include <cmath>
#include <random>
#include <utility>
#include <algorithm>

#include <chrono>
#include <cstdlib>
#include <iostream>

#include <unordered_map>
#include <memory>
#include "src/general/generalFunctions.hxx"
#include "src/general/generalcontainerfunctions.hxx"
#include <algorithm>    // std::max
#include "src/optimization/parameters.hpp"
#include <math.h>
#define _USE_MATH_DEFINES

struct TemperatureFunction
{
    TemperatureFunction(const long double rh_cooling_factor = 0.99)
    {
        cooling_factor = rh_cooling_factor;
    }
    long double operator()(const long double temperature)const
    {
//        std::cout << "Temperature from " << temperature << " to (" << cooling_factor << ")" << temperature*cooling_factor << std::endl;
        return temperature * cooling_factor; //equivalent with T = starting_temperature * cooling_factor^iteration
    }

private:
    long double cooling_factor;
};


struct EnergyFunction
{
    EnergyFunction(){}
    long double operator()(const Parameters & p)const
    {
        auto x1 = p.get_parameter_value<long double>("x1");
        return x1*x1;
    }
};

struct Rosen
{
    Rosen(){}
    long double operator()(const Parameters & p )const
    {
        auto x1 = p.get_parameter_value<long double>("x1");
        auto x2 = p.get_parameter_value<long double>("x2");
        return 100.0 * pow(x2 - x1*x1, 2) + pow(1-x1,2 );
    }
};

struct Ackley
{
    Ackley(){}
    long double operator()(const Parameters & p )const
    {
        auto x1 = p.get_parameter_value<long double>("x1");
        auto x2 = p.get_parameter_value<long double>("x2");

        return -20.* std::exp(-0.2 * std::sqrt(0.5*(x1*x1 + x2*x2 )))
                - std::exp(0.5 * (cos(2*M_PI*x1) + cos(2*M_PI*x2)))
                + std::exp(0.0) + 20;
    }
};
struct Levi
{
    Levi () {}
    long double operator()(const Parameters & p )const
    {
        auto x1 = p.get_parameter_value<long double>("x1");
        auto x2 = p.get_parameter_value<long double>("x2");
        return  pow(sin(3*M_PI*x1),2)
                + pow(x1-1, 2)*(1 + pow(sin(3*M_PI*x2), 2))
                + pow(x2-1,2)*( 1 + pow(sin(2*M_PI*x2), 2));
    }
};
template<typename status, typename count, typename energy_function, typename temperature_function, typename next_function>
status simulated_annealing(status i_old, count c, const energy_function& ef, const temperature_function& tf, const next_function& nf, int version = 0, long double starting_chance = 1.0/*, generator& g*/)
{
    auto   e_old  = ef(i_old);

    status i_best = i_old;
    auto   e_best = e_old;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<decltype(e_old)> rf(0, 1);
    long double t = 1.0;
    long double acceptance_prob = 1.0; //TODO: Make this configurable
    for(; c > 0; --c)
    {
//        status i_new = nf(i_old, g);
        status i_new = i_old.generate_neighbour(nf, t);
        auto   e_new = ef(i_new);

        if(e_new < e_best) //Minimization
          {
               i_best =           i_new ;
               e_best =           e_new ;
//               std::cout << e_best << std::endl;
               i_old  = i_new;
               e_old  = e_new;
               continue;
           }

           //auto t = tf(c);
           auto delta_e = e_new - e_old;

//           if( delta_e > 10.0 * t) continue;  //as std::exp(-10.0) is a very small number
           switch(version)
           {
                case 0:   /*std::cout << "PROB "<< delta_e << "," << t << ": " <<  std::exp( - delta_e / t ) << std::endl;*/
                          if( delta_e <= 0.0 || std::exp( - delta_e / t ) > rf(gen) )
                          {
                               i_old  = std::move(i_new);
                               e_old  = std::move(e_new);
                          }
                          break;
                case 1:   if( delta_e <= 0.0 || rf(gen) < acceptance_prob )
                          {
                               i_old  = std::move(i_new);
                               e_old  = std::move(e_new);
                          }
                          break;
                default:
                        std::cout << "Undefined version " << version << std::endl;
                        exit(1);


           }

           t = tf(t);

    }
    return(i_best);
}


#endif // SIMULATED_ANNEALING_HPP
