#ifndef OBJECTIVEFUNCTION_HPP
#define OBJECTIVEFUNCTION_HPP

#include <cmath>  //abs

template<class ValueType>
class MeanSquaredError
{
public:
    MeanSquaredError(){}
    ValueType operator()(const std::vector<ValueType> & y, const std::vector<ValueType> & y_hat);
};



template<class ValueType>
class MeanAbsoluteError
{
public:
    MeanAbsoluteError(){}
    ValueType operator()(const std::vector<ValueType> & y, const std::vector<ValueType> & y_hat);
};


template< class PredictiveLoss, class PriorLoss, class Regularization>
class ObjectiveFunction
{
public:
    ObjectiveFunction();
private:
    PredictiveLoss pred_loss;
    PriorLoss priorloss;
    Regularization reg;
};


template<class _ValueType>
_ValueType MeanSquaredError<_ValueType>::operator()( const std::vector<_ValueType> & y, const std::vector<_ValueType> & y_hat )
{
    typename std::vector<_ValueType>::const_iterator y_it = y.begin();
    typename std::vector<_ValueType>::const_iterator y_hat_it = y_hat.begin();
    _ValueType error = 0;
    assert(y.size() == y_hat.size());
    while( y_it != y.end() )
    {
        error += (*y_it - *y_hat_it)*(*y_it - *y_hat_it);
        ++y_it;
        ++y_hat_it;
    }
    error = error / y_it.size();
    return error;
}

template<class _ValueType>
_ValueType MeanAbsoluteError<_ValueType>::operator()( const std::vector<_ValueType> & y, const std::vector<_ValueType> & y_hat )
{
    typename std::vector<_ValueType>::const_iterator y_it = y.begin();
    typename std::vector<_ValueType>::const_iterator y_hat_it = y_hat.begin();
    _ValueType error = 0;
    assert(y.size() == y_hat.size());
    while( y_it != y.end() )
    {
        error += abs(*y_it - *y_hat_it);
        ++y_it;
        ++y_hat_it;
    }
    error = error / y_it.size();
    return error;
}

template<typename OutputType>
double res(const OutputType & real_result, const OutputType & estimated_result)
{
    std::cout << "Adjust res in objectivefunction.hpp" << NEW_LINE;
    return 0.;
}
#endif // OBJECTIVEFUNCTION_HPP
