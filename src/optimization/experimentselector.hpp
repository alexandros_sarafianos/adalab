#ifndef ExperimentSelector_HPP
#define ExperimentSelector_HPP

#include <iostream>                  // for std::stringstream os; os
#include <utility>                   // for std::pair
#include <algorithm>                 // for std::for_each
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include "src/experiment/experimentalspace.hpp"
#include <stdlib.h>     /* srand, rand */
#include <iostream>
#include <memory>

#include <chrono>

class Default {};

template<class ExperimentDatabase = Default, class ExperimentalBatch = Default>
struct AbstractInformativeness
{
public:
    AbstractInformativeness(){}
    AbstractInformativeness(const typename std::shared_ptr<ExperimentDatabase> rh_exp_db_p, const typename std::shared_ptr<ExperimentalBatch> rh_exp_batch_p){}
//    void update_experiment_database(std::shared_ptr<ExperimentDatabase> rh_exp_db){}
//    void update_experimentalbatch( const ExperimentalBatch & rh_exp_batch){}
    double operator()(){srand (time(NULL)); return rand();}
private:
    //
    typename std::shared_ptr<ExperimentDatabase> exp_db_p;
    typename std::shared_ptr<ExperimentalBatch> exp_batch_p;
};

template<class InformativenessFunction,class ExperimentalSpace>
class ExperimentSelector
{
public:
    ExperimentSelector(const typename std::shared_ptr<InformativenessFunction> & i_func_p, const typename std::shared_ptr<ExperimentalSpace> & rh_exp_space_p)
    {
        inf_func_p = i_func_p;
        exp_space_p = rh_exp_space_p;
    }
    template< class ExperimentalBatch, class Sampler, class ExperimentDatabase, class Experiment>
    void experiment_sampling(const size_t batch_size,
                             const ExperimentDatabase & exp_db,
                             const size_t sampleSize,
                             ExperimentalBatch & batch_solution);
    template<class ExperimentDatabase, class Optimizer, class ExperimentalBatch>
    void optimal_sampling(const size_t batch_size,
                          const ExperimentDatabase & exp_db,
                          Optimizer opt,
                          ExperimentalBatch & batch_solution);
    template< class ExperimentDatabase, class Optimizer, class ExperimentalBatch>
    void optimal_sampling2(const size_t batch_size,
                           const ExperimentDatabase & exp_db,
                           Optimizer opt,
                           ExperimentalBatch & batch_solution);
    template < class Sampler>
    void test(const size_t batch_size, const size_t sampleSize, Sampler & s);
private:
    typename std::shared_ptr<InformativenessFunction> inf_func_p; //function calling simulator several times
    typename std::shared_ptr<ExperimentalSpace> exp_space_p; //ExperimentalSpace
//    Optimizer opt;
};
//
template< class _InformativenessFunction,class _ExperimentalSpace>
template< class Sampler >
void ExperimentSelector<_InformativenessFunction,_ExperimentalSpace>::test(const size_t batch_size,
                                                                           const size_t sampleSize,
                                                                           Sampler &s)
{

    auto start_sampling = std::chrono::system_clock::now();
    std::vector<Experiment> experiment_sample = s(*exp_space_p, sampleSize);
    auto end_sampling = std::chrono::system_clock::now();
    auto elapsed_sampling = std::chrono::duration_cast<std::chrono::milliseconds>(end_sampling-start_sampling);
    std::cout << "Sampling took: " << elapsed_sampling.count() << "ms " << std::endl;
    std::vector<Experiment> batch_solution ;
    bool batch_not_full = true;
    auto start_batch = std::chrono::system_clock::now();
    while( batch_not_full )
    {
        size_t index = 0;
        double max_informativeness = 0.;
        size_t max_informativeness_index = 0;
        for (auto exp_it = experiment_sample.begin(); exp_it != experiment_sample.end(); ++exp_it)
        {
            double new_informativeness = inf_func_p->operator()();
            if(new_informativeness > max_informativeness)
            {
                max_informativeness_index = index;
                max_informativeness = new_informativeness;
            }
            index++;
        }
        batch_solution.push_back(experiment_sample[max_informativeness_index]);
        if(batch_solution.size() == batch_size)
            batch_not_full = false;
    }
     auto end_batch = std::chrono::system_clock::now();
     auto elapsed_batch =std::chrono::duration_cast<std::chrono::milliseconds>(end_batch-start_batch)  ;
     std::cout << "2: " << elapsed_batch.count() << "ms " << std::endl;
}




template<class _InformativenessFunction,class _ExperimentalSpace>
template<class _ExperimentalBatch, class _Sampler, class _ExperimentDatabase, class _Experiment>
void ExperimentSelector<_InformativenessFunction,_ExperimentalSpace>::experiment_sampling(const size_t batch_size,
                                                                                          const _ExperimentDatabase & exp_db,
                                                                                          const size_t sampleSize,
                                                                                          _ExperimentalBatch & batch_solution)
{
    assert(batch_solution.size()==0);
    std::vector<Experiment> experiment_sample = _Sampler(*exp_space_p, sampleSize);
    bool batch_not_full = true;
    while( batch_not_full)
    {
        size_t index = 0;
        double max_informativeness = 0.;
        size_t max_informativeness_index = 0;
        for (auto exp_it = experiment_sample.begin(); exp_it != experiment_sample.end(); ++exp_it)
        {
            double new_informativeness = inf_func_p->operator()(*exp_it, exp_db, batch_solution, *exp_space_p);
            if(new_informativeness > max_informativeness)
            {
                max_informativeness_index = index;
                max_informativeness = new_informativeness;
            }
            index++;
        }
        batch_solution.add(experiment_sample[max_informativeness_index]);
        if(batch_solution.size() == batch_size)
            batch_not_full = false;
    }
}

template<class _InformativenessFunction, class _ExperimentalSpace>
template<class _ExperimentDatabase, class _Optimizer, class _ExperimentalBatch >
void ExperimentSelector<_InformativenessFunction,_ExperimentalSpace>::optimal_sampling(const size_t batch_size,
                                                                               const _ExperimentDatabase & exp_db,
                                                                               _Optimizer opt,
                                                                               _ExperimentalBatch & batch_solution)
{
    assert(batch_solution.size() == 0);
    bool batch_not_full = true;
    std::vector<std::string> genes;
    while( batch_not_full )
    {
        for(auto gene = genes.begin(); gene != genes.end(); ++gene)
        {
            Experiment e = opt(exp_db, inf_func_p, exp_space_p, batch_solution);
            batch_solution.add(e);
        }
        if(batch_solution.size() == batch_size)
            batch_not_full = false;
    }
}

#endif // ExperimentSelector_HPP
