#ifndef OPTIMIZATIONWRAPPER_HPP
#define OPTIMIZATIONWRAPPER_HPP

template< class _ExperimentDatabase, class _ExperimentalSpace, class Optimizer >
class OptimizationWrapper
{
public:
    OptimizationWrapper(const Optimizer & opt);
    template<class _Experiment, class _InformativenessFunction>
    _Experiment operator()(const _ExperimentDatabase & exp_db,
                           const _InformativenessFunction & inf_func,
                           const _ExperimentalSpace & exp_space);
private:
    Optimizer opt;
};

#endif // OPTIMIZATIONWRAPPER_HPP
