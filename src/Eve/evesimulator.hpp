#ifndef EVESIMULATOR_HPP
#define EVESIMULATOR_HPP
#include "src/network/network.hpp"
#include "src/experiment/experiment.hpp"
#include "src/experiment/experimentaloutput.hpp"
#include "src/general/maths/sigmoid.hpp"
#include "src/network/visitors.hxx"
#include "src/general/compile_configuration.hpp"
#include "src/general/logger.hpp"

#include <unordered_map>
#include <chrono>
#include <RcppCommon.h>
//#include "src/R_interface.hxx"
#include <boost/numeric/ublas/matrix.hpp>


#include "src/Eve/evesimulatorparameters.hpp"
#include "src/experiment/experimentalspace.hpp"

using namespace boost::numeric::ublas;

template< class _VertexOutputType >
class EveSimulator
{
public:
//    typedef typename Network<_VertexOutputType>::vertex_t vertex_t;
//    typedef typename Network<_VertexOutputType>::vertex_iter vertex_iter;
//    typedef typename Network<_VertexOutputType>::in_edge_it in_edge_it;

    EveSimulator(){std::cout << "Eve" << NEW_LINE;}
    EveSimulator(const std::string toy_network_file_name, const std::string noise_distribution, const double noise_param, const std::string & xml_nodes);
//    EveSimulator(const std::string toy_network_file_name, RandomWrapper & m_r, ThresholdFunction * m_t_func,const std::string & xml_nodes="");
    EveSimulator(const std::string toy_network_file_name, RandomWrapper & m_r,const std::string & xml_nodes="");

    ExperimentalOutput perform_experiment(const Experiment & e, const int final_time, const float ratio ,
                                          const int initialization_type, const std::unordered_map<std::string, double> & initial_values);
    inline double random_value(){return random_number_generator();}
    std::vector<ExperimentalOutput> run_experiment_for_colony(const Experiment & e,const int final_time ,const int colony_size, const float ratio,
                                                              const int initialization_type, const std::unordered_map<std::string, double> & initial_values);
    std::vector<double> run_experiment_time_step(const Experiment & e, const std::vector<std::string> & metabolite_keys,
                                                 const std::vector<double> & metabolite_vals, const int iteration, const int timesteps, const std::vector<std::string> & gene_keys );
    void check_all_diauxics(const std::vector<ExperimentalOutput> & outs);
    void print_underlying_network(std::ostream & os);
    template< typename InputType, typename ParameterContainer, typename OutputType>
    double residual(const std::pair<InputType, OutputType> & data, const ParameterContainer & params);
    template< typename OutputType, typename InputType, typename ParamContainer>
    OutputType operator()(const InputType & input, const ParamContainer & params);
    operator SEXP();
    double r_test(){return 666.;}
    void load_vector_meaning(const std::string & vector_meaning_file);
    void set_vector_meanings(const std::vector<std::string> & vec);
    void random_initialize_node_values(const float ratio= 0.5, const bool only_genes = true);
    void initialize_node_values_with_map( const std::vector<std::string> & keys, const std::vector<double> &values);
    void reset_state_map();
    void update_network_parameters(const matrix<_VertexOutputType> & new_parameters);
    void get_adjacency_matrix(matrix<double> &adjacency_matrix, std::vector<std::string> & node_names)const;
    void create_settings_file(const std::string & file_name, const std::string & file_name_exp)const;
private:
    Network<_VertexOutputType> grn;
    RandomWrapper random_number_generator;
//    ThresholdFunction* t_func;
    std::vector<std::string> vector_meanings;
};

#include <Rcpp.h>

//RCPP_EXPOSED_CLASS(EveSimulator<double>);

//RCPP_MODULE(EveSimulatorDouble_module)
//{
//    class_<EveSimulator>("EveSimulator")
//            ;
//}
//extern "C" SEXP make_eve()
//{
//    EveSimulator<double> eve;
//    return Rcpp::wrap ( eve );
//}

//extern "C" SEXP print_eve(SEXP e)
//{
//    std::cout << "EVE" << NEW_LINE;
//    return R_NilValue;

//}
/*IMPLEMENTATIONS*/
/************************************************************Constructors*********************************************************************/
template< class VertexOutputType >
//EveSimulator<VertexOutputType>::EveSimulator(const std::string toy_network_file_name,  RandomWrapper & m_r, ThresholdFunction * m_t_func,const std::string & xml_nodes)
EveSimulator<VertexOutputType>::EveSimulator(const std::string toy_network_file_name,  RandomWrapper & m_r, const std::string & xml_nodes)

{
    auto start_init = std::chrono::system_clock::now();
    grn = Network<VertexOutputType>(toy_network_file_name, m_r,/* m_t_func,*/ xml_nodes);
    random_number_generator = m_r;
//    t_func = m_t_func;
    INFO("Succesfully created EveSimulator from file " << toy_network_file_name);
    auto end_init = std::chrono::system_clock::now();
    auto elapsed_init = std::chrono::duration_cast<std::chrono::milliseconds>(end_init - start_init);
    std::cout << "Initializing network took " << elapsed_init.count() << "ms \n";
//    grn.get_adjacency_matrix();
    std::cout << "-----------------------" << NEW_LINE;
}
/**/
template < class VertexOutputType >
EveSimulator<VertexOutputType>::EveSimulator(const std::string toy_network_file_name, const std::string noise_distribution, const double noise_param, const std::string & xml_nodes)
{
    auto start_init = std::chrono::system_clock::now();
    RandomWrapper m_r  (noise_distribution, noise_param);
//    ThresholdFunction* thresh;
    grn = Network<VertexOutputType>(toy_network_file_name, m_r/*, thresh*/, xml_nodes);
    random_number_generator = m_r;
//    t_func = thresh;
    INFO("Succesfully created EveSimulator from file " << toy_network_file_name);
    auto end_init = std::chrono::system_clock::now();
    auto elapsed_init = std::chrono::duration_cast<std::chrono::milliseconds>(end_init - start_init);
    std::cout << "Initializing network took " << elapsed_init.count() << "ms \n";
//    grn.get_adjacency_matrix();
    std::cout << "-----------------------" << NEW_LINE;
}
/**/
template< class VertexOutputType >
ExperimentalOutput EveSimulator<VertexOutputType>::perform_experiment(const Experiment & e, const int final_time, const float ratio,
                                                    const int initialization_type, const std::unordered_map<std::string, double> & initial_values)
{

    INFO_LOW("Experiment "<< e);


    grn.perform_experiment(e,final_time, ratio, initialization_type, initial_values);

    ExperimentalOutput out = grn.extract_time_series(true); //reset state map of nodes

    return out;
}
#
template< class VertexOutputType >
std::vector<ExperimentalOutput> EveSimulator<VertexOutputType>::run_experiment_for_colony(const Experiment & e,const int final_time,const int colony_size, const float ratio,
                                                                        const int initialization_type,const std::unordered_map<std::string, double> & initial_values )
{
    auto start_experiment = std::chrono::system_clock::now();
    std::vector<ExperimentalOutput> output_vector(colony_size);
//  #pragma omp parallel for
    for(int iteration = 0; iteration < colony_size; iteration++)
    {

        ExperimentalOutput eo = perform_experiment(e, final_time, ratio, initialization_type, initial_values);

        output_vector[iteration] = eo;

    }

    check_all_diauxics(output_vector);

    auto stop_experiment = std::chrono::system_clock::now();
    auto elapsed_experiment = std::chrono::duration_cast<std::chrono::milliseconds>(stop_experiment - start_experiment);
    std::cout << "Running experiment for " << final_time << " time steps, " << colony_size << " times took " << elapsed_experiment.count() << "ms \n";
    return output_vector;
}

template<class VertexOutputType >
std::vector<double> EveSimulator<VertexOutputType>::run_experiment_time_step(const Experiment & e, const std::vector<std::string> & metabolite_keys, const std::vector<double> & metabolite_vals,
                                                                             const int iteration, const int timesteps , const std::vector<std::string> & gene_keys)
{
//    if( iteration == 0 )
//        reset_state_map();

    auto start_experiment = std::chrono::system_clock::now();
    std::cout << "Performing experiment " << NEW_LINE;
    int t = iteration * timesteps;
    std::unordered_map<std::string, double> metabolite_map =  map_from_key_value_vectors(metabolite_keys, metabolite_vals);
    grn.perform_experiment(e, timesteps, 0., 0, std::unordered_map<std::string, double>(),  t, metabolite_map, false,false);
    std::cout << "Extracting time series " << NEW_LINE;
    ExperimentalOutput e_o = grn.extract_time_series(false);
    auto stop_experiment = std::chrono::system_clock::now();
    auto elapsed_experiment = std::chrono::duration_cast<std::chrono::milliseconds>(stop_experiment - start_experiment);
    std::cout << "Running experiment took " << elapsed_experiment.count() << "ms \n";
    std::vector<double> output_vector;
    for( std::vector<std::string>::const_iterator it = gene_keys.begin(); it != gene_keys.end(); ++it)
    {
        output_vector.push_back(e_o.get_final_value_of_node<VertexOutputType>(*it));
    }
    std::cout << "Returning back up" << NEW_LINE;
    return output_vector;
}


template < class VertexOutputType>
void EveSimulator<VertexOutputType>::check_all_diauxics(const std::vector<ExperimentalOutput> & outs)
{
    int positives = 0;
    int negatives = 0;
    int positives_ethanol = 0;
    int negatives_ethanol = 0;
    for( std::vector<ExperimentalOutput>::const_iterator it = outs.begin(); it != outs.end(); it++)
    {
        if( it->get_diauxic_value() )
            positives++;
        else
            negatives++;
        if( it->node_upregulated_once("ethanol") )
            positives_ethanol++;
        else
            negatives_ethanol++;
    }
    std::cout<<"DS(P-N):" <<positives << "-" << negatives << NEW_LINE;
    std::cout<<"Ethanol(P-N):" <<positives_ethanol << "-" << negatives_ethanol << NEW_LINE;
    std::cout << "Ethanol=" << (float) positives_ethanol/(negatives_ethanol+positives_ethanol) << NEW_LINE;
    std::cout << "Diauxic_shift="<< (float) positives/(negatives+positives)<<NEW_LINE;

    return;
}

/*******************************************print_network****************************************/
template < class VertexOutputType >
void EveSimulator<VertexOutputType>::print_underlying_network(std::ostream & os)
{
    grn.print_network(os);
}

/********************************************residual********************************************/
template<class VertexOutputType>
template< typename InputType, typename ParameterContainer, typename OutputType>
double EveSimulator<VertexOutputType>::residual(const std::pair<InputType, OutputType> & data, const ParameterContainer & params)
{
    std::cout << "Adjust function residual in EveSimulator.hpp! " << NEW_LINE;
    OutputType simulated_response = grn(data.first, params);
    //Add some regularization term
    return res(data.second, simulated_response);
}

/*********************************************Operator()******************************************/
template<class VertexOutputType>
template< typename OutputType, typename InputType, typename ParameterContainer>
OutputType EveSimulator<VertexOutputType>::operator()(const InputType & input, const ParameterContainer & params)
{
    std::cout << "Fill in operator() in evesimulator.hpp" << NEW_LINE;
    grn.update_parameters_of_network(params);
    //In a loop call (with interaction) FBA and network updates

    return OutputType();
}
/*********************************************load_vector_meaning************************************/
template<class VertexOutputType>
void EveSimulator<VertexOutputType>::load_vector_meaning(const std::string & vector_meaning_file)
{
    if(vector_meaning_file == "" )
        return;
    std::ifstream opened_file;
    open_file_safely(vector_meaning_file, opened_file);
    std::string line;
    while( getline(opened_file, line ))
    {
        if(line.length() > 0 && line[0] == '#' )
        {
            continue;
        }
        vector_meanings.push_back(line);
    }

}

template<class VertexOutputType>
void EveSimulator<VertexOutputType>::set_vector_meanings(const std::vector<std::string> & vec)
{
    vector_meanings = vec;
}

/*****************************************random_initialization_node_values***************************/
template< class VertexOutputType >
void EveSimulator<VertexOutputType>::random_initialize_node_values (const float ratio, const bool only_genes)
{
    grn.random_initialization_node_values(ratio, only_genes);
}
/***************************************initialize_node_values_with_map*******************************/
template< class VertexOutputType >
void EveSimulator<VertexOutputType>::initialize_node_values_with_map(const std::vector<std::string> &keys , const std::vector<double> &values)
{
    assert( keys.size() == values.size());
    std::unordered_map<std::string, double> init_map;
    int index= 0;
    for( ; index < keys.size(); ++index)
    {
        init_map[ keys[index]] = values[index];
    }
    grn.initialization_node_values_with_map(init_map);
}
/************************************reset_state_map***************************************************/
template< class VertexOutputType>
void EveSimulator<VertexOutputType>::reset_state_map()
{
    grn.reset_state_map();
}
//random_initialization_node_values , random_initialization_node_values
/***********************************update_network_parameters*****************************************/
template <class VertexOutputType>
void EveSimulator<VertexOutputType>::update_network_parameters(const matrix<VertexOutputType> & new_parameters)
{
    std::vector<std::string> node_names;
    matrix<double> adj_matrix;
    grn.get_adjacency_matrix( adj_matrix , node_names, random_number_generator);
    assert( adj_matrix.size1() == new_parameters.size1());
    assert( new_parameters.size2() == adj_matrix.size2());
    assert( node_names.size() > 0);
    INFO_VALUE_UPDATE("Updating network!");
    grn = Network<VertexOutputType>(new_parameters, node_names, random_number_generator);

}

template<class VertexOutputType>
void EveSimulator<VertexOutputType>::get_adjacency_matrix(matrix<double> & adjacency_matrix, std::vector<std::string> & node_names)const
{
 grn.get_adjacency_matrix(adjacency_matrix, node_names);
}


#endif // EVESIMULATOR_HPP
