#ifndef EVESIMULATORPARAMETERS_HPP
#define EVESIMULATORPARAMETERS_HPP
#include "src/network/network.hpp"
#include <set>
#include <vector>
#include <map>
#include <unordered_map>
#include <boost/numeric/ublas/matrix.hpp>
#include "src/network/vertex.hpp"

using namespace boost::numeric::ublas;

template< class _VertexOutputType >
class EveSimulatorParameters
{
public:
    EveSimulatorParameters(const Network<_VertexOutputType> & grn, std::ofstream & os );
private:
    matrix<double> parameter_matrix;
    std::map<VertexType, std::set<std::string> > vertex_type_map;
    std::map<VertexType, std::pair<double, double> > min_max_per_type;
//    std::map<VertexType, FactorSettingType> setting_type_per_type;
    std::map<VertexType, ParameterType> parameter_type;
    std::vector<std::string> node_names;
    void initialize_min_max_per_type();
};

template< class VertexOutputType>
EveSimulatorParameters<VertexOutputType>::EveSimulatorParameters(const Network<VertexOutputType> & grn, std::ofstream & os )
{

    grn.get_adjacency_matrix(parameter_matrix, node_names);
    auto vertex_type_map = Vertex<VertexOutputType>::get_vertex_map(  );
    os << "% Nodes " << NEW_LINE;
    std::cout << 1 << NEW_LINE;
    for( auto it = vertex_type_map.begin() ; it != vertex_type_map.end(); ++it)
    {
          os << "node(\"" << it->first << "\"," << get_string_for_vertex_type(it->second) << ")."<<NEW_LINE;
    }
//    for(auto it = min_max_per_type.begin(); it != min_max_per_type.end(); ++it )
//    {
//        double min = it->second.first;
//        double max = it->second.second;
//        os << "range(" << get_string_for_vertex_type(it->first) << ",[" << min <<","<<max << "])." << NEW_LINE;
//    }
    os << "%Parameter type " << NEW_LINE;
    for( auto it = parameter_type.begin(); it != parameter_type.end(); ++it )
    {
        os << "param_type( "<<  get_string_for_vertex_type(it->first) << "," << get_string_from_parameter_type(it->second) << ")." << NEW_LINE;
    }
    for( int row = 0; row < parameter_matrix.size1(); ++row )
    {
        for( int col = 0 ; col < parameter_matrix.size2()-1; ++col)
        {
            if( !approximately_equal<VertexOutputType>( parameter_matrix(row,col ), 0) )
            {
                os << "regulates(\"" << node_names[col] << "\",\""<<node_names[row] << "\","<< parameter_matrix(row,col) << ")." <<NEW_LINE;
            }
        }

    }
    os << "regulates(R1,R2,0)." <<NEW_LINE;
    //Check if std::set and std::map are ordered
    //if continuous, every parameter of parameter_matrix can be altered in continuous way
    //if discrete: std::vector of adjacency matrices?

}


#endif // EVESIMULATORPARAMETERS_HPP
