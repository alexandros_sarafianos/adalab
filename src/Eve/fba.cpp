#include "fba.hpp"

#include <iostream>
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"

#include <glpk.h>             /* GNU GLPK linear/mixed integer solver */
LIBSBML_CPP_NAMESPACE_USE

FBA::FBA(const std::string & sbml_file)
{
    std::string id;
    SBMLDocument* document = readSBML( "/home/alexs/adalab_personal/Data/yeast_smbl_model/yeast_7.00.xml" );
    SBMLReader reader;
    model = document->getModel();
    std::cout << "            filename: " << sbml_file              << NEW_LINE;
    unsigned int level   = document->getLevel  ();
    unsigned int version = document->getVersion();
    std::cout     << "File: " << sbml_file
         << " (Level " << level << ", version " << version << ")" << NEW_LINE;

    std::cout << "               "
         << (level == 1 ? "name: " : "  id: ")
         << (model->isSetId() ? model->getId() : std::string("(empty)")) << NEW_LINE;
    if (model->isSetSBOTerm())
      std::cout << "      model sboTerm: " << model->getSBOTerm() << NEW_LINE;
    std::cout << "functionDefinitions: " << model->getNumFunctionDefinitions() << NEW_LINE;
    std::cout << "    unitDefinitions: " << model->getNumUnitDefinitions    () << NEW_LINE;
    std::cout << "   compartmentTypes: " << model->getNumCompartmentTypes   () << NEW_LINE;
    std::cout << "        specieTypes: " << model->getNumSpeciesTypes       () << NEW_LINE;
    std::cout << "       compartments: " << model->getNumCompartments       () << NEW_LINE;
    std::cout << "            species: " << model->getNumSpecies            () << NEW_LINE;
    std::cout << "         parameters: " << model->getNumParameters         () << NEW_LINE;
    std::cout << " initialAssignments: " << model->getNumInitialAssignments () << NEW_LINE;
    std::cout << "              rules: " << model->getNumRules              () << NEW_LINE;
    std::cout << "        constraints: " << model->getNumConstraints        () << NEW_LINE;
    std::cout << "          reactions: " << model->getNumReactions          () << NEW_LINE;
    std::cout << "             events: " << model->getNumEvents             () << NEW_LINE;
//    model->getListOfReactions ();
    std::cout << NEW_LINE;
    fbc_model
        = static_cast<FbcModelPlugin*>(model->getPlugin("fbc"));


    unsigned int k;
    for( k = 0; k < fbc_model->getNumFluxBounds(); ++k)
    {
        FluxBound * f = fbc_model->getFluxBound(k);
        std::cout << f->getReaction()<<" " << f->getValue()<<" " << f->getOperation() <<NEW_LINE;
    }
    unsigned int i,j;
    for(i=0; i < model->getNumReactions(); i++)
    {
      Reaction* re = model->getReaction(i);

    }
    optimize();
    std::cout << "Metabolites: " << model->getNumSpecies() << NEW_LINE;
}

void FBA::optimize()
{
    glp_prob *lp;
    lp = glp_create_prob();
    glp_set_prob_name(lp, "FBA");
    glp_set_obj_dir(lp, GLP_MAX);
    //add_decision_variables

    std::cout << "NUM " << model->getNumSpecies() <<NEW_LINE;
    glp_add_rows(lp, model->getNumSpecies());
    glp_add_cols(lp, model->getNumReactions() );
    std::cout << "A" <<NEW_LINE;
    Objective * active_objective = fbc_model->getActiveObjective 	();
    std::cout << "B" <<NEW_LINE;
    FluxObjective * flux_objective = active_objective->getFluxObjective(0);
    std::cout << "Coeff: " << flux_objective->getCoefficient () << " ID : " << flux_objective->getReaction() << NEW_LINE;
    unsigned int num_of_flux_objs = active_objective->getNumFluxObjectives();
    for(unsigned int i = 1; i <= model->getNumReactions(); ++i)
    {
        glp_set_col_name(lp, i , model->getReaction(i-1)->getId().c_str() );
        std::string bound_type;
        double lower_bound;
        //TODO: Should bound_type and lower_bound be read from fluxbound in file? Seems to be constant in yeast_7.0.0.xml
        glp_set_col_kind(lp,i,GLP_CV);
        glp_set_col_bnds(lp, i, GLP_LO, 0.0, 0.0); //fluxbounds!!!!, should be greaterOrEqual than 0
        double coefficient = 0;
        for(unsigned j = 0; j < active_objective->getNumFluxObjectives(); ++j)
        {
            if(active_objective->getFluxObjective(j)->getReaction() == model->getReaction(i-1)->getId())
            {
                //TODO check if coefficient is at right location, not one more or less...
                coefficient = active_objective->getFluxObjective(j)->getCoefficient();
                std::cout << "Setting coefficient " << coefficient << " for reaction " << active_objective->getFluxObjective(j)->getReaction() << "!" << NEW_LINE;
            }
        }
        glp_set_obj_coef(lp, i, coefficient);
    }

    int index = 1 ;
    std::vector<int> ia;// ia.reserve((model->getNumSpecies()+1)*(model->getNumReactions+1));
    std::vector<int> ja;// ja.reserve((model->getNumSpecies()+1)*(model->getNumReactions+1));
    std::vector<double> ar;// ar.reserve((model->getNumSpecies()+1)*(model->getNumReactions+1));
    for(unsigned int row = 1; row <= model->getNumSpecies();++row)
    {
        for(unsigned int col = 1; col <= model->getNumReactions(); ++col)
        {

            double coefficient = 0.;
            ia.push_back(row);
            ja.push_back(col);
            ar.push_back(coefficient);
        }
    }
//    int index = 1;
//    for(std::vector<transition>::const_iterator trans = transitions.begin(); trans != transitions.end();++trans)
//    {
//        std::string intStr = boost::lexical_cast<std::string>(index);
//        std::string str = "t" + std::string(intStr);
//        const char * c = str.c_str();
//        glp_set_col_name(lp, index, c);
//        glp_set_col_bnds(lp, index, GLP_LO, 0.0, 0.0);
//        glp_set_obj_coef(lp, index, trans->getProbability());
//        glp_set_col_kind(lp, index, GLP_BV); //all variables are binary variables (BV)
//        ++index;
//    }
//    set_constraints


//    glp_set_row_name(lp, 1, "c1");
//    glp_set_row_bnds(lp, 1, GLP_UP, 40, 40);  // Upper bound of 40
//    unsigned long int index = 1;

//    for (std::vector<transition>::const_iterator trans = transitions.begin(); trans != transitions.end();++trans)
//    {
//        ia[index] = 1;
//        ja[index] = index; //column
//        ar[index] = 1.0; //linear combination, all equal and 1
//        ++index;
//    }

    std::cout<< "Let's solve the problem now..." << NEW_LINE;
    /* solve problem */
    glp_simplex(lp, NULL);
    double z = glp_get_obj_val(lp);
    std::cout << z << NEW_LINE;
//    std::cout << glp_get_col_prim(lp, index);

    /* housekeeping */
     glp_delete_prob(lp);
     glp_free_env();
}
