#ifndef FBA_HPP
#define FBA_HPP

#include <string>
#include <sbml/SBMLTypes.h>

#include <sbml/common/extern.h>
#include <sbml/packages/fbc/common/FbcExtensionTypes.h>

class FBA
{
public:
    FBA(const std::string &sbml_file = "/home/alexs/adalab_personal/Data/yeast_smbl_model/yeast_7.00.xml");
    void optimize();
private:
    FbcModelPlugin* fbc_model;
    Model* model;
};

#endif // FBA_HPP
