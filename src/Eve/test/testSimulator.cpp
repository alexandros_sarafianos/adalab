#include <iostream>
#include "src/Eve/evesimulator.hpp"
#include "data.hpp"
#include "src/general/compile_configuration.hpp"
#include "src/experiment/experimentaloutput.hpp"
#include "src/general/maths/unit_step.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"

//bool difference_present_in_values(const std::map<int, double> & values)
//{
//    std::map<int, double>::const_iterator it = values.begin();
//    double prev_value = it->second;
//    std::cout << prev_value ;
//    ++it;
//    bool result = false;
//    for(it; it != values.end(); ++it)
//    {

//        if(!approximately_equal<double>(it->second, prev_value))
//            result = true;
//        prev_value = it->second;
//        std::cout <<", " << prev_value;
//    }
//    return result;
//}

//bool test_fixed_experiment(EveSimulator<bool> & eve, double lower,double upper)
//{
//    Experiment e (DATA_FILE_TEST_EXPERIMENT,lower,upper);

//    std::vector<ExperimentalOutput> o = eve.run_experiment_for_colony(e, 20, 1, 0., 0,  std::unordered_map<std::string,double>());
//    std::map<int, double> A_values = o[0].get_values_of_node<bool>("A");
//    bool ok = !difference_present_in_values(A_values);
//    if (!ok)
//            std::cout << "Error 1" << NEW_LINE;
//    bool ok2 = approximately_equal(A_values[0],lower);
//    if ( !ok2 )
//        std::cout << "Error 2 " << NEW_LINE;

//    return (ok && ok2);
//}

//bool test_fixed_experiment2(EveSimulator<bool> & eve, double lower,double upper)
//{
//    std::unordered_map<std::string,ExperimentalInfo> experiment;
//    experiment.insert(std::pair<std::string, ExperimentalInfo>("A", ExperimentalInfo( 1, true )));
//    Experiment e(experiment,lower,upper);
//    std::vector<ExperimentalOutput> o = eve.run_experiment_for_colony(e, 20, 1, 0., 0,  std::unordered_map<std::string,double>());
//    std::map<int, double> A_values = o[0].get_values_of_node<bool>("A");
//    bool ok = !difference_present_in_values(A_values);
//    if (!ok)
//            std::cout << "Error 3" << NEW_LINE;
//    bool ok2 = approximately_equal(A_values[0],upper);
//    if ( !ok2 )
//        std::cout << "Error 4 " << NEW_LINE;
//    return (ok && ok2 );
//}

//bool test_fixed_experiment3(EveSimulator<bool> & eve, double lower,double upper)
//{
//    std::unordered_map<std::string,ExperimentalInfo> experiment;
//    experiment.insert(std::pair<std::string, ExperimentalInfo>("A", ExperimentalInfo( 1, false )));
//    Experiment e(experiment,lower,upper);
//    std::vector<ExperimentalOutput> o = eve.run_experiment_for_colony(e, 20, 1, 0., 0,  std::unordered_map<std::string,double>());
//    std::map<int, double> A_values = o[0].get_values_of_node<bool>("A");
//    bool ok = difference_present_in_values(A_values);
//    if (!ok)
//            std::cout << "Error 5" << NEW_LINE;
//    bool ok2 = approximately_equal(A_values[-1], upper);
//    if ( !ok2 )
//        std::cout << "Error 6 " << NEW_LINE;
//    return (ok && ok2 );
//}

//bool test_fixed_experiment4(EveSimulator<bool> & eve, double lower,double upper)
//{
//    std::unordered_map<std::string,ExperimentalInfo> experiment;
//    experiment.insert(std::pair<std::string, ExperimentalInfo>("A", ExperimentalInfo( 0, false )));
//    Experiment e(experiment,lower,upper);
//    std::vector<ExperimentalOutput> o = eve.run_experiment_for_colony(e, 20, 1, 0., 0,  std::unordered_map<std::string,double>());
//    std::cout << o << NEW_LINE;
//    std::map<int, double> A_values = o[0].get_values_of_node<bool>("A");
//    bool ok = difference_present_in_values(A_values);
//    if (!ok)
//            std::cout << "Error 7" << NEW_LINE;
//    bool ok2 = approximately_equal(A_values[-1], lower);
//    if ( !ok2 )
//        std::cout << "Error 8 " << NEW_LINE;
//    return (ok && ok2 );
//}

void print_vals(std::map<int,bool> a)
{
    std::map<int,bool>::iterator it = a.begin();
    std::cout << it->second;
    for(it; it!=a.end(); ++it)
    {
        std::cout << "," <<it->second;
    }
    std::cout << NEW_LINE;

}
bool value_propagation_test()
{
    Sigmoid sigmoid(15,0.5);
//    ThresholdFunction * thresh;
//    thresh = &sigmoid;
    RandomWrapper random_number_generator ("uniform", 0.1);
//    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator, thresh);
    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator);

    Experiment e;
    std::vector<ExperimentalOutput> o = eve.run_experiment_for_colony(e, 20, 1, 0., 0,  std::unordered_map<std::string,double>());
    bool expected_A_vals[] = {true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true};
    bool expected_B_vals[] = {true,true,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false};
    bool expected_C_vals[] = {true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true};
    std::map<int, bool> A_values = o[0].get_values_of_node<bool>("A");
    std::map<int, bool> B_values = o[0].get_values_of_node<bool>("B");
    std::map<int, bool> C_values = o[0].get_values_of_node<bool>("C");

    std::map<int,bool>::const_iterator it_A = A_values.begin();
    std::map<int,bool>::const_iterator it_B = B_values.begin();
    int index = 0;
    bool ok = 1;
    bool prev_ok = 1;
    for(std::map<int,bool>::const_iterator it_C = C_values.begin(); it_C!=C_values.end();++it_C)
    {

        ok = ok &&  !(it_A->second== expected_A_vals[index]);
        if(prev_ok != ok)
        {
            std::cout << "---+----" << it_A->second << " and "<< expected_A_vals[index] <<NEW_LINE;
            std::cout << "Error for node A: ";print_vals(A_values);
            std::cout << " Expected ";
            for( int i = 0; i < 21 ; ++i)
                std::cout <<  expected_A_vals[i];
        }
        prev_ok= ok;
        ok = ok &&  !( it_B->second  == expected_B_vals[index]);
        if(prev_ok != ok)
        {
            std::cout << "---+----" << it_B->second << " and "<< expected_B_vals[index] <<NEW_LINE;
            std::cout << "Error for node B: " ;print_vals(B_values);
            std::cout << " Expected ";
            for( int i = 0; i < 21 ; ++i)
                std::cout <<  expected_B_vals[i];

        }
        prev_ok= ok;
        ok = ok &&  !( it_C->second  == expected_C_vals[index]);
        if(prev_ok != ok)
        {            std::cout << "---+----" << it_C->second << " and "<< expected_C_vals[index] <<NEW_LINE;

                std::cout << "Error for node C: " ;print_vals(C_values);
                std::cout << " Expected ";
                for( int i = 0; i < 21 ; ++i)
                    std::cout <<  expected_C_vals[i];}
        prev_ok= ok;
        it_A++;
        it_B++;
        index++;
    }
    if( !ok )
    {
        std::cout << "Value propagation test failed!" << NEW_LINE;
            std::cout << "A values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = A_values.begin(); it!=A_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
            std::cout << "B values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = B_values.begin(); it!=B_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
            std::cout << "C values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = C_values.begin(); it!=C_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
    }
    std::cout << "Value propagation test simple boolean network succeeded! " << NEW_LINE;
    return ok;
}

bool value_propagation_test_unit_step()
{
    Unit_step unit_step(0.0,1.0,0.5);
//    ThresholdFunction * thresh;
//    thresh = &unit_step;
    RandomWrapper random_number_generator ("uniform", 0.0);
//    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator, thresh);
      EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator);
    Experiment e;
    std::vector<ExperimentalOutput> o = eve.run_experiment_for_colony(e, 20, 1, 0., 0,  std::unordered_map<std::string,double>());
    // Values are "inverted" true means 0, false means 1 (too lazy to change)
    bool expected_A_vals[] = {true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true};
    bool expected_B_vals[] = {true,true,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false};
    bool expected_C_vals[] = {true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true};
    std::map<int, bool> A_values = o[0].get_values_of_node<bool>("A");
    std::map<int, bool> B_values = o[0].get_values_of_node<bool>("B");
    std::map<int, bool> C_values = o[0].get_values_of_node<bool>("C");

    std::map<int, bool>::const_iterator it_A = A_values.begin();
    std::map<int, bool>::const_iterator it_B = B_values.begin();
    int index = 0;
    bool ok = 1;
    bool prev_ok = 1;
    for(std::map<int,bool>::const_iterator it_C = C_values.begin(); it_C!=C_values.end();++it_C)
    {

        ok = ok &&  !( it_A->second  == expected_A_vals[index]);//see comment above
        if(prev_ok != ok)
        {
            std::cout << "---+----" << it_A->second << " and "<< expected_A_vals[index] <<NEW_LINE;
            std::cout << "Error for node A: ";print_vals(A_values);
            std::cout << " Expected ";
            for( int i = 0; i < 21 ; ++i)
                std::cout <<  expected_A_vals[i];;
        }
        prev_ok= ok;
        ok = ok &&  !(it_B->second  == expected_B_vals[index]);
        if(prev_ok != ok)
        {
            std::cout << "---+----" << it_B->second << " and "<< expected_B_vals[index] <<NEW_LINE;
            std::cout << "Error for node B: " ;print_vals(B_values); std::cout <<  " Expected " << expected_B_vals;
            std::cout << " Expected ";
            for( int i = 0; i < 21 ; ++i)
                std::cout <<  expected_B_vals[i];
        }
        prev_ok= ok;
        ok = ok &&  !(it_C->second  == expected_C_vals[index]);
        if(prev_ok != ok)
        {            std::cout << "---+----" << it_C->second << " and "<< expected_C_vals[index] <<NEW_LINE;

                std::cout << "Error for node C: " ;print_vals(C_values);
                std::cout << " Expected ";
                for( int i = 0; i < 21 ; ++i)
                    std::cout <<  expected_C_vals[i];}
        prev_ok= ok;
        it_A++;
        it_B++;
        index++;
    }
    if( !ok )
    {
        std::cout << "Value propagation test failed unit step!" << NEW_LINE;
            std::cout << "A values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = A_values.begin(); it!=A_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
            std::cout << "B values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = B_values.begin(); it!=B_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
            std::cout << "C values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = C_values.begin(); it!=C_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
    }
    return ok;
}


bool value_propagation_test_unit_step_network2()
{

    Unit_step unit_step(0.0,1.0,0.5);
    RandomWrapper random_number_generator ("uniform", 0.0);
//    ThresholdFunction * thresh;
//    thresh = &unit_step;
//    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK2, random_number_generator, thresh);
    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK2, random_number_generator);
    Experiment e;
    std::vector<ExperimentalOutput> o = eve.run_experiment_for_colony(e, 20, 1, 0., 0,  std::unordered_map<std::string,double>());
    // Values are not inverted here, false = 0, true = 1
    bool expected_A_vals[] = {false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false, true,true,false};
    bool expected_B_vals[] = {false,false,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true};
    bool expected_C_vals[] = {false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false};
    bool expected_D_vals[] = {false,false,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true,true,false,false,true};
    std::map<int, bool> A_values = o[0].get_values_of_node<bool>("A");
    std::map<int, bool> B_values = o[0].get_values_of_node<bool>("B");
    std::map<int, bool> C_values = o[0].get_values_of_node<bool>("C");
    std::map<int, bool> D_values = o[0].get_values_of_node<bool>("D");

    std::map<int,bool>::const_iterator it_A = A_values.begin();
    std::map<int,bool>::const_iterator it_B = B_values.begin();
    std::map<int,bool>::const_iterator it_D = D_values.begin();

    int index = 0;
    bool ok = 1;
    bool prev_ok = 1;
    for(std::map<int,bool>::const_iterator it_C = C_values.begin(); it_C!=C_values.end();++it_C)
    {

        ok = ok &&  (it_A->second == expected_A_vals[index]);
        if(prev_ok != ok)
        {
            std::cout << "---+----" << it_A->second << " and "<< expected_A_vals[index] <<NEW_LINE;
            std::cout << "Error for node A: ";print_vals(A_values);
            std::cout << " Expected ";
            for( int i = 0; i < 21 ; ++i)
                std::cout <<  expected_A_vals[i];
        }
        prev_ok= ok;
        ok = ok &&  ( it_B->second  == expected_B_vals[index]);
        if(prev_ok != ok)
        {
            std::cout << "---+----" << it_B->second << " and "<< expected_B_vals[index] <<NEW_LINE;
            std::cout << "Error for node B: " ;print_vals(B_values);
            std::cout << " Expected ";
            for( int i = 0; i < 21 ; ++i)
                std::cout <<  expected_B_vals[i];
        }
        prev_ok= ok;
        ok = ok &&  ( it_C->second == expected_C_vals[index]);
        if(prev_ok != ok)
        {            std::cout << "---+----" << it_C->second << " and "<< expected_C_vals[index] <<NEW_LINE;

                std::cout << "Error for node C: " ;print_vals(C_values);
                std::cout << " Expected ";
                for( int i = 0; i < 21 ; ++i)
                    std::cout <<  expected_C_vals[i];
        }
        prev_ok= ok;
        ok = ok &&  ( it_D->second == expected_D_vals[index]);
        if(prev_ok != ok)
        {            std::cout << "---+----" << it_D->second << " and "<< expected_D_vals[index] <<NEW_LINE;

                std::cout << "Error for node D: " ;print_vals(D_values);
                std::cout << " Expected ";
                for( int i = 0; i < 21 ; ++i)
                    std::cout <<  expected_D_vals[i];}
        prev_ok= ok;
        it_A++;
        it_B++;
        it_D++;
        index++;
    }
    if( !ok )
    {
        std::cout << "Value propagation test failed (network 2) !" << NEW_LINE;
            std::cout << "A values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = A_values.begin(); it!=A_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
            std::cout << "B values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = B_values.begin(); it!=B_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
            std::cout << "C values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = C_values.begin(); it!=C_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
            std::cout << "D values: "<< NEW_LINE;
            for(std::map<int,bool>::const_iterator it = D_values.begin(); it!=D_values.end();++it)
                std::cout << it->second << "\t";
            std::cout <<NEW_LINE;
    }
    return ok;
}

/**
    std::vector<double> run_experiment_time_step(const Experiment & e, const int colony_size, const std::vector<std::string> & metabolite_keys,
                                                 const std::vector<double> & metabolite_vals, const int iteration, const int timesteps, const std::vector<std::string> & gene_keys );
 */

bool run_experiment_time_step_basic()
{
    std::cout << "------------Testing basic boolean operation, initial 1, empty experiment, no metabolite changes-----------" << NEW_LINE;
    bool expected_A_vals[] = {false, true, false, true, false, true, false, true, false, true};
    bool expected_B_vals[] = {true, false, true, false, true, false, true, false, true, false};
    bool expected_C_vals[] = {false, true, false, true, false, true, false, true, false, true};
    Unit_step unit_step(0.0,1.0,0.5);
//    ThresholdFunction * thresh;
//    thresh = &unit_step;
    RandomWrapper random_number_generator ("uniform", 0.0);
//    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator, thresh);
    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator);

    Experiment eve_test_experiment;
    std::vector<std::string> initial_keys = {"A","B", "C"};
    std::vector<double> initial_values {1.,1.,1.};
    eve.initialize_node_values_with_map(initial_keys, initial_values);
    std::vector<std::string> gene_keys = {"A", "B", "C"};
    bool end_ok = true;
    for( int t = 0; t < 10; ++t)
    {
        std::vector<double> gene_vals = eve.run_experiment_time_step(  eve_test_experiment, std::vector<std::string>() ,std::vector<double>(), t,2, gene_keys );
        if( gene_vals[0] != expected_A_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of A at t="<< t << ", expected "<< expected_A_vals[t] << "but found " << gene_vals[0] << NEW_LINE;
            end_ok = false;
        }
        if( gene_vals[1] != expected_B_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of B at t="<< t << ", expected "<< expected_B_vals[t] << "but found " << gene_vals[1] << NEW_LINE;
            end_ok = false;
        }
        if( gene_vals[2] != expected_C_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of C at t="<< t << ", expected "<< expected_C_vals[t] << "but found " << gene_vals[2] << NEW_LINE;
            end_ok = false;
        }
        std::cout << gene_vals << NEW_LINE;
    }
    std::string text = "FAILED";
    if (end_ok == true)
        text = "SUCCEEDED";
    std::cout << "------------basic boolean operation, initial 1, empty experiment, no metabolite changes " << text <<"!-----------" << NEW_LINE;

    return end_ok;
}
bool convert(const double val, const double thr)
{
    if (val > thr)
        return true;
    else
        return false;
}
bool run_experiment_time_step_met()
{
    std::cout << "------------Testing boolean operation, initial 0, empty experiment, with metabolite changes at t=3 t=5-----------" << NEW_LINE;
    bool expected_A_vals[] = {true,false, true,  true, false, true, false, true, false, true };
    bool expected_B_vals[] = {false, true, false, false, true, false, true, false, true, false};
    bool expected_C_vals[] = {true, false, true, true, false, true, false, true, false, true};
    Unit_step unit_step(0.0,1.0,0.5);
//    ThresholdFunction * thresh;
//    thresh = &unit_step;
    RandomWrapper random_number_generator ("uniform", 0.0);
//    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator, thresh);
      EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator);
    Experiment eve_test_experiment;
    std::vector<std::string> initial_keys = {"A","B", "C"};
    std::vector<double> initial_values {0.,0.,0.};
    eve.initialize_node_values_with_map(initial_keys, initial_values);
    std::vector<std::string> gene_keys = {"A", "B", "C"};
    bool end_ok = true;
    for( int t = 0; t < 10; ++t)
    {
        std::cout << "<<<<<<<<<<<<<TIME STEP " << t <<">>>>>>>>>>>>>" <<  NEW_LINE;
        std::vector<double> gene_vals;
        if( t == 3 || t == 5 )
        {
            std::cout << "before, " << t << NEW_LINE;
            std::vector<std::string> metabolite_keys = {"A", "B","C" };
            std::vector<double> metabolite_values = {0., 1., 0. };
            gene_vals = eve.run_experiment_time_step( eve_test_experiment, metabolite_keys, metabolite_values, t, 2, gene_keys );
            std::cout << "after " << t << NEW_LINE;
        }
        else
        {
            gene_vals = eve.run_experiment_time_step(  eve_test_experiment, std::vector<std::string>() ,std::vector<double>(), t,2, gene_keys );
        }

        assert(gene_vals.size() == 3);
        if( convert(gene_vals[0],0.5) != expected_A_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of A at t="<< t << ", expected "<< expected_A_vals[t] << "but found " << gene_vals[0] << NEW_LINE;
            end_ok = false;
        }
        if( convert(gene_vals[1],0.5) != expected_B_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of B at t="<< t << ", expected "<< expected_B_vals[t] << "but found " << gene_vals[1] << NEW_LINE;
            end_ok = false;
        }
        if( convert(gene_vals[2],0.5) != expected_C_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of C at t="<< t << ", expected "<< expected_C_vals[t] << "but found " << gene_vals[2] << NEW_LINE;
            end_ok = false;
        }
        std::cout << gene_vals << NEW_LINE;
    }
    std::string text = "FAILED";
    if (end_ok == true)
        text = "SUCCEEDED";
    std::cout << "------------basic boolean operation, initial 1, empty experiment,  with metabolite changes at t=3 t=5 " << text <<"!-----------" << NEW_LINE;

    return end_ok;
}

bool run_experiment_time_step_met_exp()
{
    std::cout << "------------Testing boolean operation, initial 0, empty experiment, with metabolite changes at t=3 t=5 and experiment-----------" << NEW_LINE;
    bool expected_A_vals[] = {false, false, false, false, false, false, false, false, false, false};
    bool expected_B_vals[] = {false, false, false, false, false, false, false, false, false, false};
    bool expected_C_vals[] = {true, true, true, true, true, true, true, true, true, true};
    Unit_step unit_step(0.0,1.0,0.5);
//    ThresholdFunction * thresh;
//    thresh = &unit_step;
    RandomWrapper random_number_generator ("uniform", 0.0);
//    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator, thresh);
        EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator);
    Experiment e;
    e = Experiment(DATA_FILE_TEST_EXPERIMENT, "");
    std::vector<std::string> initial_keys = {"A","B", "C"};
    std::vector<double> initial_values {1.,1.,1.};
    eve.initialize_node_values_with_map(initial_keys, initial_values);
    std::vector<std::string> gene_keys = {"A", "B", "C"};
    bool end_ok = true;
    for( int t = 0; t < 10; ++t)
    {
        std::cout << "<<<<<<<<<<<<<TIME STEP " << t <<">>>>>>>>>>>>>" <<  NEW_LINE;
        std::vector<double> gene_vals;
        if( t == 3 || t == 5 )
        {//test this part correctly!
            std::cout << "before, " << t << NEW_LINE;
            std::vector<std::string> metabolite_keys = {"A", "B","C" };
            std::vector<double> metabolite_values = {0., 1., 0. };
            gene_vals = eve.run_experiment_time_step( e, metabolite_keys, metabolite_values, t, 2, gene_keys );
            std::cout << "after " << t << NEW_LINE;
        }
        else
        {
            gene_vals = eve.run_experiment_time_step(  e, std::vector<std::string>() ,std::vector<double>(), t,2, gene_keys );
        }

        assert(gene_vals.size() == 3);
        if( convert(gene_vals[0],0.5) != expected_A_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of A at t="<< t << ", expected "<< expected_A_vals[t] << "but found " << gene_vals[0] << NEW_LINE;
            end_ok = false;
        }
        if( convert(gene_vals[1],0.5) != expected_B_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of B at t="<< t << ", expected "<< expected_B_vals[t] << "but found " << gene_vals[1] << NEW_LINE;
            end_ok = false;
        }
        if( convert(gene_vals[2],0.5) != expected_C_vals[t])
        {
            std::cout << "Error in run_experiment_time_step, value of C at t="<< t << ", expected "<< expected_C_vals[t] << "but found " << gene_vals[2] << NEW_LINE;
            end_ok = false;
        }
        std::cout << gene_vals << NEW_LINE;
    }
    std::string text = "FAILED";
    if (end_ok == true)
        text = "SUCCEEDED";
    std::cout << "------------basic boolean operation, initial 1, empty experiment,  with metabolite changes at t=3 t=5 and experiment" << text <<"!-----------" << NEW_LINE;

    return end_ok;
}

bool test_adjacency_matrix()
{
    RandomWrapper random_number_generator ("uniform", 0);
//    ThresholdFunction * thresh;
//    EveSimulator<double> eve(DATA_FILE_TEST_NETWORKCONT, random_number_generator, thresh);
      EveSimulator<double> eve(DATA_FILE_TEST_NETWORKCONT, random_number_generator);
    boost::numeric::ublas::matrix<double> adjacency_matrix;
    std::vector<std::string> node_names;
    eve.get_adjacency_matrix(adjacency_matrix, node_names);
    boost::numeric::ublas::matrix<double> expected_adjacency_matrix(4,5);
    std::vector<double> expected_values = {0.05,0,0,0,-0.0004,-0.05,0.05,0.51,0,-0.0003,0.23,0.66,0.01,0,-0.012,0,-0.21,0,0.01,-0.003};
    my_copy(expected_values, expected_adjacency_matrix);

    bool ok = equal_matrices(adjacency_matrix,expected_adjacency_matrix);
    if( !ok )
        std::cout << "Test adjacency-matrix failed (equality)" << NEW_LINE;
    std::vector<double> incorrect_expected_values = {0.01,0,0,0,-0.0004,-0.05,0.05,0.51,0,-0.0003,0.23,0.66,0.01,0,-0.012,0,-0.21,0,0.01,-0.003};
    boost::numeric::ublas::matrix<double> incorrect_expected_adjacency_matrix(4,5);
    my_copy(incorrect_expected_values, incorrect_expected_adjacency_matrix);
    bool ok2 = !equal_matrices(adjacency_matrix,incorrect_expected_adjacency_matrix);
    if( !ok2 )
        std::cout << "Test adjacency-matrix failed (unequality)" << NEW_LINE;
    return ok && ok2;
}

//test metabolite keys set at certain time step
int main(int argc, char * argv[])
{
//    ThresholdFunction * thresh;
    Sigmoid sigmoid(5,0.,1.,2.);
//    thresh = &sigmoid;
    RandomWrapper random_number_generator ("uniform", 0.2);
//    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator, thresh);
    EveSimulator<bool> eve(DATA_FILE_TEST_NETWORK, random_number_generator);
    LoggerSingleton::Instance()->open_log_file(std::string(argv[0]));
    std::vector<bool> succeeded_tests;
    std::cout << "1" << NEW_LINE;
    succeeded_tests.push_back(value_propagation_test_unit_step());
    std::cout << "2" << NEW_LINE;
    succeeded_tests.push_back(value_propagation_test_unit_step_network2());std::cout << "3" << NEW_LINE;
    succeeded_tests.push_back(value_propagation_test());std::cout << "4" << NEW_LINE;
    succeeded_tests.push_back(run_experiment_time_step_basic());std::cout << "5" << NEW_LINE;
    LoggerSingleton::Instance()->set_silent(false);
    succeeded_tests.push_back(run_experiment_time_step_met());std::cout << "6" << NEW_LINE;
    succeeded_tests.push_back(run_experiment_time_step_met_exp());std::cout << "7" << NEW_LINE;
    succeeded_tests.push_back(test_adjacency_matrix());std::cout << "1" << NEW_LINE;
    size_t positives = 0; size_t negatives = 0;
    if(!test_all_trues(succeeded_tests, positives, negatives))
    {
       std::cout << "EveSimulatorTest failed " << NEW_LINE;
       return  -1;
    }

    return 0;
}
