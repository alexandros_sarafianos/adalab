include(${PROJECT_SOURCE_DIR}/CMakeMacro.txt)

ADD_DATA_FILE(TEST_NETWORK test_network.txt)
ADD_DATA_FILE(TEST_EXPERIMENT test_experiment.xml)
ADD_DATA_FILE(TEST_NETWORK2 test_network_2.txt)
ADD_DATA_FILE(TEST_NETWORKCONT ../../network/test/test_cont_network.xml)
CREATE_TEST(testSimulator testSimulator.cpp)
