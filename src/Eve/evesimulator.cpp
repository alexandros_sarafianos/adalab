#include <src/Eve/evesimulator.hpp>

template<>
void EveSimulator<bool>::create_settings_file(const std::string & file_name, const std::string & file_name_exp)const
{
    remove_contents_of_file(file_name);
    remove_contents_of_file(file_name_exp);

    std::ofstream os, os_2;
    open_output_file_safely(file_name, os);
    open_output_file_safely(file_name_exp, os_2);
    EveSimulatorParameters<bool>(grn, os );
    ExperimentalSpace<> exp_f((Exp_Type) boolean, os_2);

}
template<>
void EveSimulator<double>::create_settings_file(const std::string & file_name, const std::string & file_name_exp)const
{
    remove_contents_of_file(file_name);
    remove_contents_of_file(file_name_exp);

    std::ofstream os, os_2;
    open_output_file_safely(file_name, os);
    open_output_file_safely(file_name_exp, os_2);
    EveSimulatorParameters<double>(grn, os );
    ExperimentalSpace<> exp_f( (Exp_Type) continuous, os_2);

}
