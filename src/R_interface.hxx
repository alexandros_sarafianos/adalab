#ifndef R_INTERFACE_HXX
#define R_INTERFACE_HXX
#include <RcppCommon.h>
#include "src/Eve/evesimulator.hpp"
#include "src/experiment/experiment.hpp"

namespace Rcpp{
    namespace traits{
//        template< typename T > SEXP wrap(const EveSimulator<T> &eve)
//        {
//            return Rcpp::wrap(eve);
//        }
//        template< typename T > EveSimulator<T> as(SEXP e_sexp)
//        {
//            EveSimulator<T> e;
//            return e;
//        }
//        SEXP wrap( const Experiment & exp){return Rcpp::wrap(eve);}
//        Experiment as( SEXP ex_sexp){Experiment exp(); return exp ;}
    template< typename T > SEXP wrap(const EveSimulator<T> &eve);
//    {
//        return Rcpp::wrap(eve);
//    }
    template< typename T > EveSimulator<T> as(SEXP e_sexp);
//    {
//        EveSimulator<T> e;
//        return e;
//    }
    SEXP wrap( const Experiment & exp);//{return Rcpp::wrap(eve);}
    Experiment as( SEXP ex_sexp);//{Experiment exp(); return exp ;}
    }
}

#include <Rcpp.h>
#endif // R_INTERFACE_HXX
