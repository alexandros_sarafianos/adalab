#include <src/Eve/evesimulator.hpp>
//NEEDS to be in .cpp file!
//RCCP_EXPOSED_CLASS(Experiment)

RCPP_EXPOSED_CLASS(EveSimulatorBool)
RCPP_EXPOSED_CLASS(EveSimulatorDouble)
RCPP_EXPOSED_CLASS(Experiment)
RCPP_EXPOSED_CLASS(VertexDouble)
RCPP_EXPOSED_CLASS(VertexBool)
RCPP_EXPOSED_CLASS(Edge)
RCPP_EXPOSED_CLASS(ExperimentalOutput)


RCPP_MODULE(simulator){
    Rcpp::class_<EveSimulator<bool>>("EveSimulatorBool")
        .constructor()
        .constructor<std::string, std::string, double, std::string>()
        .method("r_test", &EveSimulator<bool>::r_test)
        .method("random_initialize_node_values", &EveSimulator<bool>::random_initialize_node_values)
        .method("initialize_node_values_with_map", &EveSimulator<bool>::initialize_node_values_with_map)
        .method("run_experiment_time_step", &EveSimulator<bool>::run_experiment_time_step)
        .method("reset_state_map", &EveSimulator<bool>::reset_state_map)
        ;
    Rcpp::class_<EveSimulator<double>>("EveSimulatorDouble")
        .constructor()
        .constructor<std::string, std::string, double, std::string>()
        .method("r_test", &EveSimulator<double>::r_test)
        .method("random_initialize_node_values", &EveSimulator<double>::random_initialize_node_values)
        .method("initialize_node_values_with_map", &EveSimulator<double>::initialize_node_values_with_map)
        .method("run_experiment_time_step", &EveSimulator<double>::run_experiment_time_step)
        .method("reset_state_map", &EveSimulator<double>::reset_state_map)

            ;
    Rcpp::class_<Experiment>("Experiment")
    .constructor<std::string,std::string>()
    .constructor<std::string>()
    .constructor()
            ;
    Rcpp::class_<Vertex<double>>("VertexDouble")
            .constructor()
            ;
    Rcpp::class_<Vertex<bool>>("VertexBool")
            .constructor()
            ;
    Rcpp::class_<Edge>("Edge")
            .constructor()
            ;
    Rcpp::class_<ExperimentalOutput>("ExperimentalOutput")
            .constructor()
            ;

}
