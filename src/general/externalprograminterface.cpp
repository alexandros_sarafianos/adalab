#include "externalprograminterface.hpp"
#include <iostream>

ExternalProgramInterface::ExternalProgramInterface()
{}


bool ExternalProgramInterface::open_pipe(const std::string &cmd_string, const std::string mode)
{
    if( system(NULL)) puts ("Ok");
    else
    {
        std::stringstream os; os << "CPU not ready" ;LoggerSingleton::Instance()->info_print(os);
        return false;
    }
    handle = popen(cmd_string.c_str(), mode.c_str());
    if( handle == NULL)
    {
        std::stringstream os; os << "Error opening pipe for " << cmd_string << " mode " << mode ;LoggerSingleton::Instance()->info_print(os);
        return false;
    }
    return true;
}

bool ExternalProgramInterface::read_from_pipe(const std::string &cmd_string, const std::string mode)
{
    if( open_pipe(cmd_string, mode) )
    {
        char buf[64];
        size_t readn;
        while((readn = fread(buf,1,sizeof(buf), handle ))>0)
        {
            fwrite(buf, 1, readn, stdout);
        }
        pclose(handle);
        return true;
    }
    else
        return false;
}

bool ExternalProgramInterface::write_to_pipe(const std::string &message)
{
    fwrite( message.c_str(),1, sizeof(message.c_str()), handle );
    return true;
}

