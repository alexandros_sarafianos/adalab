#ifndef EXTERNALPROGRAMINTERFACE_HPP
#define EXTERNALPROGRAMINTERFACE_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "src/general/compile_configuration.hpp"


class ExternalProgramInterface
{
public:
    ExternalProgramInterface();

    bool open_pipe(const std::string &cmd_string, const std::string mode);

    bool read_from_pipe(const std::string &cmd_string, const std::string mode);

    bool write_to_pipe( const std::string & message );
private:
    FILE *handle;
};

#endif // EXTERNALPROGRAMINTERFACE_HPP
