#ifndef SIGMOID_HPP
#define SIGMOID_HPP
#include <cmath>
#include "src/general/maths/thresholdFunction.hpp"
#include <iostream>
#include <assert.h>
#include "src/general/compile_configuration.hpp"
#include <algorithm>    // std::min, std::max
class Sigmoid : public ThresholdFunction
{
public:
    Sigmoid(double m_slope,  double m_threshold );
    Sigmoid(double m_slope,  double m_threshold, double m_y_offset, double m_gain );
    Sigmoid(){;}
//    inline double operator()(double m_activation, double m_threshold, double m_slope)
//    {
//        double exponent = -1*m_slope*(m_activation-m_threshold);
//        return 1/(1+exp(exponent));
//    }
    double operator()( const double activation )const
    {
        double exponent = -1*slope*(activation-threshold);
        return gain/(1+exp(exponent))-y_offset;  //TESTING
    }
    double complement(const double activation) const{ return operator()(2*threshold-activation); }
    double lower_y_range()const{return -1*y_offset;}
    double upper_y_range()const{return gain-y_offset;}
    double disjunction(double d1, double d2)const{return d1+d2;}//{return std::max(d1,d2);}//
    double conjunction(double d1, double d2)const{return d1*d2;}//{return std::min(d1,d2);}//
    double negation(double d)const{return 1-d;}//{std::cout << "Negation of "<< d << "=" << 2*threshold-d << NEW_LINE;return 2*threshold-d;}//{return 1-d;}//
    double disjunction_alt(double d1, double d2)const{return std::max(d1,d2);}////
    double conjunction_alt(double d1, double d2)const{return std::min(d1,d2);}////
    double negation_alt(double d)const{return 1-d;}//{std::cout << "Negation of "<< d << "=" << 2*threshold-d << NEW_LINE;return 2*threshold-d;}//{return 1-d;}//
    double add_noise(const double d, RandomWrapper & noise_generator)const;
private:
    double  threshold, slope,y_offset, gain;
};

#endif // SIGMOID_HPP
