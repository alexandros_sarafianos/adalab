#include "unit_step.hpp"
#include "src/general/compile_configuration.hpp"

Unit_step::Unit_step(double m_value_low, double m_value_high, double m_turn_point)
{
    value_low = m_value_low;
    value_high = m_value_high;
    turn_point = m_turn_point;
    INFO_LOW("New unit step function v_l:" << value_low << ",v_h:" << value_high<<",t_p:"<< turn_point);
    std::cout << "New unit step function v_l:" << value_low << ",v_h:" << value_high<<",t_p:"<< turn_point << NEW_LINE;
}
double Unit_step::disjunction(double d1, double d2)const
{
    if (d1>= turn_point || d2 >= turn_point )
        return value_high;
    else
        return value_low;
}
double Unit_step::conjunction(double d1, double d2)const
{
    if (d1>= turn_point && d2 >= turn_point )
        return value_high;
    else
        return value_low;
}
double Unit_step::negation(double d)const
{
    if( d >= turn_point)
        return value_low;
    else
        return value_high;
}

double Unit_step::add_noise(const double d, RandomWrapper & noise_generator) const
{
    double noise = noise_generator();
    return noise; //TODO: meaningful implementation
}
