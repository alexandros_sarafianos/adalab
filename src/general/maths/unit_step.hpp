#ifndef UNIT_STEP_HPP
#define UNIT_STEP_HPP
#include "src/general/maths/thresholdFunction.hpp"

class Unit_step : public ThresholdFunction
{
public:
    Unit_step(double m_value_low, double m_value_high, double m_turn_point);

    Unit_step(){;}
    double operator()( const double activation )const{
        if( activation >= turn_point)
            return value_high;
        else
            return value_low;
    }
    double lower_y_range()const{return value_low;}
    double upper_y_range()const{return value_high;}
    double disjunction(double d1, double d2)const;
    double conjunction(double d1, double d2)const;//{return std::min(d1,d2);}//
    double negation(double d)const;//{std::cout << "Negation of "<< d << "=" << 2*threshold-d << NEW_LINE;return 2*threshold-d;}//{return 1-d;}//
    double add_noise(const double d, RandomWrapper & noise_generator) const;
private:
    double  value_low, value_high, turn_point;
};

#endif // UNIT_STEP_HPP
