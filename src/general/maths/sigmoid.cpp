#include "src/general/maths/sigmoid.hpp"
#undef NDEBUG
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"

Sigmoid::Sigmoid(double m_slope,  double m_threshold )
{
    slope = m_slope;
    threshold = m_threshold;
    y_offset = 0;
    gain = 1;
    INFO_LOW(" New Sigmoid created, parameters: " << m_slope << "," << m_threshold << "," << lower_y_range() << "," << upper_y_range());
}
Sigmoid::Sigmoid(double m_slope,  double m_threshold, double m_y_offset, double m_gain )
{
    slope = m_slope;
    threshold = m_threshold;
    y_offset = m_y_offset;
    gain = m_gain;
    INFO_LOW(" New Sigmoid created, parameters: " << m_slope << "," << m_threshold << "," << lower_y_range() << "," << upper_y_range() );
}
double Sigmoid::add_noise(const double d, RandomWrapper & noise_generator)const
{
    //TODO: MEaningful implementation
    double noise = noise_generator();

    return 1.0;
}
