#ifndef THRESHOLDFUNCTION_H
#define THRESHOLDFUNCTION_H
#include "src/general/compile_configuration.hpp"
/*Abstract class*/
#include "src/general/maths/RandomWrapper.hpp"

class ThresholdFunction
{
public:
    // Returns ThresholdFunction(activation)
    ThresholdFunction(){}
    virtual double operator()(const double activation)const = 0;
    virtual double lower_y_range()const = 0 ;
    virtual double upper_y_range()const = 0 ;
    virtual double disjunction(double d1, double d2)const = 0;
    virtual double conjunction(double d1, double d2)const = 0;
    virtual double negation(double d)const = 0;
    virtual double add_noise(const double d, RandomWrapper & noise_generator) const = 0;
private:
};
#endif // THRESHOLDFUNCTION_H
