#include <iostream>
#include "src/general/maths/unit_step.hpp"
#include "src/general/compile_configuration.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
bool test_operator()
{
    Unit_step unit_step(0., 1., 0.5);

    bool ok1 = unit_step (0.) == 0.;
    if( !ok1 )
        std::cout << "error operator value 0, found "<< unit_step(0) << NEW_LINE;
    bool ok2 = unit_step(1.) == 1.;
    if(!ok2 )
        std::cout << "error operator value 1, found " << unit_step(1) << NEW_LINE;
    return ok1 && ok2 ;
}

bool test_disjunction()
{
    Unit_step unit_step(0., 1., 0.5);

    bool ok1 = unit_step.disjunction (0., 0.) == 0.;
    if( !ok1 )
        std::cout << "error disjunction value 0,0: , found "<< unit_step.disjunction (0., 0.)  << NEW_LINE;
    bool ok2 = unit_step.disjunction (0., 1.) == 1.;
    if(!ok2 )
        std::cout << "error disjunction value 0,1, found " << unit_step.disjunction (0., 1.) << NEW_LINE;

    bool ok3 = unit_step.disjunction (1., 0.) == 1.;
    if(!ok3 )
        std::cout << "error disjunction value 1,0, found " << unit_step.disjunction (1., 0.) << NEW_LINE;

    bool ok4 = unit_step.disjunction (1., 1.) == 1.;
    if(!ok4 )
        std::cout << "error disjunction value 1,1, found " << unit_step.disjunction (1., 1.) << NEW_LINE;
    return ok1 && ok2 && ok3 && ok4 ;
}

bool test_conjunction()
{
    Unit_step unit_step(0., 1., 0.5);

    bool ok1 = unit_step.conjunction (0., 0.) == 0.;
    if( !ok1 )
        std::cout << "error conjunction value 0,0: , found "<< unit_step.conjunction (0., 0.)  << NEW_LINE;
    bool ok2 = unit_step.conjunction (0., 1.) == 0.;
    if(!ok2 )
        std::cout << "error conjunction value 0,1, found " << unit_step.conjunction (0., 1.) << NEW_LINE;

    bool ok3 = unit_step.conjunction (1., 0.) == 0.;
    if(!ok3 )
        std::cout << "error conjunction value 1,0, found " << unit_step.conjunction (1., 0.) << NEW_LINE;

    bool ok4 = unit_step.conjunction (1., 1.) == 1.;
    if(!ok4 )
        std::cout << "error conjunction value 1,1, found " << unit_step.conjunction (1., 1.) << NEW_LINE;
    return ok1 && ok2 && ok3 && ok4 ;
}

bool test_negation()
{
    Unit_step unit_step(0., 1., 0.5);
    bool ok1 = unit_step.negation(0.) == 1.;
    if( !ok1 )
         std::cout << "error negation value 0: , found "<< unit_step.negation (0.)  << NEW_LINE;
    bool ok2 = unit_step.negation(1.) == 0.;
    if( !ok2 )
         std::cout << "error negation value 1: , found "<< unit_step.negation (1.)  << NEW_LINE;
    return ok1 && ok2;
}

int main(int argc, char * argv[])
{

    bool value = (test_operator());

    value =  test_disjunction() && value;
    value =  test_conjunction() && value;
    value = test_negation() && value;
    if(!value)
    {
       std::cout << "UnitStepTest failed " << NEW_LINE;
       return  -1;
    }

    return 0;
}
