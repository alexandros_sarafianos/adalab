#include <iostream>
#include "src/optimization/simulatedannealing.hpp"


bool test_parameters()
{
    Parameters params;
    params.add_parameter("x1", true);
    params.add_parameter("x2", (long double) 0.69);
    std::cout << params;

    Parameters params2;
    params2 = params;
    params.add_parameter("x3", false );
    std::cout << "P " << params;
    std::cout << "P2 " << params2;
    Neighbour n;
    Parameters p = params.generate_neighbour(n, 1.);
    std::cout << "1Neighbour of " << params << ":" << p;
    Parameters p2 = params.generate_neighbour(n, 1.);
    std::cout << "2Neighbour of " << params << ":" << p2;
    Parameters p3 = params.generate_neighbour(n, 0.);
    std::cout << "3Neighbour of " << params << ":" << p3;

    bool ok1 = p3 == params;
    if ( !ok1 )
        std::cout << "Error1 : Expected p3 == params: " << p3 << params << std::endl;
    bool ok2 = p2 != params;
    if ( !ok2 )
        std::cout << "Error2: Expected p2 != params: " << p2 << params << std::endl;

    bool ok3 = p != params;
    if ( !ok3 )
        std::cout << "Error3: Expected p != params: " << p << params << std::endl;

    return ok1 && ok2 && ok3 ;
}

bool test_simulated_annealing_squared()
{
    std::cout <<"-------test_simulated_annealing-----------" << std::endl;
    Parameters params;
    params.add_parameter("x1",(long double) 0.0);
    params.randomize_parameters();
    Neighbour nf;
    TemperatureFunction tf;
    EnergyFunction ef;
    int first_version = 0;
    auto sol1 = simulated_annealing(params, 10000, ef, tf, nf, first_version );
    std::cout << "Solution version 0"<<  sol1 << " val: " << ef(sol1) << std::endl;
    int second_version = 1;
    auto sol2 = simulated_annealing(params, 10000, ef, tf, nf, second_version );
    std::cout << "Solution version 1 "<<  sol2 << " val: " << ef(sol2) << std::endl;
    Neighbour nf2(0);
    auto sol25 = simulated_annealing(params, 10000, ef, tf, nf2, second_version );
    std::cout << "Solution version 2 "<<  sol25 << " val: " << ef(sol25) << std::endl;
    std::cout << "Changing boundaries " << std::endl;

    params.add_boundaries("x1", std::pair<long double, long double>(-20.,20.));
    auto sol3 = simulated_annealing(params, 10000, ef, tf, nf, first_version );
    std::cout << "Solution version 0 (boundary change)"<<  sol3<< " val: " << ef(sol3) << std::endl;
    auto sol4 = simulated_annealing(params, 10000, ef, tf, nf, second_version );
    std::cout << "Solution version 1 (boundary change)"<<  sol4 << " val: " << ef(sol4) << std::endl;
    auto sol5 = simulated_annealing(params, 10000, ef, tf, nf2, second_version );
    std::cout << "Solution version 2 boundary change"<<  sol5 << " val: " << ef(sol5) << std::endl;
    std::cout << "-----END of test_simulated_annealing_squared----- " << std::endl;

    return true;
    //version 0 is better
}
//https://en.wikipedia.org/wiki/Test_functions_for_optimization
bool test_randomized_parameters()
{
    std::cout <<"-------test_randomized_parameters-----------" << std::endl;
    Parameters params;
    params.add_parameter("x1",(long double) 0.0);
    params.add_parameter("x2", (long double) 0.0);
    params.add_parameter("x3", true);
    int n = 0;
    for(; n < 100 ; ++n)
    {
        Parameters params_old = params;
        params.randomize_parameters();
        if( params_old == params)
            return false;
        INFO_DEBUG( "P_old : " << params_old << "-----");

    }
    std::cout << n << " random parameters generated ! " << std::endl;
    return true;
}
bool test_simulated_annealing_rosen()
{
    std::cout <<"-------test_simulated_annealing_rosen-----------" << std::endl;
    Parameters params;
    params.add_parameter("x1",(long double) 0.0);
    params.add_boundaries("x1", std::pair<long double, long double>(-20.,20.));
    params.add_parameter("x2",(long double) 0.0);
    params.add_boundaries("x2", std::pair<long double, long double>(-20.,20.));
    params.randomize_parameters();
    Neighbour nf;
    TemperatureFunction tf;
    Rosen ef;
    int first_version = 0;
    auto sol1 = simulated_annealing(params, 10000, ef, tf, nf, first_version );
    std::cout << "Solution version 0 "<<  sol1<< " val: " << ef(sol1) << std::endl;
    int second_version = 1;
    auto sol2 = simulated_annealing(params, 10000, ef, tf, nf, second_version );
    std::cout << "Solution version 1"<<  sol2 <<  " val: " << ef(sol2) << std::endl;
    Neighbour nf2= Neighbour(0);
    auto sol3 = simulated_annealing(params, 10000, ef, tf, nf2, second_version );
    std::cout << "Solution version 2"<<  sol3 <<  " val: " << ef(sol3) << std::endl;

    std::cout <<" Optimal solution : f(1,1,...1) = 0  " << std::endl;
    std::cout << "-----END of test_simulated_annealing_rosen----- " << std::endl;
    return true;
    //version 0 is better
}

bool test_simulated_annealing_levi()
{
    std::cout <<"-------test_simulated_annealing_levi-----------" << std::endl;
    Parameters params;
    params.add_parameter("x1",(long double) 0.0);
    params.add_boundaries("x1", std::pair<long double, long double>(-10.,10.));
    params.add_parameter("x2",(long double) 0.0);
    params.add_boundaries("x2", std::pair<long double, long double>(-10.,10.));
    params.randomize_parameters();
    Neighbour nf;
    TemperatureFunction tf;
    Levi ef;
    int first_version = 0;
    auto sol1 = simulated_annealing(params, 10000, ef, tf, nf, first_version );
    std::cout << "Solution version 0 "<<  sol1<< " val: " << ef(sol1) << std::endl;
    int second_version = 1;
    auto sol2 = simulated_annealing(params, 10000, ef, tf, nf, second_version );
    std::cout << "Solution version 1"<<  sol2 << " val: " << ef(sol2) << std::endl;
    Neighbour nf2= Neighbour(0);
    auto sol3 = simulated_annealing(params, 10000, ef, tf, nf2, second_version );
    std::cout << "Solution version 2"<<  sol3 <<  " val: " << ef(sol3) << std::endl;

    std::cout <<" Optimal solution : f(1,1,...1) = 0  " << std::endl;
    std::cout << "-----END of test_simulated_annealing_levi----- " << std::endl;
    return true;
    //version 0 is better
}

bool test_simulated_annealing_ackley()
{
    std::cout <<"-------test_simulated_annealing_ackley-----------" << std::endl;
    Parameters params;
    params.add_parameter("x1",(long double) 0.0);
    params.add_boundaries("x1", std::pair<long double, long double>(-5.,5.));
    params.add_parameter("x2",(long double) 0.0);
    params.add_boundaries("x2", std::pair<long double, long double>(-5.,5.));
    params.randomize_parameters();
    Neighbour nf;
    TemperatureFunction tf;
    Ackley ef;
    int first_version = 0;
    auto sol1 = simulated_annealing(params, 10000, ef, tf, nf, first_version );
    std::cout << "Solution version 0 "<<  sol1<< " val: " << ef(sol1) << std::endl;
    int second_version = 1;
    auto sol2 = simulated_annealing(params, 10000, ef, tf, nf, second_version );
    std::cout << "Solution version 1"<<  sol2 << " val: " << ef(sol2) << std::endl;
    Neighbour nf2 = Neighbour(0);
    auto sol3 = simulated_annealing(params, 10000, ef, tf, nf2, second_version );
    std::cout << "Solution version 2"<<  sol3 <<  " val: " << ef(sol3) << std::endl;

    std::cout <<" Optimal solution : f(0,0) = 0  " << std::endl;
    std::cout << "-----END of test_simulated_annealing_ackley----- " << std::endl;
    return true;
    //version 0 is better
}
int main()
{
    bool ok = test_parameters();
    if(! ok )
        std::cout << "test_parameters failed "<< std::endl;
    bool ok2 = test_simulated_annealing_squared();
    if( !ok2 )
        std::cout <<" test_simulated_annealing_squared failed! " << std::endl;
    bool ok3 = test_randomized_parameters();
    if ( !ok3 )
        std::cout << "test_randomized_parameters failed! " << std::endl;
    bool ok4 = test_simulated_annealing_rosen();
    if( !ok4 )
        std::cout << "test_simulated_annealing_rosen failed !" << std::endl;
    bool ok5 = test_simulated_annealing_levi();
    if( !ok5 )
        std::cout << "test_simulated_annealing_levi failed !" << std::endl;
    bool ok6 = test_simulated_annealing_ackley();
    if( !ok6 )
        std::cout << "test_simulated_annealing_ackley failed !" << std::endl;


    return ok && ok2 && ok3 && ok4 && ok5 && ok6? 0 : 1;
}

//Neighbour function version 1 and simulated annealing version 0 seems best
// => NF v1 = range decreases with temperature function
// => sim_an v0 =
