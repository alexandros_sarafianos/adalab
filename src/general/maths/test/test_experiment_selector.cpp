#include "src/optimization/experimentselector.hpp"
#include "data.hpp"
#include "src/Eve/evesimulator.hpp"


int main(int argc, char * argv[])
{
//    Sigmoid sigmoid(15,0.5);
//    ThresholdFunction * thresh;
//    thresh = &sigmoid;
    RandomWrapper random_number_generator;
    EveSimulator<double> eve(DATA_FILE_MY_NETWORK, random_number_generator/*, thresh*/);


    std::ofstream os;
    std::shared_ptr<AbstractInformativeness<> > inf = std::make_shared<AbstractInformativeness<> >();
    std::shared_ptr<ExperimentalSpace<> > exp_space = std::make_shared<ExperimentalSpace<> >((Exp_Type) continuous, os);
    ExperimentSelector<AbstractInformativeness<>, ExperimentalSpace<> > selector(inf,exp_space);
    ExperimentSampler s;
    selector.test(100, 10000, s);
    std::cout << "5" <<NEW_LINE;

    return 0;
}

