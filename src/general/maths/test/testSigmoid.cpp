#include <iostream>
#include "src/general/maths/sigmoid.hpp"
#include "src/general/compile_configuration.hpp"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
bool test_complement()
{
    Sigmoid s1(5,0.,1.,2.);
    bool ok1 = (approximately_equal( s1(0.5) + s1.complement(0.5),0. ) && approximately_equal( s1(-0.3) + s1.complement(-0.3),0. ));
    if (!ok1 )
        std::cout << "Error 1 " << NEW_LINE;
    s1 = Sigmoid(5,0.5 ,1.,2.);
    bool ok2 = (approximately_equal( s1(0.5) + s1.complement(0.5),0. ) && approximately_equal( s1(-0.3) + s1.complement(-0.3),0. ));
    if (!ok2 )
        std::cout << "Error 1 " << NEW_LINE;
    return ok1 && ok2;
}

int main(int argc, char * argv[])
{

    bool value = (test_complement());
    if(!value)
    {
       std::cout << "SigmoidTest failed " << NEW_LINE;
       return  -1;
    }

    return 0;
}
