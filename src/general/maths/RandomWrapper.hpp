#ifndef RANDOMWRAPPER_HPP
#define RANDOMWRAPPER_HPP

#include <random>
#include "src/general/compile_configuration.hpp"

enum DistrType
{
    uniform,
    normal,
    nothing
};

//template<typename Distribution>
struct RandomWrapper
{
public:
    RandomWrapper()
    {
        dist_type = nothing;
    } //default constructor
    RandomWrapper(const std::string m_distr, const double value)
    {
        if( m_distr == "uniform")
        {
            dist_type = uniform;
            uni_d = std::uniform_real_distribution<double>(-1*value,value);
        }
        else if(m_distr=="normal")
        {
            dist_type = normal;
            norm_d  = std::normal_distribution<double>(0.0,value);
        }
        else
        {
            std::stringstream os; os << "Strange parameters found for Randomwrapper :" << m_distr
            << " and " << value << "!" ;LoggerSingleton::Instance()->info_print(os);
        }
    }

    double operator()()
    {
        switch(dist_type)
        {
            case uniform:
                return uni_d(generator);
                break;
            case normal:
                return norm_d(generator);
                break;
            case nothing:
                return 0.0;
                break;
            default:
                return 0.0;
        }
    }
private:
      std::default_random_engine generator;
      DistrType dist_type;
      std::normal_distribution<double> norm_d;
      std::uniform_real_distribution<double> uni_d;
};
#endif // RANDOMWRAPPER_HPP
