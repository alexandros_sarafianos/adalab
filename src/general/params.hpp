#ifndef PARAMS_HPP
#define PARAMS_HPP
#include <boost/program_options.hpp>
#include "src/general/compile_configuration.hpp"

using namespace boost::program_options;

class Params
{
public:
    Params(variables_map vm);
};

#endif // PARAMS_HPP
