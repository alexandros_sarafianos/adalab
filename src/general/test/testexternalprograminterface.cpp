#include "src/general/externalprograminterface.hpp"
#include <iostream>
#include "src/general/compile_configuration.hpp"

int test_read_from_pipe()
{
    ExternalProgramInterface e;
    bool ok = e.read_from_pipe("ls", "r");
    int code;
    (ok == true) ? code = 0 : code= 1;
    return code;
}
int test_read_from_pipe2()
{
    ExternalProgramInterface e;
    std::stringstream os; os << "Step 1 ";LoggerSingleton::Instance()->info_print(os);
    bool ok = e.open_pipe("cd /home/alexandros/Development/adalab/general_scripts/python/diauxic_shift && /home/alexandros/Development/ACE-ilProlog-1.2.20/linux/bin/ace", "w");
    std::stringstream os1; os1 << "Step 1 ";LoggerSingleton::Instance()->info_print(os1);
    e.write_to_pipe("tilde");//induce(tilde,30)).
    std::stringstream os2; os2 << "Step 2 ";LoggerSingleton::Instance()->info_print(os2);
    e.write_to_pipe("quit");
    std::stringstream os3; os3 << "Step 3 ";LoggerSingleton::Instance()->info_print(os3);
    int code;
    (ok == true) ? code = 0 : code= 1;
    return code;
}

int main(int, char*[])
{
    int value = test_read_from_pipe();
    if(value == 0)
    {
        std::stringstream os; os << "Test readfrompipe succeeded!" ;LoggerSingleton::Instance()->info_print(os);
    }
    else
    {
        std::stringstream os; os << "Test readfrompipe failed!" ;LoggerSingleton::Instance()->info_print(os);
    }
    //test_read_from_pipe2();
    return 0;
}


