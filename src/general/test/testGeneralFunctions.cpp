#include <iostream>
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"

int test_get_index()
{
    std::vector<int> v = {9,10,11,12};
    size_t index = get_index(v, 9);
    bool ok1 = index == 0;
    if( !ok1 )
        std::cout << "Expected index 0, but found " << index <<"!"<<std::endl;
    size_t index1 = get_index(v, 12);
    bool ok2 = index1 == 3;
    if( !ok2 )
        std::cout << "Expected index 3, but found " << index1 <<"!"<<std::endl;
    size_t index2 = get_index(v, 13);
    bool ok3 = index2 == 4;
    if( !ok3 )
        std::cout << "Expected index 4(out of bounds), but found " << index2 <<"!"<<std::endl;
    return ok1 && ok2 && ok3;

}

bool test_generate_prolog_term()
{
    std::string foo = "foo";
    std::string param = "bar";
    bool ok =  make_prolog_code( foo, param) == "foo(bar).";
    if ( !ok )
        std::cout << "Test generate_prolog_term error, expected foo(bar)., but found " << make_prolog_code(foo, param) << "\n";
    std::vector<std::string> bars = {"a", "b"};
    bool ok2 = make_prolog_code(foo, bars ) == "foo([a,b]).";
    if ( !ok2 )
        std::cout <<"Test_generate_prolog_term error, expected foo([a,b]). but found "<< make_prolog_code(foo, bars) << "\n";
    return ok && ok2;
}

bool test_range()
{
    std::vector<int> v1 {0,1,2,3,4,5,6,7,8};
    bool ok1 = python_range(0,9) == v1;
    std::vector<int> v2 {0,3,6};
    bool ok2 = python_range(0,9,3) == v2;

    return ok1 && ok2;
}

int main(int, char*[])
{
    bool value = test_get_index();
    bool value2 = test_generate_prolog_term();
    bool val3 = test_range();

    if(value )
        std::cout << "Test get_index succeeded" <<NEW_LINE;
    if( value2 )
        std::cout << "Test_generate_prolog_term succeeded" << NEW_LINE;
    if(val3)
        std::cout << "Test_range succeeded " << NEW_LINE;
    //test_read_from_pipe2();
    if(value && value2 && val3)
        return 0;
    else
        return 1;
}



