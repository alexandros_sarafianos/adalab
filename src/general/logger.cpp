#include "src/general/logger.hpp"
#include "src/general/compile_configuration.hpp"
#include <boost/filesystem.hpp>
#define STDOUT
#ifdef STDOUT
#define OS std::cout
#else
#define OS os
#endif
void Logger::open_log_file(const std::string & directory, const std::string & executable_name)
{
#ifdef LOGGING
#ifndef STDOUT
    boost::filesystem::path p(executable_name);
    boost::filesystem::path dir = p.parent_path();
    boost::filesystem::path filename = p.filename();
//    if(boost::filesystem::create_directories("logs"))
//    {
//            std::cerr<< "Directory Created: logs "<<NEW_LINE;
//    }
    std::string log_file = directory+"/"+std::string(filename.c_str())+"_"+currentDateTime()+".log";
//    std::string log_file = "logs/"+std::string(filename.c_str())+"_"+currentDateTime()+".log";
    std::cout << "Log file " << log_file << " created " << NEW_LINE;
    open_output_file_safely(log_file, os);
#endif
#endif
}
void Logger::open_log_file(const std::string & executable_name)
 {
 #ifdef LOGGING
 #ifndef STDOUT
     boost::filesystem::path p(executable_name);
     boost::filesystem::path dir = p.parent_path();
     boost::filesystem::path filename = p.filename();
     if(boost::filesystem::create_directory("logs"))
     {
        std::cerr<< "Directory Created: logs "<<NEW_LINE;
     }
     std::string date_time = currentDateTime();
     std::replace( date_time.begin(), date_time.end(), ':', '-');
     std::string log_file = "logs/"+std::string(filename.c_str())+"_"+date_time+".log";
     std::cout << "Log file " << log_file << " created " << NEW_LINE;
     open_output_file_safely(log_file, os);
 #endif
 #endif
}
void Logger::printer(const std::stringstream & s_str )
{
#ifdef LOGGING
    if(!silent)
    {
        OS << s_str.str();
    }
#endif
}
void Logger::info_print(const std::stringstream & s_str )
{
#ifdef LOGGING
    if(!silent)
    {
        OS << "<info>"; printer(s_str); OS<< "</info>" << "\n";
    }
#endif
}
void Logger::info_warning( const std::stringstream & s_str )
{
    #ifdef LOGGING
    if(!silent)
    {
        OS << "<info_warning>"; printer(s_str); OS<< "</info_warning>" << "\n";
    }
    #endif
}
void Logger::info_debug( const std::stringstream & s_str )
{
    #ifdef LOGGING
    if(!silent)
    {
        OS << "<info_debug>"; printer(s_str); OS<< "</info_debug>" << "\n";
    }
    #endif
}
void Logger::info_error( const std::stringstream & s_str )
{
    #ifdef LOGGING
    if(!silent)
    {
        OS << "<info_error>"; printer(s_str); OS<< "</info_error>" << "\n";
    }
    #endif
}
void Logger::info_low( const std::stringstream & s_str )
{
    #ifdef LOGGING
    if(!silent)
    {
        OS << "<info_low>"; printer(s_str); OS<< "</info_low>" << "\n";
    }
    #endif
}
void Logger::info_high( const std::stringstream & s_str )
{
    #ifdef LOGGING
    if(!silent)
    {
        OS << "<info_high>"; printer(s_str); OS<< "</info_high>" << "\n";
    }
    #endif
}
void Logger::info_value_update(const std::stringstream & s_str )
{
    #ifdef LOGGING
    if(!silent)
    {
        OS << "<info_value_update>"; printer(s_str); OS<< "</info_value_update>" << "\n";
    }
#endif
}
void Logger::info_custom(const std::stringstream & s_str, const std::string & custom_string )
{
    #ifdef LOGGING
    if(!silent)
    {
        OS << "<" << custom_string << ">"; printer(s_str); OS<< "</"<< custom_string <<">" << "\n";
    }
    #endif
}
