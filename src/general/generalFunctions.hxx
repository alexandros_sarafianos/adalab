#ifndef GENERALFUNCTIONS_HXX
#define GENERALFUNCTIONS_HXX

#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <iostream>
#include <set>
#include <boost/unordered_set.hpp>
#include <sstream>
#include <map>
#include <boost/unordered_map.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <cmath>
#include <limits>
#include <sstream>
#include "src/general/compile_configuration.hpp"
#include <unordered_map>

//template <typename T>
//std::string to_string(T value)
//{
// //create an output string stream
// std::ostringstream os ;

// //throw the value into the string stream
// os << value ;

// //convert the string stream into a string and return
// return os.str() ;
//}



template< class T >
inline bool my_equal( const T & v1, const T & v2 )
{
   return v1 == v2;
}

template<class T, class V>
inline bool my_equal(const typename std::pair<T,V> p1, const typename std::pair<T,V> p2)
{
    return p1.first == p2.first && p1.second == p2.second;
}

template<typename _Type>
_Type convertStringToType(const std::string & my_string)
{
  _Type value = 0.;
  if( !my_string.empty() )
  {
    try{value = boost::lexical_cast<_Type>(my_string);}
    catch( boost::bad_lexical_cast const& ){std::cout << "Lexical cast exception: input string "<< my_string <<" could not be converted !" << std::endl;}
  }
  return value;
}


//template < class _Type>
//bool approximately_equal(_Type a, _Type b, int factor = 2)
//{

//    return std::fabs(a - b) <= factor*std::numeric_limits<_Type>::epsilon();
//}
template < class _Type>
bool approximately_equal(_Type a, _Type b, int factor = 2)
{
    bool almost_equal;
    if( a == 0 || b== 0 )
    {
        almost_equal = std::fabs(a-b) <= factor*std::numeric_limits<_Type>::epsilon();
    }
    else
    {
        almost_equal = std::fabs(a-b) <= std::numeric_limits<_Type>::epsilon()*std::fabs(a);
        almost_equal = almost_equal &&  std::fabs(a-b) <= std::numeric_limits<_Type>::epsilon()*std::fabs(b);
    }
    return almost_equal;
}
/**/


template< class T,class V,class W>
inline bool in_range(T lower, V upper, W value)
{
   return ( value >= lower && value <= upper);
}

template< class _ValueType >
inline _ValueType convert_value_to_new_range(_ValueType initial_lower_range,
                                             _ValueType initial_upper_range,
                                             _ValueType value,
                                             _ValueType new_lower_range,
                                             _ValueType new_upper_range)
{
    _ValueType old_range = initial_upper_range - initial_lower_range;
    _ValueType new_range = (new_upper_range - new_lower_range);
    _ValueType new_value = (((value - initial_lower_range) * new_range) / old_range) + new_lower_range;
    return new_value;
}

template< typename T>
inline T fuzzy_not(const T val)
{
    return 1-val;
}



template<typename OutputType, typename InputType>
OutputType my_conversion( InputType value)
{
    return value;
}


template< class T>
std::string make_string_from_parameters(const T  params)
{
    return std::string(params);
}

template < class T >
std::string make_string_from_parameters(const std::vector<T>  params)
{
    std::string partial_string = "[";
    auto iterator = params.begin();
    partial_string += make_string_from_parameters(*iterator);
    ++iterator;
    for( ; iterator != params.end(); ++iterator )
    {
        partial_string += ","+ make_string_from_parameters(*iterator);
    }
    return partial_string + "]";
}


template< class T >
std::string make_prolog_code( const std::string & functor, const T  params)
{
    std::string prolog_term = functor + "(";
    std::string parameter_string = make_string_from_parameters(params);
    return prolog_term + parameter_string + ").";
}

//=======
//    for( int row = 0; row < m.size1(); ++row)
//    {
//        if( m.size2() != 0)
//            os << m(row, 0);
//        for( int column = 1; column < m.size2(); ++column)
//        {
//           os << "," << m(row,column);
//        }
//        os << std::endl;
//    }
//    return os;
//}

//template <typename ItemType>
//inline void print_matrix_with_names(std::ostream& os, const matrix<ItemType> & m, const std::vector<std::string> & names)
//{
//    assert(m.size1() == m.size2() && m.size1() == names.size());
//    os << "\t";
//    for( auto it = names.begin(); it != names.end(); ++it)
//    {
//        os << *it << "\t";
//    }
//    os << std::endl;
//    for( int row = 0; row < m.size1(); ++row)
//    {
//        os << names[row];
//        for( int column = 1; column < m.size2(); ++column)
//        {
//           os << "\t" << m(row,column);
//        }
//        os << std::endl;
//    }
//    std::cout << os << std::endl;
//}

#endif
