
#include "src/general/generalFunctions.hpp"
#include <cstdlib>
#include <fstream>      // std::filebuf
#include <stdio.h>
#include <time.h>
#include "src/general/compile_configuration.hpp"

void open_file_safely(const std::string filename, std::ifstream &opened_file)
{
    opened_file.open(filename.c_str());
    if ( !opened_file )
    {
        std::stringstream os; os << "Error loading file " << filename ;LoggerSingleton::Instance()->info_print(os);
    }
    else
    {
        std::stringstream os; os<< "File " << filename << " succesfully loaded!" ;LoggerSingleton::Instance()->info_print(os);
    }
}

void open_output_file_safely(const std::string filename, std::ofstream &opened_file )
{

    opened_file.open(filename.c_str(), std::ofstream::out | std::ofstream::app);
    if ( !opened_file )
    {
        std::stringstream os; os << "Error opening file " << filename ;LoggerSingleton::Instance()->info_print(os);
    }
    else
    {
        std::stringstream os; os<< "File " << filename << " succesfully opening!" ;LoggerSingleton::Instance()->info_print(os);
    }
}

void parse_network_line(const std::string line, std::vector< std::pair<std::string, bool> > & causes, bool & stimulation, std::string & effect , boost::unordered_set<std::string> &node_set)
{
    if( line.length() > 1 && line[0] == '#' )
    {
        return;
    }
    std::string temp_string;
    bool invert=false;
    boost::unordered_set<std::string>::iterator set_it = node_set.begin();
    for(std::string::const_iterator it = line.begin(); it != line.end(); ++it)
    {
        switch(*it)
        {
            case '&':
                causes.push_back(std::make_pair (temp_string,invert));
                set_it = node_set.insert(set_it, temp_string);
                invert = false;
                temp_string = "";
                break;

            case ' ':
                break;

            case '-':
                causes.push_back(std::make_pair (temp_string,invert));
                set_it = node_set.insert(set_it, temp_string);
                invert = false;
                temp_string = "-";
                break;

            case '>':
                if( temp_string == "-")
                    stimulation=true;
                else
                {
                    std::stringstream os; os << "Found unexpected character " << *(it-1) << " in line "<< line <<"("<<temp_string <<")!" ;LoggerSingleton::Instance()->info_print(os);
                    exit(0);
                }
                temp_string = "";
                break;

            case '|':
                if( temp_string == "-")
                    stimulation=false;
                else
                {
                    std::stringstream os; os << "Found unexpected character " << *(it-1) << " in line "<< line <<"("<<temp_string <<")!" ;LoggerSingleton::Instance()->info_print(os);
                    exit(0);
                }
                temp_string = "";
                break;

            case '^':
                invert = true;
                break;

            default:
                temp_string+=*it;
        }
    }
    effect = temp_string;
    node_set.insert(set_it, temp_string);
}

std::string removeInvalidCharactersFromStringAndWhiteSpace(const std::string & str )
{
    std::string strippedStr = "";
    for(std::string::const_iterator it = str.begin(); it != str.end(); ++it)
    {
     //   std::stringstream os; os << *it ;LoggerSingleton::Instance()->info_print(os);
        if( *it > 32 )
        {
            strippedStr += *it;
        }
    }
    return strippedStr;
}


bool string_contains_substring(const std::string & str, const std::string & substring)
{
    if (str.find(substring) != std::string::npos) {
        return true;
    }
    return false;
}
const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d_%X", &tstruct);

    return buf;
}
void write_parameters(const std::string &path ,const std::string &network_file,const std::string &xml_nodes, const std::string &noise_distribution,
                      int thresh_func,int colony_size, int duration, double threshold_func_param1,double threshold_func_param2, double noise_param    )
{
    std::ofstream os;
    open_output_file_safely(path, os);
    os << "network_file="<<network_file<<"\n";
    os << "xml_nodes="<<xml_nodes<<"\n";
    os << "noise_distribution="<<noise_distribution<<"\n";
    os << "thresh_func="<<thresh_func<<"\n";
    os << "colony_size="<<colony_size<<"\n";
    os << "duration="<<duration<<"\n";
    os << "threshold_func_param1="<<threshold_func_param1<<"\n";
    os << "threshold_func_param2="<<threshold_func_param2<<"\n";
    os << "noise_param="<<noise_param<<"\n";
    os.close();
}

std::string get_home_directory()
{
    char const* tmp = getenv( "HOME" );
    if ( tmp == NULL ) {
        std::cout << "User home directory not found! " << NEW_LINE;
        exit(0);
    }
    std::string home_path( tmp );
    return home_path;
}

std::unordered_map<std::string, double> read_initial_values(const std::string initial_values_file)
{
    std::unordered_map<std::string, double> initial_value_map;
    if( initial_values_file == "")
        return initial_value_map;
    std::ifstream opened_file;
    open_file_safely(initial_values_file, opened_file);
    std::string line;
    while(getline(opened_file , line))
    {
        std::string key= "";
        std::string value = "";
        bool val_started = false;
        for(std::string::const_iterator ch = line.begin(); ch != line.end(); ch++)
        {
            if( *ch == ':')
                val_started = true;
            else
            {
                if( val_started)
                    value+=*ch;
                else
                    key+=*ch;
            }
        }
         //insert in map
        initial_value_map[key] = convertStringToType<double>(value);
    }
    return initial_value_map;
}

template<>
bool my_conversion( double value)
{
    if (value > 0.5)
        return true;
    else
        return false;
}

bool test_all_trues(const std::vector<bool> & tests, size_t &positives, size_t & negatives)
{
    std::cout << tests << NEW_LINE;
    positives = 0;
    negatives = 0;
    for(std::vector<bool>::const_iterator it = tests.begin(); it != tests.end(); ++it)
    {
        if( *it == true )
        {
            ++positives;
        }
        else
        {
            ++negatives;
        }
    }
    return (positives == tests.size());
}
//SEXP hello_world()
//{
//    std::cout << "Hello world!" << NEW_LINE;
//    return R_NilValue;
//}

template<>
bool equal_matrices(const typename boost::numeric::ublas::matrix<double> & mat1, const typename boost::numeric::ublas::matrix<double> & mat2)
{
    if( mat1.size1() != mat2.size1())
    {
        std::cout << "Sizes of 1st dimension don't match! " << NEW_LINE;
        return false;
    }
    if( mat1.size2() != mat2.size2())
    {
        std::cout <<"Sizes of 2st dimension don't match!"<< NEW_LINE;
        return false;
    }
    for( int row = 0; row < mat1.size1(); ++row)
    {
        for( int column = 0; column < mat1.size2(); ++column)
        {

           if( !approximately_equal(mat1(row,column),mat2(row,column)))
           {
               return false;
           }
        }
    }
    return true;
}

template <>
bool approximately_equal(bool a, bool b, int factor)
{
    return a==b;
}
std::string get_string_from_parameter_type(const ParameterType p)
{
    std::string s ;
    switch(p)
    {
        case Boolean:
            s = "boolean";
            break;
        case Continuous:
            s = "continuous";
            break;
        default:
            std::cout << "Should never get here!" << NEW_LINE;
            assert(false);
    }
    return s;
}

void remove_contents_of_file(const std::string & file_name)
{
    std::ofstream f( file_name.c_str()  );
    f.close( );
}

std::vector<int> python_range(const int begin_value,const int one_past_end_value,const int step )
{
    std::vector<int> range_vector;
    for(int i = begin_value; i < one_past_end_value; i += step)
    {
        range_vector.push_back(i);
    }
    return range_vector;
}




