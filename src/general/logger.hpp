#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <string>
#include <fstream>
#include <iostream>
#include "src/general/singleton.hxx"
#include "src/general/generalFunctions.hpp"
#include "src/general/generalcontainerfunctions.hxx"
#include <sstream>
//#include "src/general/logger.hpp"
//std::stringstream os; os << a;
//;LoggerSingleton::Instance()->info_print(os);

#define INFO(text) std::stringstream os; os << text; LoggerSingleton::Instance()->info_print(os);
#define INFO_WARNING(text) std::stringstream os1; os1 << text; LoggerSingleton::Instance()->info_warning(os1);
#define INFO_DEBUG(text ) std::stringstream os2; os2 << text; LoggerSingleton::Instance()->info_debug(os2);
#define INFO_ERROR(text) std::stringstream os3; os3 << text; LoggerSingleton::Instance()->info_error(os3);
#define INFO_LOW(text) std::stringstream os4; os4 << text; LoggerSingleton::Instance()->info_low(os4);
#define INFO_HIGH(text) std::stringstream os5; os5 << text; LoggerSingleton::Instance()->info_high(os5);
#define INFO_VALUE_UPDATE(text ) std::stringstream os6; os6 << text; LoggerSingleton::Instance()->info_value_update(os6);
#define INFO_CUSTOM(text, text2 ) std::stringstream os7; os7 << text; LoggerSingleton::Instance()->info_custom(os7,text2);

class Logger
{
public:
   Logger(){}
   ~Logger() {os.close();}
   void open_log_file(const std::string&, const std::string &);
   void open_log_file(const std::string&);
   void info_print(const std::stringstream & s_str );
   void info_warning(const std::stringstream & s_str);
   void info_debug(const std::stringstream & s_str );
   void info_error(const std::stringstream & s_str);
   void info_low(const std::stringstream & s_str);
   void info_high(const std::stringstream & s_str);
   void info_value_update(const std::stringstream & s_str );
   void info_custom(const std::stringstream & s_str, const std::string & custom_string );
   void set_silent(const bool m_silent){silent=m_silent;}

private:
   void printer(const std::stringstream & s_str );
   std::ofstream os ;
   bool silent=false;
};
typedef Singleton<Logger> LoggerSingleton;

#endif // LOGGER_HPP
