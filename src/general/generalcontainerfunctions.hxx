#ifndef GENERALCONTAINERFUNCTIONS_HXX
#define GENERALCONTAINERFUNCTIONS_HXX

#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <iostream>
#include <set>
#include <sstream>
#include <map>
#include <cmath>
#include <limits>
#include <sstream>
#include <unordered_map>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/unordered_map.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/unordered_set.hpp>


#include "src/general/generalFunctions.hpp"
#include "src/general/compile_configuration.hpp"

template < class T >
inline std::ostream& operator << (std::ostream& os, const std::vector<T>& v)
{
    os << "[";
    for (typename std::vector<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        if( ii != v.end()-1)
            os << "\"" << *ii << "\"" << "," ;
        else
            os << "\"" << *ii << "\"";
    }
    os << "]";
    return os;
}


template < class T, class U >
inline std::ostream& operator << (std::ostream& os, const std::pair<T,U>& v)
{
    os << "<"<< "\"" << v.first << "\"" << ","<< "\"" << v.second << "\">" ;
    return os;
}


template < class T >
inline std::ostream& operator << (std::ostream& os, const std::set<T>& v)
{
    os << "{ ";
    for (typename std::set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\"" << *ii << "\"" << " " ;
    }
    os << "}";
    return os;
}


template < class T >
inline std::ostream& operator << (std::ostream& os, const boost::unordered_set<T>& v)
{
    os << "{ ";
    for (typename boost::unordered_set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\"" << *ii << "\"" << " " ;
    }
    os << "}";
    return os;
}

template < class T, class V >
inline std::ostream& operator << (std::ostream& os, const std::map<T,V>& v)
{
    os << "{ ";
    for (typename std::map<T,V>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        if( ii != v.begin() )
            os << " , ";
        os << "\"" << ii->first << "\"" << "" ;
        os << ":\"" << ii->second << "\"" ;
    }
    os << "}";
    return os;
}
template < class T, class V >
inline std::ostream& operator << (std::ostream& os, const std::unordered_map<T,V>& v)
{
    os << "{ ";
    for (typename std::unordered_map<T,V>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\n\"" << ii->first << "\"" << " " ;
        os << ":\"" << ii->second << "\"  " ;
    }

    os << "}";
    return os;
}
template < class T, class V, class W >
inline std::ostream& operator << (std::ostream& os, const std::unordered_map<T,V,W>& v)
{
    os << "{ ";
    for (typename std::unordered_map<T,V,W>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\"" << ii->first << "\"" << " : " << "\"" << ii->second << "\"" << " " ;
    }
    os << "}";
    return os;
}

template< class Container >
bool contains_duplicate_elements(Container c)
{
    for(typename Container::const_iterator it = c.begin(); it != c.end()-1; ++it)
    {
        for(typename Container::const_iterator it_internal = c.begin()+1; it_internal != c.end(); ++it_internal)
        {
            if( *it == *it_internal )
                return true;
        }
    }
    return false;
}

template < class T >
inline std::stringstream& operator << (std::stringstream& os, const std::vector<T>& v)
{
    os << "[";
    for (typename std::vector<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        if( ii != v.end()-1)
            os << "\"" << *ii << "\"" << "," ;
        else
            os << "\"" << *ii << "\"";
    }
    os << "]";
    return os;
}


template < class T, class U >
inline std::stringstream& operator << (std::stringstream& os, const std::pair<T,U>& v)
{
    os << "<"<< "\"" << v.first << "\"" << ","<< "\"" << v.second << "\">" ;
    return os;
}

template < class T >
inline std::stringstream& operator << (std::stringstream& os, const std::set<T>& v)
{
    os << "{ ";
    for (typename std::set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\"" << *ii << "\"" << " " ;
    }
    os << "}";
    return os;
}


template < class T >
inline std::stringstream& operator << (std::stringstream& os, const boost::unordered_set<T>& v)
{
    os << "{ ";
    for (typename boost::unordered_set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\"" << *ii << "\"" << " " ;
    }
    os << "}";
    return os;
}

template < class T, class V >
inline std::stringstream& operator << (std::stringstream& os, const std::map<T,V>& v)
{
    os << "{ ";
    for (typename std::map<T,V>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\n\"" << ii->first << "\"" << " " ;
        os << ":\"" << ii->second << "\"  " ;
    }
    os << "}";
    return os;
}

template < class T, class V >
inline std::stringstream& operator << (std::stringstream& os, const std::unordered_map<T,V>& v)
{
    os << "{ ";
    for (typename std::unordered_map<T,V>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\n\"" << ii->first << "\"" << " " ;
        os << ":\"" << ii->second << "\"  " ;
    }
    os << "}";
    return os;
}

template < class T, class V, class W >
inline std::stringstream& operator << (std::stringstream& os, const std::unordered_map<T,V,W>& v)
{
    os << "{ ";
    for (typename std::unordered_map<T,V,W>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << "\"" << ii->first << "\"" << " : " << "\"" << ii->second << "\"" << " " ;
    }
    os << "}";
    return os;
}

template< class Key, class Value >
bool equal_maps(const std::map<Key,Value> & map1, const std::map<Key,Value> & map2)
{
    if( map1.size() != map2.size() )
        return false;
    for(auto it_map1 = map1.begin(); it_map1 != map1.end(); ++it_map1)
    {
        auto it_map2 = map2.find(it_map1->first);
        if( it_map2 == map2.end() )
        {
            std::cout << "not found: key " << it_map1->first << " in " << map2 << std::endl;
            return false;
        }
        else if(!my_equal(it_map1->second, it_map2->second))
        {
            std::cout << "not found: val " << it_map1->second << " in " << map2 << std::endl;
             return false;
        }

    }
    return true;
}

template< class Key, class Value >
bool equal_maps(const std::unordered_map<Key,Value> & map1, const std::unordered_map<Key,Value> & map2)
{
    if( map1.size() != map2.size() )
        return false;
    for(auto it_map1 = map1.begin(); it_map1 != map1.end(); ++it_map1)
    {
        auto it_map2 = map2.find(it_map1->first);
        if( it_map2 == map2.end() )
        {
            std::cout << "not found: key " << it_map1->first << " in " << map2 << std::endl;
            return false;
        }
        else if(!my_equal(it_map1->second, it_map2->second))
        {
            std::cout << "not found: val " << it_map1->second << " in " << map2 << std::endl;
             return false;
        }

    }
    return true;
}
template< typename _Container >
_Container ones(int size)
{
    _Container c;
    for ( int i_th_element= 0; i_th_element < size;i_th_element++)
        c.push_back(1);
    assert(c.size() == size);
    return c;
}



template<template <typename, typename> class Container_1D, typename ContainerItemType, typename T>
size_t get_index(const Container_1D<ContainerItemType, T> & container, const ContainerItemType & item )
{
    size_t index = 0;
    for( auto container_iterator = container.begin(); container_iterator != container.end(); ++container_iterator)
    {
        if( *container_iterator == item)
            return index;
        index++;
    }
    return index;
}



template< typename T1, typename T2>
typename std::unordered_map<T1, T2> map_from_key_value_vectors(const std::vector<T1> & keys, const std::vector<T2> & values)
{
    assert(keys.size()==values.size());
    std::unordered_map<T1,T2> output_map;
    for(int index = 0; index < keys.size() ; ++index)
    {
        output_map.insert(std::pair<T1,T2>(keys[index], values[index]));
    }
    return output_map;
}

template< class T>
typename std::vector<T> get(const std::vector<T> & items, const std::vector<int> &  indices)
{
    typename std::vector<T> result_vector;
    for( auto index_iterator = indices.begin(); index_iterator != indices.end(); ++index_iterator )
    {
        std::cout << *index_iterator << items.size() << std::endl;
        assert( *index_iterator >= 0 && *index_iterator <items.size() );
        result_vector.push_back(items.at(*index_iterator));
    }
    return result_vector;
}

template< class T >
bool all_different(const typename std::vector<T> & v)
{
    typename std::set<T> s(v.begin(), v.end());
    return s.size() == v.size();
}

template< class T >
bool equal_containers(const std::vector<T> & v1, const std::vector<T> & v2 )
{
    if( v1.size() != v2.size() )
    {
        return false;
    }
    auto v1_iterator = v1.begin();
    for( auto v2_iterator = v2.begin(); v2_iterator != v2.end(); ++v2_iterator)
    {
        if( !my_equal(*v1_iterator, *v2_iterator) )
            return false;
        ++v1_iterator;
    }
    return true;
}




using namespace boost::numeric::ublas;
template<typename NumberType>
void emptyInitialize(matrix<NumberType> & m)
{
    for( int i = 0; i < m.size1(); ++i)
    {
        for( int j = 0; j < m.size2(); ++j)
        {
            m(i,j) = 0;
        }
    }
}

template <typename ItemType>
inline std::ostream& operator <<(std::ostream& os, const matrix<ItemType> & m)
{

    for( int row = 0; row < m.size1(); ++row)
    {
        if( m.size2() != 0)
            os << m(row, 0);
        for( int column = 1; column < m.size2(); ++column)
        {
           os << "," << m(row,column);
        }
        os << std::endl;
    }
    return os;
}
template <typename ItemType>
inline std::ostream& print_matrix_with_names(std::ostream& os, const matrix<ItemType> & m, const std::vector<std::string> & names)
{
    assert(m.size1() == m.size2() && m.size1() == names.size());
    os << "\t";
    for( auto it = names.begin(); it != names.end(); ++it)
    {
        os << *it << "\t";
    }
    os << std::endl;
    for( int row = 0; row < m.size1(); ++row)
    {
        os << names[row];
        for( int column = 1; column < m.size2(); ++column)
        {
           os << "\t" << m(row,column);
        }
        os << std::endl;
    }
    return os;
}


template <typename T>
void my_copy( const typename std::vector<T> & v, boost::numeric::ublas::matrix<T> & mat)
{
    assert(mat.size1() * mat.size2() == v.size() );
    auto vector_iterator = v.begin();
    for( int row = 0; row < mat.size1(); ++row)
    {
        for( int column = 0; column < mat.size2(); ++column)
        {
           mat(row,column) = *vector_iterator;
           ++vector_iterator;
        }
    }
}

template< typename T>
bool equal_matrices(const typename boost::numeric::ublas::matrix<T> & mat1, const typename boost::numeric::ublas::matrix<T> & mat2)
{
    if( mat1.size1() != mat2.size1())
    {
        std::cout <<"Sizes of 1st dimension don't match! "<< std::endl;
        return false;
    }
    if( mat1.size2() != mat2.size2())
    {
        std::cout <<"Sizes of 2st dimension don't match! "<< std::endl;
        return false;
    }
    for( int row = 0; row < mat1.size1(); ++row)
    {
        for( int column = 0; column < mat1.size2(); ++column)
        {
           if( !my_equal(mat1(row,column), mat2(row,column)))
           {
               return false;
           }
        }
    }
    return true;
}

#endif
