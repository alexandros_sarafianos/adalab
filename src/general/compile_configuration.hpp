#ifndef COMPILE_CONFIGURATION_HPP
#define COMPILE_CONFIGURATION_HPP
#include <iostream>
#include "src/general/logger.hpp"
#include <assert.h>

//Make things faster by #define NDEBUG
#undef NDEBUG
#define LOGGING
#undef STDOUT


#ifdef NDEBUG
    #define NEW_LINE "\n"
#else
    #define NEW_LINE std::endl
#endif

#define ASSERT(condition, message) assert(condition && printf(message))
#endif // COMPILE_CONFIGURATION_HPP
// #include "src/general/compile_configuration.hpp"
