#ifndef GENERAL_FUNCTIONS_HPP
#define GENERAL_FUNCTIONS_HPP

#include "src/general/generalFunctions.hxx"
#include <string>
#include <unordered_map>

enum ParameterType
{
    Boolean,
    Continuous
};


void open_file_safely(const std::string filename, std::ifstream &opened_file );

void open_output_file_safely(const std::string filename, std::ofstream &opened_file );

void parse_network_line(const std::string line, std::vector< std::pair<std::string, bool> > & causes, bool & type, std::string & effect, boost::unordered_set<std::string> & node_set);

std::string removeInvalidCharactersFromStringAndWhiteSpace(const std::string & str );

bool string_contains_substring(const std::string & str, const std::string & substring);

const std::string currentDateTime();

void write_parameters(const std::string &path ,const std::string &network_file,const std::string &xml_nodes, const std::string &noise_distribution, int thresh_func,int colony_size, int duration, double threshold_func_param1,double threshold_func_param2, double noise_param    );

std::string get_home_directory();

std::unordered_map<std::string, double> read_initial_values(const std::string initial_values_file);

//extern "C" SEXP hello_world();
bool test_all_trues(const std::vector<bool> & tests, size_t & positives, size_t & negatives);

std::string get_string_from_parameter_type(const ParameterType p);

void remove_contents_of_file(const std::string & file_name );

std::vector<int> python_range(const int begin_value,const int one_past_end_value,const int step=1);

#endif

//http://dirk.eddelbuettel.com/code/rcpp/Rcpp-quickref.pdf
