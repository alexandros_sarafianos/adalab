#include <dlib/optimization.h>
#include <iostream>
#include <vector>

using namespace dlib;

typedef matrix<double,0,1> column_vector;
typedef matrix<double,2,1> input_vector;
typedef matrix<double,3,1> parameter_vector;


double model ( const input_vector& input, const parameter_vector& params)
{
    const double p0 = params(0);
    const double p1 = params(1);
    const double p2 = params(2);

    const double x0 = input(0);
    const double x1 = input(1);

    const double temp = p0*x0 + p1*x1 + p2;
    // y = (p0*x0 + p1*x& + p 2 )^2
    return temp*temp;
}
// is to find the set of parameters which makes the residual small on all the data pairs.
double residual (
    const std::pair<input_vector, double>& data,
    const parameter_vector& params
)
{
    return model(data.first, params) - data.second;
    //returns y_approx-y
}



bool test_optimization()
{

   const parameter_vector params = 10*randm(3,1);
   std::cout << "params: " << trans(params) << NEW_LINE;
   std::vector<std::pair<input_vector, double> > data_samples; //[( X, y),..]
   input_vector input;
   for (int i = 0; i < 1000; ++i)
   {
       input = 10*randm(2,1);
       const double output = model(input, params);

       // save the pair
       data_samples.push_back(std::make_pair(input, output));
   }

    parameter_vector x;
    x= 1;
    solve_least_squares_lm(objective_delta_stop_strategy(1e-7).be_verbose(),
                           residual,
                           derivative(residual),//numerical aproximation
                           data_samples,
                           x);
    std::cout << "inferred parameters: "<< trans(x) << NEW_LINE;
    std::cout << "solution error:      "<< length(x - params) << NEW_LINE;
    std::cout << NEW_LINE;
    return true;
}


int main()
{
    bool ok = test_optimization();
    if( !ok )
    {
        std::cout <<"Test optimization failed ! " << NEW_LINE;
    }
    return 0;
}
