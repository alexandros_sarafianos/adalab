---
title: "CoRegFlux vignette"
author: "Daniel Trejo Banos"
date: "5 Dec 2016"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## CoRegFlux vignette


```{r,eval=F}
library(CoRegFlux)
```

Initial conditions

We initialize a metabolite data frame, using adalab conditions. Ethanol is set to 0.01 (we assume it's initial concentration should be close to 0).

We also load the first batch of OD measurements from Adalab.

```{r,warning=F,error=F,message=F}

library(CoRegFlux)
library(sybil)
library(sybilEFBA)
data("names.x.ORF")
data("iMM904")
 
 
 iMM904 <- upgradeModelorg(iMM904) 
 ## Data frame for metabolites
 
name=c("glucose","ethanol")
 concentrations=c(13.8768699776138,0.01)
metabolites<-data.frame(name,concentrations)
 
 ## we read the initial biomass from adalab od readings
data("All_Wild_Type_GrowthCurves")
#The following line is to only pick reasings below 24 hours 
#All_Wild_Type_GrowthCurves<- All_Wild_Type_GrowthCurves[All_Wild_Type_GrowthCurves$X<24,]
time<-All_Wild_Type_GrowthCurves$Time
Tfinal<-time[length(time)]

 OD<-as.matrix(All_Wild_Type_GrowthCurves[,-1])
 OD<-as.matrix(All_Wild_Type_GrowthCurves[,-1])
 rownames(OD)<-time

 OD<-t(OD)
 #in case you want to normalize the OD
 #mOD <- max(OD)
 #OD<-OD/mOD
 meanOD<-colMeans(OD)

 propConst <- 0.45
 initial.biomass <- propConst*meanOD[1]
print(initial.biomass)
print(time)
```





Here we create a data frame with the name of the metabolic genes and their state, all genomic state data frames should follow the same format. Here I knock out all the metabolic genes by setting their expression levels to almost zero (given how i designed the algorithm, the value of 0 does NOT infact knock out the gene, I may correct this later)

```{r}
tmp<-merge(data.frame(geneName_ORF=allGenes(iMM904)),names.x.ORF)
gene.state<-data.frame(Name=tmp$geneName,State=rep(1e-6,length(tmp$geneName)))
```



The simulation receives as parameter the gene.state function, this function will receive as parameters a gene.state and metabolite state(in the same format as the data frame previously used). in this example the gene state function just returns the value of the gene.state variable created in the previous chunk. Additionally, you have the option of adjusting the softplus parameter.

We use the softplus function to translate gene expression to fluxes


```{r,echo=F,warning=F,tidy=T}
library(ggplot2)
library(latex2exp)
eq.title<-TeX('$v_{i}\\leq\\ln\\left(1+ \\exp\\left(\\theta+gpr_{i}\\left(X\\right)\\right)\\right)$')
fun.1 <- function(x)log(1+exp(x))

p <- ggplot(data = data.frame(x = 0), mapping = aes(x = x))
p + stat_function(fun = fun.1,colour="red") + xlim(-5,5)+ggtitle(eq.title)+geom_vline(xintercept = 0)+geom_hline(yintercept = 0)
```

where $\theta$ is the soft plus parameter, $gpr_{i}\left(X\right)$ is the result of evaluating the gene-protein-reaction rules for a set of gene expression levels of the metabolic genes $X$. These rules relate genes to reactions and are logical form. CoRegflux transform these rules as follows 
 
 - AND are substitued by MIN()
 - OR are substituted by MAX().
 The current implementation of coregflux performs the translation of rules and the softplus mapping, the only adjustable parameter for now is the location parameter $\theta$, which is the same for all fluxes.
 
The simulation of the strain with al metabolic genes knocked out.

```{r,warning=F,error=F,message=F,cache=T}
result<-Simulation(model=iMM904,
                   as.numeric(time),
                   metabolites,
                   initial.biomass,
                   coregnet=NULL,
                   knock.out=NULL,
                   influence=NULL,
                   gene.state.function=function(a,b){gene.state},
                   softplus.parameter = 0)
plot(result$biomass.history)
```

Simulation of wild type

```{r,warning=F,error=F,message=F,cache=T}
result<-Simulation(model=iMM904,
                   as.numeric(time),
                   metabolites,
                   initial.biomass,
                   coregnet=NULL,
                   knock.out=NULL,
                   influence=NULL,
                   softplus.parameter = 0)
plot(result$biomass.history)
```


The additional parameters coregnet, knock.out and influence are used if you want to perform  a simulation using a given coregnet, with a single knock out given by the variable knock.out. The influence parameter is a real and is the value to be used by the logistic function to readjust the weights.

Right now the simulations can be performed with or without a coregnet and with or without a gene.state function (the GRN simulator)

If you wish more control you can run a single simulation step.
  
```{r,eval=FALSE}
Simulation.Step (model,
                 coregnet,
                 metabolites,
                 metabolites.concentrations.t0,
                 biomass.t0,
                 TF,
                 influence,
                 time.step,
                 gene.state)
```

a single simulation step receives a metabolic model and performs:
  - update fluxes by metabolites concentrations
  
  - update fluxes by coregnet and influence value
  
  - update fluxes by gene state from the GRN simulator


The result is a list containing
  
  - objective.history: time series of objective function value for the linear program 
  
  - metabolites: metabolites concentrations in the last time step
  
  - fluxes.history: time series of the fluxes values for all the time series
  
  - metabolites.concentration.history: time series of metabolite concentrations 
  
  - metabolites.fluxes.history: time series of the metabolites fluxes during all the simulation
  
  - rate.history: time series of the growth rate values for all simulation
  
  - time: vector containing the simulation times
  
  - gene.state.history: list containing the values for the gene state during all the simulation.
```{r,eval=FALSE}
result<-list(objective.history = objective.history,
               metabolites= step.results$metabolites,
               fluxes.history = fluxes.history,
               metabolites.cocentration.history = metabolites.conentration.history,
               metabolites.fluxes.history = metabolites.fluxes.history,
               rate.history = rate.history,biomass.history = biomass.history,
               time = time,
               gene.state.history=gene.state.history)
```

The fluxes for the simulation time are stored in a matrix which row names are the fluxes reaction id


```{r}
result$fluxes.history[1:10,1:10]
```

To have access to the gprRules you can use the sybil package, which returns a vector of equal size as the number of fluxes and with the genes involved in each flux.

```{r}
gpr(iMM904)[1:5]
```

If you only wish to know which gene affects which reaction; the sybil objects have a slot for obtaining the fluxxgene matrix

```{r}
iMM904@rxnGeneMat[1:10,1:10]
```

## fluxes from observations.

Here we will compute the fluxes from the observed growth rates (which can be obtained directly from the growth curves)

asume we have an bserved growth rate of 0.3
```{r}
fluxes.obs <- 
  get.fba.fluxes.from.observations(iMM904,0.3)
fluxes.obs[1:10,]
```

Given that the fba solution is not unique, if you wish to see the intervals of maximum and minum allowed fluxes for a reaction, use flux variability analysis

```{r}
fluxes.intervals.obs <-
  get.fva.intervals.from.observations(iMM904,0.3) 
fluxes.intervals.obs[1:10,]
```


its worth noticing that none of the two methods guarantee that we will reach the observed growth rate

```{r}
fluxes.obs[Get.Biomass.Flux.Position(iMM904),]
fluxes.intervals.obs[Get.Biomass.Flux.Position(iMM904),]
```

This could mean that the uptake rates for the limiting substrates (most commonly glucose uptake rate) does not allow for higher growth.


Now we will use the observed glucose and ethanol consumption rates to constrain the model

```{r}
metabolites$rates<-c(-20,5)
fluxes.obs <- 
  get.fba.fluxes.from.observations(
    model = iMM904,
    observed.growth.rate = 0.3,
    metabolites.rates = metabolites) 

fluxes.obs[Get.Biomass.Flux.Position(iMM904),]

fluxes.interval.obs <- 
  get.fva.intervals.from.observations(
    model = iMM904,
    observed.growth.rate =0.3,
    metabolites.rates = metabolites) 
fluxes.interval.obs[Get.Biomass.Flux.Position(iMM904),]

```
